<link href="<?php echo e(asset('theme/css/icheck/all.css')); ?>" rel="stylesheet">

<?php $__currentLoopData = $store; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  
<div class="col-md-4 form-group store_i_check">
                       <?php echo e($s->name); ?> : <input type="checkbox" name="store_id[]" class="check_test" value="<?php echo e($s->id); ?>" <?php echo e($s->is_check==1?'checked':''); ?>>                      
                    </div>
             <input type="hidden" name="allot_id[]" value="<?php echo e($s->allot_id); ?>">       
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                    

<script src="<?php echo e(asset('theme/js/jquery.min.js')); ?>"></script>

<script src="<?php echo e(asset('theme/js/icheck/icheck.min.js')); ?>"></script>

<script type="text/javascript">
	$(document).ready(function(){

    $('.store_i_check input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
         });
	});
</script>