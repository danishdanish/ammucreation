<?php $__env->startSection('content'); ?>

<section class="content-header">

 <!--    <div class="">           
                      <table class="table table-striped table-bordered table-hover" id="datatable">
                        <tr>
                            <th>AM IN</th>
                            <th>AM OUT</th>
                            <th>PM IN</th>
                            <th>PM OUT</th>
                        </tr>
                </table>
            </div> -->
      <h1>
  Add Task
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/task_add_page')); ?>">Back to Task allotment</a></li>
       
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
         
              <div class="box-body ">
               

                  <div class="form-group">
                  <label for="exampleInputEmail1">Employee - <?php echo e($data->empname); ?></label>

                  <input type="hidden" name="employeeid" id ="employeeid" value="<?php echo e($data->id); ?>"> 
                </div>

                                <div class="form-group">
                  <label for="exampleInputEmail1">Store</label>
                  <select name="storesel" class="form-control" id="storesel">
                          <option value="" >Select Here</option> 
                   <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($store->storeid); ?>" ><?php echo e($store->storename); ?></option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>
                </div>


                   <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
             <input type="text" name="taddress"  class="form-control" id="taddress" placeholder="Enter Address" value="" readonly>

                </div>


                 <div class="form-group">
                  <label for="exampleInputEmail1">Location</label>
                  <input type="text" name="tlocation"  class="form-control" id="tlocation" placeholder="Enter Location" value="" readonly>
                </div>


                      <div class="form-group">
                  <label for="exampleInputEmail1">Plan</label>
                  <select name="tmode" class="form-control" id="tmode">
             <option value="" >Please Select Mode</option>      
         <option value="sale" >Sale</option>
          <option value="pay" >Payment</option>          

       </select>

                </div>



                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="tdescription"  class="form-control" id="tdescription" placeholder="Enter Description" value="<?php echo e(old('tdescription')); ?>">
                </div>
                
           <div class="form-group">
                  <label for="exampleInputEmail1">Store Mobile No</label>
             <input type="text" name="tmobile"  class="form-control" id="tmobile" placeholder="Enter Mobile" value="" readonly>

                </div>

            

            <div class="form-group">
                  <label for="exampleInputEmail1">Date</label>
                <input id="datepicker" class="form-control" name="tdate" placeholder="Enter Date" />
            </div>



                <div class="form-group">
                  <label for="exampleInputEmail1">Time</label>
               <input  type="text" class="form-control" id="ttime" />
            </div>

                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 
                       </div>
              <!-- /.box-body -->
              <div id="demo">   </div>

              <div class="box-footer">
                <button  class="btn btn-primary" id="allottask">Submit</button>
              </div>
        
      
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#allottask',function(){ 
 var empid = ($('#employeeid').val());
  var store = ($('#storesel').val());
    var address = ($('#taddress').val());
      var location = ($('#tlocation').val());
        var mode = ($('#tmode').val());
        var description = ($('#tdescription').val());
        var mobile = ($('#tmobile').val());    
          var tdate = document.getElementById("datepicker").value; 
          var ttime = ($('#ttime').val());


      $.ajax({
               type:'GET',
              data: {'employeeid': empid,'store':store,'address':address,'location':location,'mode':mode,'description':description,'mobile':mobile,'tdate':tdate,'ttime':ttime}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('task_add')); ?>",
                             success:function(data){
      
         //    $('#datatable tr').not(':first').not(':last').remove();
         //    var html = '';
     
         //        html += '<tr>'+
         //                    '<td>' + data['id'] + '</td>' +  
         //                    '<td>' + data['store'] + '</td>' +                             
         //                '</tr>';
          

         // $('#datatable tr').first().after(html);
         //  // $('#datatable tr').after(html);
         if($.isEmptyObject(data.error)){
    console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append('<li>Add Task Successsfully</li>');
     //  window.location.reload();
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
      
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   



 $(function() {
// $('#empsel').change(function() {
// var eid= ($(this).val());

//      $.ajax({
//                type:'GET',
//                 data: {'id': eid},               
//                url: "<?php echo e(URL::to('task_add_mapped_store')); ?>",
//                success:function(data){ 
//            //    $("#tmobile").val (data);       
//             alert("hi");
//                }
//           });



// })


$('#storesel').change(function() {
var sid= ($(this).val());

     $.ajax({
               type:'GET',
                data: {'id': sid},               
               url: "<?php echo e(URL::to('task_add_store')); ?>",
               success:function(data){ 
               $("#tlocation").val (data.location);       
                   $("#taddress").val (data.address); 
                     $("#tmobile").val (data.mobile); 
               }
          });



})

})


</script>


</script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $("#datepicker").datepicker({
                minDate: 0
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>