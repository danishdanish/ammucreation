<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/task')); ?>">Order process</a></li>
        <li class="active">Order Process</li>
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row" style="margin-top:30px;">

        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" >

        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">
             <?php echo e($process['empname']); ?>

              <input type="hidden" name="employee"  class="form-control" id="employee" placeholder="" value="<?php echo e($process['empname']); ?>">

               <input type="hidden" name="taskpid"  class="form-control" id="taskid" placeholder="Order Details" value="<?php echo e($process['taskid']); ?>"> 

               <input type="hidden" name="orderid"  class="form-control" id="orderid" placeholder="Order Details" value="<?php echo e($orders['id']); ?>"> 

             </h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">  </div>
          
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Description:</td>
                        <td><?php echo e($process['description']); ?></td>
                      </tr>
                      <tr>
                        <td>Alloted date:</td>
                        <td><?php echo e($process['date']); ?></td>
                      </tr>
                      <tr>
                        <td>Time</td>
                        <td><?php echo e($process['time']); ?></td>
                      </tr>
                    
                      <tr>
                        <td>Mode</td>
                        <td><?php echo e($process['mode']); ?></td>
                      </tr>
                         <tr>
                             <tr>
                        <td>Store Name</td>
                        <td>
<?php echo e($stores['store']); ?>


    <input type="hidden" name="store"  class="form-control" id="store" placeholder="Store Details" value="<?php echo e($stores['store']); ?>">


                        </td>
                      </tr>
                        <tr>
                        <td>Store Address</td>
                        <td><?php echo e($process['address']); ?></td>
                      </tr>
                      <tr>
                        <td>Location</td>
                        <td><?php echo e($process['location']); ?></td>

                      </tr>
                        <td>Store Phone Number</td>
                        <td>
                        </td></tr>
                        <tr>
                          <td>  Order taken  </td>
                        <td>  
                          <?php echo e($process['order']); ?>

                                                   </td>
                                                 </tr>
                                                 <tr>
                                                   <td> Order placed </td>
                        <td>       <?php echo e($orders['quantity']); ?> </td>

                           
                      </tr>

                       <tr>
                          <td> Payment Collected  </td>
                        <td> 
                         <?php echo e($process['payment']); ?>


                           </td>
                           
                      </tr>

                           <tr>
                          <td> Quantity  </td>
                        <td>  <div class="form-group">
                  <label for="exampleInputEmail1"></label>

             <input type="text" name="quantity"  class="form-control" id="quantity" placeholder="Quantity" value=" <?php echo e($orders['quantity']); ?>">

                </div>   </td>
                           
                      </tr>
                      <tr><td> Bill no </td>
                        <td>          <div class="form-group">
                  <label for="exampleInputEmail1"></label>
             <input type="text" name="tbillno"  class="form-control" id="billno" placeholder="Bill no" value=" <?php echo e($orders['billno']); ?> ">

                </div> 
                  </td>  </tr>
                      <tr>
                        <td>Status  </td>
                        <td>         <div class="form-group">
       
                  <select name="status" class="form-control" id="status">
                     <option value="" <?php if ($orders['status']=='nil') { ?> selected="selected" <?php } ?> >Please select </option> 
             <option value="oncourier" <?php if ($orders['status']=='oncourier') { ?> selected="selected" <?php } ?> >On courier</option> 
               <option value="onprocess" <?php if ($orders['status']=='onprocess') { ?> selected="selected" <?php } ?> >On process</option>
                 <option value="recieved" <?php if ($orders['status']=='recieved') { ?> selected="selected" <?php } ?> >Recieved</option>  
                  
                 
       </select>

                </div>
              </td></tr>
              <tr><td> </td><td> <div class="box-footer">
                <button  class="btn btn-primary" id="orderprocess">Submit</button>
              </div></td></tr>
                     
                    </tbody>
                  </table>
                  
             
                </div>
              </div>
            </div>
            
            
          </div>
                   
                         <!--  <?php echo e($orders['quantity']); ?>  -->
                         
                  


          

<!-- <br><p> store name </p> -->

                    <!--    <?php echo e($stores['store']); ?> -->
                    <!--      <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php echo e($v); ?>  -->
                         
                  <!--     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> -->
<!-- <br><p> store name </p>
                   <?php echo e($process['store']); ?><br>
                       <?php echo e($process['date']); ?> -->
     
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#orderprocess',function(){
   var taskid = document.getElementById("taskid").value;  
  var employee = document.getElementById("employee").value; 
  var store = ($('#store').val());
 var quantity = ($('#quantity').val());
     var billno = ($('#billno').val());
         var orderid = ($('#orderid').val());
  var status = document.getElementById("status").value; 


      $.ajax({
               type:'GET',
              data: {'taskid':taskid,'employee': employee,'store':store,'quantity':quantity,'billno':billno,'status':status,'orderid':orderid}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('post_order_process')); ?>",
                             success:function(data){
    
         if($.isEmptyObject(data.error)){
  //  console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append('<li>Updated Succesfully</li>');
   //   window.location.reload();
   //  alert(data.success);
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   



 





</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>