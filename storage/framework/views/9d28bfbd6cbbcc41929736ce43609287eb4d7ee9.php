<link href="<?php echo e(asset('theme/css/icheck/all.css')); ?>" rel="stylesheet">

<?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  
<div class="col-md-4 form-group employee_i_check">
                       <?php echo e($e->empname); ?> : <input type="checkbox" name="employee_ids[]" class="check_test" value="<?php echo e($e->id); ?>" <?php echo e($e->is_check==1?'checked':''); ?>>                      
                    </div>
             <input type="hidden" name="allot_id[]" value="<?php echo e($e->allot_id); ?>">       
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                    

<script src="<?php echo e(asset('theme/js/jquery.min.js')); ?>"></script>

<script src="<?php echo e(asset('theme/js/icheck/icheck.min.js')); ?>"></script>

<script type="text/javascript">
	$(document).ready(function(){

    $('.employee_i_check input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
         });
	});
</script>