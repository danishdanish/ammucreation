<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Ammu Creations | Log in</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="<?php echo e(asset('theme/css/bootstrap.min.css')); ?>">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo e(asset('theme/fonts/font-awesome.min.css')); ?>">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo e(asset('theme/css/ionicons.min.css')); ?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo e(asset('theme/css/AdminLTE.min.css')); ?>">
<!-- iCheck -->
<!--<link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">-->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo"> <a href="#">Ammu Creations</a> </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="height:330px;">
    <p class="login-box-msg">Log in</p>
    <!--<div id="login_error" style="color:red;text-align: center;"></div>-->
    <div id="login_error" class="alert alert-danger" style="display:none">Invalid credentials</div>
    <?php if(count($errors) > 0): ?>
    <div class = "alert" style="background:#3c8dbc!important;color:#fff !important;">
      <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </ul>
    </div>
    <?php endif; ?>
    <form class="" method="POST"  id="login_form" action="<?php echo e(route('login')); ?>">
      <?php echo e(csrf_field()); ?>

      <div class="form-group has-feedback">
        <div id="user_error" style="color:red"></div>
        <input id="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span> </div>
      <div class="form-group has-feedback">
             <input id="password" type="password" class="form-control" name="password">
      </span>  <span class="glyphicon glyphicon-lock form-control-feedback"></span> </div>
      
      <!--     <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>-->
      
      <div class="form-group">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat btn_submit"> Login </button>
          <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>"> Forgot Your Password? </a> </div>
      </div>
    </form>
    
    <!-- /.social-auth-links --> 
    
    <!--<a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a>--> 
    
  </div>
  <!-- /.login-box-body --> 
</div>
<!-- /.login-box -->

</body>
</html>
