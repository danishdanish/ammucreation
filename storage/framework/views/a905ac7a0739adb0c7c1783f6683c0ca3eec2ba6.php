<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>
  Target Edit
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/allotment')); ?>">Target</a></li>
        <li class="active">Target Edit</li>
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
         <form action="<?php echo e(URL::to('/allotment/update_allot')); ?>" method="POST"> 
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
        
              <div class="box-body ">


                  <div class="form-group">
                  <label for="exampleInputEmail1">Employee</label>
                  <select name="employee" class="form-control" id="emp_sel" required="required">
                   <?php $__currentLoopData = $emps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($emp->id); ?>" <?php echo e($emp->id==$edit_data->user_id?'selected=selected':''); ?> ><?php echo e($emp->empname); ?></option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>

                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Sales in INR</label>
                  <input type="text" name="sales"  class="form-control" id="" placeholder="Enter Sales" value="<?php echo e($edit_data->sales); ?>" required="required">
                </div>
                

                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 
                 <input type="hidden" name="edit_id" value="<?php echo e($edit_data->id); ?>">
                       </div>
              <!-- /.box-body -->
              <div id="demo">   </div>

              <div class="box-footer">
                <button type="submit"  class="btn btn-primary" id="allot">Submit</button>
              </div>
        
        
          </div>
          </form>
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });

        $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 
          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>