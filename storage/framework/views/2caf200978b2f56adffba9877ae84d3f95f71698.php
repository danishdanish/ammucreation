<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>
       Add User
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/user')); ?>">Users</a></li>
        <li class="active">Edit User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         <?php if($errors->any()): ?>   
  <div class="" style="margin-top: 25px;">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>

 <?php if(Session::has('flash_message')): ?>
      <div class="alert alert-success"> <?php echo e(Session::get('flash_message')); ?> </div>
      <?php endif; ?>  
</div>
 <!-- flash end-->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(URL::to('/user/user_edit')); ?>" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="empname"  class="form-control" id="exampleInputEmail1" placeholder="Enter name"  value="<?php echo e($profile->empname); ?>">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"  value="<?php echo e($profile->email); ?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Phone no</label>
                  <input type="text" name="phone" class="form-control" id="phone_no" placeholder="Enter phone no"  value="<?php echo e($profile->phone); ?>">
                </div>

                  <div class="form-group">
                  <label for="exampleInputEmail1">Date of Joining</label>
                <input id="datepicker" class="form-control" name="doj" placeholder="Enter Date of Joining" value="<?php echo e($profile->doj); ?>"/>
            </div>

            <div class="form-group">
                  <label for="exampleInputFile">Designation</label>
                  <input type="text" name="designation" class="form-control" id="phone_no" placeholder="Enter Designation"  value="<?php echo e($profile->designation); ?>">               
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                  <textarea name="address" class="form-control" placeholder="Enter Address"><?php echo e($profile->address); ?></textarea>
                </div>  


                  <div class="form-group">
                     
                  <label for="exampleInputEmail1">Is Active</label>
               

<select id="is_active" name="is_active">
<option value="1" <?php if ($profile->is_active==1) { ?> selected="selected" <?php } ?>>1</option>
<option value="0" <?php if ($profile->is_active==0) { ?> selected="selected" <?php } ?>>0</option>

</select>
                </div>         

                 <div class="form-group">
                  <label for="exampleInputFile">Profile Photo</label>
                   <img style="width:70px; height:70px;" src="<?php echo e(url('theme/image/profile_photo/'.$profile->photo)); ?>"/>
                  <input type="file" name="profile_photo" id="exampleInputFile" >                  
                </div>


                <input type="hidden" name="uid" value="<?php echo e($profile->id); ?>">
                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>