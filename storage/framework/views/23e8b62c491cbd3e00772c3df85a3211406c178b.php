<?php $__env->startSection('content'); ?>

<section class="content-header">

 <!--    <div class="">           
                      <table class="table table-striped table-bordered table-hover" id="datatable">
                        <tr>
                            <th>AM IN</th>
                            <th>AM OUT</th>
                            <th>PM IN</th>
                            <th>PM OUT</th>
                        </tr>
                </table>
            </div> -->
      <h1>
  Add Task
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Task</a></li>
        <li class="active">Task Allotment -Add Task</li>
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
         
              <div class="box-body ">
               
 <form action="<?php echo e(URL::to('/task/task_add_emp')); ?>" method="POST" role="form" enctype="multipart/form-data" >
                  <div class="form-group">
                  <label for="exampleInputEmail1">Employee</label>
                  <select name="empsel" class="form-control" id="empsel">
                                 <?php $__currentLoopData = $emps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($emp->id); ?>" ><?php echo e($emp->empname); ?></option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>
                </div>


            
              <div class="box-footer">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <button  class="btn btn-primary" id="allottask">Submit</button>
              </div>
          
            </form>



        
      
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>



    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $("#datepicker").datepicker({
                minDate: 0
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>