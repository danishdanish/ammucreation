<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/store')); ?>">Payment list all</a></li>
        <li class="active"> Payment View </li>
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row" style="margin-top:30px;">

        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" >

        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" style="height: 50px;">  
        
     <?php echo e($store->name); ?>

               
             </h3>

                  <p class="">   

    
     <?php echo e($store->address); ?> <br> <?php echo e($store->city); ?><br>  <?php echo e($store->state); ?><br>  <?php echo e($store->country); ?>




               
             </p>

                <p class="">     
     Contact Name : <?php echo e($store->contactname); ?>               
             </p>

                <p class="">     
     Contact Mobile : <?php echo e($store->contactno); ?>               
             </p>
            </div>



              <div class="panel-body">
              <div class="row">
                <div class="col-md-5 col-lg-5" align="center"> <p style="font-size: 20px;">  Amount Due  Rs <?php echo e($store->total); ?>  <br>  Paid   Rs <?php echo e($store->paid); ?> <br> Cash <?php if($cashTotal): ?> Rs. <?php echo e($cashTotal); ?><?php else: ?> <?php echo e(0); ?><?php endif; ?> <br> Cheque <?php if($chequeTotal): ?> Rs. <?php echo e($chequeTotal); ?><?php else: ?> <?php echo e(0); ?><?php endif; ?> <br>  Balance  Rs <?php echo e($store->balance); ?> </p>  </div></div></div>
            <div class="panel-body">
              <div class="row">
              
          
                <div class=" col-md-9 col-lg-9 "> 

          
                  </div>
              </div>
            </div>    
             
         
             
           


          </div>
                   
                     
     
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#orderprocess',function(){
   var id = document.getElementById("placeorderid").value;  
 
  var courrier = ($('#courrier').val());
  var billno = ($('#billno').val());
         var invoiceno = ($('#invoiceno').val());
      $.ajax({
               type:'GET',
              data: {'id':id,'courrier': courrier,'billno':billno,'invoiceno':invoiceno}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('post_order_process')); ?>",
                             success:function(data){
    
         if($.isEmptyObject(data.error)){
  //  console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append('<li>Updated Succesfully</li>');
   //   window.location.reload();
   //  alert(data.success);
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   



 





</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>