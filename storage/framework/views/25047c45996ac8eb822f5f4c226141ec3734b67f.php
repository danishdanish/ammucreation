<?php $__env->startSection('content'); ?>

<style type="text/css">
  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #3c8dbc;
    border-color: #367fa9;
}
</style>

<section class="content-header">
      <h1>
   <?php echo date('Y') ?> Leave Management
      </h1>
      <ol class="breadcrumb">
             <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/leave')); ?>">Leave Management All</a></li>
        <li class="active">Leave Management</li>
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
        <form action="<?php echo e(URL::to('/leave/insert')); ?>" id="target_form" method="POST">  
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
           
        
              <div class="box-body col-lg-6">

                <div class="form-group">
                  <label for="exampleInputEmail1">Total Casual Leave</label>
                  <input type="text" name="casual"  class="form-control" id="target_amount" placeholder="Enter casual leave" value="" required="required">
                 
                </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">Total Sick Leave</label>
                  <input type="text" name="sick"  class="form-control" id="target_amount" placeholder="Enter Sick leave" value="" required="required">
                 
                </div>
                
          
                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

               <div class="box-footer">
                <button type="Submit"  class="btn btn-primary" id="allot" <?php echo e($leave_count>0?'disabled=disabled':''); ?>>Submit</button>
              </div>
                 
                       </div>
              <!-- /.box-body -->
              <div id="demo">   </div>

              
        
        
          </div>
          
          </form>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script>
 $(document).ready(function () {
   
   $('.select2').select2();

    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });

        $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap'
    });
});

</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>