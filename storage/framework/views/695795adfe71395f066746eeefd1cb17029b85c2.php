<?php $__env->startSection('content'); ?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
Sales List All
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/home')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sales List All</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">

          <!-- flash start-->
        <div class="col-md-12">

      <div class="alert alert-success" id="smessage" style="display:none; "> </div>
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Si No</th>
                  <th>Date</th> 
                     <th>Employee</th> 
                       <th>Store</th> 
                         <th>Amount in INR</th>             
                
                     <th>Actions</th>              
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- modal  -->

       
        <!-- modal end -->
    </section>

    <?php $__env->stopSection(); ?>

 <?php $__env->startSection('script'); ?>

     <?php if(Session::get('add')==1): ?>
        <script>
          Msg.success('successfully added',2000);
        </script>
   <?php endif; ?>

   <?php if(Session::get('edit')==1): ?>
        <script>
          Msg.success('successfully updated',1500);
        </script>
   <?php endif; ?>

 <script>
  $(function () {
    
    var table =  $('#example1').DataTable({
        //"processing": true,
        "bserverSide": true,
        'searching'   : true,
        "ajax": {
            url: "<?php echo e(URL::to('/sales/ajax_data_emps')); ?>",
            type: 'GET'
        },
        "columns": [
            {"data": "si_no"},
            {"data": "date"},
              {"data": "employee"},
                 {"data": "store"},
                     {"data": "amount"},
             {"data": "actions"}            
           
                ],
    });

      $('.input-sm').keyup(function(){
         table.columns( 2 ).search(  '^' + this.value, true, false ).draw();
      });


    $(document).on('click','#deletebtntask',function(){
         var uid = $(this).data('id');
   $.createDialog({

    acceptAction: alertCall,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCall(){
  
       $.ajax({
               type:'GET',
                data: {'id': uid},               
               url: "<?php echo e(URL::to('task/delete')); ?>",
               success:function(data){ 
                 table.ajax.reload();
              document.getElementById("smessage").style.display = "block";                  
                 document.getElementById("smessage").innerHTML = "Succesfully Deleted!";              
              
               }
          });
    }
      
    }); 

    


    
   
  })
</script>






<script>
 $(document).on('click','#checkconnect',function(){       
   $.createDialog({

    acceptAction: alertCallcheck,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCallcheck(){
       $.ajax({
               type:'GET',
               url: "<?php echo e(URL::to('user/user_check1')); ?>",
               success:function(data){
            document.getElementById("demo").innerHTML = "Succesfully Deleted!";            
               }
          });
    }
      
    });   




</script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>