<?php $__env->startSection('content'); ?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
    Leave Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/home')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo e(URL::to('/leave')); ?>">Leave Management List</a></li>
        <li class="active"> Leave Management</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">

          <!-- flash start-->
        <div class="col-md-12">

      <div class="alert alert-success" id="smessage" style="display:none; "> </div>
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
    <div class="col-md-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="panel-title">Leave Details</h3>
          
        </div>
        <div class="panel-body">
          <table class="table">
                  <tbody>

                    <tr>
                    <td class="text-success" style="width:40%;"> Casual leaves taken : </td>
                    <td><?php echo e($casual_leave_taken>1?$casual_leave_taken.' days':$casual_leave_taken.' day'); ?></td>
                    </tr>

                    <tr>
                    <td class="text-success" style="width:40%;"> Sick leaves taken : </td>
                    <td><?php echo e($sick_leave_taken>1?$sick_leave_taken." days":$sick_leave_taken. " day"); ?></td>
                    </tr>

                    <tr>
                    <td class="text-success" style="width:40%;"> Loss of pay : </td>
                    <td><?php echo e($loss_of_pay>1?$sick_leave_taken. " days":$sick_leave_taken." day"); ?></td>
                    </tr>

                    <tr>
                    <td class="text-success" style="width:40%;"> Balance casual leaves : </td>
                    <td><?php echo e($balance_casual_leave>1?$balance_casual_leave." days":$balance_casual_leave. " day"); ?></td>
                    </tr>

                    <tr>
                    <td class="text-success" style="width:40%;"> Balance sick leaves : </td>
                    <td><?php echo e($balance_sick_leave>1?$balance_sick_leave." days":$balance_sick_leave. " day"); ?></td>
                    </tr>
                                                               
                    
                    </tbody>
               </table>
        </div>
      </div>
    </div>
              
            </div>

    </section>


    <section class="content">
 <div class="row">

          <!-- flash start-->
        <div class="col-md-12">

      <div class="alert alert-success" id="smessage" style="display:none; "> </div>
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
    <thead>
      <tr>
        <th>Si No</th>
        <th>Leave Date</th>
        <th>Reason</th>
        <th>Leave Type</th>
        <th>Day Type</th>
      </tr>
    </thead>
    <tbody>
    <?php
     $i=1; 
    ?>
    <?php $__currentLoopData = $leave_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $l): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td><?php echo e($i++); ?></td>
        <td><?php echo e($l->leave_date); ?></td>
        <td><?php echo e($l->reason); ?></td>
        <td><?php echo e($l->leave_type==1?'Casual leave':($l->leave_type==2?'Sick Leave':'Loss of pay')); ?></td>
        <td><?php echo e($l->day_type==1?'full day':'half day('.$l->slot.')'); ?></td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
    </tbody>
  </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

     
      <!-- modal  -->
<input type="hidden" id="jsondata2" value="[{'X':0,'Y':0,'W':0,'H':500},{'X':358,'Y':62,'W':200,'H':500}]"/>
       
        <!-- modal end -->
    </section>

    <?php $__env->stopSection(); ?>

 <?php $__env->startSection('script'); ?>

   

   <?php if(Session::get('update')==1): ?>
        <script>
          Msg.success('successfully updated',1500);
        </script>
   <?php endif; ?>

 <script>
  $(function () {
   

    var table =  $('#example1').DataTable({
        //"processing": true,
        "bserverSide": true,
        'searching'   : true,
        "ajax": {
            "url": "<?php echo e(URL::to('/leave/leave_details_ajax_data/2')); ?>",
            "type": 'GET',
            
        },
        "columns": [
            {"data": "si_no"},
            {"data": "leave_date"},
            {"data": "reason"},
            {"data":"leave_type"},
            {"data":"day_type"},
            
           
                ],
    });

      $('.input-sm').keyup(function(){
         table.columns( 2 ).search(  '^' + this.value, true, false ).draw();
      });


    $(document).on('click','.delete',function(){
      
     

       var id = $(this).closest('.action_div').find('#delete_id').val();

    
   $.createDialog({

    acceptAction: alertCall,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCall(){

    

       $.ajax({
               type:'GET',
               url:'<?php echo e(URL::to("/leave/delete")); ?>/'+id,
               success:function(msg){
                 if(msg==1){
                   table.ajax.reload();
                    Msg.success('successfully deleted',1500);
                 }
               }
          });

          
    }


      
    });  

    


    
   
  })
</script>






<script>
 $(document).on('click','#checkconnect',function(){       
   $.createDialog({

    acceptAction: alertCallcheck,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCallcheck(){
       $.ajax({
               type:'GET',
               url: "<?php echo e(URL::to('user/user_check1')); ?>",
               success:function(data){
            document.getElementById("demo").innerHTML = "Succesfully Deleted!";            
               }
          });
    }
      
    });   




</script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>