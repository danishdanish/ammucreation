 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 <table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Leave Date</th>
        <th>Reason</th>
        <th>Leave Type</th>
        <th>Day Type</th>
      </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $leave; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $l): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td><?php echo e($l->empname); ?></td>
        <td><?php echo e($l->leave_date); ?></td>
        <td><?php echo e($l->reason); ?></td>
        <td><?php echo e($l->leave_type); ?></td>
        <td><?php echo e($l->day_type); ?></td>
      </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
  </table>
   <div class="pagination pull-right">
   <?php echo e($leave->appends(request()->query())->links()); ?>

     <!--<ul class="pagination">
    <li><a href="#">1</a></li>
    <li class="active"><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
  </ul>-->
  </div>