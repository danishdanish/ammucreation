 <section class="sidebar">

      <!-- Sidebar user panel -->

      <div class="user-panel">

        <div class="pull-left image">

        <!--   <img src="<?php echo e(url('theme/image/logo_img.png')); ?>" class="img-circle" alt="User Image" style="width: 35px;height: 35px;"> -->

        </div>

        <div class="pull-left info">

          <p style="margin-top: 3px;"></p>

          

        </div>

      </div>

      <!-- search form -->

      

      <!-- /.search form -->

      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu" data-widget="tree">

        <li class="header">MAIN NAVIGATION</li>

      <li>

          <a href="<?php echo e(URL::to('/home')); ?>">

            <i class="fa fa-dashboard"></i> <span>Dashboard</span>

          </a>  

        </li>           

           <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>

        <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Users </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/user')); ?>"><i class="fa fa-circle-o"></i> List</a></li>

            <li><a href="<?php echo e(URL::to('/user_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>

          </ul>

        </li> 

        <?php endif; ?>

             <?php if((Auth::user()->user_role) == 1): ?>

        <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Roles </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/role')); ?>"><i class="fa fa-circle-o"></i> List</a></li>

            <li><a href="<?php echo e(URL::to('/role_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>

           

          </ul>

        </li> 

      

        

            <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Modules </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/module')); ?>"><i class="fa fa-circle-o"></i> List</a></li>

            <li><a href="<?php echo e(URL::to('/module_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>

          </ul>

        </li>

        <?php endif; ?>

                
                   <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>     

  

                            <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Store Manager </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/store')); ?>"><i class="fa fa-circle-o"></i> List</a></li>

            <li><a href="<?php echo e(URL::to('/store_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
                  <li class=""><a href="<?php echo e(URL::to('/store_allot')); ?>"><i class="fa fa-circle-o"></i> Store Allotment</a></li>

          </ul>

        </li>  

        



        <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Salesman Tracking </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/location')); ?>"><i class="fa fa-circle-o"></i> List</a></li>

            

          </ul>

        </li> 

  <?php endif; ?>


   <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>

         <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Task Manager</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

         

            <li><a href="<?php echo e(URL::to('/task_add_page')); ?>"><i class="fa fa-circle-o"></i> Task Add</a></li>

       

            <li class=""><a href="<?php echo e(URL::to('/allotment')); ?>"><i class="fa fa-circle-o"></i>  Target List</a></li>

            <li><a href="<?php echo e(URL::to('/allotment_add_page')); ?>"><i class="fa fa-circle-o"></i>Target Add</a></li>

              <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?> 

             <li class=""><a href="<?php echo e(URL::to('/alloted_tasks')); ?>"><i class="fa fa-circle-o"></i>Ongoing Tasks</a></li>       

    <li class=""><a href="<?php echo e(URL::to('/pending_tasks')); ?>"><i class="fa fa-circle-o"></i>Pending Tasks</a></li>

     <li class=""><a href="<?php echo e(URL::to('/visited_tasks')); ?>"><i class="fa fa-circle-o"></i>Visited Tasks</a></li>

       <?php elseif((Auth::user()->user_role) == 4): ?>  
         <li class=""><a href="<?php echo e(URL::to('/alloted_tasks_individual/')); ?>"><i class="fa fa-circle-o"></i>Task List</a></li>
         <?php endif; ?>       

          </ul>

          </li> 

          <?php endif; ?>


               <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>

<li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Order</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

      

          <ul class="treeview-menu">

                 <li class=""><a href="<?php echo e(URL::to('/order_process')); ?>"><i class="fa fa-circle-o"></i>Place Order List</a></li>
                     <li class=""><a href="<?php echo e(URL::to('/order_despatch')); ?>"><i class="fa fa-circle-o"></i> Despatched List</a></li>

                         <li class=""><a href="<?php echo e(URL::to('/order_despatch_pending')); ?>"><i class="fa fa-circle-o"></i> Despatched List Pending</a></li>
          

          </ul>

        </li>



        <?php endif; ?>


  <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>     

      <!--   <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Cash in Hand </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/cash_in_hand')); ?>"><i class="fa fa-circle-o"></i> Cash in Hand Store Wise</a></li>
       
             <li class=""><a href="<?php echo e(URL::to('/cash_in_hand_empwise')); ?>"><i class="fa fa-circle-o"></i> Cash in Hand Employee Wise</a></li>
 -->

               <ul class="treeview-menu">   

          </ul>

        </li> 

             <?php elseif((Auth::user()->user_role) == 4): ?>

                    <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Cash in Hand - Salesman </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/cash_in_hand_emp')); ?>"><i class="fa fa-circle-o"></i> List</a></li>

            

          </ul>

        </li>

  <?php endif; ?>





              <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>

        <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Sales</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

      

          <ul class="treeview-menu">

                 <li class=""><a href="<?php echo e(URL::to('/sales')); ?>"><i class="fa fa-circle-o"></i>  List</a></li>

          

          </ul>

        </li>

         <?php elseif((Auth::user()->user_role) == 4): ?>

       <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Sales</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

      

          <ul class="treeview-menu">

                 <li class=""><a href="<?php echo e(URL::to('/sales_individual')); ?>"><i class="fa fa-circle-o"></i>  List</a></li>

          

          </ul>

        </li>

        <?php endif; ?>

             <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>

                       <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Products </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/product')); ?>"><i class="fa fa-circle-o"></i> Product List</a></li>

            <li><a href="<?php echo e(URL::to('/product_add_page')); ?>"><i class="fa fa-circle-o"></i> Product Add</a>

            </li>

                <li class=""><a href="<?php echo e(URL::to('/brands')); ?>"><i class="fa fa-circle-o"></i> Brand List</a></li>

            <li><a href="<?php echo e(URL::to('/brands_add_page')); ?>"><i class="fa fa-circle-o"></i>Brand Add</a></li>

            <li class=""><a href="<?php echo e(URL::to('/catgs')); ?>"><i class="fa fa-circle-o"></i>Catogory List</a></li>

            <li><a href="<?php echo e(URL::to('/catgs_add_page')); ?>"><i class="fa fa-circle-o"></i> Catagory Add</a></li>

                       <li class=""><a href="<?php echo e(URL::to('/size')); ?>"><i class="fa fa-circle-o"></i> Sizes</a></li>

             <li class=""><a href="<?php echo e(URL::to('/size_add_page')); ?>"><i class="fa fa-circle-o"></i> Add Sizes</a></li>

            <li><a href="<?php echo e(URL::to('/color')); ?>"><i class="fa fa-circle-o"></i> Colors</a></li>

            <li><a href="<?php echo e(URL::to('/color_add_page')); ?>"><i class="fa fa-circle-o"></i>Add Colors</a></li>         

          </ul>

        </li>     



<?php endif; ?>

               <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>

        <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Leave Management </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class=""><a href="<?php echo e(URL::to('/leave')); ?>"><i class="fa fa-circle-o"></i> List</a></li>

            <li class=""><a href="<?php echo e(URL::to('/leave/add')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>

            <li class=""><a href="<?php echo e(URL::to('/leave/leave_report')); ?>"><i class="fa fa-circle-o"></i> Leave Report</a></li>

            

          </ul>

        </li> 

         <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Reports </span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">


            <li class=""><a href="<?php echo e(URL::to('/task_report')); ?>"><i class="fa fa-circle-o"></i>Task</a></li>

            <li class=""><a href="<?php echo e(URL::to('/order_dispatch')); ?>"><i class="fa fa-circle-o"></i>Order Dispatch</a></li>

            <li class=""><a href="<?php echo e(URL::to('/cash_in_hand')); ?>"><i class="fa fa-circle-o"></i>Cash in Hand</a></li>

            
            <li class=""><a href="<?php echo e(URL::to('/payment_report')); ?>"><i class="fa fa-circle-o"></i> Payment</a></li>

            

          </ul>

        </li> 

       

        <?php endif; ?>


         <?php if(((Auth::user()->user_role) == 1)||((Auth::user()->user_role) == 5)): ?>

       <li class=" treeview">

          <a href="#">

            <i class="fa fa-user"></i> <span>Stock</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

      

          <ul class="treeview-menu">

                 <li class=""><a href="<?php echo e(URL::to('/stock')); ?>"><i class="fa fa-circle-o"></i>  List</a></li>

                <li class=""><a href="<?php echo e(URL::to('/stock/add')); ?>"><i class="fa fa-circle-o"></i>  Add</a></li>

          </ul>

        </li>

        <?php endif; ?>



      </ul>

    </section>

