<?php $__env->startSection('content'); ?>

<style type="text/css">
  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #3c8dbc;
    border-color: #367fa9;
}
</style>

<section class="content-header">
      <h1>
Target
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/allotment')); ?>">Target</a></li>
        <li class="active">Target add</li>
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
        <form action="<?php echo e(URL::to('/allotment_add_allot')); ?>" id="target_form" method="POST">  
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
        
              <div class="box-body ">


                  <div class="form-group">
                  <label for="exampleInputEmail1">Employee</label>
                <!--<select name="employee[]" class="form-control select2" id="emp_sel" multiple="multiple" data-placeholder="Select Employee" required="required">-->
                <select name="employee[]" class="form-control select2" multiple="multiple" data-placeholder="Select Employee" id="emp_sel" required="required" style="width: 100%;">
                   <?php $__currentLoopData = $emps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($emp->id); ?>" ><?php echo e($emp->empname); ?></option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>
              
                </div>



                <div class="form-group">
                  <label for="exampleInputEmail1">Box count</label>
                  <input type="text" name="box"  class="form-control" id="target_amount" placeholder="Enter Sales" value="<?php echo e(old('name')); ?>" required="required">
                 
                </div>
                
          
                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 
                       </div>
              <!-- /.box-body -->
              <div id="demo">   </div>

              <div class="box-footer">
                <button type="Submit"  class="btn btn-primary" id="allot">Submit</button>
              </div>
        
        
          </div>
          
          </form>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script>
 $(document).ready(function () {
   
   $('.select2').select2();

    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });

        $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap'
    });
});



/*$('#allot').click(function(event){

   event.preventDefault();

     var employee = $('#emp_sel').val();

     var target = $('#target_amount').val();

     if(employee=='')
     {
     
       $('.employee_error').text('Please select atleast one employee');

       return false;

     }

     if(target=='')
     {

       $('.target_error').text('please enter amount');

       return false ;
     }

     $.ajax({

        type:'POST',
        url:'<?php echo e(URL::to("/allotment_add_allot")); ?>',
        data:$('#target_form').serialize();
        success:function(msg)
        {

        }
     });
});*/


 

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });
   //allotment_add_allot
    }   


</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>