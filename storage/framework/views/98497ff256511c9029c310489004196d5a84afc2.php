<?php $__env->startSection('content'); ?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
    Leave Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/home')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Leave Management All</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">

          <!-- flash start-->
        <div class="col-md-12">

      <div class="alert alert-success" id="smessage" style="display:none; "> </div>
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Si No</th>
                  <th>Name</th>
                  <th>Total Casual Leave</th>
                  <th>Total Sick Leave</th>
                  <th>Year</th>
                  <th>Actions</th>              
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- modal  -->

       
        <!-- modal end -->
    </section>

    <?php $__env->stopSection(); ?>

 <?php $__env->startSection('script'); ?>

   

   <?php if(Session::get('update')==1): ?>
        <script>
          Msg.success('successfully updated',1500);
        </script>
   <?php endif; ?>

 <script>
  $(function () {
    
    var table =  $('#example1').DataTable({
        //"processing": true,
        "bserverSide": true,
        'searching'   : true,
        "ajax": {
            url: "<?php echo e(URL::to('/leave/ajax_data')); ?>",
            type: 'GET'
        },
        "columns": [
            {"data": "si_no"},
            {"data": "name"},
            {"data": "casual"},
            {"data":"sick"},
            {"data":"year"},
            {"data": "actions"}
           
                ],
    });

      $('.input-sm').keyup(function(){
         table.columns( 2 ).search(  '^' + this.value, true, false ).draw();
      });


    $(document).on('click','.delete',function(){
      
     

       var id = $(this).closest('.action_div').find('#delete_id').val();

    
   $.createDialog({

    acceptAction: alertCall,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCall(){

    

       $.ajax({
               type:'GET',
               url:'<?php echo e(URL::to("/leave/delete")); ?>/'+id,
               success:function(msg){
                 if(msg==1){
                   table.ajax.reload();
                    Msg.success('successfully deleted',1500);
                 }
               }
          });

          
    }


      
    });  

    


    
   
  })
</script>






<script>
 $(document).on('click','#checkconnect',function(){       
   $.createDialog({

    acceptAction: alertCallcheck,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCallcheck(){
       $.ajax({
               type:'GET',
               url: "<?php echo e(URL::to('user/user_check1')); ?>",
               success:function(data){
            document.getElementById("demo").innerHTML = "Succesfully Deleted!";            
               }
          });
    }
      
    });   




</script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>