<?php $__env->startSection('content'); ?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      User
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/role')); ?>"><i class="fa fa-dashboard"></i> User</a></li>
        <li class="active">Permissions</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">

          <!-- flash start-->
        <div class="col-md-12">
        <?php if(Session::has('message')): ?>
   <div class="alert alert-success"><?php echo e(Session::get('message')); ?></div>
<?php endif; ?>
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
               <form action="<?php echo e(URL::to('/user/user_permissions_add')); ?>" method="POST" role="form" enctype="multipart/form-data" >
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr> <?php echo e($data->empname); ?>  <td> </td><td> 
                  
                  

                  </td>
      
                  </tr>
                <tr>                  
                  <td>Si No</td>
                  <td>Name</td>
                                
                </tr>
     
                <tr>
                  <td> <?php echo e($data->email); ?></td>
                  <td>
                  <select name="role_sel"> 
                    
                   <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <option value="<?php echo e($role->id); ?>" <?php if ($role->id==$data->user_role) { ?> selected="selected" <?php } ?> ><?php echo e($role->name); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
 </td>
</tr>
</thead>
  <tbody>
    </tbody>               
              </table>
                             <input type="hidden" name="uid" value="<?php echo e($data->id); ?>">
              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                  <button type="submit" id="editstatus1"  class="btn btn-success btn-xs" title="Approved">
          <span class="glyphicon glyphicon-ok"> Save</span>
        </button>
      
 </form>

      </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- modal  -->

       
        <!-- modal end -->
    </section>

    <?php $__env->stopSection(); ?>

 <?php $__env->startSection('script'); ?>
  <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>