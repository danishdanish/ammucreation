<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/task')); ?>">Task</a></li>
        <li class="active"></li>
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row">

        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo e($results->empname); ?> 
               <input type="hidden" name="taskpid"  class="form-control" id="taskid" placeholder="Order Details" value="<?php echo e($results->taskid); ?>"> 
               <input type="hidden" name="empid"  class="form-control" id="empid" placeholder="Order Details" value="<?php echo e($results->id); ?>"></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">  </div>
          
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Description:</td>
                        <td><?php echo e($results->description); ?></td>
                      </tr>
                      <tr>
                        <td>Alloted date:</td>
                        <td><?php echo e($results->tdate); ?></td>
                      </tr>
                      <tr>
                        <td>Time</td>
                        <td><?php echo e($results->ttime); ?></td>
                      </tr>
                    
                      <tr>
                        <td>Mode</td>
                        <td><?php echo e($results->mode); ?></td>
                      </tr>
                         <tr>
                             <tr>
                        <td>Store Name</td>
                        <td><?php echo e($stores->name); ?></td>
                      </tr>
                        <tr>
                        <td>Store Address</td>
                        <td><?php echo e($results->storeaddress); ?></td>
                      </tr>
                      <tr>
                        <td>Location</td>
                        <td><?php echo e($results->location); ?></td>

                      </tr>
                        <td>Phone Number</td>
                        <td><?php echo e($results->mobile); ?>

                        </td></tr>
                        <tr>
                          <td>  Order taken  </td>
                        <td>  <div class="form-group">
                  <label for="exampleInputEmail1"></label>
             <input type="text" name="torder"  class="form-control" id="torder" placeholder="Order Details" value="<?php echo e($results->torder); ?>">

                </div>   </td>
                           
                      </tr>

                       <tr>
                          <td> Payment Collected  </td>
                        <td>  <div class="form-group">
                  <label for="exampleInputEmail1"></label>
             <input type="text" name="tpayment"  class="form-control" id="tpayment" placeholder="Payment" value="<?php echo e($results->tpayment); ?>">

                </div>   </td>
                           
                      </tr>

                           <tr>
                          <td> Remark  </td>
                        <td>  <div class="form-group">
                  <label for="exampleInputEmail1"></label>
             <input type="text" name="tremark"  class="form-control" id="tremark" placeholder="Remark" value="<?php echo e($results->tremark); ?>">

                </div>   </td>
                           
                      </tr>
                      <tr>
                        <td>Status  </td>
                        <td>         <div class="form-group">
       
                  <select name="status" class="form-control" id="status">
                     <option value="" >****</option> 
             <option value="ongoing"<?php if ($results->tstatus=='ongoing') { ?> selected="selected" <?php } ?> >Ongoing</option>      
         <option value="completed" <?php if ($results->tstatus=='completed') { ?> selected="selected" <?php } ?> >Completed</option>            
       </select>

                </div>
              </td></tr>
              <tr><td> </td><td> <div class="box-footer">
                <button  class="btn btn-primary" id="taskprocess">Submit</button>
              </div></td></tr>
                     
                    </tbody>
                  </table>
                  
             
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#taskprocess',function(){
   var taskid = document.getElementById("taskid").value;  
  var empid = document.getElementById("empid").value; 
  var torder = ($('#torder').val());
 var tpayment = ($('#tpayment').val());
     var tremark = ($('#tremark').val());
  var status = document.getElementById("status").value; 


      $.ajax({
               type:'GET',
              data: {'taskid':taskid,'empid': empid,'torder':torder,'tpayment':tpayment,'tremark':tremark,'tstatus':status}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('task_process')); ?>",
                             success:function(data){
    
         if($.isEmptyObject(data.error)){
    console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append('<li>Updated Successsfully</li>');
     //  window.location.reload();
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   



 





</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>