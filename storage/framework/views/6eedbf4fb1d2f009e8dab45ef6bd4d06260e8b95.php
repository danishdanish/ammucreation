<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/cash_in_hand_empwise')); ?>">Cash in hand list all</a></li>
        <li class="active">Cash in hand </li>
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row" style="margin-top:30px;">

        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" >

        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" style="height: 50px;">
      
              <input type="hidden" name="placeorderid"  class="form-control" id="placeorderid" placeholder="" value="{">

   Cash in hand for Employee -  <?php echo e($emp->empname); ?>

               
             </h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"><b>By Cash</b>  </div>
          
                <div class=" col-md-9 col-lg-9 "> 
          <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Date Recieved</th>
                  <th>Employee</th>                 
                  <th>Amount in INR</th>                                           
                </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $cash; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                 
              
                     
               <tr>   <td> <?php echo e($c->date); ?>  </td>
                <td> <?php echo e($c->empname); ?>   </td>
                <td> <?php echo e($c->amount); ?>   </td></tr>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <tr>   <td> </td>
                <td>  Total </td>
                <td> Rs.<?php echo e($cashTotal); ?></td></tr>
                </tbody>
                
              </table>
                  
             
                </div>
              </div>
            </div>
            
                <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"><b>By Cheque</b>  </div>
          
                <div class=" col-md-9 col-lg-9 "> 
          <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Date</th>
                  <th>Employee</th>            
              
                  <th>Cheque </th> 
                      <th>Amount in INR</th>                            
                </tr>
                </thead>
                <tbody>

                   <?php $__currentLoopData = $cheque; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                 
              
                     
               <tr>   <td> <?php echo e($cq->date); ?>  </td>
                <td> <?php echo e($cq->empname); ?>   </td>
                <td> <?php echo e($cq->cheque); ?>   </td>

                   <td> <?php echo e($cq->amount); ?>   </td>

               <!--    join  working  <td> <?php echo e($cq->storename); ?>   </td> -->
              </tr>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <tr>   <td> </td><td> </td>
                <td>  Total </td>
                <td><?php if($chequeTotal): ?> Rs. <?php echo e($chequeTotal); ?><?php else: ?> <?php echo e(0); ?><?php endif; ?></td></tr>
                
                </tbody>
                
              </table>
                  
             
                </div>
              </div>
            </div>



          </div>
                   
                     
     
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#orderprocess',function(){
   var id = document.getElementById("placeorderid").value;  
 
  var courrier = ($('#courrier').val());
  var billno = ($('#billno').val());
         var invoiceno = ($('#invoiceno').val());
      $.ajax({
               type:'GET',
              data: {'id':id,'courrier': courrier,'billno':billno,'invoiceno':invoiceno}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('post_order_process')); ?>",
                             success:function(data){
    
         if($.isEmptyObject(data.error)){
  //  console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append('<li>Updated Succesfully</li>');
   //   window.location.reload();
   //  alert(data.success);
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   



 





</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>