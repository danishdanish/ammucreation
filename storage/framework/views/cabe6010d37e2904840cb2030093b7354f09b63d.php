  <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="#" class="img-circle" alt="User Image" style="width: 35px;height: 35px;">
        </div>
        <div class="pull-left info">
          <p style="margin-top: 3px;"></p>
          
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

      <li>
          <a href="<?php echo e(URL::to('/home')); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>  
        </li> 


        <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/user')); ?>"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="<?php echo e(URL::to('/user_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
          </ul>
        </li> 


        <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Roles </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/role')); ?>"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="<?php echo e(URL::to('/role_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
           
          </ul>
        </li> 
        
            <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Modules </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/module')); ?>"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="<?php echo e(URL::to('/module_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
          </ul>
        </li>



         <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Task Manager</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/task')); ?>"><i class="fa fa-circle-o"></i>Task  List</a></li>
            <li><a href="<?php echo e(URL::to('/task_add_page')); ?>"><i class="fa fa-circle-o"></i> Task Add</a></li>
       
            <li class=""><a href="<?php echo e(URL::to('/allotment')); ?>"><i class="fa fa-circle-o"></i>  Target List</a></li>
            <li><a href="<?php echo e(URL::to('/allotment_add_page')); ?>"><i class="fa fa-circle-o"></i>Target Add</a></li>

              
          </ul>
          </li> 
       <!--        <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Brands </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/brands')); ?>"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="<?php echo e(URL::to('/brands_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
          </ul>
        </li>  --> 
        

                   <!--   <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Catagories </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/catgs')); ?>"><i class="fa fa-circle-o"></i>Catogory List</a></li>
            <li><a href="<?php echo e(URL::to('/catgs_add_page')); ?>"><i class="fa fa-circle-o"></i> Catagory Add</a></li>
          </ul>
        </li>   -->
        
 <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Employee Tasks Process</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
      
          <ul class="treeview-menu">
                 <li class=""><a href="<?php echo e(URL::to('/alloted_tasks')); ?>"><i class="fa fa-circle-o"></i>Alloted Tasks</a></li>
          

          </ul>
        </li>

<li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Order Process</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
      
          <ul class="treeview-menu">
                 <li class=""><a href="<?php echo e(URL::to('/order_process')); ?>"><i class="fa fa-circle-o"></i>List</a></li>
          

          </ul>
        </li>





                       <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Products </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/product')); ?>"><i class="fa fa-circle-o"></i> Product List</a></li>
            <li><a href="<?php echo e(URL::to('/product_add_page')); ?>"><i class="fa fa-circle-o"></i> Product Add</a>
            </li>
                <li class=""><a href="<?php echo e(URL::to('/brands')); ?>"><i class="fa fa-circle-o"></i> Brand List</a></li>
            <li><a href="<?php echo e(URL::to('/brands_add_page')); ?>"><i class="fa fa-circle-o"></i>Brand Add</a></li>
            <li class=""><a href="<?php echo e(URL::to('/catgs')); ?>"><i class="fa fa-circle-o"></i>Catogory List</a></li>
            <li><a href="<?php echo e(URL::to('/catgs_add_page')); ?>"><i class="fa fa-circle-o"></i> Catagory Add</a></li>         

          </ul>
        </li>  
        

                <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Sizes and Colors </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/size')); ?>"><i class="fa fa-circle-o"></i> Sizes</a></li>
             <li class=""><a href="<?php echo e(URL::to('/size_add_page')); ?>"><i class="fa fa-circle-o"></i> Add Sizes</a></li>
            <li><a href="<?php echo e(URL::to('/color')); ?>"><i class="fa fa-circle-o"></i> Colors</a></li>
            <li><a href="<?php echo e(URL::to('/color_add_page')); ?>"><i class="fa fa-circle-o"></i>Add Colors</a></li>
          </ul>
        </li>





                            <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Store </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo e(URL::to('/store')); ?>"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="<?php echo e(URL::to('/store_add_page')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
          </ul>
        </li>  
        


      </ul>
    </section>