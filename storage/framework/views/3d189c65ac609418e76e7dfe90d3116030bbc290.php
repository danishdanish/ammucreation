<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>
  Task Allotment
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/task')); ?>">Task</a></li>
        <li class="active">Task Allotment</li>
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
         
              <div class="box-body ">
          

                  <div class="form-group">
                  <label for="exampleInputEmail1">Employee</label>
                  <select name="empsel" class="form-control" id="empsel">
                 
                   <?php $__currentLoopData = $emps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($emp->id); ?>" <?php if ($emp->id==$results->employeeid) { ?> selected="selected" <?php } ?> ><?php echo e($emp->empname); ?></option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>

                </div>


                      <div class="form-group">
                  <label for="exampleInputEmail1">Store</label>
                  <select name="storesel" class="form-control" id="storesel">
                    
                   <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($store->id); ?>" <?php if ($store->id==$results->store) { ?> selected="selected" <?php } ?> ><?php echo e($store->name); ?></option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>
                </div>


                   <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
             <input type="text" name="taddress"  class="form-control" id="taddress" placeholder="Enter Address" value="<?php echo e($results->address); ?>">

                </div>


                 <div class="form-group">
                  <label for="exampleInputEmail1">Location</label>
                  <input type="text" name="tlocation"  class="form-control" id="tlocation" placeholder="Enter Location" value="<?php echo e($results->location); ?>">
                </div>


                      <div class="form-group">
                  <label for="exampleInputEmail1">Plan</label>
                  <select name="tmode" class="form-control" id="tmode">
              
         <option value="pay" <?php if ($results->mode=="pay") { ?> selected="selected" <?php } ?> >Payment</option>
          <option value="sale" <?php if ($results->mode=="sale") { ?> selected="selected" <?php } ?> >Sale</option>          

       </select>

                </div>



                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="tdescription"  class="form-control" id="tdescription" placeholder="Enter Description" value="<?php echo e($results->description); ?>">
                </div>
                
           <div class="form-group">
                  <label for="exampleInputEmail1">Employee Mobile No</label>
             <input type="text" name="tmobile"  class="form-control" id="tmobile" placeholder="Enter Mobile" value="<?php echo e($results->mobile); ?>">

                </div>

                      <div class="form-group">
                  <label for="exampleInputEmail1">Date</label>
                <input id="datepicker1" class="form-control" name="tdate" placeholder="Date" value="<?php echo e($results->tdate); ?>" />
            </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">Time</label>
               <input  type="text" class="form-control" id="ttime" value="<?php echo e($results->ttime); ?>" />
            </div>
            <input type="hidden" name="taskid" id ="taskid" value="<?php echo e($results->taskid); ?>">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 
                       </div>
              <!-- /.box-body -->
              <div id="demo">   </div>

              <div class="box-footer">
                <button  class="btn btn-primary" id="edittask">Submit</button>
              </div>
        
      
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#edittask',function(){ 
   var taskid = document.getElementById("taskid").value; 
 var empid = ($('#empsel').val());
  var store = ($('#storesel').val());
    var address = ($('#taddress').val());
      var location = ($('#tlocation').val());
        var mode = ($('#tmode').val());
        var description = ($('#tdescription').val());
        var mobile = ($('#tmobile').val());    
          var tdate = document.getElementById("datepicker1").value; 
          var ttime = ($('#ttime').val());


      $.ajax({
               type:'GET',
              data: {'employeeid': empid,'store':store,'address':address,'location':location,'mode':mode,'description':description,'mobile':mobile,'tdate':tdate,'ttime':ttime,'taskid': taskid}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('task_edit')); ?>",
                             success:function(data){
    
         if($.isEmptyObject(data.error)){
    console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append('<li>Edit Task Successsfully</li>');
     //  window.location.reload();
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
      
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   



 $(function() {
$('#empsel').change(function() {
var eid= ($(this).val());

     $.ajax({
               type:'GET',
                data: {'id': eid},               
               url: "<?php echo e(URL::to('task_add_details')); ?>",
               success:function(data){ 
               $("#tmobile").val (data.phone);       
            
               }
          });



})


$('#storesel').change(function() {
var sid= ($(this).val());

     $.ajax({
               type:'GET',
                data: {'id': sid},               
               url: "<?php echo e(URL::to('task_add_store')); ?>",
               success:function(data){ 
               $("#tlocation").val (data.location);       
                   $("#taddress").val (data.address); 
               }
          });



})

})


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>