<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/order_process')); ?>">Order process</a></li>
        <li class="active">Order Process</li>
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row" style="margin-top:30px;">

        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" >

        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" style="height: 50px;">
      
              <input type="hidden" name="placeorderid"  class="form-control" id="placeorderid" placeholder="" value="<?php echo e($data->placeorder); ?>">
                 <input type="hidden" name="stock"  class="form-control" id="stock" placeholder="" value="<?php echo e($totalstock); ?>">
             <input type="hidden" name="storeid"  class="form-control" id="storeid" placeholder="" value="<?php echo e($data->storeid); ?>">
              <input type="hidden" name="pprice"  class="form-control" id="pprice" placeholder="" value="<?php echo e($data->price); ?>">
               <?php echo e($data->employee); ?>

             </h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">  </div>
          
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Stores:</td>
                        <td> <?php echo e($data->store); ?></td>
                      </tr>
                      <tr>
                        <td>Date:</td>
                        <td> <?php echo e($data->date); ?></td>
                      </tr>
                   
                    
                      <tr>
                        <td>Product</td>
                        <td><?php echo e($data->productname); ?></td>
                      </tr>
                         <tr>

                       <!--      <tr>
                        <td>Stock</td>
                        <td><?php echo e($totalstock); ?></td>
                      </tr> -->
                         <tr>
                             <tr>
                        <td>Quantity</td>
                        <td>

 <?php echo e($data->quantity); ?>



                        </td>
                     
                           <tr>


                            <tr>
                        <td>Price</td>
                        <td>

 Rs.<?php echo e($data->price); ?>



                        </td>
                 
                           <tr>   </tbody>
                  </table>

                    <div class="form-group">
                  <label for="exampleInputEmail1"></label>
   <label for="exampleInputEmail1">Order Quantity</label>
   <p id="errorval"> Please enter a value less than or equal to  <?php echo e($data->quantity); ?>  </p>
             <input type="text" name="quantity"  class="form-control" id="quantity" placeholder="Qantity" value="" onkeypress="return isNumberKey(event)" style="width: 100px;">
                </div> 

        


                   <div id="processorder" style="display:none;">         
              <div class="form-group">
                  <label for="exampleInputEmail1">Unit Price</label>

             <input type="text" name="unitprice" readonly=""  class="form-control" id="unitprice" placeholder="Unit price" value="">
                </div>                            
                     
        <div class="form-group">
                  <label for="exampleInputEmail1">Order Amount</label>

             <input type="text" name="orderamount" readonly=""  class="form-control" id="orderamount" placeholder="orderamount" value="">
                </div>


              <div class="form-group">
                  <label for="exampleInputEmail1"></label>

             <input type="text" name="courrier"  class="form-control" id="courrier" placeholder="Courrier name" value="">
                </div>                            
                     
                        <div class="form-group">
                  <label for="exampleInputEmail1"></label>
             <input type="text" name="billno"  class="form-control" id="billno" placeholder="Bill no" value="">

                </div> 
                     <div class="form-group">
                  <label for="exampleInputEmail1"></label>
             <input type="text" name="invoiceno"  class="form-control" id="invoiceno" placeholder="Invoice no" value="">

                </div> 
              <div class="box-footer">
                <button  class="btn btn-primary" id="orderprocess">Submit</button>
              </div>
               
               
             </div>
         <!--     end process -->

                </div>
              </div>
            </div>
            
            
          </div>
                   
                     
     
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#orderprocess',function(){
   var id = document.getElementById("placeorderid").value;  
 var storeid = document.getElementById("storeid").value;
  var pprice = document.getElementById("pprice").value;
  var courrier = ($('#courrier').val());
  var billno = ($('#billno').val());  
  var orderamount = document.getElementById("orderamount").value;
   var quantity = document.getElementById("quantity").value;
         var invoiceno = ($('#invoiceno').val());
      $.ajax({
               type:'GET',
              data: {'id':id,'courrier': courrier,'billno':billno,'invoiceno':invoiceno,'storeid':storeid,'pprice':pprice,'quantity':quantity,'orderamount':orderamount}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('post_order_process')); ?>",
                             success:function(data){
    
         if($.isEmptyObject(data.error)){
  //  console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


      //  $(".print-error-msg").find("ul").append('<li>Updated Succesfully</li>'); 
   window.location.href = '/order_process';
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   

//order process qty

 $(document).on('click','#orderprocessqty',function(){
   var id = document.getElementById("placeorderid").value;  
   var quantity = document.getElementById("quantity").value;  

      $.ajax({
               type:'GET',
              data: {'quantity':quantity,'id':id}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('post_order_process_qty')); ?>",
                             success:function(data){
                              $("#processorder").show();
                                   $("#unitprice").val (data.unitprice);       
                   $("#orderamount").val (data.orderprice); 

         if($.isEmptyObject(data.error)){
  //  console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append(data.success);

         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   

 
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }


   $('#quantity').keyup(function(){
       var id = document.getElementById("placeorderid").value;  
   var quantity = document.getElementById("quantity").value;  
 var stock = document.getElementById("stock").value; 
      $.ajax({
               type:'GET',            
              data: {'quantity':quantity,'id':id,'stock':stock}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('post_order_process_qty_keyup')); ?>",
                             success:function(data){
                     
                   if ((data.dataqty < data.reqqty) || data.datazero == 0 || data.datastock ==0)               
                  document.getElementById('processorder').style.display = 'none'; 
                  else if ((data.datast < data.reqqty))               
                document.getElementById('processorder').style.display = 'none';                 
               else
                       $("#processorder").show();
                              $("#unitprice").val (data.unitprice);       
                   $("#orderamount").val (data.orderprice);
              }
})




      });

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>