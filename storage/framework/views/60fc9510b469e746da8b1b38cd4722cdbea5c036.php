<!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo e(asset('theme/css/bootstrap.min.css')); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('theme/fonts/font-awesome.min.css')); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(asset('theme/css/ionicons.min.css')); ?>">

   <link rel="stylesheet" href="<?php echo e(asset('theme/css/dataTables.bootstrap.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('theme/css/AdminLTE.min.css')); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo e(asset('theme/css/skins/_all-skins.min.css')); ?>">

  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('theme/css/bootstrap-msg.css')); ?>" />

  <link href="<?php echo e(asset('theme/css/confirmDialog.css')); ?>" rel="stylesheet">
  

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />

