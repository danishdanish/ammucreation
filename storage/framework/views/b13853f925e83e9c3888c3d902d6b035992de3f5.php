<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>
       Add Product Size
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/product')); ?>">Products</a></li>
        <li class="active">Product Size</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         <?php if($errors->any()): ?>   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>

 <?php if(Session::has('flash_message')): ?>
      <div class="alert alert-success"> <?php echo e(Session::get('flash_message')); ?> </div>
      <?php endif; ?>  
</div>
 <!-- flash end-->
  <div class="col-md-8"> Product Name : <?php echo e($product->product); ?>  </div>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(URL::to('/product/product_size')); ?>" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                          
                <div class="form-group">              
 <label for="exampleInputEmail1">Select Size </label>
                  <select id="sizesel" name="sizeid" class="form-control">                 
                     <?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($size->id); ?>"><?php echo e($size->sizename); ?></option>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   </select>  
                </div>           
             
               <input type="hidden" name="productid" value="<?php echo e($product->id); ?>">
                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->

    </section>
        <section class="content">
     <div class="container">
  <div class="row"> 
    
   <table class="table table-stripped table-bordered">
      <thead>
        <tr>        
          <th>Size Name</th>
          <th>Size Code</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
            <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
           <td><?php echo e($color->sizename); ?></td>
          <td><?php echo e($color->sizecode); ?></td>
            <td>
               <button title="delete" id="sizedel" class="delete btn btn-danger" data-id="<?php echo e($color->id); ?>" data-pid="<?php echo e($product->id); ?>"><span class="glyphicon glyphicon-remove"></span></button></td>       
        </tr> 
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </tbody>
      </table>
  </div>
</div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
	 $(document).on('click','#sizedel',function(){ 
  var id = $(this).data('id');  
  var pid = $(this).data('pid'); 
  if (confirm('Are you sure?')) {

$.ajax({
        type:'GET',
      data: {'id':id,'pid':pid},
               url:"<?php echo e(URL::to('product/size_delete')); ?>",
               success:function(data){
               alert(data.success);
                window.location.reload();
               }
          });
}
 
    });

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>