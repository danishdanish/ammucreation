<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>
       Add Modules
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/module')); ?>">Modules</a></li>
        <li class="active">Add Module</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
         <?php if($errors->any()): ?>   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>

 <?php if(Session::has('flash_message')): ?>
      <div class="alert alert-success"> <?php echo e(Session::get('flash_message')); ?> </div>
      <?php endif; ?>  
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(URL::to('/module/add_module')); ?>" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name"  class="form-control" id="" placeholder="Enter name" value="<?php echo e(old('name')); ?>">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="description" class="form-control" id="" placeholder="Enter descrition" value="<?php echo e(old('description')); ?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Display Name</label>
                  <input type="text" name="displayname" class="form-control" id="desc" placeholder="Enter display name" value="<?php echo e(old('displayname')); ?>">
                </div>

                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>