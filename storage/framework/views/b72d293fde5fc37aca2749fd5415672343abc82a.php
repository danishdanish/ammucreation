 <!-- Logo -->

    <a href="#" class="logo">

      <!-- mini logo for sidebar mini 50x50 pixels -->

      <span class="logo-mini"><b>Ammu</b>Creations</span>

      <!-- logo for regular state and mobile devices -->

      <span class="logo-lg" style=""> <img src="<?php echo e(url('theme/image/logo_img_am.png')); ?>" class="" alt="Ammu Creations" style="width: 124px;height: 45px;"></span>

    </a>

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top">

      <!-- Sidebar toggle button-->

      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">

        <span class="sr-only">Toggle navigation</span>

      </a>



      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">

          <!-- Messages: style can be found in dropdown.less-->

         

          <!-- Notifications: style can be found in dropdown.less -->

         

          <!-- Tasks: style can be found in dropdown.less -->

          

          <!-- User Account: style can be found in dropdown.less -->

          <li class="dropdown user user-menu">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

          <?php echo e(Auth::user()->name); ?>


              <span class="hidden-xs"></span>

            </a>

            <ul class="dropdown-menu">

              <!--  -->

              <li class="user-header">

              



                <p>

               

                </p>

              </li>

              <!-- Menu Body -->

              

              <!-- Menu Footer-->

              <li class="user-footer">

                <div class="pull-left">

                  <a href="#" class="btn btn-default btn-flat">Profile</a>

                </div>

                <div class="pull-right">

                  <a href="<?php echo e(route('logout')); ?>" class="btn btn-default btn-flat"  onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">Logout</a>



                     <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">

                                            <?php echo e(csrf_field()); ?>


                                        </form>



                </div>

              </li>

            </ul>

          </li>

          

          

        </ul>

      </div>

    </nav>