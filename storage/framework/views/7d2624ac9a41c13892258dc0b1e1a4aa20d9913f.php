<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Payment Report</a></li>
      
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row" style="margin-top:30px;">

        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11" >

        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" style="height:110px;">
      <label for="exampleInputEmail1">Payment Report</label> <br><br>
              <input type="hidden" name="placeorderid"  class="form-control" id="placeorderid" placeholder="" value="">
 <form action="<?php echo e(URL::to('/payment_report_process')); ?>" method="POST" role="form" enctype="multipart/form-data" >
   <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="exampleInputEmail1">Employee</label>

         <select name="emp" class="form-control" id="empsel">
             <option value="allemps" value="allemps" >All</option>    
                                 <?php $__currentLoopData = $emps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($emp->id); ?>" <?php if($idlog == $emp->id): ?> selected="selected" <?php endif; ?> ><?php echo e($emp->empname); ?></option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>  </div>
         <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
         <label for="exampleInputEmail1">Store</label>
        <select name="store" class="form-control" id="storesel">
              <option value="allstores" >All</option> 
                                 <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <option value="<?php echo e($store->id); ?>" <?php if($stidlog == $store->id): ?> selected="selected" <?php endif; ?> ><?php echo e($store->name); ?> </option>         
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>
     </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                  <label for="exampleInputEmail1">From</label>
                <input id="datepicker" class="form-control" name="from" placeholder="Enter Date" value="<?php echo e($fromlog); ?>" />
           </div>
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
           
                  <label for="exampleInputEmail1">To</label>
                <input id="datepicker1" class="form-control" name="to" placeholder="Enter Date" value="<?php echo e($tolog); ?>" />
       </div>
              <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">  <br>
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <button  class="btn btn-primary" id="#">Submit</button>
         
          </div>
            </form>

             </h3>
            </div>
         
            
                <div class="panel-body">
              <div class="row">
              
          
                <div class="col-md-12 col-lg-12 "> 
          <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Date</th>
                  <th>Employee</th>   <th>Store</th>             
                     <th>Amount </th> 
                  <th>Pay Mode </th>
                   <th>Cheque No </th>                                             
                </tr>
                </thead>
                <tbody>

              <?php $__currentLoopData = $cash; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                
              
                     
               <tr>   <td> <?php echo e($c->date); ?> </td>
                <td> <?php echo e($c->empname); ?>   </td>
                   <td> <?php echo e($c->storename); ?>   </td>
                <td> <?php echo e($c->amount); ?>  </td>

                   <td> <?php echo e(ucfirst($c->mode)); ?>   </td>
                     <td> <?php echo e(ucfirst($c->cheque)); ?>   </td>
               <!--    join  working  <td> <?php echo e($c->storename); ?>   </td> -->
              </tr>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <tr>   <td> </td><td> </td>
                <td>  </td> <td> </td><td> </td>
                <td></td></tr>
                
                </tbody>
                
              </table>
             <div class="panel-heading" style=" font-size:18px;"> 
            Total Amount Collected : <?php if($cashtotal): ?> Rs. <?php echo e($cashtotal); ?><?php else: ?> <?php echo e(0); ?><?php endif; ?>    </div>   
              <div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
     
          <?php echo e($cash->appends(request()->input())->links()); ?>


    </div>
                </div>
              </div>
            </div>



          </div>
                   
                     
     
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#paymentrepbtn',function(){
    var from = document.getElementById("datepicker").value;  
  var to = document.getElementById("datepicker1").value; 
  var emp = ($('#empsel').val());
  var store = ($('#storesel').val());

      $.ajax({
               type:'GET',
              data: {'from':from,'to': to,'emp':emp,'store':store}, 
                dataType: 'json',               
               url: "<?php echo e(URL::to('payment_report_process')); ?>",
                             success:function(data){
    
   //    alert(data.mess);


        },
         error: function (data) {

           alert("no");
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   


</script>


</script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $("#datepicker").datepicker({
              maxDate: 0
            });
        });
    </script>

       <script language="javascript">
        $(document).ready(function () {
            $("#datepicker1").datepicker({
             maxDate: 0
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>