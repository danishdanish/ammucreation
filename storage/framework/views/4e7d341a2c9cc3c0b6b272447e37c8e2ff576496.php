<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>
       Edit Product
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/user')); ?>">Products</a></li>
        <li class="active">Edit Product</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         <?php if($errors->any()): ?>   
  <div class="" style="margin-top: 25px;">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>

 <?php if(Session::has('flash_message')): ?>
      <div class="alert alert-success"> <?php echo e(Session::get('flash_message')); ?> </div>
      <?php endif; ?>  
</div>
 <!-- flash end-->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(URL::to('/product/product_edit')); ?>" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Product</label>
                  <input type="text" name="product"  class="form-control" id="exampleInputEmail1" placeholder="Enter name"  value="<?php echo e($product->product); ?>">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="test" name="price" class="form-control" id="exampleInputEmail1" placeholder="Enter Price"  value="<?php echo e($product->price); ?>">
                </div>

                   <div class="form-group">
                  <label for="exampleInputEmail1">Price Unit</label>
                  <input type="text" name="priceunit" class="form-control" id="exampleInputEmail1" placeholder="Enter Price Unit" value="<?php echo e($product->priceunit); ?>">
                </div>


                <div class="form-group">
                     <label for="exampleInputEmail1">Brand</label>
                  <select id="brand" name="brandid" class="form-control">
                     <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($brand->id); ?>"><?php echo e($brand->brand); ?></option> 
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   </select>                
                </div>

      <div class="form-group">
                  <label for="exampleInputEmail1">Catagory</label>
                <select id="brand" name="catid" class="form-control">
                     <?php $__currentLoopData = $catagories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catagory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($catagory->id); ?>"><?php echo e($catagory->catagory); ?></option> 
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   </select>  
            </div>

             <div class="form-group">
                  <label for="exampleInputFile">Description</label>
                  <input type="text" name="description" class="form-control" id="desc" placeholder="Enter Description" value="<?php echo e($product->description); ?>" >               
                </div>


              

<!-- 
                  <div class="form-group">
                     
                  <label for="exampleInputEmail1">Is Active</label>
               

<select id="is_active" name="is_active">
<option value="1" <?php if ($product->is_active==1) { ?> selected="selected" <?php } ?>>1</option>
<option value="0" <?php if ($product->is_active==0) { ?> selected="selected" <?php } ?>>0</option>

</select>
                </div>         
 -->
                 <div class="form-group">
                  <label for="exampleInputFile">Profile Photo</label>
                   <img style="width:70px; height:70px;" src="<?php echo e(url($product->pimage)); ?>"/>
                  <input type="file" name="pimage" id="exampleInputFile" >                  
                </div>


                <input type="hidden" name="id" value="<?php echo e($product->id); ?>">
                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>