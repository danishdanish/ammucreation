<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo e(asset('theme/js/jquery.min.js')); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo e(asset('theme/js/jquery-ui.min.js')); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(asset('theme/js/bootstrap.min.js')); ?>"></script>

<!-- DataTables -->
<script src="<?php echo e(asset('theme/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/js/dataTables.bootstrap.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('theme/js/bootstrap-msg.js')); ?>"></script>

<!-- AdminLTE App -->
<script src="<?php echo e(asset('theme/js/adminlte.min.js')); ?>"></script>

<script src="<?php echo e(asset('theme/js/confirmDialog.jquery.min.js')); ?>"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script src="<?php echo e(asset('theme/js/icheck/icheck.min.js')); ?>"></script>

<script src="<?php echo e(asset('theme/js/select2.full.min.js')); ?>"></script>

<script src="<?php echo e(asset('/theme/js/bootstrap-datepicker.min.js')); ?>"></script>
