<?php $__env->startSection('content'); ?>

<style type="text/css">
   .error_display{
      color: red;
      
   }
   .error_display input{
     border-color: red;
   }
</style>

<section class="content-header">
      <h1>
       Allot Store
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Stores</a></li>
        <li class="active">Allot Store</li>
      </ol>
    </section>


  <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">

   <?php if(($errors->has('employee'))||($errors->has('store_id'))): ?>
   
  <div class="alert alert-danger" style="margin-top: 25px;">

    <strong>Whoops!</strong> There were some problems with your input.

    <br/>

    <ul>

      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

      <li><?php echo e($error); ?></li>

      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </ul>

  </div>

<?php endif; ?>
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(URL::to('/store_allot/insert_allot_store')); ?>" id="allot_form" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body col-lg-6">
                

                <div class="form-group">
                  <label for="exampleInputEmail1">Employee</label>
                 <select name="employee" id="employee" class="form-control">
                   <option value="">Select Employee</option>
                   <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <option value="<?php echo e($e->id); ?>"><?php echo e($e->empname); ?></option>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 </select>
                  <div class="doctor_error" style="color: red"></div>
                </div>

               

                
              </div>
                <div class="col-lg-12 dynamic_allot">
                <?php $__currentLoopData = $store; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="col-md-4 form-group">
                       <?php echo e($s->name); ?> : <input type="checkbox" name="store_id[]" class="check_test" value="<?php echo e($s->id); ?>">                      
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                </div>


                
                 <input type="hidden" name="_token" id="token" value="<?php echo e(csrf_token()); ?>">
                
               <input type="hidden" name="edit_id" value="0" id="edit_id">
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary add_date" style="margin-top: 20px;margin-bottom: 15px;">Submit</button>
              </div>
             
              <!-- /.box-body -->

             
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>






    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">

    <?php if(($errors->has('employee_ids'))||($errors->has('store'))): ?>
   
  <div class="alert alert-danger" style="margin-top: 25px;">

    <strong>Whoops!</strong> There were some problems with your input.

    <br/>

    <ul>

      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

      <li><?php echo e($error); ?></li>

      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </ul>

  </div>

<?php endif; ?>
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(URL::to('/store_allot/insert_allot_employee')); ?>" id="allot_form" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body col-lg-6">
                

                <div class="form-group">
                  <label for="exampleInputEmail1">Stores</label>
                 <select name="store" id="store" class="form-control">
                   <option value="">Select Store</option>
                   <?php $__currentLoopData = $store; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <option value="<?php echo e($s->id); ?>"><?php echo e($s->name); ?></option>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 </select>
                  <div class="doctor_error" style="color: red"></div>
                </div>

               

                
              </div>
                <div class="col-lg-12 dynamic_employee">
                <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="col-md-4 form-group">
                       <?php echo e($e->empname); ?> : <input type="checkbox" name="employee_ids[]" class="check_test" value="<?php echo e($s->id); ?>">                      
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                </div>


                
                 <input type="hidden" name="_token" id="token" value="<?php echo e(csrf_token()); ?>">
                
               <input type="hidden" name="edit_id" value="0" id="edit_id">
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary add_date" style="margin-top: 20px;margin-bottom: 15px;">Submit</button>
              </div>
             
              <!-- /.box-body -->

             
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>


  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<?php if(Session::get('success')==1): ?>
        <script>
          Msg.success('successfully updated',2000);
        </script>
<?php endif; ?>
<script>
  $(document).ready(function(){

    $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
         });


    $('#employee').change(function(){


        var id  = $(this).val();
         if(id=='')
         {
           id=0;
         }
        $('.dynamic_allot').load('<?php echo e(URL::to("/store_allot/ajax_view/")); ?>/'+id);

        
    });



    $('#store').change(function(){

      var id  = $(this).val();
         if(id=='')
         {
           id=0;
         }
        $('.dynamic_employee').load('<?php echo e(URL::to("/store_allot/employee_ajax_view/")); ?>/'+id);

    });
   
    


      function clear_form()
      {
         $('#start_date').val(''); 

         $('#end_date').val('');

         $('#edit_id').val(0);

         $('#doctor').val('');

         

        $('input[type="checkbox"]').iCheck('uncheck');
         //$('.dynamic_allot').find('').removeClass('.checked');
         $('.icheckbox_flat-blue').removeClass('.checked');
         table.ajax.reload();




      }

      //$('.dynamic_allot').load('<?php echo e(URL::to("/hdoctor/ajax_time_slot_view")); ?>',send_data);

  
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>