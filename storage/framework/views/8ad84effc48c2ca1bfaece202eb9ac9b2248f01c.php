 
 <?php $__env->startSection('content'); ?>
 <?php
//print_r($ordercount);
//exit;

 ?>

<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo e($ordercount[0][0]); ?></h3>

              <p>Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo e($salecount[0][0]); ?><sup style="font-size: 20px"></sup></h3>

              <p>Sales</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo e($payment); ?></h3>

              <p>Payments</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo e($visit); ?></h3>

              <p>Visits</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
  
 <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"> Orders vs Sales </h3>
<p>
<?php
  $months = array();
for ($i = 0; $i < 8; $i++) {
    $timestamp = mktime(0, 0, 0, date('n') - $i, 1);
    $months[date('n', $timestamp)] = date('F', $timestamp);
    // echo $months[date('n', $timestamp)];
    //echo date('M');
  //  echo date("M", strtotime("-1 months"));
}  ?>    </p>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
             <!--    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="canvas"" style="height:280px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>




<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
  <script>
  var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

  var barChartData = {
    labels : ["<?php echo e(date('M')); ?>","<?php echo e(date('M', strtotime('-1 months'))); ?>","<?php echo e(date('M', strtotime('-2 months'))); ?>","<?php echo e(date('M', strtotime('-3 months'))); ?>","<?php echo e(date('M', strtotime('-4 months'))); ?>","<?php echo e(date('M', strtotime('-5 months'))); ?>"],
    datasets : [
      {
        fillColor : "rgba(220,220,220,0.5)",
        strokeColor : "rgba(220,220,220,0.8)",
        highlightFill: "rgba(220,220,220,0.75)",
        highlightStroke: "rgba(220,220,220,1)",
        data : [<?php echo e($ordercount[0][0]); ?>,<?php echo e($ordercount[1][1]); ?>,<?php echo e($ordercount[2][2]); ?>,<?php echo e($ordercount[3][3]); ?>,<?php echo e($ordercount[4][4]); ?>,<?php echo e($ordercount[5][5]); ?>]
      },
      {
        fillColor : "rgba(151,187,205,0.5)",
        strokeColor : "rgba(151,187,205,0.8)",
        highlightFill : "rgba(151,187,205,0.75)",
        highlightStroke : "rgba(151,187,205,1)",
        data : [<?php echo e($salecount[0][0]); ?>,<?php echo e($salecount[1][1]); ?>,<?php echo e($salecount[2][2]); ?>,<?php echo e($salecount[3][3]); ?>,<?php echo e($salecount[4][4]); ?>,<?php echo e($salecount[5][5]); ?>]
      }
    ]

  }
  window.onload = function(){
    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx).Bar(barChartData, {
      responsive : true
    });
  }

  </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>