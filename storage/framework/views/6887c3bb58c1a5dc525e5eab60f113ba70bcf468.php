<?php $__env->startSection('content'); ?>

<section class="content-header">
      <h1>
     View User
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo e(URL::to('/user')); ?>">Users</a></li>
        <li class="active">View User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         <?php if($errors->any()): ?>   
  <div class="" style="margin-top: 25px;">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>

 <?php if(Session::has('flash_message')): ?>
      <div class="alert alert-success"> <?php echo e(Session::get('flash_message')); ?> </div>
      <?php endif; ?>  
</div>
 <!-- flash end-->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
             <div class="form-group">
              
                   <img style="width:110px; height:110px;" src="<?php echo e(url('theme/image/profile_photo/'.$profile->photo)); ?>"/>
                                        </div>
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <?php echo e($profile->empname); ?>

                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
               <?php echo e($profile->email); ?>"
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Phone no</label>
               <?php echo e($profile->phone); ?>

                </div>

                  <div class="form-group">
                  <label for="exampleInputEmail1">Date of Joining</label>
             <?php echo e($profile->doj); ?>

            </div>

       
                <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                <?php echo e($profile->address); ?>

                </div>  




                  <div class="form-group">
                  <label for="exampleInputFile">Designation</label>
             <?php echo e($profile->designation); ?>


 
</div>


        

          
              <!-- /.box-body -->

            
          
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout/defualt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>