-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2018 at 12:37 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ammu_creations`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand`, `bimage`, `created_at`, `updated_at`) VALUES
(2, 'Allen Solly Shirts', '1512463322thaali3.jpg', '2017-12-05 01:59:13', '2017-12-05 03:12:02'),
(3, 'Bata1', '1512462093Solidarity-Group-Loans-Large.jpg', '2017-12-05 02:01:47', '2017-12-05 02:51:33'),
(4, 'Zero Shirts', '1512462083goldloan1.jpg', '2017-12-05 02:02:44', '2017-12-05 02:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_hands`
--

CREATE TABLE `cash_in_hands` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` int(20) DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `mode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transferdate` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cash_in_hands`
--

INSERT INTO `cash_in_hands` (`id`, `date`, `employee`, `store`, `amount`, `cheque`, `transfer`, `mode`, `transferdate`, `created_at`, `updated_at`) VALUES
(1, '2018-01-17', '12', 5, '100', 'sbin55555', 'no', 'cheque', '2018-01-17', '2018-01-17 06:00:30', '2018-01-17 06:09:33'),
(2, '2018-01-17', '12', 5, '100', 'sbin55555', 'no', 'cheque', '2018-01-17', '2018-01-17 06:00:31', '2018-01-17 06:09:08'),
(3, '2018-01-17', '12', 5, '100', 'sbin55555', 'no', 'cheque', '2018-01-17', '2018-01-17 06:00:32', '2018-01-17 06:09:08'),
(4, '2018-01-17', '12', 5, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:00:35', '2018-01-17 06:06:56'),
(5, '2018-01-17', '12', 5, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:00:35', '2018-01-17 06:09:41'),
(6, '2018-01-17', '12', 1, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:00:37', '2018-01-17 06:06:56'),
(7, '2018-01-17', '12', 1, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:02:19', '2018-01-17 06:06:56'),
(8, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:54:58', '2018-01-18 02:54:58'),
(9, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:01', '2018-01-18 02:55:01'),
(10, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:03', '2018-01-18 02:55:03'),
(11, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:07', '2018-01-18 02:55:07'),
(12, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:08', '2018-01-18 02:55:08'),
(13, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:27', '2018-01-23 00:03:27'),
(14, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:29', '2018-01-23 00:03:29'),
(15, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:30', '2018-01-23 00:03:30'),
(16, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:32', '2018-01-23 00:03:32'),
(17, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:33', '2018-01-23 00:03:33'),
(18, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:34', '2018-01-23 00:03:34'),
(19, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:35', '2018-01-23 00:03:35'),
(20, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:36', '2018-01-23 00:03:36'),
(21, '2018-01-23', '7', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 00:06:02', '2018-01-23 00:06:02'),
(22, '2018-01-23', '7', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 00:06:03', '2018-01-23 00:06:03'),
(23, '2018-01-23', '7', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 00:06:05', '2018-01-23 00:06:05'),
(24, '2018-01-23', '8', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:01:47', '2018-01-23 01:01:47'),
(25, '2018-01-23', '8', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:01:48', '2018-01-23 01:01:48'),
(26, '2018-01-23', '8', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:01:49', '2018-01-23 01:01:49'),
(27, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:09', '2018-01-23 01:02:09'),
(28, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:10', '2018-01-23 01:02:10'),
(29, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:11', '2018-01-23 01:02:11'),
(30, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:12', '2018-01-23 01:02:12'),
(31, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:13', '2018-01-23 01:02:13'),
(32, '2018-01-24', '11', 9, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-24 05:48:53', '2018-01-24 05:48:53'),
(33, '2018-01-24', '11', 9, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-24 05:48:58', '2018-01-24 05:48:58'),
(34, '2018-01-24', '11', 9, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-24 05:48:59', '2018-01-24 05:48:59'),
(35, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:28', '2018-01-24 05:49:28'),
(36, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:29', '2018-01-24 05:49:29'),
(37, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:30', '2018-01-24 05:49:30'),
(38, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:31', '2018-01-24 05:49:31');

-- --------------------------------------------------------

--
-- Table structure for table `catagories`
--

CREATE TABLE `catagories` (
  `id` int(10) UNSIGNED NOT NULL,
  `catagory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catagories`
--

INSERT INTO `catagories` (`id`, `catagory`, `cimage`, `created_at`, `updated_at`) VALUES
(1, 'T Shirts', '1512469509ems.png', NULL, '2017-12-05 04:55:09'),
(2, 'Shirts Cotton', '1512472910cash-credit-250x250.jpg', '2017-12-05 04:34:08', '2017-12-05 05:51:50');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `colorname`, `colorcode`, `created_at`, `updated_at`) VALUES
(1, 'Yellow', '#23df30', '2017-12-14 02:52:11', '2017-12-14 02:52:11'),
(2, 'Green', '#hhhj', '2017-12-14 02:52:34', '2017-12-14 02:52:34'),
(4, 'Pale yellow', '#dfb923', '2017-12-14 03:17:33', '2017-12-14 03:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `leave_management`
--

CREATE TABLE `leave_management` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_casual_leave` int(11) NOT NULL DEFAULT '0',
  `total_sick_leave` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_management`
--

INSERT INTO `leave_management` (`id`, `user_id`, `total_casual_leave`, `total_sick_leave`, `date`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 7, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL),
(2, 8, 12, 8, '2018-01-09', 0, '2018-01-09 09:45:53', '2018-01-09 10:34:14'),
(3, 10, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', '2018-01-09 10:21:34'),
(4, 11, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL),
(5, 12, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL),
(6, 14, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_taken`
--

CREATE TABLE `leave_taken` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `leave_date` date DEFAULT NULL,
  `reason` text,
  `leave_type` int(11) DEFAULT NULL COMMENT '1-casual,2-sick,3-loss_of_pay',
  `day_type` int(11) NOT NULL COMMENT '1-fullday,2-halfday',
  `slot` varchar(30) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_taken`
--

INSERT INTO `leave_taken` (`id`, `user_id`, `leave_date`, `reason`, `leave_type`, `day_type`, `slot`, `is_delete`, `created_at`, `updated_at`) VALUES
(14, 11, '2018-01-04', 'test', 1, 1, '', 0, '2018-01-12 11:00:13', '2018-01-13 04:00:09'),
(15, 11, '2018-01-04', 'test', 1, 2, 'afternoon', 0, '2018-01-12 11:00:13', '2018-01-13 04:00:09'),
(16, 11, '2018-01-04', 'test', 1, 2, 'afternoon', 0, '2018-01-12 11:00:13', '2018-01-13 04:00:09'),
(17, 11, '2018-01-05', 'test', 1, 1, '', 1, '2018-01-12 11:00:51', NULL),
(18, 11, '2018-01-06', 'test', 1, 2, 'afternoon', 1, '2018-01-12 11:00:51', NULL),
(19, 11, '2018-01-07', 'test', 1, 2, 'afternoon', 1, '2018-01-12 11:00:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2017_12_13_081600_create_stores_table', 1),
(19, '2017_12_13_102900_create_sizes_table', 2),
(20, '2017_12_14_081028_create_colors_table', 3),
(21, '2017_12_15_070132_create_tasks_table', 4),
(22, '2017_12_20_110508_add_fields_to_tasks', 5),
(23, '2017_12_21_091811_create_order_table', 6),
(24, '2018_01_01_070111_create_placeorder_table', 7),
(25, '2018_01_01_111813_create_payment_table', 8),
(26, '2018_01_29_062309_create_stocks_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `displayname`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Designation', 'Designation', 'Designation for  Employee', '2017-11-29 04:30:27', '2017-11-29 04:43:47'),
(2, 'Work Allot', 'Allotment', 'Work Allotment', '2017-11-30 01:02:18', '2017-11-30 01:02:18'),
(3, 'Products', 'Product s', 'Products', '2017-12-01 01:06:53', '2017-12-01 01:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `taskid`, `store`, `employee`, `quantity`, `billno`, `status`, `created_at`, `updated_at`) VALUES
(1, '12/21/2017', '11', 'Store3', 'employee2', '30', 'BILLNO55566', 'oncourier', '2017-12-21 04:35:33', '2017-12-22 05:35:44'),
(2, '12/22/2017', '10', 'Store3', 'employee2', '60', 'BILLNO9909', 'oncourier', '2017-12-22 04:42:27', '2017-12-22 05:46:04'),
(3, '12/22/2017', '5', 'Store3', 'employee1', '50', 'BILL8900', 'oncourier', '2017-12-22 05:06:24', '2017-12-22 05:42:21'),
(4, '12/22/2017', '3', NULL, 'employee1', '17', 'BILL66677', 'oncourier', '2017-12-22 05:08:06', '2017-12-22 05:08:06'),
(5, '12/22/2017', '1', 'Store1', 'employee1', '56', 'BILL8889', 'oncourier', '2017-12-22 05:09:04', '2018-01-05 03:32:18'),
(6, '12/22/2017', '6', 'Store3', 'employee1', '50', 'BILLNO5555', 'oncourier', '2017-12-22 05:42:50', '2017-12-22 05:42:50'),
(7, '12/22/2017', '2', 'Store1', 'employee1', '67', 'BIL888776', 'oncourier', '2017-12-22 05:52:40', '2017-12-22 05:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `date`, `taskid`, `employee`, `store`, `amount`, `created_at`, `updated_at`) VALUES
(5, '01/02/2018', '1', '14', '3', '100', '2018-01-02 00:09:29', '2018-01-02 00:09:29'),
(6, '01/02/2018', '1', '14', '3', '100', '2018-01-02 00:10:07', '2018-01-02 00:10:07'),
(7, '01/02/2018', '1', '14', '3', '100', '2018-01-02 00:11:50', '2018-01-02 00:11:50');

-- --------------------------------------------------------

--
-- Table structure for table `placeorders`
--

CREATE TABLE `placeorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unitprice` int(50) DEFAULT NULL,
  `price` int(50) DEFAULT NULL,
  `courrier` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processdate` date DEFAULT NULL,
  `billno` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceno` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processsqty` int(100) DEFAULT NULL,
  `processamount` int(100) DEFAULT NULL,
  `login` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partial` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `placeorders`
--

INSERT INTO `placeorders` (`id`, `date`, `taskid`, `placeid`, `employee`, `store`, `product`, `quantity`, `size`, `color`, `unitprice`, `price`, `courrier`, `processdate`, `billno`, `invoiceno`, `processsqty`, `processamount`, `login`, `partial`, `status`, `created_at`, `updated_at`) VALUES
(58, '1900-01-08', '39', 'ODR5a534edc0a6f1', '14', '7', '2', '2', '6', '5', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:28:36', '2018-01-08 05:28:36'),
(59, '1900-01-08', '39', 'ODR5a534edc0a6f1', '14', '7', '2', '2', '7', '7', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:28:36', '2018-01-08 05:28:36'),
(60, '1900-01-08', '39', 'ODR5a53534fc5aba', '14', '7', '2', '2', '6', '5', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:47:35', '2018-01-08 05:47:35'),
(61, '1900-01-08', '39', 'ODR5a53534fc5aba', '14', '7', '2', '2', '7', '7', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:47:35', '2018-01-08 05:47:35'),
(62, '2018-01-08', '0', 'ODR5a54a97141e4f', '14', '7', '2', '2', '6', '5', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', 'po', '2018-01-09 06:07:21', '2018-01-09 06:07:21'),
(63, '2018-01-08', '0', 'ODR5a54a97141e4f', '14', '7', '9', '2', '7', '7', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', 'po', '2018-01-09 06:07:21', '2018-01-09 06:07:21'),
(64, '2018-01-09', '0', 'ODR5a54ada0d2538', '14', '7', '2', '2', '6', '5', 0, NULL, 'FIRST FLIGHT', '2018-01-10', 'BIll 88877', 'INV 5555', NULL, 0, '', 'no', 'cu', '2018-01-09 06:25:12', '2018-01-10 00:32:51'),
(65, '2018-01-09', '0', 'ODR5a54ada0d2538', '14', '7', '9', '2', '7', '7', 0, NULL, 'DTTC', '2018-01-10', 'BillNo  344458', 'INV56668', NULL, 0, '', 'no', 'cu', '2018-01-09 06:25:12', '2018-01-10 00:25:45'),
(66, '2018-01-10', '0', 'ODR5a55b836521e4', '14', '7', '2', '2', '6', '5', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-10 01:22:38', '2018-01-10 01:22:38'),
(67, '2018-01-10', '0', 'ODR5a55b836521e4', '14', '7', '9', '2', '7', '7', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-10 01:22:38', '2018-01-10 01:22:38'),
(68, '2018-01-11', '0', 'ODR5a56f40f87209', '14', '7', '2', '2', '6', '5', 0, 460, 'DTTC', '2018-01-13', 'Billno44433', 'Inv44455', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:50:15', '2018-01-13 00:25:48'),
(69, '2018-01-11', '0', 'ODR5a56f40f87209', '14', '7', '9', '2', '7', '7', 0, 700, 'DTTC', '2018-01-13', '4445676', 'INV4444', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:50:15', '2018-01-12 23:55:21'),
(70, '2018-01-11', '0', 'ODR5a56f4ba691f5', '14', '7', '2', '2', '6', '5', 230, 460, 'DTTC', '2018-01-11', 'Bill 444333', 'Inv 333445', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:53:06', '2018-01-11 01:19:50'),
(71, '2018-01-11', '0', 'ODR5a56f4ba691f5', '14', '7', '9', '2', '7', '7', 350, 700, 'Proffessional', '2018-01-11', 'Bill No 334443', 'INV 44456', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:53:06', '2018-01-11 01:17:59'),
(72, '2018-01-13', '0', 'ODR5a599d1cd21e6', '14', '7', '2', '2', '6', '5', 230, 460, 'First Flight', '2018-01-13', 'BIll77766', 'INV44455', NULL, 0, NULL, 'no', 'cu', '2018-01-13 00:16:04', '2018-01-13 00:20:34'),
(74, '2018-01-15', '0', 'ODR5a5c527768c27', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:34:23', '2018-01-15 01:34:23'),
(75, '2018-01-15', '0', 'ODR5a5c527768c27', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:34:23', '2018-01-15 01:34:23'),
(76, '2018-01-15', '0', 'ODR5a5c5406e36bb', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:41:02', '2018-01-15 01:41:02'),
(77, '2018-01-15', '0', 'ODR5a5c5406e36bb', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:41:02', '2018-01-15 01:41:02'),
(78, '2018-01-15', '0', 'ODR5a5c5519ccc26', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:45:37', '2018-01-15 01:45:37'),
(79, '2018-01-15', '0', 'ODR5a5c5519ccc26', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:45:37', '2018-01-15 01:45:37'),
(80, '2018-01-15', '0', 'ODR5a5c55656d1e5', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:46:53', '2018-01-15 01:46:53'),
(81, '2018-01-15', '0', 'ODR5a5c55656d1e5', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:46:53', '2018-01-15 01:46:53'),
(82, '2018-01-15', '0', 'ODR5a5c55869ce0e', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:26', '2018-01-15 01:47:26'),
(83, '2018-01-15', '0', 'ODR5a5c55869ce0e', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:26', '2018-01-15 01:47:26'),
(84, '2018-01-15', '0', 'ODR5a5c559f26b1d', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:51', '2018-01-15 01:47:51'),
(85, '2018-01-15', '0', 'ODR5a5c559f26b1d', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:51', '2018-01-15 01:47:51'),
(86, '2018-01-15', '0', 'ODR5a5c5621b402d', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:50:01', '2018-01-15 01:50:01'),
(87, '2018-01-15', '0', 'ODR5a5c5621b402d', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:50:01', '2018-01-15 01:50:01'),
(88, '2018-01-15', '0', 'ODR5a5c567c50d25', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:32', '2018-01-15 01:51:32'),
(89, '2018-01-15', '0', 'ODR5a5c567c50d25', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:32', '2018-01-15 01:51:32'),
(90, '2018-01-15', '0', 'ODR5a5c568b7dc4f', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:47', '2018-01-15 01:51:47'),
(91, '2018-01-15', '0', 'ODR5a5c568b7dc4f', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:47', '2018-01-15 01:51:47'),
(92, '2018-01-15', '0', 'ODR5a5c56c09f1c8', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:52:40', '2018-01-15 01:52:40'),
(93, '2018-01-15', '0', 'ODR5a5c56e68adba', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:53:18', '2018-01-15 01:53:18'),
(94, '2018-01-15', '0', 'ODR5a5c56e68adba', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:53:18', '2018-01-15 01:53:18'),
(95, '2018-01-15', '0', 'ODR5a5c5782093f4', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:55:54', '2018-01-15 01:55:54'),
(96, '2018-01-15', '0', 'ODR5a5c5782093f4', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:55:54', '2018-01-15 01:55:54'),
(97, '2018-01-15', '0', 'ODR5a5c57d7dac67', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:57:19', '2018-01-15 01:57:19'),
(98, '2018-01-15', '0', 'ODR5a5c57d7dac67', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:57:19', '2018-01-15 01:57:19'),
(99, '2018-01-15', '0', 'ODR5a5c58042958c', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:58:04', '2018-01-15 01:58:04'),
(100, '2018-01-15', '0', 'ODR5a5c58042958c', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:58:04', '2018-01-15 01:58:04'),
(101, '2018-01-15', '0', 'ODR5a5c586af2cbb', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:59:46', '2018-01-15 01:59:46'),
(102, '2018-01-15', '0', 'ODR5a5c586af2cbb', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:59:47', '2018-01-15 01:59:47'),
(103, '2018-01-15', '0', 'ODR5a5c58cd67cce', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:25', '2018-01-15 02:01:25'),
(104, '2018-01-15', '0', 'ODR5a5c58cd67cce', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:25', '2018-01-15 02:01:25'),
(105, '2018-01-15', '0', 'ODR5a5c58e114f6f', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:45', '2018-01-15 02:01:45'),
(106, '2018-01-15', '0', 'ODR5a5c58e114f6f', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:45', '2018-01-15 02:01:45'),
(107, '2018-01-15', '0', 'ODR5a5c59096ca9a', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:02:25', '2018-01-15 02:02:25'),
(108, '2018-01-15', '0', 'ODR5a5c59096ca9a', '14', '7', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-25', 'Billno33334', 'inv4445', 1, 350, NULL, 'yes', 'cu', '2018-01-15 02:02:25', '2018-01-25 02:27:25'),
(109, '2018-01-15', '0', 'ODR5a5c59171ce05', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:02:39', '2018-01-15 02:02:39'),
(110, '2018-01-15', '0', 'ODR5a5c59171ce05', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:02:39', '2018-01-15 02:02:39'),
(111, '2018-01-15', '0', 'ODR5a5c5997d2bb0', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:04:47', '2018-01-15 02:04:47'),
(112, '2018-01-15', '0', 'ODR5a5c5997d2bb0', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:04:47', '2018-01-15 02:04:47'),
(113, '2018-01-15', '0', 'ODR5a5c5a0dac34a', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:06:45', '2018-01-15 02:06:45'),
(114, '2018-01-15', '0', 'ODR5a5c5a0dac34a', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:06:45', '2018-01-15 02:06:45'),
(115, '2018-01-15', '0', 'ODR5a5c5a373989d', '14', '7', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', '66677', '77888', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:07:27', '2018-01-15 02:46:02'),
(116, '2018-01-15', '0', 'ODR5a5c5a373989d', '14', '7', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', 'bill999', 'inv5555', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:07:27', '2018-01-15 02:11:24'),
(117, '2018-01-15', '0', 'ODR5a5c5b6270641', '14', '7', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', '666777', '777777', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:12:26', '2018-01-15 02:42:17'),
(118, '2018-01-15', '0', 'ODR5a5c5b6270641', '14', '7', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', '6666', '777', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:12:26', '2018-01-15 02:32:35'),
(119, '2018-01-15', '0', 'ODR5a5c5ba0b3da2', '14', '3', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', '66677', '78888', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:13:28', '2018-01-15 02:39:30'),
(120, '2018-01-15', '0', 'ODR5a5c5ba0b3da2', '14', '3', '9', '2', '7', '7', 350, 700, 'First Flight', '2018-01-15', 'bill7777', 'Inv 444', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:13:28', '2018-01-15 02:30:57'),
(121, '2018-01-15', '0', 'ODR5a5c5cd439512', '14', '3', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', 'billno77788', 'inv666', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:18:36', '2018-01-15 02:29:19'),
(122, '2018-01-15', '0', 'ODR5a5c5cd439512', '14', '3', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', 'bill9999', 'inv5555', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:18:36', '2018-01-15 02:28:07'),
(123, '2018-01-15', '0', 'ODR5a5c6439425a6', '14', '4', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:50:09', '2018-01-15 02:50:09'),
(124, '2018-01-15', '0', 'ODR5a5c6439425a6', '14', '4', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', 'B777', 'I888', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:50:09', '2018-01-15 02:52:07'),
(125, '2018-01-17', '0', 'ODR5a5f1113cbac8', '14', '4', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-17 03:32:11', '2018-01-17 03:32:11'),
(126, '2018-01-17', '0', 'ODR5a5f1113cbac8', '14', '4', '9', '2', '7', '7', 350, 700, 'FFcourrier', '2018-01-25', 'Billno7777', 'Inv4444', 2, 700, NULL, 'no', 'cu', '2018-01-17 03:32:11', '2018-01-25 03:22:50'),
(127, '2018-01-25', '0', 'ODR5a6997b879e11', '14', '4', '2', '7', '6', '5', 230, 1610, 'DTTC5555', '2018-01-25', 'Billno777', 'Inv7777', 2, 460, NULL, 'yes', 'cu', '2018-01-25 03:09:20', '2018-01-25 03:16:20'),
(128, '2018-01-25', '0', 'ODR5a6997b879e11', '14', '4', '9', '8', '7', '7', 350, 2800, 'FFCUR', '2018-01-25', 'Bill666', 'INV555', 3, 1050, NULL, 'yes', 'cu', '2018-01-25 03:09:20', '2018-01-25 03:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `productcolors`
--

CREATE TABLE `productcolors` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productcolors`
--

INSERT INTO `productcolors` (`id`, `productid`, `colorid`, `colorname`, `colorcode`, `created_at`, `updated_at`) VALUES
(3, '5', '', 'Yellow', '#hhhj', '2017-12-09 03:22:29', '2017-12-09 03:22:29'),
(4, '2', '', 'Green', '#23df30', '2017-12-11 06:39:43', '2017-12-11 06:39:43'),
(9, '10', '2', 'Green', '#hhhj', '2018-01-03 02:33:03', '2018-01-03 02:33:03'),
(11, '10', '1', 'Yellow', '#23df30', '2018-01-03 02:38:13', '2018-01-03 02:38:13');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `itemcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priceunit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brandid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `itemcode`, `product`, `price`, `priceunit`, `brandid`, `catid`, `description`, `pimage`, `active`, `created_at`, `updated_at`) VALUES
(2, 'ITEM15396', 'p1', '230', '56', '2', '1', 'desc', 'theme/image/product/1512621019mort.jpg', 0, '2017-12-06 06:34:55', '2018-01-29 02:23:44'),
(7, 'ITEM15395', 'check product pr1', '1200', '1', '2', '1', 'desc check pr1', 'theme/image/product/1515665268goldloan1.jpg', 1, '2018-01-03 06:43:39', '2018-01-11 04:37:48'),
(8, 'ITEM15394', 'Product 1', '200', 'Piece', '2', '1', 'Description', 'theme/image/product/1515826031Solidarity-Group-Loans-Large.jpg', 1, '2018-01-03 08:28:26', '2018-01-13 01:17:11'),
(9, 'ITEM15393', 'Product 3', '350', 'Piece', '3', '1', 'Product 3 description', 'theme/image/product/1517216450vegcultivation.jpg', 0, '2018-01-03 11:33:47', '2018-01-29 03:30:50'),
(10, 'ITEM15392', 'Prod', '400', 'Box', '2', '1', 'Descr', 'theme/image/product/1517216356interest.jpg', 1, '2018-01-06 07:24:07', '2018-01-29 03:29:16'),
(11, 'ITEM15391', 'np1', '150', '1', '2', '1', 'np1 desc', 'theme/image/product/1517208701.jpg', 1, '2018-01-29 01:21:41', '2018-01-29 01:24:28'),
(12, 'ITEM15390', 'np2', '300', '1', '2', '1', 'np2 desc', 'theme/image/product/1517211999industrial-loans.jpg', 0, '2018-01-29 01:38:57', '2018-01-29 02:16:39'),
(13, 'ITEM15389', 'np3', '500', '2', '3', '3', 'np2 desc', 'theme/image/product/1517211797.jpg', 1, '2018-01-29 02:13:17', '2018-01-29 02:13:17');

-- --------------------------------------------------------

--
-- Table structure for table `productsizes`
--

CREATE TABLE `productsizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizeid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productsizes`
--

INSERT INTO `productsizes` (`id`, `productid`, `sizeid`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(2, '5', '', 'Medium', 'M', '2017-12-11 01:19:09', '2017-12-11 01:19:09'),
(3, '5', '', 'Small', 'S', '2017-12-11 01:20:52', '2017-12-11 01:20:52'),
(4, '2', '', 'Medium', 'M', '2017-12-11 05:37:39', '2017-12-11 05:37:39'),
(5, '2', '', 'Baby', 'BBY', '2017-12-11 05:37:51', '2017-12-11 05:37:51'),
(6, '2', '', 'Large', 'XL', '2017-12-12 03:42:44', '2017-12-12 03:42:44'),
(7, '6', '', 'Baby', 'BB', '2017-12-12 03:43:31', '2017-12-12 03:43:31'),
(8, '6', '', 'Large', 'XL', '2017-12-12 03:43:36', '2017-12-12 03:43:36'),
(9, '8', '1', 'Medium', 'M', '2018-01-03 00:51:59', '2018-01-03 00:51:59'),
(12, '10', '2', 'Baby', 'BB', '2018-01-03 01:41:34', '2018-01-03 01:41:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modules` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `displayname`, `modules`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'For Administration full control', 'Admin', '1,2', '2017-11-29 02:00:28', '2017-12-01 00:57:50'),
(2, 'user', 'User', 'User', '1,2', '2017-11-30 01:18:20', '2017-12-01 00:58:38'),
(3, 'Role1', 'Role1 Desc', 'Role1', '2,3', '2017-12-01 01:05:54', '2017-12-02 05:14:30'),
(4, 'Employee', 'Employee role', 'Employee', '0', '2017-12-02 00:28:24', '2017-12-02 00:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `orderid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales` int(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `date`, `orderid`, `employee`, `store`, `sales`, `created_at`, `updated_at`) VALUES
(1, '2018-01-11', '71', '14', '1', 700, '2018-01-11 01:17:59', '2018-01-11 01:17:59'),
(2, '2017-12-09', '70', '14', '1', 460, '2018-01-11 01:19:50', '2018-01-11 01:19:50'),
(3, '2017-12-02', '68', '14', NULL, 460, '2018-01-12 23:38:53', '2018-01-12 23:38:53'),
(4, '2017-10-12', '66', '14', NULL, 500, '2018-01-12 23:40:05', '2018-01-12 23:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(1, 'Medium', 'M', '2017-12-14 02:27:43', '2017-12-14 02:27:43'),
(2, 'Baby', 'BB', '2017-12-14 02:28:03', '2017-12-14 02:28:03'),
(4, '85', 'EF', '2017-12-14 02:38:56', '2017-12-14 02:38:56');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `item_code`, `stock`, `status`, `created_at`, `updated_at`) VALUES
(14, 'ITEM15389', 60, 1, '2018-01-29 03:57:49', NULL),
(15, 'ITEM15390', 70, 1, '2018-01-29 03:57:49', NULL),
(16, 'ITEM15391', 30, 1, '2018-01-29 03:57:49', NULL),
(17, 'ITEM15392', 60, 1, '2018-01-29 03:57:49', NULL),
(18, 'ITEM15393', 110, 1, '2018-01-29 03:57:49', NULL),
(19, 'ITEM15394', 20, 1, '2018-01-29 03:57:49', NULL),
(21, 'ITEM15396', 333, 1, '2018-01-29 03:57:49', NULL),
(22, 'ITEM15396', -10, 1, '2018-01-29 04:26:55', NULL),
(23, 'ITEM15389', 60, 1, '2018-01-30 05:50:50', NULL),
(24, 'ITEM15390', 70, 1, '2018-01-30 05:50:50', NULL),
(25, 'ITEM15391', 30, 1, '2018-01-30 05:50:50', NULL),
(26, 'ITEM15392', 60, 1, '2018-01-30 05:50:50', NULL),
(27, 'ITEM15393', 110, 1, '2018-01-30 05:50:50', NULL),
(28, 'ITEM15394', 20, 1, '2018-01-30 05:50:50', NULL),
(29, 'ITEM15395', 0, 1, '2018-01-30 05:50:50', NULL),
(30, 'ITEM15396', 333, 1, '2018-01-30 05:50:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `paid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `balance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `description`, `contactname`, `contactno`, `address`, `location`, `zipcode`, `city`, `state`, `country`, `total`, `paid`, `balance`, `created_at`, `updated_at`) VALUES
(1, 'Store1', '', 'Mr. Jeevanand Raj', '8887766655', 'Adresss store 1', 'P M Kutty Road', '8776555', 'Calicut', 'Kerala', 'India', '300000', '200', '299800', '2017-12-13 05:41:52', '2018-01-01 23:22:47'),
(3, 'Store3', 'Kaloor Branch', 'Mr. Geo Jacob', '8888877766', 'Adresss store 2 3', 'Kaloor Stadium Junction', '677855566', 'Ernakulam', 'kerala', 'India1', '400000', '250000', '150000', '2017-12-13 06:29:17', '2018-01-03 03:52:18'),
(4, 'store5', '', 'Mr. Naveen', '8888877777', 'Store-5 address', NULL, '678999', 'Calicut', 'Kerala', 'India', '500000', '200000', '300000', '2018-01-01 23:32:23', '2018-01-01 23:38:15'),
(5, 'store 5', '', 'Mr. Naveen', '8888877766', 'Adresss store 5', NULL, '675007', 'Calicut', 'Kerala', 'India', NULL, '0', '0', '2018-01-02 01:04:59', '2018-01-02 01:04:59'),
(7, 'store6', 'test', 'test', '234567', 'test', 'calicut', '12345', 'nadakkavu', 'kerala', 'india', NULL, '0', '0', '2018-01-10 22:46:19', '2018-01-10 22:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `store_allot`
--

CREATE TABLE `store_allot` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_allot`
--

INSERT INTO `store_allot` (`id`, `employee_id`, `store_id`, `is_delete`, `created_at`, `updated_at`) VALUES
(32, 7, 1, 1, '2018-01-20 03:35:14', NULL),
(33, 8, 1, 0, '2018-01-20 03:35:14', '2018-01-20 03:38:30'),
(34, 7, 3, 1, '2018-01-20 04:17:04', NULL),
(35, 8, 3, 1, '2018-01-20 04:17:04', NULL),
(36, 10, 3, 1, '2018-01-20 04:17:04', NULL),
(37, 11, 3, 1, '2018-01-20 04:17:04', NULL),
(38, 12, 3, 1, '2018-01-20 04:17:04', NULL),
(39, 14, 3, 1, '2018-01-20 04:17:04', NULL),
(40, 7, 7, 0, '2018-01-22 06:19:29', '2018-01-22 06:19:43');

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE `targets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sales` float DEFAULT NULL COMMENT 'box_count',
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targets`
--

INSERT INTO `targets` (`id`, `user_id`, `sales`, `date`, `created_at`, `updated_at`) VALUES
(13, 12, 10, '2018-01-09', '2018-01-09 01:21:32', NULL),
(14, 14, 40, '2018-01-09', '2018-01-09 01:21:32', '2018-01-09 01:40:46'),
(15, 8, 30, '2018-01-23', '2018-01-22 23:02:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeeid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tdate` date DEFAULT NULL,
  `ttime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `torder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tpayment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interest` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tremark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tstatus` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `employeeid`, `store`, `address`, `location`, `mode`, `description`, `mobile`, `tdate`, `ttime`, `placeid`, `paymentid`, `torder`, `created_at`, `updated_at`, `tpayment`, `interest`, `tremark`, `tstatus`) VALUES
(1, '12', '1', 'employee23 addresss', 'P M Kutty Road', 'pay', 'desc', '6666688888', '2018-01-04', '3:20am', '', 'PAY5a5343200d115', '56', '2017-12-15 01:57:57', '2018-01-10 00:20:31', 'nil', NULL, 'good', 'payment'),
(2, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'sale', 'desc', '6666688888', '2018-01-20', '3.00am', '', '', '67', '2017-12-15 01:58:16', '2017-12-22 05:52:12', 'nil', NULL, 'good', 'ongoing'),
(3, '11', '3', 'employee1 Address', 'Kaloor Stadium Junction', 'sale', 'desc12 emp12', '6666688888', '2018-01-08', '3.00am', '', '', '17', '2017-12-15 01:58:33', '2018-01-08 23:15:05', 'nil', NULL, 'satisfied', 'ongoing'),
(4, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '2018-01-15', '3.00am', '', '', '', '2017-12-15 01:58:53', '2018-01-02 11:19:56', '', NULL, '', 'completed'),
(5, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'desc', '6666688888', '2018-01-10', '3.00am', '', '', '50', '2017-12-15 01:59:00', '2017-12-22 04:54:08', 'nil', NULL, 'good', 'ongoing'),
(6, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'Description', '6666688888', '2018-01-05', '5.00pm', 'ODR5a534c2979acf', '', '50', '2017-12-15 02:06:37', '2018-01-08 05:17:05', 'nil', NULL, 'good', 'po completed'),
(7, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'sale', 'Desc', '6666688888', '2018-01-09', '3.00pm', '', '', '50', '2017-12-15 02:07:32', '2017-12-22 01:39:10', 'nil', NULL, 'good', 'ongoing'),
(10, '12', '3', 'employee23 addresss', 'Kaloor Stadium Junction', 'pay', 'desc12 emp12', '4445566677', '0000-00-00', '3.00am', '', '', '60', '2017-12-15 04:50:52', '2017-12-20 06:40:49', 'nil', NULL, 'good', 'ongoing'),
(11, '12', '3', 'Adresss store 3', 'Kaloor Stadium Junction', 'sale', 'sale', '4445566677', '0000-00-00', '4.00pm', '', '', '30', '2017-12-20 05:49:30', '2017-12-21 06:48:16', 'nil', NULL, 'good', 'completed'),
(12, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'gmlfmldg', '8887766655', '0000-00-00', '9:54 AM', '', '', NULL, '2017-12-23 03:01:39', '2018-01-04 22:07:51', NULL, NULL, NULL, 'completed'),
(13, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'gmlfmldg', '8887766655', '0000-00-00', '2.30am', '', '', NULL, '2017-12-23 03:01:54', '2018-01-03 03:56:22', NULL, NULL, NULL, 'completed'),
(14, '12', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'sale desc', '8887766655', '0000-00-00', '3.00pm', '', '', NULL, '2017-12-23 04:06:26', '2017-12-23 04:06:26', NULL, NULL, NULL, 'ongoing'),
(15, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'desc12 emp7', '7778866655', '0000-00-00', '3.00pm', '', '', NULL, '2017-12-23 10:36:41', '2017-12-23 10:36:41', NULL, NULL, NULL, 'ongoing'),
(16, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'collect payment', '8888877766', '0000-00-00', '3.01pm', '', '', NULL, '2018-01-01 05:19:40', '2018-01-02 11:30:36', NULL, NULL, NULL, 'completed'),
(17, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'collect payment', '8887766655', '0000-00-00', '2.59pm', '', '', NULL, '2018-01-01 05:20:19', '2018-01-02 11:23:28', NULL, NULL, NULL, 'completed'),
(18, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'collect pay', '7778899955', '0000-00-00', '6:03 PM', '', '', NULL, '2018-01-02 11:41:26', '2018-01-02 14:39:43', NULL, NULL, NULL, 'completed'),
(19, '14', '6', 'address store 7', 'Thane', 'pay', 'sale 100 nos', '7778899955', '0000-00-00', '4.00pm', '', '', NULL, '2018-01-02 11:41:58', '2018-01-04 05:31:31', NULL, NULL, NULL, 'completed'),
(20, '12', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'sale', '8887766655', '0000-00-00', '2.30pm', '', '', NULL, '2018-01-02 11:42:47', '2018-01-02 11:42:47', NULL, NULL, NULL, 'ongoing'),
(21, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'sale', '7778899955', '0000-00-00', '4:06 AM', '', '', NULL, '2018-01-02 11:43:27', '2018-01-02 14:40:11', NULL, NULL, NULL, 'completed'),
(22, '12', '7', 'address store 9', 'Caltex  junction', 'sale', 'sale', '8887766655', '0000-00-00', '12.30pm', '', '', NULL, '2018-01-02 11:43:52', '2018-01-02 11:43:52', NULL, NULL, NULL, 'ongoing'),
(23, '12', '5', 'address store 6', 'Near KSRTC', 'sale', 'sale 20 nos', '7778899955', '0000-00-00', '2.20pm', '', '', NULL, '2018-01-02 11:44:54', '2018-01-02 11:44:54', NULL, NULL, NULL, 'ongoing'),
(24, '14', '7', 'address store 9', 'Caltex  junction', 'pay', 'Payment Description', '8887766655', '0000-00-00', '10.20 AM', '', '', NULL, '2018-01-04 05:33:33', '2018-01-04 06:07:34', NULL, NULL, NULL, 'completed'),
(25, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'Decription will be shown here', '8887766655', '0000-00-00', '10.20 AM', '', '', NULL, '2018-01-04 06:08:52', '2018-01-04 06:11:16', NULL, NULL, NULL, 'completed'),
(26, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'Description will be shown here', '8888877766', '0000-00-00', '100.20 AM', '', '', NULL, '2018-01-04 06:12:16', '2018-01-04 06:14:06', NULL, NULL, NULL, 'completed'),
(27, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'Description will be shown here', '8888877766', '0000-00-00', '10.20 AM', '', '', NULL, '2018-01-04 06:14:23', '2018-01-04 22:05:40', NULL, NULL, NULL, 'completed'),
(28, '14', '6', 'address store 7', 'Thane', 'pay', 'Description will be shown here', '7778899955', '0000-00-00', '10.20 AM', '', '', NULL, '2018-01-04 06:15:19', '2018-01-04 22:06:06', NULL, NULL, NULL, 'completed'),
(29, '14', '6', 'address store 7', 'Thane', 'pay', 'Description will be shown here', '7778899955', '0000-00-00', '10.20 AM', '', '', NULL, '2018-01-04 06:16:16', '2018-01-04 22:06:25', NULL, NULL, NULL, 'completed'),
(30, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'des', '8887766655', '0000-00-00', '12.20AM', '', '', NULL, '2018-01-05 06:39:47', '2018-01-05 07:00:11', NULL, NULL, NULL, 'completed'),
(31, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'des', '8888877766', '0000-00-00', '1.20 PM', '', '', NULL, '2018-01-05 06:40:28', '2018-01-05 07:01:49', NULL, NULL, NULL, 'completed'),
(32, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'des', '8888877766', '0000-00-00', '1.20 PM', '', '', NULL, '2018-01-05 07:05:48', '2018-01-05 11:25:59', NULL, NULL, NULL, 'completed'),
(35, '14', '6', 'address store 7', 'Thane', 'pay', 'des', '7778899955', '0000-00-00', '1.20 PM', '', '', NULL, '2018-01-05 07:06:56', '2018-01-05 07:06:56', NULL, NULL, NULL, 'ongoing'),
(36, '14', '8', 'add', 'loc', 'pay', 'des', '1236547890', '0000-00-00', '1.20 PM', '', '', NULL, '2018-01-05 07:08:36', '2018-01-05 11:44:34', NULL, NULL, NULL, 'completed'),
(37, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'dfdsfsdfsdfs', '8887766655', '0000-00-00', '1.00 PM', '', '', NULL, '2018-01-05 09:21:16', '2018-01-05 09:21:16', NULL, NULL, NULL, 'ongoing'),
(38, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'pay', 'dfdsfsdfsdfs', '8887766655', '0000-00-00', '1.00 PM', '', '', NULL, '2018-01-05 09:21:35', '2018-01-05 10:21:22', NULL, NULL, NULL, 'completed'),
(39, '14', '7', 'address store 9', 'Caltex  junction', 'sale', 'dfdsfsdfsdfs', '8887766655', '0000-00-00', '1.00 PM', 'ODR5a53534fc5aba', '', NULL, '2018-01-05 09:21:52', '2018-01-08 05:47:35', NULL, NULL, 'good', 'po completed'),
(40, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'pay', 'description', '8887766655', '0000-00-00', '8.00 PM', '', '', NULL, '2018-01-05 11:45:51', '2018-01-05 11:45:51', NULL, NULL, NULL, 'ongoing'),
(41, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'description', '7778899955', '0000-00-00', '8.00 PM', '', '', NULL, '2018-01-05 11:46:04', '2018-01-05 11:46:04', NULL, NULL, NULL, 'ongoing'),
(42, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'description', '8887766655', '0000-00-00', '8.00 PM', '', '', NULL, '2018-01-05 11:46:19', '2018-01-05 14:26:45', NULL, NULL, NULL, 'completed'),
(43, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'Sales details', '8887766655', '0000-00-00', '10.00 AM', '', '', NULL, '2018-01-05 11:50:37', '2018-01-05 11:50:37', NULL, NULL, NULL, 'ongoing'),
(44, '14', '6', 'address store 7', 'Thane', 'sale', 'Sales details', '7778899955', '0000-00-00', '11.00 AM', '', '', NULL, '2018-01-05 11:50:55', '2018-01-05 11:50:55', NULL, NULL, NULL, 'ongoing'),
(45, '14', '7', 'address store 9', 'Caltex  junction', 'pay', 'Sales details', '8887766655', '0000-00-00', '11.00 AM', '', '', NULL, '2018-01-05 11:51:29', '2018-01-06 08:21:03', NULL, NULL, NULL, 'completed'),
(46, '14', '5', 'address store 6', 'Near KSRTC', 'sale', 'Sales details', '7778899955', '0000-00-00', '11.00 AM', '', '', NULL, '2018-01-05 11:52:19', '2018-01-05 11:52:19', NULL, NULL, NULL, 'ongoing'),
(47, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'Sales details', '7778899955', '0000-00-00', '9.00 AM', '', '', NULL, '2018-01-05 11:52:42', '2018-01-05 11:52:42', NULL, NULL, NULL, 'ongoing'),
(48, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'pay', 'To collect', '8887766655', '0000-00-00', '10.00pm', '', '', NULL, '2018-01-06 07:42:01', '2018-01-06 07:42:01', NULL, NULL, NULL, 'ongoing'),
(49, '12', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '0000-00-00', '10:10:33am', NULL, 'PAY5a534399cef18', NULL, '2018-01-08 04:40:33', '2018-01-08 04:40:33', NULL, NULL, 'good', 'payment'),
(50, '12', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '0000-00-00', '10:50:02am', NULL, 'PAY5a534cda80807', NULL, '2018-01-08 05:20:02', '2018-01-08 05:20:02', NULL, NULL, 'good', 'payment completed'),
(51, '11', '3', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-08', '10:57:06am', 'ODR5a534e82a9a09', NULL, NULL, '2018-01-08 05:27:06', '2018-01-08 05:27:06', NULL, NULL, 'good', 'po completed'),
(52, '12', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '0000-00-00', '11:18:07am', NULL, 'PAY5a53536f9d8c2', NULL, '2018-01-08 05:48:07', '2018-01-08 05:48:07', NULL, NULL, 'good', 'payment completed'),
(53, '14', '8', 'add', 'loc', 'sale', 'store test emp7', '8887766677', '2018-02-05', '3.00pm', NULL, NULL, NULL, '2018-01-09 00:07:56', '2018-01-09 00:07:56', NULL, NULL, NULL, 'ongoing'),
(54, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-09', NULL, NULL, NULL, NULL, '2018-01-09 00:59:23', '2018-01-09 00:59:23', NULL, NULL, 'good', 'visit completed'),
(55, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-09', '3.00pm', NULL, NULL, NULL, '2018-01-09 01:02:33', '2018-01-09 01:02:33', NULL, NULL, 'good', 'visit completed'),
(56, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-09', '3.00pm', NULL, NULL, NULL, '2018-01-09 01:07:42', '2018-01-09 01:07:42', NULL, NULL, 'interested', 'visit completed'),
(57, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-09', '11:55:12am', 'ODR5a54ada0d2538', NULL, NULL, '2018-01-09 06:25:12', '2018-01-09 06:25:12', NULL, NULL, 'good', 'po completed'),
(58, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-10', '06:52:38am', 'ODR5a55b836521e4', NULL, NULL, '2018-01-10 01:22:38', '2018-01-10 01:22:38', NULL, NULL, 'good', 'po completed'),
(63, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-11', '05:20:15am', 'ODR5a56f40f87209', NULL, NULL, '2018-01-10 23:50:15', '2018-01-10 23:50:15', NULL, NULL, 'good', 'po completed'),
(64, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-11', '05:23:06am', 'ODR5a56f4ba691f5', NULL, NULL, '2018-01-10 23:53:06', '2018-01-10 23:53:06', NULL, NULL, 'good', 'po completed'),
(67, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-12', '9:18 am', NULL, NULL, NULL, '2018-01-12 03:48:22', '2018-01-12 03:48:22', NULL, NULL, 'interested remark', 'visit completed'),
(68, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-12', '9:20 am', NULL, NULL, NULL, '2018-01-12 03:50:50', '2018-01-12 03:50:50', NULL, 'interest', 'interested remark', 'visit completed'),
(69, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'sale 10 nos', '8887766655', '2018-01-25', '3:00pm', NULL, NULL, NULL, '2018-01-12 04:49:34', '2018-01-12 04:49:34', NULL, NULL, NULL, 'ongoing'),
(70, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'test desc', '8888877766', '2018-01-12', '3.00pm', NULL, NULL, NULL, '2018-01-12 05:42:32', '2018-01-12 05:42:32', NULL, NULL, NULL, 'ongoing'),
(71, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'test desc', '8888877766', '2018-01-12', '3.00pm', NULL, NULL, NULL, '2018-01-12 05:46:49', '2018-01-12 05:46:49', NULL, NULL, NULL, 'ongoing'),
(72, '11', '6', 'address store 7', 'Thane', 'pay', 'tst desc', '7778899955', '2018-01-13', '3.00pm', NULL, NULL, NULL, '2018-01-12 05:47:45', '2018-01-12 05:47:45', NULL, NULL, NULL, 'ongoing'),
(73, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-12', '11:43 am', NULL, NULL, NULL, '2018-01-12 06:13:43', '2018-01-12 06:13:43', NULL, 'interest', 'interested remark', 'visit completed'),
(74, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-13', '4:14 am', NULL, NULL, NULL, '2018-01-12 22:44:09', '2018-01-12 22:44:09', NULL, 'interested', 'ok', 'visit completed'),
(75, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-13', '4:15 am', NULL, NULL, NULL, '2018-01-12 22:45:49', '2018-01-12 22:45:49', NULL, 'interested', 'ok', 'visit completed'),
(76, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-13', '05:46:05am', 'ODR5a599d1cd21e6', NULL, NULL, '2018-01-13 00:16:05', '2018-01-13 00:16:05', NULL, NULL, 'good', 'po completed');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_role` int(50) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_role`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin1', 'admin1@gmail.com', '$2y$10$r/koJG3jvk5B4r/j5yxhguJQtMZyarsYo3Olk7GHnG5pzPrWv7KS6', 'sjlXB1YtfKwG35UJfXr9WuHPPw5Hi2w04OvCSRElWaWbR604EoRHPWFQJHp7', '2017-11-23 03:42:16', '2018-01-27 03:05:04'),
(7, 4, 'usr725509', 'emp1@gmail.com', '$2y$10$smaKquyuNuFBI2tgTDcyUuyqVyl4nNmyJs2f/potHB9AXOwnOqKi6', '0Q4CYPOsoWJgKRPTy8HUHq4AyjffDU0RMABcAuxIziqgil88r3JlqC5sNfuC', '2017-11-28 05:59:11', '2018-01-12 01:18:21'),
(8, 4, 'usr136497', 'emp5@gmail.com', '$2y$10$Agj3jurxDyHgQEhslPLVjeW7jYRwaLnL6ntddGbUdnfx4byRp3Zii', NULL, '2017-11-28 06:56:58', '2018-01-12 01:18:12'),
(10, 1, 'usr624571', 'admin2@gmail.com', '$2y$10$V.c6FLs38Ki1RKYS9kF87.yGrXnRtwfnCYp0qjkfFGc9iA217rXVq', 'RU65u0dDGE0Dbsfpk9IPk2s6LRxpTM5mWMc30gIKZNgdfueA1bgI087pz3oE', '2017-12-01 02:07:39', '2018-01-12 01:18:02'),
(11, 4, 'usr369800', 'employee1@gmail.com', '$2y$10$U/oc.FgDHFAy4G9A5pj3RuLhPUfBl4EV56jkPiExvcvgBU9ETAqJq', 'WcRPvuYUMU3iowQk67HbxEunij0vQEai19U9m6PRgrdTWCCWG0M4M2a99hu0', '2017-12-01 02:12:59', '2018-01-12 01:17:43'),
(12, 4, 'usr780052', 'employee2@gmail.com', '$2y$10$PAxNd9R3G1yWsfihIkUBhup2OsqzwILH1M6JMmKFVwr4QUsawhR3O', 'SHEumCuexSVCEXM8yMRKZfEZiDG7otYNa5ITIUQBZDx1eIRnwCBg2gArpFMk', '2017-12-01 02:13:53', '2018-01-12 01:17:20'),
(14, 4, 'usr486226', 'emp7@gmail.com', '$2y$10$Lk9NMSgkgsuL/HnNm89OMOhdJ2Vp./e45ZNF0r/oS3hwCkQKqcrXS', 'eMwSEf65p8HpqX79YtLOex69a629SExNqmTzWZKggI5agwfdqrbywCKgQGHw', '2017-12-23 02:58:06', '2018-01-12 01:16:14');

-- --------------------------------------------------------

--
-- Table structure for table `user_informations`
--

CREATE TABLE `user_informations` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doj` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` int(11) DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_informations`
--

INSERT INTO `user_informations` (`id`, `empname`, `email`, `phone`, `doj`, `designation`, `photo`, `address`, `is_active`, `created_at`, `updated_at`) VALUES
('7', 'emp1', 'emp1@gmail.com', '5556677788', '11/03/2017', 2, '1511868584interest.jpg', 'emp1 addresss', '1', '2017-11-28 05:59:11', '2018-01-12 01:18:21'),
('8', 'emp5', 'emp5@gmail.com', '6666677777', '11/09/2017', 1, '1511872018interest.jpg', 'address', '1', '2017-11-28 06:56:58', '2018-01-12 01:18:12'),
('10', 'admin2', 'admin2@gmail.com', '8887755566', '11/01/2017', 4, '1512113859self-employment-ideas.jpg', 'address', '1', '2017-12-01 02:07:39', '2018-01-12 01:18:02'),
('11', 'employee1', 'employee1@gmail.com', '6666688888', '12/01/2017', 5, '1512114179ems.jpg', 'employee1 Address', '1', '2017-12-01 02:12:59', '2017-12-02 00:30:27'),
('12', 'employee2', 'employee2@gmail.com', '4445566677', '12/02/2017', 6, '1512114233industrial-loans.jpg', 'employee23 addresss', '1', '2017-12-01 02:13:53', '2017-12-02 00:30:39'),
('14', 'emp7', 'emp7@gmail.com', '7778866655', '12/21/2017', 6, '1514017686ems.png', 'emp7 address', '1', '2017-12-23 02:58:06', '2018-01-12 01:16:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_in_hands`
--
ALTER TABLE `cash_in_hands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catagories`
--
ALTER TABLE `catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_management`
--
ALTER TABLE `leave_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_taken`
--
ALTER TABLE `leave_taken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `placeorders`
--
ALTER TABLE `placeorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcolors`
--
ALTER TABLE `productcolors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productsizes`
--
ALTER TABLE `productsizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_allot`
--
ALTER TABLE `store_allot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cash_in_hands`
--
ALTER TABLE `cash_in_hands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `catagories`
--
ALTER TABLE `catagories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `leave_management`
--
ALTER TABLE `leave_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `leave_taken`
--
ALTER TABLE `leave_taken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `placeorders`
--
ALTER TABLE `placeorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `productcolors`
--
ALTER TABLE `productcolors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `productsizes`
--
ALTER TABLE `productsizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `store_allot`
--
ALTER TABLE `store_allot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `targets`
--
ALTER TABLE `targets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
