<!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('theme/fonts/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('theme/css/ionicons.min.css')}}">

   <link rel="stylesheet" href="{{asset('theme/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('theme/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('theme/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" type="text/css" href="{{asset('theme/css/bootstrap-msg.css')}}" />

  <link href="{{asset('theme/css/confirmDialog.css')}}" rel="stylesheet">

  <link rel="stylesheet" href="{{asset('theme/css/select2.min.css')}}">

  <link href="{{asset('theme/css/icheck/all.css')}}" rel="stylesheet">

  <!-- chart  -->
   <script type="text/javascript" src="{{asset('theme/chart.js/Chart.js')}}"></script>

  <!-- date picker -->
  <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
        type="text/css" />


  <link rel="stylesheet" href="{{asset('/theme/css/bootstrap-datepicker.min.css')}}">      

  

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />

