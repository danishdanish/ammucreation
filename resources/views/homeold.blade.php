 @extends('layout/defualt')
 @section('content')
<div class="container">

    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">

                <div class="panel-heading">Dashboard </div>

                <div class="panel-body">

                    <div id="container"></div>

                </div>

            </div>

        </div>

    </div>

</div>

@stop


@section('script')

 <script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">

    $(function () { 

        var data_click = <?php echo $click; ?>;

        var data_viewer = <?php echo $viewer; ?>;

    $('#container').highcharts({

        chart: {

            type: 'column'

        },

        title: {

            text: 'Order and Sales '

        },

        xAxis: {

    categories: ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'],

     type: 'datetime',
        dateTimeLabelFormats: { // don't display the dummy year
            month: '%e. %b',
            year: '%b'
        },
        title: {
            text: 'Month'
        }
       //    data: [01,02,03,04,05,06]

        },

        yAxis: {

            title: {

                text: 'INR'

            }

        },

        series: [{

            name: 'Total Sales',

            data: data_click

        }, {

         
            name: 'Total Order',

            data: data_viewer

        }]

    });

});

</script>

@stop

