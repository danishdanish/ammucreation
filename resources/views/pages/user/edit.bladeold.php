@extends('layout/defualt')

@section('content')


<section class="content-header">
      <h1>
       Edit User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/user')}}">Users</a></li>
        <li class="active">Edit User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/user/add_user')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name"  class="form-control" id="exampleInputEmail1" placeholder="Enter name" required="required" value="{{$data->name}}">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required="required" value="{{$data->email}}">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Phone no</label>
                  <input type="text" name="phone" class="form-control" id="phone_no" placeholder="Enter phone no" required="required" value="{{$data->phone_no}}">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Username</label>
                  <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="Enter username" required="required" value="{{$data->username}}">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" required="required" value="{{$data->password}}">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputPassword1">Confirm Password</label>
                  <input type="password" class="form-control" id="confirm" placeholder="Enter confirm Password" >
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">Profile Photo</label>
                  <input type="file" name="profile_photo" id="exampleInputFile">

                  
                </div>
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="edit_id" value="{{$data->id}}"/>
                 <input type="hidden" id="hidden_password" value="{{$data->password}}"/>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var password = $('#password').val();
      	var check_password = $('#hidden_password').val();
        var confirm = $(this).val();
          if(password!=check_password && password!=confirm ){
          	$("#confirm").attr('required', 'required');
          }
          else{
            $("#confirm").removeAttr('required');
          }
      	  
      });

      $('#password').change(function(){

          var password = $(this).val();
          var hidden_password = $('#hidden_password').val();
          if(password!=hidden_password){
            $("#confirm").attr('required', 'required');
          }
          else{
            $("#confirm").removeAttr('required');
          }
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>
@stop