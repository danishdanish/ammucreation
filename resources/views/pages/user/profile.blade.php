@extends('layout/defualt')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/user')}}"><i class="fa fa-dashboard"></i> User</a></li>
        <li class="active">Profile Change Password</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">

          <!-- flash start-->
            <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
  
</div>

     <div class="col-md-12">
        @if (Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
               <form action="{{URL::to('/user/profile_edit')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">{{$data->name}}</label>
                
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">{{$data->email}}</label>              
                </div>

                 <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Enter New Password" value="">
                </div>

                  <div class="form-group">
                  <label for="exampleInputEmail1">Password Confirm</label>
                  <input type="password" name="password_confirm" class="form-control" id="exampleInputEmail1" placeholder="Password Confirm" value="">
                </div>
                     <div class="box-footer">
                             <input type="hidden" name="id" value="{{$data->id}}">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
      
 </form>

      </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- modal  -->

       
        <!-- modal end -->
    </section>

    @stop

 @section('script')
  @stop