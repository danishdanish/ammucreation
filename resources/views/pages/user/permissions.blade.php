@extends('layout/defualt')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      User Role
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/user')}}"><i class="fa fa-dashboard"></i> User</a></li>
        <li class="active">Role</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">

          <!-- flash start-->
        <div class="col-md-12">
        @if (Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
               <form action="{{URL::to('/user/user_permissions_add')}}" method="POST" role="form" enctype="multipart/form-data" >
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr> {{$data->emp}}  <td> </td><td> 
                  
                  

                  </td>
      
                  </tr>
                <tr>                  
                  <td>Si No</td>
                  <td>Name</td>
                                
                </tr>
     
                <tr>
                  <td> {{$data->email}}</td>
                  <td>
                  <select name="role_sel"> 
                    
                   @foreach ($roles as $role)
 <option value="{{ $role->id }}" <?php if ($role->id==$data->user_role) { ?> selected="selected" <?php } ?> >{{ $role->name}}</option>
@endforeach
</select>
 </td>
</tr>
</thead>
  <tbody>
    </tbody>               
              </table>
                             <input type="hidden" name="uid" value="{{$data->id}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" id="editstatus1"  class="btn btn-success btn-xs" title="Approved">
          <span class="glyphicon glyphicon-ok"> Save</span>
        </button>
      
 </form>

      </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- modal  -->

       
        <!-- modal end -->
    </section>

    @stop

 @section('script')
  @stop