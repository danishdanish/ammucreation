@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
     View User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/user')}}">Users</a></li>
        <li class="active">View User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         @if($errors->any())   
  <div class="" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>
 <!-- flash end-->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
             <div class="form-group">
              
                   <img style="width:110px; height:110px;" src="{{url('theme/image/profile_photo/'.$profile->photo)}}"/>
                                        </div>
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  {{$profile->empname}}
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
               {{$profile->email}}"
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Phone no</label>
               {{$profile->phone}}
                </div>

                  <div class="form-group">
                  <label for="exampleInputEmail1">Date of Joining</label>
             {{$profile->doj}}
            </div>

       
                <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                {{$profile->address}}
                </div>  




                  <div class="form-group">
                  <label for="exampleInputFile">Designation</label>
             {{$profile->designation}}

 
</div>


        

          
              <!-- /.box-body -->

            
          
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

@stop