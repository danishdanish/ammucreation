@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Add User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/user')}}">Users</a></li>
        <li class="active">Add User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
                <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/user/add_user')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="empname"  class="form-control" id="exampleInputEmail1" placeholder="Enter name" value="{{ old('empname') }}" >
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="{{ old('email') }}" >
                </div>

                    <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Enter Password" >
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Phone no</label>
                  <input type="text" name="phone" class="form-control" id="phone_no" placeholder="Enter phone no"  value="{{ old('phone')}}">
                </div>

                  <div class="form-group">
                  <label for="exampleInputEmail1">Date of Joining</label>
                <input id="datepicker" class="form-control" name="doj" placeholder="Enter Date of Joining" value="{{ old('doj') }}" />
            </div>


        

  <div class="form-group">
                  <label for="exampleInputFile">Designation - Role</label>
                    <select name="role" class="form-control"> 
   <option value="">Please select Role</option>                 
                   @foreach ($roles as $role)
 <option value="{{ $role->id }}">{{ $role->name}}</option>
@endforeach
</select>
</div>


    <div class="form-group">
                  <label for="exampleInputFile">Designation - Type</label>
                <select id="is_active" name="designation" class="form-control">
                   <option value="">Please select designation type</option> 
<option value="admin">Administrator</option>
<option value="salesman">Salesman</option>
</select>              
                </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                  <textarea name="address" class="form-control" placeholder="Enter Address">{{ old('address') }}</textarea>
                </div>          

                 <div class="form-group">
                  <label for="exampleInputFile">Profile Photo</label>
                  <input type="file" name="profile_photo" id="exampleInputFile" >
                  
                </div>

           <input type="hidden" name="is_active" value="1">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

@stop