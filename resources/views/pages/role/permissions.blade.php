@extends('layout/defualt')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Roles
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/role')}}"><i class="fa fa-dashboard"></i> Roles</a></li>
        <li class="active">Permissions</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">

          <!-- flash start-->
        <div class="col-md-12">
        @if (Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
               <form action="{{URL::to('/role/role_permissions_add')}}" method="POST" role="form" enctype="multipart/form-data" >
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr> <td> Permissions for {{$data->displayname}} </td><td>   </td><?php   
             $mids = explode(",", $data->modules); ?>
      
                  </tr>
                <tr>                  
                  <td>Si No</td>
                  <td>Name</td>
                                
                </tr>
                @foreach ($modules as $module)
                <tr>
                  <td> {{$module->name}} </td>
                  <td>
        
<?php echo '<input type="checkbox" name="checklist[]" value="'.$module->id.'" '.(in_array($module->id, $mids) ? 'checked="checked"':'').' />'; 

  ?> {{$module->id}} </td>
</tr>
@endforeach

                </thead>
                <tbody>
                
                </tbody>
                
              </table>
               <input type="hidden" name="rid" value="{{$data->id}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" id="editstatus1"  class="btn btn-success btn-xs" title="Approved">
          <span class="glyphicon glyphicon-ok"> Save</span>
        </button>

      </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- modal  -->

       
        <!-- modal end -->
    </section>

    @stop

 @section('script')

     @if (Session::get('add')==1)
        <script>
          Msg.success('successfully added',2000);
        </script>
   @endif

   @if (Session::get('edit')==1)
        <script>
          Msg.success('successfully updated',1500);
        </script>
   @endif

 <script>
  $(function () {
    
    var table =  $('#example1').DataTable({
        //"processing": true,
        "bserverSide": true,
        'searching'   : true,
        "ajax": {
            url: "{{URL::to('/role/ajax_data')}}",
            type: 'GET'
        },
        "columns": [
            {"data": "si_no"},
            {"data": "name"},
            {"data": "displayname"},
            {"data": "description"},
            {"data": "actions"}
           
                ],
    });

      $('.input-sm').keyup(function(){
         table.columns( 2 ).search(  '^' + this.value, true, false ).draw();
      });


    $(document).on('click','#deletebtnrole',function(){
         var uid = $(this).data('id');
   $.createDialog({

    acceptAction: alertCall,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCall(){
  
       $.ajax({
               type:'GET',
                data: {'id': uid},               
               url: "{{URL::to('role/role_delete')}}",
               success:function(data){ 
                 table.ajax.reload();
              document.getElementById("smessage").style.display = "block";                  
                 document.getElementById("smessage").innerHTML = "Succesfully Deleted!";              
              
               }
          });
    }
      
    }); 

    


    
   
  })
</script>






<script>
 $(document).on('click','#checkconnect',function(){       
   $.createDialog({

    acceptAction: alertCallcheck,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCallcheck(){
       $.ajax({
               type:'GET',
               url: "{{URL::to('user/user_check1')}}",
               success:function(data){
            document.getElementById("demo").innerHTML = "Succesfully Deleted!";            
               }
          });
    }
      
    });   




</script>
    @stop