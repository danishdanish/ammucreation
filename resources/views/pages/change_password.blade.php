@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Add User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/user')}}">Users</a></li>
        <li class="active">Add User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/user/add_user')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body col-lg-6 ">
                

                <div class="form-group">
                  <label for="exampleInputPassword1">Old password</label>
                  <input type="password" name="old_password" class="form-control" id="old_password" placeholder="Enter Password" required="required">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputPassword1">New Password</label>
                  <input type="password" class="form-control" id="new_password" placeholder="Enter confirm Password" required="required" data-validation="length" data-validation-length="min8">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Confirm Password</label>
                  <input type="password" class="form-control" id="confirm_password" placeholder="Enter confirm Password" required="required">
                </div>

                <!--<div class=" pull-left form-group col-lg-3">
                   <button type="submit" class="btn btn-primary form-control">Submit</button>
                </div>-->

                

                  
                </div>
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){
       $.validate({
    
  });
      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#old_password').focusout(function(){

           var old_password = $(this).val();

           if(old_password!='')
           {
               
               $.ajax({
                  type:'POST',
                  url:"{{URL::to('/change_password/check_old_password')}}";
                  data:{'old_password':old_password},
                  success:function(msg)
                  {
                    alert('sdfsd');
                  }
               });
           }
      });

	});
</script>
@stop