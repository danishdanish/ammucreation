@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Add Product Size
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/product')}}">Products</a></li>
        <li class="active">Product Size</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>
 <!-- flash end-->
  <div class="col-md-8"> Product Name : {{$product->product}}  </div>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/product/product_size')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                          
                <div class="form-group">              
 <label for="exampleInputEmail1">Select Size </label>
                  <select id="sizesel" name="sizeid" class="form-control">                 
                     @foreach ($sizes as $size)
                          <option value="{{$size->id}}">{{$size->sizename}}</option>
                     @endforeach
                   </select>  
                </div>           
             
               <input type="hidden" name="productid" value="{{$product->id}}">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->

    </section>
        <section class="content">
     <div class="container">
  <div class="row"> 
    
   <table class="table table-stripped table-bordered">
      <thead>
        <tr>        
          <th>Size Name</th>
          <th>Size Code</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
            @foreach ($colors as $color)
        <tr>
           <td>{{$color->sizename}}</td>
          <td>{{$color->sizecode}}</td>
            <td>
               <button title="delete" id="sizedel" class="delete btn btn-danger" data-id="{{$color->id}}" data-pid="{{$product->id}}"><span class="glyphicon glyphicon-remove"></span></button></td>       
        </tr> 
         @endforeach
         </tbody>
      </table>
  </div>
</div>
</section>
@stop

@section('script')
<script>
	 $(document).on('click','#sizedel',function(){ 
  var id = $(this).data('id');  
  var pid = $(this).data('pid'); 
  if (confirm('Are you sure?')) {

$.ajax({
        type:'GET',
      data: {'id':id,'pid':pid},
               url:"{{URL::to('product/size_delete')}}",
               success:function(data){
               alert(data.success);
                window.location.reload();
               }
          });
}
 
    });

</script>

@stop