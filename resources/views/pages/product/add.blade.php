@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Add Product
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/product')}}">Products</a></li>
        <li class="active">Add Product</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
                <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/product/add_product')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">

                 <div class="form-group">
                  <label for="exampleInputEmail1">Item Code</label>
                  <input type="text" name="itemcode"  class="form-control" id="exampleInputEmail1" placeholder="Enter Item Code" value="{{$itemcode}}" >
                </div>

                 <div class="form-group">
                  <label for="exampleInputEmail1">Product Code</label>
                  <input type="text" name="productcode"  class="form-control" id="exampleInputEmail1" placeholder="Enter Product Code" value="" >
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="product"  class="form-control" id="exampleInputEmail1" placeholder="Enter Product Name" value="{{ old('product') }}" >
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Price in INR</label>
                  <input type="text" name="price" class="form-control" id="exampleInputEmail1" placeholder="Enter Price" value="{{ old('price') }}" >
                </div>

                    <div class="form-group">
                  <label for="exampleInputEmail1">Price Unit</label>
                  <input type="text" name="priceunit" class="form-control" id="exampleInputEmail1" placeholder="Enter Price Unit" value="{{ old('priceunit') }}">
                </div>

                <div class="form-group">
                     <label for="exampleInputEmail1">Brand</label>
                  <select id="brand" name="brandid" class="form-control">
                     @foreach ($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->brand}}</option> 
                     @endforeach
                   </select>                
                </div>

               
                  <div class="form-group">
                  <label for="exampleInputEmail1">Catagory</label>
                <select id="brand" name="catid" class="form-control">
                     @foreach ($catagories as $catagory)
                    <option value="{{$catagory->id}}">{{$catagory->catagory}}</option> 
                     @endforeach
                   </select>  
            </div>


            <div class="form-group">
                  <label for="exampleInputFile">Description</label>
                  <input type="text" name="description" class="form-control" id="desc" placeholder="Enter Description" value="{{ old('description') }}" >               
                </div>

                      <div class="form-group">
                     <label for="exampleInputEmail1">Size</label>
                  <select id="brand" name="sizeid" class="form-control">
                    <option value="" >Please Select Size</option>
                     @foreach ($sizes as $size)
                          <option value="{{$size->id}}">{{$size->sizename}}</option> 

                     @endforeach
                   </select>                
                </div>


<div class="form-group">
                     <label for="exampleInputEmail1">Colors</label>
                  <select id="brand" name="colorid" class="form-control">
                      <option value="" >Please Select Color</option> 
                     @foreach ($colors as $color)
                    <option value="{{$color->id}}">{{$color->colorname}}</option> 
                     @endforeach
                   </select>                
                </div>

                    

                 <div class="form-group">
                  <label for="exampleInputFile"> Product Image</label>
                  <input type="file" name="product_image" id="exampleInputFile" >
                  
                </div>
          
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

@stop