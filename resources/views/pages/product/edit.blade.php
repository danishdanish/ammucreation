@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Edit Product
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/product')}}">Products</a></li>
        <li class="active">Edit Product</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         @if($errors->any())   
  <div class="" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>
 <!-- flash end-->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/product/product_edit')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
  
                <div class="form-group">
                  <label for="exampleInputEmail1">Product</label>
                  <input type="text" name="product"  class="form-control" id="exampleInputEmail1" placeholder="Enter name"  value="{{$product->product}}">
                </div>

                 <div class="form-group">
                  <label for="exampleInputEmail1">Item Code</label>
                  <input type="text" name="itemcode"  class="form-control" id="exampleInputEmail1" placeholder="Enter name"  value="{{$product->itemcode}}">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Product Code</label>
                  <input type="text" name="productcode"  class="form-control" id="exampleInputEmail1" placeholder="Enter name"  value="{{$product->productcode}}">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Price -INR</label>
                  <input type="test" name="price" class="form-control" id="exampleInputEmail1" placeholder="Enter Price"  value="{{$product->price}}">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Price Unit</label>
                  <input type="text" name="priceunit" class="form-control" id="exampleInputEmail1" placeholder="Enter Price Unit" value="{{$product->priceunit}}">
                </div>


                <div class="form-group">
                     <label for="exampleInputEmail1">Brand</label>
                  <select id="brand" name="brandid" class="form-control">
                     @foreach ($brands as $brand)
                    <option value="{{$brand->id}}" <?php if ($brand->id==$product->brandid) { ?> selected="selected" <?php } ?>>{{$brand->brand}}</option> 
                     @endforeach
                   </select>                
                </div>

      <div class="form-group">
                  <label for="exampleInputEmail1">Catagory</label>
                <select id="brand" name="catid" class="form-control">
                     @foreach ($catagories as $catagory)
                    <option value="{{$catagory->id}}"  <?php if ($catagory->id==$product->catid) { ?> selected="selected" <?php } ?>>{{$catagory->catagory}}</option> 
                     @endforeach
                   </select>  
            </div>

              <div class="form-group">
                  <label for="exampleInputFile">Description</label>
                  <input type="text" name="description" class="form-control" id="desc" placeholder="Enter Description" value="{{$product->description}}" >               
                </div> 

                 <div class="form-group">
                     
                  <label for="exampleInputEmail1">Is Active</label>
               

<select id="is_active" name="active">
<option value="1" <?php if ($product->active==1) { ?> selected="selected" <?php } ?>>1</option>
<option value="0" <?php if ($product->active==0) { ?> selected="selected" <?php } ?>>0</option>

</select>
                </div>         

           <div class="form-group">
                  <label for="exampleInputFile">Profile Photo</label>
                   <img style="width:70px; height:70px;" src="{{url('theme/image/product/'.$product->pimage)}}"/>
                  <input type="file" name="pimage" id="exampleInputFile" >                  
                </div>

      <input type="hidden" name="id" value="{{$product->id}}">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         </div>
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

@stop