@extends('layout/defualt')

@section('content')

<style type="text/css">
   .error_display{
      color: red;
      
   }
   .error_display input{
     border-color: red;
   }
</style>

<section class="content-header">
      <h1>
       Allot Store
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Stores</a></li>
        <li class="active">Allot Store</li>
      </ol>
    </section>


  <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">

   @if(($errors->has('employee'))||($errors->has('store_id')))
   
  <div class="alert alert-danger" style="margin-top: 25px;">

    <strong>Whoops!</strong> There were some problems with your input.

    <br/>

    <ul>

      @foreach($errors->all() as $error)

      <li>{{ $error }}</li>

      @endforeach

    </ul>

  </div>

@endif
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/store_allot/insert_allot_store')}}" id="allot_form" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body col-lg-6">
                

                <div class="form-group">
                  <label for="exampleInputEmail1">Employee</label>
                 <select name="employee" id="employee" class="form-control">
                   <option value="">Select Employee</option>
                   @foreach($employee as $e)
                   <option value="{{$e->id}}">{{$e->empname}}</option>
                   @endforeach
                 </select>
                  <div class="doctor_error" style="color: red"></div>
                </div>

               

                
              </div>
                <div class="col-lg-12 dynamic_allot">
                @foreach($store as $s)
                  <div class="col-md-4 form-group">
                       {{$s->name}} : <input type="checkbox" name="store_id[]" class="check_test" value="{{$s->id}}">                      
                    </div>
                @endforeach    
                </div>


                
                 <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                
               <input type="hidden" name="edit_id" value="0" id="edit_id">
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary add_date" style="margin-top: 20px;margin-bottom: 15px;">Submit</button>
              </div>
             
              <!-- /.box-body -->

             
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>






    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">

    @if(($errors->has('employee_ids'))||($errors->has('store')))
   
  <div class="alert alert-danger" style="margin-top: 25px;">

    <strong>Whoops!</strong> There were some problems with your input.

    <br/>

    <ul>

      @foreach($errors->all() as $error)

      <li>{{ $error }}</li>

      @endforeach

    </ul>

  </div>

@endif
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/store_allot/insert_allot_employee')}}" id="allot_form" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body col-lg-6">
                

                <div class="form-group">
                  <label for="exampleInputEmail1">Stores</label>
                 <select name="store" id="store" class="form-control">
                   <option value="">Select Store</option>
                   @foreach($store as $s)
                   <option value="{{$s->id}}">{{$s->name}}</option>
                   @endforeach
                 </select>
                  <div class="doctor_error" style="color: red"></div>
                </div>

               

                
              </div>
                <div class="col-lg-12 dynamic_employee">
                @foreach($employee as $e)
                  <div class="col-md-4 form-group">
                       {{$e->empname}} : <input type="checkbox" name="employee_ids[]" class="check_test" value="{{$s->id}}">                      
                    </div>
                @endforeach    
                </div>


                
                 <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                
               <input type="hidden" name="edit_id" value="0" id="edit_id">
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary add_date" style="margin-top: 20px;margin-bottom: 15px;">Submit</button>
              </div>
             
              <!-- /.box-body -->

             
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>


  
@stop

@section('script')

@if (Session::get('success')==1)
        <script>
          Msg.success('successfully updated',2000);
        </script>
@endif
<script>
  $(document).ready(function(){

    $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
         });


    $('#employee').change(function(){


        var id  = $(this).val();
         if(id=='')
         {
           id=0;
         }
        $('.dynamic_allot').load('{{URL::to("/store_allot/ajax_view/")}}/'+id);

        
    });



    $('#store').change(function(){

      var id  = $(this).val();
         if(id=='')
         {
           id=0;
         }
        $('.dynamic_employee').load('{{URL::to("/store_allot/employee_ajax_view/")}}/'+id);

    });
   
    


      function clear_form()
      {
         $('#start_date').val(''); 

         $('#end_date').val('');

         $('#edit_id').val(0);

         $('#doctor').val('');

         

        $('input[type="checkbox"]').iCheck('uncheck');
         //$('.dynamic_allot').find('').removeClass('.checked');
         $('.icheckbox_flat-blue').removeClass('.checked');
         table.ajax.reload();




      }

      //$('.dynamic_allot').load('{{URL::to("/hdoctor/ajax_time_slot_view")}}',send_data);

  
  });
</script>
@stop