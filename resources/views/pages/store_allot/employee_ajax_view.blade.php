<link href="{{asset('theme/css/icheck/all.css')}}" rel="stylesheet">

@foreach($employee as $e)
  
<div class="col-md-4 form-group employee_i_check">
                       {{$e->empname}} : <input type="checkbox" name="employee_ids[]" class="check_test" value="{{$e->id}}" {{$e->is_check==1?'checked':''}}>                      
                    </div>
             <input type="hidden" name="allot_id[]" value="{{$e->allot_id}}">       
@endforeach                    

<script src="{{asset('theme/js/jquery.min.js')}}"></script>

<script src="{{asset('theme/js/icheck/icheck.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){

    $('.employee_i_check input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
         });
	});
</script>