<link href="{{asset('theme/css/icheck/all.css')}}" rel="stylesheet">

@foreach($store as $s)
  
<div class="col-md-4 form-group store_i_check">
                       {{$s->name}} : <input type="checkbox" name="store_id[]" class="check_test" value="{{$s->id}}" {{$s->is_check==1?'checked':''}}>                      
                    </div>
             <input type="hidden" name="allot_id[]" value="{{$s->allot_id}}">       
@endforeach                    

<script src="{{asset('theme/js/jquery.min.js')}}"></script>

<script src="{{asset('theme/js/icheck/icheck.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){

    $('.store_i_check input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
         });
	});
</script>