 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 <table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Leave Date</th>
        <th>Reason</th>
        <th>Leave Type</th>
        <th>Day Type</th>
      </tr>
    </thead>
    <tbody>
    @foreach($leave as $l)
      <tr>
        <td>{{$l->empname}}</td>
        <td>{{$l->leave_date}}</td>
        <td>{{$l->reason}}</td>
        <td>{{$l->leave_type}}</td>
        <td>{{$l->day_type}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
   <div class="pagination pull-right">
   {{ $leave->appends(request()->query())->links() }}
     <!--<ul class="pagination">
    <li><a href="#">1</a></li>
    <li class="active"><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
  </ul>-->
  </div>