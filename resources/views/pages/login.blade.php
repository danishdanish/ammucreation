<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('theme/fonts/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('theme/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('theme/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <!--<link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">-->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <!--<div id="login_error" style="color:red;text-align: center;"></div>-->
    <div id="login_error" class="alert alert-danger" style="display:none">Invalid credentials</div>
    <form action="" method="post" id="login_form">

      <div class="form-group has-feedback">
        <div id="user_error" style="color:red"></div>
        <input type="text" name="username" class="form-control" placeholder="Username" id="username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <div id="password_error" style="color:red"></div>
        <input type="password" name="password" class="form-control" placeholder="Password" id="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!--<div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>-->
        <!-- /.col -->
        <div class="col-xs-12">
          <button  class="btn btn-primary btn-block btn-flat btn_submit">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    </form>

    
    <!-- /.social-auth-links -->

    <!--<a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a>-->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('theme/js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<!--<script src="../../plugins/iCheck/icheck.min.js"></script>-->
<script>
  $(document).ready(function(){
      //$("#login_error").delay(1000).fadeOut();
       $('.btn_submit').click(function(event){

           event.preventDefault();
          var username = $('#username').val();

          var password = $('#password').val();

          if(username=='')
          {
            $('#user_error').text('please enter username');
            return false;
          }
          if(password=='')
          {
            $('#password_error').text('please enter password');
            return false;
          }

         

           $.ajax({
              type:'POST',
              url:'{{URL::to("/check_login")}}',
              data:$('#login_form').serialize(),
              success:function(msg){
                if(msg==1){
                  window.location="{{URL::to('/')}}";
                }
                else{
                   $('#user_error').val('');
                   $('#password_error').val('');

                   $("#login_error").css("display","");
                   $('#login_error').delay(2000).fadeOut();
                }
              }
           });
       });

    
  });
</script>
</body>
</html>
