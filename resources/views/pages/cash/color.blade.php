@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Add Product Color
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/color')}}">Product Colors</a></li>
        <li class="active">Product Color</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>
 <!-- flash end-->
  <div class="col-md-8">  </div>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/color/add')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Color Name</label>
                  <input type="text" name="colorname"  class="form-control" id="exampleInputEmail1" placeholder="Color Name"  value="">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Color Code</label>
                  <input type="test" name="colorcode" class="form-control" id="exampleInputEmail1" placeholder="Color Code"  value="">
                </div>           
             
             
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->

    </section>
        <section class="content">
     <div class="container">
  <div class="row"> 
    

  </div>
</div>
</section>
@stop

@section('script')

	

@stop