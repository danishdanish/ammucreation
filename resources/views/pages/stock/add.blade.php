
@extends('layout/defualt')

@section('content')



<section class="content-header">
      <h1>
  Stock Add
      </h1>
      <ol class="breadcrumb">
             <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/leave')}}">Leave Management All</a></li>
        <li class="active">Leave Management</li>
      </ol>
    </section>

<div class="content">
<div class="panel panel-default">
    <div class="panel-body"><b>Stock Add  By : </b>
      <div class="form-group" style="margin-top: 20px;">
        Excel : <input type="radio" name="add_type" value="1" class="add_type" >
        Manual : <input type="radio" name="add_type" value="2" class="add_type">
    </div>
    </div>

  </div>
</div>


    <section class="content manual_section" style="margin-top:-100px; display:none;" >

    
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
        <form action="{{URL::to('/stock/insert')}}" id="target_form" method="POST">  
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
           
        
              <div class="box-body col-lg-6">

                <div class="form-group">
                  <label for="exampleInputEmail1">Item Code</label>
                  <select name="item_code" class="form-control" required="required">
                    <option value="">select item code</option>
                    @if(!empty($item_code))
                     @foreach($item_code as $item)
                    <option value="{{$item->itemcode}}">{{$item->product}}</option>
                    @endforeach
                    @endif
                  </select>
                 
                </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">Stock</label>
                  <input type="text" name="stock"  class="form-control" id="target_amount" placeholder="Enter Stock" value="" required="required">
                 
                </div>
                
          
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">

               <div class="box-footer">
                <button type="Submit"  class="btn btn-primary" >Submit</button>
              </div>
                 
                       </div>
              <!-- /.box-body -->
              <div id="demo"></div>

              
        
        
          </div>
          
          </form>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>


    

    <section class="content excel_section" style="margin-top:-100px; display: none">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
        <form action="{{URL::to('/stock/insert')}}" id="target_form" method="POST" enctype='multipart/form-data'>  
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
           
        
              <div class="box-body col-lg-6">

                
                <div class="form-group">
                  <label for="exampleInputEmail1">Stock</label>
                  <input type="file" name="excel_sheet" class="form-control excel_format" required="required">
                 <div class="file_error" style="color: red"></div>
                </div>
                
          
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">

               <div class="box-footer">
                <button type="Submit"  class="btn btn-primary" >Submit</button>
              </div>
                 
                       </div>
              <!-- /.box-body -->
              <div id="demo">   </div>

              
        
        
          </div>
          
          </form>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

@stop

@section('script')


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script>
 $(document).ready(function () {
   
   $('.add_type').click(function(){
      
       var id = $(this).val();

       if(id==1)
       {

          $('.excel_section').css('display','block');
          $('.manual_section').css('display','none');
       }

       if(id==2)
       {

         $('.excel_section').css('display','none');
          $('.manual_section').css('display','block');
       }

   });


   $('.excel_format').change(function(){

        var extension = $('.excel_format').val().split('.').pop().toLowerCase();

        if(extension=='xlsx')
        {
           $('.excel_format').val('');
           $('.file_error').text('file type xlsx not supported');
        }
        else
        {
         $('.file_error').text(''); 
        }
   });

    
       
});

</script>

@stop

