@extends('layout/defualt')

@section('content')

<style type="text/css">
   .error_display{
      color: red;
      
   }
   .error_display input{
     border-color: red;
   }
</style>

<section class="content-header">
      <h1>
       {{$employee_name}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Stores</a></li>
        <li class="active">Allot Store</li>
      </ol>
    </section>


  <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">

   
            
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body col-lg-6">
                
             <input type="hidden" id="lat" value="">
             <input type="hidden" name="" id="lon" value="">
             <input type="hidden" value="Location{{$employee}}" id="user_id">
              

               
            <div id="map" style="width:500px; height: 500px;">
              
            </div>
                
              </div>
                
          

          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>


  
@stop

@section('script')

<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>

<script>
   
     var config = {
    apiKey: "AIzaSyCCDTLGesWJvAiJMhX8NyDDsVlyTEYnPrM ",
    authDomain: "ammucreations-84f46.firebaseapp.com",
    databaseURL: "https://ammucreations-84f46.firebaseio.com/",
    //storageBucket: "<BUCKET>.appspot.com",
    messagingSenderId: "811407943508",
  };
  firebase.initializeApp(config);
  var user = $('#user_id').val();
  var ref = firebase.database().ref().child(user).limitToLast(1);
  

ref.on("value", function(snapshot) {
   var data = snapshot.val();
  
   if(data!=null)
   {
     $.each(data,function(key,value){
   
   
    var test_latitude = value.Latitude;

    var test_longitude = value.Latitude
     
    if(value.hasOwnProperty('Latitude'))
    {
      
      $('#lat').val( value.Latitude);
      $('#lon').val(value.Longitude);
     
      $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCIWshmR3uZnjDRp7g7pjxSVx5ICHrKq5o&callback=initMap");
 
    }
    else
    {

      var mnc = value.Mnc;
      var mcc = value.Mcc;
      var lac = value.Lac;
      var cellid = value.CellId;
      
      var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://api.mylnikov.org/geolocation/cell?v=1.1&data=open&mcc="+mcc+"&mnc="+mnc+"&lac="+lac+"&cellid="+cellid,
      "method": "GET",
  
  }

$.ajax(settings).done(function (response) {
 
 $('#lat').val(response.data.lat);
 $('#lon').val(response.data.lon);

 $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCIWshmR3uZnjDRp7g7pjxSVx5ICHrKq5o&callback=initMap");

});

      
    }
     
     });
}
else
{
  
  var text = '<b>Sorry,cannot locate this employee</b>'

  $('#map').html(text).css('font-size',30);
}
}, function (error) {
   console.log("Error: " + error.code);
});


 function initMap() {
       var la = parseFloat($('#lat').val());
   
       var lo = parseFloat($('#lon').val());

        var myLatLng = {lat: la, lng: lo};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 17,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }


</script>

@stop