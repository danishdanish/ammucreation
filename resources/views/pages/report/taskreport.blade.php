@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Payment Report</a></li>
      
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row" style="margin-top:30px;">

        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11" >

        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" style="height:110px;">
      <label for="exampleInputEmail1">Task Report</label> <br><br>
              <input type="hidden" name="placeorderid"  class="form-control" id="placeorderid" placeholder="" value="">
 <form action="{{URL::to('/task_report_process')}}" method="POST" role="form" enctype="multipart/form-data" >
   <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="exampleInputEmail1">Employee</label>

         <select name="emp" class="form-control" id="empsel">

 
      


             <option value="allemps" value="allemps" >All</option>    
                                 @foreach ($emps as $key => $emp)
         <option value="{{$emp->id}}"  @if ($idlog == $emp->id) selected="selected" @endif >{{$emp->empname}}</option>         
         @endforeach
       </select>  </div>
         <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
         <label for="exampleInputEmail1">Store</label>
        <select name="store" class="form-control" id="storesel">
              <option value="allstores" >All</option> 
                                 @foreach ($stores as $store)
         <option value="{{$store->id}}"   @if ($stidlog == $store->id) selected="selected" @endif >{{$store->name}}</option>         
         @endforeach
       </select>
     </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                  <label for="exampleInputEmail1">From</label>
  <input id="datepicker" class="form-control" name="from" value="{{$fromlog}}" />
           </div>
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
           
                  <label for="exampleInputEmail1">To</label>
 <input id="datepicker1" class="form-control" name="to"  value="{{$tolog}}" />

       </div>
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">  <br>
              
         
                 <input type="checkbox" name="po" value="po completed"> Place Order  <br>
                  <input type="checkbox" name="pay" value="payment completed"> Payment <br> 
                       <input type="checkbox" name="visit" value="visit completed"> Visit 
          </div>


           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">  <br>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button  class="btn btn-primary" id="#">Submit</button>
         
          </div>

            </form>

             </h3>
            </div>
         
            
                <div class="panel-body">
              <div class="row">
              
          
                <div class="col-md-12 col-lg-12 "> 
          <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Date</th>
                  <th>Employee</th>   <th>Store</th>             
                     <th>Status </th>                                                             
                </tr>
                </thead>
                <tbody>

              @foreach ($cash as $c)                
              
                     
               <tr>   <td> {{$c->taskdate}} || Taskid__{{$c->taskid}} </td>
                <td> {{$c->empname}}   </td>
                   <td> {{$c->storename}}   </td>
                <td> {{$c->taskstatus}}  </td>

             
               <!--    join  working  <td> {{$c->storename}}   </td> -->
              </tr>
           @endforeach
            
                
                </tbody>
                
              </table>
              
              <div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
     
          {{ $cash->appends(request()->input())->links() }}

    </div>
                </div>
              </div>
            </div>



          </div>
                   
                     
     
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

@stop

@section('script')






  <!--   <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
    <script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script>
    <script language="javascript">
   $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
    </script>

       <script language="javascript">
        $(document).ready(function () {
           $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
        });
    </script>

@stop