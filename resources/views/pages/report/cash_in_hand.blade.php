@extends('layout/defualt')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
    Cash in Hand Report
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Leave Management All</li>
        <!-- <li><button id="checkconnect">connection check</button></li>
        <li> <p id="demo"></p></li> -->
    
      </ol>
    </section>

 

  


    <!-- Main content -->
    <section class="content">
 <div class="row">

 <div class="col-md-12">
  
  <div class="panel panel-default">
    <div class="panel-body">
   
      <div class="form-group col-md-3">
         <label>Employee</label>
         <select class="form-control" id="employee" name="employee">
            <option value="">All</option>
            @foreach($employee as $e)
            <option value="{{$e->id}}">{{$e->empname}}</option>
            @endforeach
         </select>
      </div>

      <div class="form-group col-md-3">
         <label>Store</label>
         <select class="form-control" id="store" name="store">
            <option value="">All</option>
            @foreach($store as $s)
            <option value="{{$s->id}}">{{$s->name}}</option>
            @endforeach
         </select>
      </div>


      <div class="form-group col-md-3">
         <label>Date from</label>
         <input type="text" name="date_from" class="date form-control" id="date_from" required="required" placeholder="From">
      </div>

      <div class="form-group col-md-3">
         <label>Date to</label>
         <input type="text" name="date_to" class="date form-control"  id="date_to" required="required" placeholder="To">
      </div>
     
     <div class="form-group col-md-3">

         <button  class="btn btn-primary filter_btn" style="margin-top: 24px;">Submit</button>
      </div>
      
      
    </div>
    <div class="amount_div" style="margin-bottom: 20px; margin-left: 28px;"><b><h3>Total Amount :0 </h3> </b></div>
  </div>
</div>

          <!-- flash start-->
        <div class="col-md-12">

      <div class="alert alert-success" id="smessage" style="display:none; "> </div>
    
</div>
 <!-- flash end-->
</div>
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Si No</th>
                  <th>Employee Name</th>
                  <th>Store Name</th>
                  <th>Date</th>
                 <th>Amount</th>
                    
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                
              </table>
            </div>


            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- modal  -->

       
        <!-- modal end -->
    </section>

    @stop

 @section('script')

   

   @if (Session::get('update')==1)
        <script>
          Msg.success('successfully updated',1500);
        </script>
   @endif

 <script>
  $(function () {

     $('.date').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    });

     


    var table = $('#example1').DataTable({
        
    
        "columns": [
            {"data": "si_no"},
            {"data": "employee_name"},
            {"data": "store_name"},
            {"data": "date"},
            {"data": "amount"},
            
           
                ],
    });

  $(".filter_btn").on("click", function (event) {

    var employee = $('#employee').val();

    var store = $('#store').val();

    var date_from = $('#date_from').val();

    var date_to = $("#date_to").val();
    
    
    if(date_from=='')
    {

      $('#date_from').css('border-color','red');
       return false;
    }
    else
    {
     $('#date_from').removeAttr('style'); 
    }

    if(date_to=='')
    {
      $('#date_to').css('border-color','red');
       return false;
    }
    else
    {
      $('#date_to').removeAttr('style'); 
    }

       $.ajax({

        url: "{{URL::to('/cash_in_hand/report')}}",
        type: "GET",
        data: { 'employee': employee,'store':store,'date_from':date_from,'date_to':date_to },
        success:function(msg)
        {
          var amount = 0 ;
          table.clear().draw();
        
         var data = JSON.parse(msg);
         //console.log(data);
         table.rows.add(data).draw();

         $.each(data,function(index,value){
            amount  = parseFloat(amount)+parseFloat(value.amount);
         });
         
         $('.amount_div').html('<b><h3>Total Amount :'+amount+'</h3> </b>');
        }
//gffg
       }); 
   
});   
    



      

    $(document).on('click','.delete',function(){
      
     

       var id = $(this).closest('.action_div').find('#delete_id').val();

    
   $.createDialog({

    acceptAction: alertCall,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCall(){

    

       $.ajax({
               type:'GET',
               url:'{{URL::to("/leave/delete")}}/'+id,
               success:function(msg){
                 if(msg==1){
                   table.ajax.reload();
                    Msg.success('successfully deleted',1500);
                 }
               }
          });

          
    }


      
    });  

    


    
   
  })
</script>






<script>
 $(document).on('click','#checkconnect',function(){       
   $.createDialog({

    acceptAction: alertCallcheck,
    attachAfter: '.delete',
    title: 'Are you sure you want to delete?',
    accept: 'Yes',
    refuse: 'Cancel',
    acceptStyle: 'red',
    refuseStyle: 'gray',
    
  });

   $.showDialog();

   function alertCallcheck(){
       $.ajax({
               type:'GET',
               url: "{{URL::to('user/user_check1')}}",
               success:function(data){
            document.getElementById("demo").innerHTML = "Succesfully Deleted!";            
               }
          });
    }
      
    });   




</script>
    @stop