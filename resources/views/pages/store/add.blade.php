@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Add Store
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/user')}}">Stores</a></li>
        <li class="active">Add Store</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
                <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/store/add_store')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name"  class="form-control" id="exampleInputEmail1" placeholder="Enter name" value="{{ old('name') }}" >
                </div>
                
                    <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="description"  class="form-control" id="exampleInputEmail1" placeholder="Enter description" value="{{ old('name') }}" >
                </div>

                  <div class="form-group">
                  <label for="exampleInputEmail1">GST </label>
                  <input type="text" name="gst"  class="form-control" id="exampleInputEmail1" placeholder="Enter GST No." value="{{ old('gst') }}" >
                </div>

                 <div class="form-group">
                  <label for="exampleInputFile">Contact Name</label>
                  <input type="text" name="contactname" class="form-control" id="exampleInputEmail1" placeholder="Contact name" value="{{ old('contactname') }}">
                  
                </div>

                 <div class="form-group">
                  <label for="exampleInputFile">Contact Phone</label>
                  <input type="text" name="contactno" class="form-control" id="exampleInputEmail1" placeholder="Contact No" value="{{ old('contactno') }}">
                  
                </div>
                 <div class="form-group">
                  <label for="exampleInputFile">Address</label>
                 <input type="text" name="address" class="form-control" id="exampleInputEmail1" placeholder="Address" value="{{ old('address') }}">
                  
                </div>


                    <div class="form-group">
                  <label for="exampleInputFile">Location</label>
                 <input type="text" name="location" class="form-control" id="exampleInputEmail1" placeholder="Location" value="{{ old('location') }}">
                  
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">Zip Code</label>
               <input type="text" name="zipcode" class="form-control" id="exampleInputEmail1" placeholder="Zip Code" value="{{ old('zipcode') }}">
                  
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">City</label>
                 <input type="text" name="city" class="form-control" id="exampleInputEmail1" placeholder="City" value="{{ old('city') }}">
                  
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">State</label>
             <input type="text" name="state" class="form-control" id="exampleInputEmail1" placeholder="State" value="{{ old('state') }}">                
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">Country</label>
                  <input type="text" name="country" class="form-control" id="exampleInputEmail1" placeholder="Country" value="{{ old('country') }}">
                  
                </div>




                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

@stop