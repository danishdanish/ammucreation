@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
       Add User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/store')}}">Stores</a></li>
        <li class="active">Edit Store</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- flash start-->
        <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error" style="">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>
 <!-- flash end-->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/store/store_edit')}}" method="POST" role="form" enctype="multipart/form-data" >
      <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name"  class="form-control" id="exampleInputEmail1" placeholder="Enter name" value="{{$data->name}}" >
                </div>
                
                      <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="description"  class="form-control" id="exampleInputEmail1" placeholder="Enter description" value="{{$data->description}}" >
                </div>
                       <div class="form-group">
                  <label for="exampleInputEmail1">GST No.</label>
                  <input type="text" name="gst"  class="form-control" id="exampleInputEmail1" placeholder="Enter GST" value="{{$data->gst}}" >
                </div>

                 <div class="form-group">
                  <label for="exampleInputFile">Contact Name</label>
                  <input type="text" name="contactname" class="form-control" id="exampleInputEmail1" placeholder="Contact name" value="{{$data->contactname}}">
                  
                </div>

                 <div class="form-group">
                  <label for="exampleInputFile">Contact Phone</label>
                  <input type="text" name="contactno" class="form-control" id="exampleInputEmail1" placeholder="Contact No" value="{{$data->contactno}}">
                  
                </div>
                 <div class="form-group">
                  <label for="exampleInputFile">Address</label>
                 <input type="text" name="address" class="form-control" id="exampleInputEmail1" placeholder="Address" value="{{$data->address}}">                  
                </div>

                   <div class="form-group">
                  <label for="exampleInputFile">Location</label>
                 <input type="text" name="location" class="form-control" id="exampleInputEmail1" placeholder="Location" value="{{$data->location}}">
                  
                </div>


                <div class="form-group">
                  <label for="exampleInputFile">Zip Code</label>
               <input type="text" name="zipcode" class="form-control" id="exampleInputEmail1" placeholder="Zip" value="{{$data->zipcode}}">
                  
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">City</label>
                 <input type="text" name="city" class="form-control" id="exampleInputEmail1" placeholder="City" value="{{$data->city}}">
                  
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">State</label>
             <input type="text" name="state" class="form-control" id="exampleInputEmail1" placeholder="State" value="{{$data->state}}">                
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">Country</label>
                  <input type="text" name="country" class="form-control" id="exampleInputEmail1" placeholder="Country" value="{{$data->country}}">
                  
                </div>

                 <div class="form-group">
                  <label for="exampleInputFile">Total</label>
                  <input type="text" name="total" class="form-control" id="exampleInputEmail1" placeholder="Total Amount" value="{{$data->total}}">
                  
                </div>

                 <div class="form-group">
                  <label for="exampleInputFile">Paid </label>
                  <input type="text" name="paid" class="form-control" id="exampleInputEmail1" placeholder="Paid Amount" value="{{$data->paid}}">
                                 
                </div>
                 <div class="form-group">
                  <label for="exampleInputFile">Amount Due :Rs. {{$data->balance}}</label>
                 
                  
                </div>


                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" value="{{ $data->id }}">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>

@stop

@section('script')
<script>
	$(document).ready(function(){

      $('#confirm').focusout(function(){
      	var confirm = $(this).val();
      	var password = $('#password').val();
          if(confirm!=password){
          	Msg.danger('password and confirm password not match',1500);

          	$(this).val('');
          }
      	  
      });

      $('#phone_no').focusout(function(){

      	  var no = $(this).val();
           intRegex =/[0-9 -()+]+$/;
      	   if(!intRegex.test(no)) {
      	   	Msg.danger('please enter valid phone number',1500);
      	   	$(this).val('');
      	   }
      });

	});
</script>


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

@stop