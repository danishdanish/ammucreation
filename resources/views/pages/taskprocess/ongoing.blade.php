@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>

      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Task view</a></li>
        <li class="active">Task view</li>
      </ol>
    </section>


    <section class="content">
      <div class="container">
                <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
      <div class="row" style="margin-top:30px;">

        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" >

        <div class="panel panel-info">
        
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">  </div>
          
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>

                        <tr>
                        <td>Employee :</td>
                        <td> {{$data->employeename}}</td>
                      </tr>
                      <tr>

                      <tr>
                        <td>Stores:</td>
                        <td> {{$data->storename}}</td>
                      </tr>
                   
                       <tr>
                        <td>Store Address:</td>
                        <td> {{$data->storeaddress}}</td>
                      </tr>

                        <tr>
                        <td>Store Contact Person:</td>
                        <td> {{$data->contactname}}</td>
                      </tr>

                        <tr>
                        <td>Store Contact Mobile:</td>
                        <td> {{$data->contactno}}</td>
                      </tr>

                      <tr>
                        <td>Task Date:</td>
                        <td> {{$data->taskdate}}</td>
                      </tr>
                          <tr>
                        <td>Task Time:</td>
                        <td> {{$data->tasktime}}</td>
                      </tr>

                        <tr>
                        <td>Task Status:</td>
                        <td> {{$data->taskstatus}}</td>
                      </tr>

                        <tr>
                        <td>Task Remark:</td>
                        <td> {{$data->taskremark}}</td>
                      </tr>
                      

                          
                     
             
                     
                    </tbody>
                  </table>
                  
             
                </div>
              </div>
            </div>
            
            
          </div>
                   
                     
     
        </div>
      </div>
    </div>
         
      
    </section>



    <!-- Main content -->
   

@stop

@section('script')


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 $(document).on('click','#orderprocess',function(){
   var id = document.getElementById("placeorderid").value;  
 
  var courrier = ($('#courrier').val());
  var billno = ($('#billno').val());
         var invoiceno = ($('#invoiceno').val());
      $.ajax({
               type:'GET',
              data: {'id':id,'courrier': courrier,'billno':billno,'invoiceno':invoiceno}, 
                dataType: 'json',               
               url: "{{URL::to('post_order_process')}}",
                             success:function(data){
    
         if($.isEmptyObject(data.error)){
  //  console.log(data);
         $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');


        $(".print-error-msg").find("ul").append('<li>Updated Succesfully</li>');
   //   window.location.reload();
   //  alert(data.success);
         }
             else{
                    printErrorMsg(data.error);
                  } 



        },
         error: function (data) {
        }
            
          });

 
   
    });

          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   



 





</script>

@stop