@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
  Work Allotment
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/role')}}">Roles</a></li>
        <li class="active">Work Allotment</li>
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
        <?php $i=1 ?>
         @foreach ($emps as $emp)

     <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" class="panel-title expand">
           <div class="right-arrow pull-right">+</div>
          <a href="#">  {{$emp->empname}}</a>
        </h4>
      </div>
      <div id="collapse{{$i}}" class="panel-collapse collapse">
        <div class="panel-body"><section class="content">
      <div class="row">
        <div class="col-md-12">
         @if($errors->any())   
  <div class="alert alert-error" style="margin-top: 25px;">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

 @if(Session::has('flash_message'))
      <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
      @endif  
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{URL::to('/role/add_role')}}" method="POST" role="form" enctype="multipart/form-data" >
              <div class="box-body ">
                <div class="form-group">
                  <label for="exampleInputEmail1">Sales</label>
                  <input type="text" name="name"  class="form-control" id="" placeholder="Enter name" value="{{ old('name') }}">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Order</label>
                  <input type="text" name="description" class="form-control" id="" placeholder="Enter descrition" value="{{ old('description') }}">
                </div>

                   <div class="form-group">
                  <label for="exampleInputEmail1">From</label>
                <input id="datepicker1" class="form-control" name="tfrom" placeholder="Enter Date of Joining" />
            </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">To</label>
                <input id="datepicker2" class="form-control" name="tto" placeholder="Enter Date of Joining" />
            </div>

                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         
        </div>
       
      </div>
      <!-- /.row -->
    </section>
  </div>
      </div>
    </div>
<!--  {{ ++$i}}  -->
<?php  ++$i;  ?>  
@endforeach
</div>
  </div></section>



    <!-- Main content -->
   

@stop

@section('script')


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script type="text/javascript">
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });

        $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>

@stop