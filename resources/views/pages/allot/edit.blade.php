@extends('layout/defualt')

@section('content')

<section class="content-header">
      <h1>
  Target Edit
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{URL::to('/allotment')}}">Target</a></li>
        <li class="active">Target Edit</li>
      </ol>
    </section>


    <section class="content">
         
      <div class="row">
      
     
</div>
      <div class="row">
        <div class="col-md-12">
  <div class="alert alert-danger print-error-msg" style="display:none">

        <ul></ul>

    </div> 
</div>
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
         <form action="{{URL::to('/allotment/update_allot')}}" method="POST"> 
          <div class="box box-primary col-lg-6">
            
            <!-- /.box-header -->
            <!-- form start -->
        
              <div class="box-body ">


                  <div class="form-group">
                  <label for="exampleInputEmail1">Employee</label>
                  <select name="employee" class="form-control" id="emp_sel" required="required">
                   @foreach ($emps as $emp)
         <option value="{{$emp->id}}" {{$emp->id==$edit_data->user_id?'selected=selected':''}} >{{$emp->empname}}</option>         
         @endforeach
       </select>

                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Sales in INR</label>
                  <input type="text" name="sales"  class="form-control" id="" placeholder="Enter Sales" value="{{ $edit_data->sales }}" required="required">
                </div>
                

                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 
                 <input type="hidden" name="edit_id" value="{{$edit_data->id}}">
                       </div>
              <!-- /.box-body -->
              <div id="demo">   </div>

              <div class="box-footer">
                <button type="submit"  class="btn btn-primary" id="allot">Submit</button>
              </div>
        
        
          </div>
          </form>
        </div>
       
      </div>
      <!-- /.row -->
    </section>



    <!-- Main content -->
   

@stop

@section('script')


<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> 
<script>
 $(document).ready(function () {
    $('#datepicker1').datepicker({
      uiLibrary: 'bootstrap'
    });

        $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap'
    });
});



 
          function printErrorMsg (msg) {

      $(".print-error-msg").find("ul").html('');

      $(".print-error-msg").css('display','block');

      $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

      });

    }   


</script>

@stop