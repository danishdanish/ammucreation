<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::post('auth/register', 'UserController@register');
    Route::post('auth/login', 'UserController@login');
Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('brands', 'UserController@getAuthUser');

     
           Route::get('products', 'UserController@getProducts');
      Route::get('productsbrand/{id}', 'UserController@getProductsBrand');
      Route::get('productscatagory/{id}', 'UserController@getProductsCat');
        Route::post('productssorted', 'UserController@getProductsSorted');
          Route::post('productscart', 'UserController@ProductsCart');

    	 Route::post('filter', 'UserController@getProductsFilter');
          Route::get('colors', 'UserController@getColors');
           Route::get('colorsall', 'UserController@getColorsAll');
           Route::get('salesmanstore/{id}','UserController@getStoreforSales');
               Route::get('pendingworks/{id}','UserController@getPendingWorks');
                 Route::get('workschedule/{id}', 'UserController@getSchedule');
                  Route::post('orderbook', 'UserController@placeOrder');
                     Route::post('payment', 'UserController@payMent');
                          Route::post('addstore', 'UserController@addStore');
                             Route::post('reschedule', 'UserController@reSchedule');
                              Route::get('stores','UserController@getStores');
                                      Route::post('visit','UserController@postVisit');
                                        Route::get('listwork/{id}', 'UserController@getWorklist');
                                        Route::get('listpaystore/{id}', 'UserController@getPaystorelist');
                                        Route::post('paycheque', 'UserController@payCheque');
                                          Route::get('paymentHand/{id}', 'UserController@payHand');
                                            Route::post('transferpay', 'UserController@transferPay');
                                             Route::get('getstorehistory/{id}', 'UserController@getStoreHistory');


             //leave
             
             Route::post('/leave_apply','LeaveapiController@leave_apply');
             Route::get('/leave_details/{id}','LeaveapiController@check_leave');
             Route::post('/leave_cancel','LeaveapiController@leave_cancel');
             Route::get('/get_graph/{id}','LeaveapiController@graph_details');
             Route::get('/payment_details/{id}','LeaveapiController@get_payment_details');

             Route::post('/password_edit','UserController@password_edit');                           
 });

