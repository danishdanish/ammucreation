<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/dashboard', function () {
    return view('pages/dashboard');
});

Route::get('/user','HomeController@user_list');
Route::get('/user/ajax_data','HomeController@ajax_data');
Route::get('/user_add_page','HomeController@user_add');
Route::post('/user/add_user','HomeController@insert_user_data');
Route::get('/user/user_view/{id}', 'HomeController@view_user_data');
Route::get('/user/user_edit/{id}', 'HomeController@edit_user_data');
Route::post('/user/user_edit', 'HomeController@post_edit_user_data');
Route::get('/user/user_delete', 'HomeController@delete_user_data');
Route::get('/user/user_permissions/{id}', 'HomeController@user_permissions');
Route::get('/user/user_profile/{id}', 'HomeController@user_profile');
Route::post('/user/profile_edit', 'HomeController@post_user_profile');
Route::post('/user/user_permissions_add', 'HomeController@user_permissions_add');

Route::get('/role','RoleController@role_list');
Route::get('/role/ajax_data','RoleController@ajax_data');
Route::get('/role_add_page','RoleController@role_add');
Route::post('/role/add_role','RoleController@insert_role_data');
Route::get('/role/role_edit/{id}', 'RoleController@edit_role_data');
Route::post('/role/role_edit', 'RoleController@post_edit_role_data');
Route::get('/role/role_delete', 'RoleController@delete_role_data');
Route::get('/role/role_permissions/{id}', 'RoleController@role_permissions');
Route::post('/role/role_permissions_add', 'RoleController@add_permissions');


Route::get('/module','ModuleController@module_list');
Route::get('/module/ajax_data','ModuleController@ajax_data');
Route::get('/module_add_page','ModuleController@module_add');
Route::post('/module/add_module','ModuleController@insert_module_data');
Route::get('/module/module_edit/{id}', 'ModuleController@edit_module_data');
Route::post('/module/module_edit', 'ModuleController@post_edit_module_data');
Route::get('/module/module_delete', 'ModuleController@delete_module_data');

Route::get('/allotment','AllotController@allot_list');
Route::get('/allotment/ajax_data','AllotController@ajax_data');
Route::get('/allotment_add_page','AllotController@allot_add');
Route::post('/allotment_add_allot','AllotController@add_allot');
Route::get('/allotment/delete', 'AllotController@delete');
Route::get('/allotment/allot_edit/{id}', 'AllotController@allot_edit');
Route::post('/allotment/update_allot', 'AllotController@update_allot');


Route::get('/brands','BrandController@brand_list');
Route::get('/brands/ajax_data','BrandController@ajax_data');
Route::get('/brands_add_page','BrandController@brand_add');
Route::post('/brand/add_brand','BrandController@insert_brand_data');
Route::get('/brand/brand_edit/{id}', 'BrandController@edit_brand_data');
Route::post('/brand/brand_edit', 'BrandController@post_edit_brand_data');
Route::get('/brand/delete', 'BrandController@delete_brand_data');



Route::get('/catgs','CatgsController@catgs_list');
Route::get('/catgs/ajax_data','CatgsController@ajax_data');
Route::get('/catgs_add_page','CatgsController@catgs_add');
Route::post('/catgs/add_catgs','CatgsController@insert_catgs_data');
Route::get('/catagory/catgs_edit/{id}', 'CatgsController@edit_catgs_data');
Route::post('/catagory/catgs_edit', 'CatgsController@post_edit_catgs_data');
Route::get('/catagory/delete', 'CatgsController@delete_catgs_data');


Route::get('/product','ProductController@product_list');
Route::get('/product/ajax_data','ProductController@ajax_data');
Route::get('/product_add_page','ProductController@product_add');
Route::post('/product/add_product','ProductController@insert_product_data');
Route::get('/product/product_edit/{id}','ProductController@edit_product_data');
Route::get('/product/product_color/{id}','ProductController@edit_product_color');
Route::get('/product/product_size/{id}','ProductController@edit_product_size');
Route::post('/product/product_color','ProductController@post_edit_product_color');
Route::post('/product/product_size','ProductController@post_edit_product_size');
Route::post('/product/product_edit','ProductController@post_edit_product_data');
Route::get('/product/product_delete','ProductController@delete_product_data');
Route::get('/product/size_delete','ProductController@size_delete');
Route::get('/product/color_delete','ProductController@color_delete');


Route::get('/store','StoreController@store_list');
Route::get('/store/ajax_data','StoreController@ajax_data');
Route::get('/store_add_page','StoreController@store_add');
Route::post('/store/add_store','StoreController@insert_store_data');
Route::get('/store/store_edit/{id}','StoreController@edit_store_data');
Route::post('/store/store_edit','StoreController@post_edit_store_data');
Route::get('/store/delete','StoreController@delete_store_data');




Route::get('/size','SizeController@size_list');
Route::get('/size/ajax_data','SizeController@ajax_data');
Route::get('/size_add_page','SizeController@size_add');
Route::post('/size/add','SizeController@insert_size_data');
Route::get('/size/delete','SizeController@delete_size_data');


Route::get('/color','ColorController@color_list');
Route::get('/color/ajax_data','ColorController@ajax_data');
Route::get('/color_add_page','ColorController@color_add');
Route::post('/color/add','ColorController@insert_color_data');
Route::get('/color/delete','ColorController@delete_color_data');

Route::get('/task','TaskController@task_list');
Route::get('/task/ajax_data','TaskController@ajax_data');
Route::get('/task_add_page','TaskController@task_add');
Route::get('/task_add_mapped_store','TaskController@task_add_mapped');
Route::get('/task_add_store','TaskController@task_add_store');
Route::get('/task_add','TaskController@post_task_add');
Route::get('/task/task_edit/{id}','TaskController@edit_task_data');
Route::get('/task_edit','TaskController@post_edit_task_data');
Route::get('/task/delete','TaskController@delete_task_data');

Route::post('/task/task_add_emp','TaskController@task_add_emp');

Route::get('/alloted_tasks','AllotTaskController@alloted_task_data');
Route::get('/taskprocess/ajax_data_emps','AllotTaskController@ajax_data_emps');
Route::get('/task/ongoing/{id}','AllotTaskController@onGoingTask');
Route::get('/task/delete','AllotTaskController@delete_task_data');

Route::get('/pending_tasks','AllotTaskController@pending_task_data');
Route::get('/taskprocess/ajax_data_emps_pending','AllotTaskController@ajax_data_emps_pending');

Route::get('/visited_tasks','AllotTaskController@visited_task_data');
Route::get('/taskprocess/ajax_data_emps_visited','AllotTaskController@ajax_data_emps_visited');

Route::get('/allot/process_rep/{id}','AllotTaskController@alloted_task_process');
Route::get('/task_process','AllotTaskController@post_task_process');

Route::get('/order_process','ProcessController@alloted_task_data');
Route::get('/orderprocess/ajax_data_emps','ProcessController@ajax_data_emps');
Route::get('/process/process_rep/{id}','ProcessController@process');
Route::get('/process/process_view/{id}','ProcessController@processView');
Route::get('/post_order_process','ProcessController@post_order_process');
Route::get('/post_order_process_qty','ProcessController@post_order_process_qty');
Route::get('/post_order_process_qty_keyup','ProcessController@post_order_process_qty_keyup');
Route::get('/order_delete','ProcessController@delete_order');


Route::get('/order_despatch','DespatchController@despatch_data');
Route::get('/despatch/ajax_data_emps','DespatchController@ajax_data_emps');
Route::get('/despatch/despatch_view/{id}','DespatchController@despatchView');
Route::get('/order_despatch_pending','DespatchController@despatch_data_pending');
Route::get('/despatch/ajax_data_emps_pend','DespatchController@ajax_data_emps_pending');
//sales
Route::get('/sales','SalesController@sales_data');
Route::get('/sales/ajax_data_emps','SalesController@ajax_data_emps');
Route::get('/sales/sales_view/{id}','SalesController@saleView');


// check connection
Route::get('/user/user_check1', 'HomeController@checkmodal');

Route::get('/','Auth\LoginController@showLoginForm');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//store allot

Route::get('/store_allot','AllotstoreController@allot_store_list');
Route::get('/store_allot/ajax_view/{id}','AllotstoreController@ajax_view');
Route::post('/store_allot/insert_allot_store','AllotstoreController@insert_allot_store');


Route::get('/store_allot/employee_ajax_view/{id}','AllotstoreController@employee_ajax_view');

Route::post('/store_allot/insert_allot_employee','AllotstoreController@insert_allot_employee');


//cash in hand admin -store
Route::get('/cash_in_hand','CashController@cash_list');
Route::get('/cash/ajax_data','CashController@ajax_data');
Route::get('/cash/view/{id}','CashController@cashView');


//cash in hand admin -employee
Route::get('/cash_in_hand_empwise','CashController@cash_list_empwise');
Route::get('/cash/ajax_data_empwise','CashController@ajax_data_empwise');
Route::get('/cash/view_empwise/{id}','CashController@cashView_Empwise');


Route::get('/cash_in_hand_transfer','CashController@cash_list_transfer');
Route::get('/cash/ajax_data_transfer','CashController@ajax_data_transfer');

//cash in hand employee
Route::get('/cash_in_hand_emp','CashController@cash_list_ind');
 Route::get('/cash_in_hand_ind/{id}','CashController@ajax_data_ind');
 Route::get('/cash/transfer/{id}','CashController@cash_list_ind_tranfer');
// Route::get('/cash/ajax_data','CashController@ajax_data');

 //report
 Route::get('/payment_report','ReportController@pay_rep');
  Route::post('/payment_report_process','ReportController@pay_rep_process');
   Route::get('/payment_report_process','ReportController@pay_rep_process');

 Route::get('/task_report','ReportController@task_rep');
   Route::post('/task_report_process','ReportController@task_rep_process');
   Route::get('/task_report_process','ReportController@task_rep_process');

   Route::get('/order_dispatch','ReportController@order_dispatch_report_page');
   Route::get('/order_dispatch/report','ReportController@order_dispatch_report');

   Route::get('/cash_in_hand','ReportController@cash_in_hand');
   Route::get('/cash_in_hand/report','ReportController@cash_in_hand_report');

   //store accounts
Route::get('/store/store_report/{id}','StoreController@store_data_report');


//leave
Route::get('/leave','LeavemanagementController@list_leave');
Route::get('/leave/add','LeavemanagementController@add_leave');
Route::post('/leave/insert','LeavemanagementController@insert_leave');
Route::get('/leave/ajax_data','LeavemanagementController@ajax_data');
Route::get('/leave/leave_edit/{id}','LeavemanagementController@leave_edit');
Route::post('/leave/update','LeavemanagementController@update_leave');
Route::get('/leave/delete/{id}','LeavemanagementController@leave_delete');
Route::get('/leave/leave_details/{id}','LeavemanagementController@leave_details');
Route::get('/leave/leave_report','LeavemanagementController@leave_report_page');
Route::get('/leave/filter_leave','LeavemanagementController@filter_leave');
Route::get('/leave/filter_leave','LeavemanagementController@filter_leave');




//employeee  individual

Route::get('/alloted_tasks_individual/','AllotTaskController@alloted_task_data_emp');
Route::get('/taskprocess/ajax_data_emps_ind/{id}','AllotTaskController@ajax_data_emps_ind');

Route::get('/sales_individual','SalesController@sales_data_ind');
Route::get('/sales/ajax_data_emps_ind/{id}','SalesController@ajax_data_emps_ind');

//location tracking
Route::get('/location','LocationtrackingController@employee_list');
Route::get('/location/ajax_data','LocationtrackingController@ajax_data');
Route::get('/location/details/{id}','LocationtrackingController@get_location');


//stock
Route::get('/stock','StockController@stock_list');
Route::get('/stock/ajax_data','StockController@ajax_data');
Route::get('/stock/add','StockController@stock_add_page');
Route::post('/stock/insert','StockController@stock_insert');



