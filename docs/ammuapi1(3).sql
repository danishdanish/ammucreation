-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2018 at 12:51 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ammuapi1`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand`, `bimage`, `active`, `created_at`, `updated_at`) VALUES
(2, 'Allen Solly Shirts', '1512463322thaali3.jpg', 0, '2017-12-05 01:59:13', '2018-01-29 03:30:08'),
(3, 'Bata1', '1512462093Solidarity-Group-Loans-Large.jpg', 1, '2017-12-05 02:01:47', '2018-01-29 02:05:32'),
(4, 'Zero Shirts', '1512462083goldloan1.jpg', 1, '2017-12-05 02:02:44', '2018-01-29 02:05:53'),
(5, 'Peter England', '1517216279interest.jpg', 0, '2018-01-06 07:37:22', '2018-01-29 03:27:59'),
(6, 'Dixy', '1517210761nabard.jpeg', 1, '2018-01-29 01:56:01', '2018-01-29 02:05:22'),
(7, 'Ttan', '1517211717industrial-loans.jpg', 1, '2018-01-29 02:11:57', '2018-01-29 02:11:57');

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_hands`
--

CREATE TABLE `cash_in_hands` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` int(20) DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `mode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transferdate` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cash_in_hands`
--

INSERT INTO `cash_in_hands` (`id`, `date`, `employee`, `store`, `amount`, `cheque`, `transfer`, `mode`, `transferdate`, `created_at`, `updated_at`) VALUES
(1, '2018-01-17', '12', 5, '100', 'sbin55555', 'no', 'cheque', '2018-01-17', '2018-01-17 06:00:30', '2018-01-17 06:09:33'),
(2, '2018-01-17', '12', 5, '100', 'sbin55555', 'no', 'cheque', '2018-01-17', '2018-01-17 06:00:31', '2018-01-17 06:09:08'),
(3, '2018-01-17', '12', 5, '100', 'sbin55555', 'no', 'cheque', '2018-01-17', '2018-01-17 06:00:32', '2018-01-17 06:09:08'),
(4, '2018-01-17', '12', 5, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:00:35', '2018-01-17 06:06:56'),
(5, '2018-01-17', '12', 5, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:00:35', '2018-01-17 06:09:41'),
(6, '2018-01-17', '12', 1, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:00:37', '2018-01-17 06:06:56'),
(7, '2018-01-17', '12', 1, '200', 'no', 'no', 'cash', '2018-01-17', '2018-01-17 06:02:19', '2018-01-17 06:06:56'),
(8, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:54:58', '2018-01-18 02:54:58'),
(9, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:01', '2018-01-18 02:55:01'),
(10, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:03', '2018-01-18 02:55:03'),
(11, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:07', '2018-01-18 02:55:07'),
(12, '2018-01-18', '26', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-18 02:55:08', '2018-01-18 02:55:08'),
(13, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:27', '2018-01-23 00:03:27'),
(14, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:29', '2018-01-23 00:03:29'),
(15, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:30', '2018-01-23 00:03:30'),
(16, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:32', '2018-01-23 00:03:32'),
(17, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:33', '2018-01-23 00:03:33'),
(18, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:34', '2018-01-23 00:03:34'),
(19, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:35', '2018-01-23 00:03:35'),
(20, '2018-01-23', '7', 1, '200', 'no', 'no', 'cash', NULL, '2018-01-23 00:03:36', '2018-01-23 00:03:36'),
(21, '2018-01-23', '7', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 00:06:02', '2018-01-23 00:06:02'),
(22, '2018-01-23', '7', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 00:06:03', '2018-01-23 00:06:03'),
(23, '2018-01-23', '7', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 00:06:05', '2018-01-23 00:06:05'),
(24, '2018-01-23', '8', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:01:47', '2018-01-23 01:01:47'),
(25, '2018-01-23', '8', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:01:48', '2018-01-23 01:01:48'),
(26, '2018-01-23', '8', 1, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:01:49', '2018-01-23 01:01:49'),
(27, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:09', '2018-01-23 01:02:09'),
(28, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:10', '2018-01-23 01:02:10'),
(29, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:11', '2018-01-23 01:02:11'),
(30, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:12', '2018-01-23 01:02:12'),
(31, '2018-01-23', '11', 3, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-23 01:02:13', '2018-01-23 01:02:13'),
(32, '2018-01-24', '11', 9, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-24 05:48:53', '2018-01-24 05:48:53'),
(33, '2018-01-24', '11', 9, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-24 05:48:58', '2018-01-24 05:48:58'),
(34, '2018-01-24', '11', 9, '100', 'sbin55555', 'no', 'cheque', NULL, '2018-01-24 05:48:59', '2018-01-24 05:48:59'),
(35, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:28', '2018-01-24 05:49:28'),
(36, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:29', '2018-01-24 05:49:29'),
(37, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:30', '2018-01-24 05:49:30'),
(38, '2018-01-24', '7', 9, '200', 'no', 'no', 'cash', NULL, '2018-01-24 05:49:31', '2018-01-24 05:49:31');

-- --------------------------------------------------------

--
-- Table structure for table `catagories`
--

CREATE TABLE `catagories` (
  `id` int(10) UNSIGNED NOT NULL,
  `catagory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catagories`
--

INSERT INTO `catagories` (`id`, `catagory`, `cimage`, `active`, `created_at`, `updated_at`) VALUES
(1, 'T Shirts', '1512469509ems.png', 1, NULL, '2017-12-05 04:55:09'),
(2, 'Shirts Cotton', '1512472910cash-credit-250x250.jpg', 1, '2017-12-05 04:34:08', '2017-12-05 05:51:50'),
(3, 'cat1', '1517215301mangalyam.jpg', 0, '2018-01-06 07:38:21', '2018-01-29 03:11:41'),
(4, 'Leggins', '1517211681interest.jpg', 1, '2018-01-29 02:11:21', '2018-01-29 02:11:21'),
(5, 'cat2', '1517215357cash-credit-250x250.jpg', 1, '2018-01-29 03:12:37', '2018-01-29 03:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `def` int(1) NOT NULL DEFAULT '0',
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `colorname`, `def`, `colorcode`, `created_at`, `updated_at`) VALUES
(1, 'Yellow', 0, '#e0d077', '2017-12-14 02:52:11', '2017-12-14 02:52:11'),
(4, 'Pale yellow', 0, '#f5f4eb', '2017-12-14 03:17:33', '2017-12-14 03:17:33'),
(5, 'Red', 1, '#ca3b5c', '2017-12-29 04:58:22', '2017-12-29 04:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `leave_management`
--

CREATE TABLE `leave_management` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_casual_leave` int(11) NOT NULL DEFAULT '0',
  `total_sick_leave` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_management`
--

INSERT INTO `leave_management` (`id`, `user_id`, `total_casual_leave`, `total_sick_leave`, `date`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 7, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL),
(2, 8, 12, 8, '2018-01-09', 0, '2018-01-09 09:45:53', '2018-01-09 10:34:14'),
(3, 10, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', '2018-01-09 10:21:34'),
(4, 11, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL),
(5, 12, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL),
(6, 14, 12, 8, '2018-01-09', 1, '2018-01-09 09:45:53', NULL),
(7, 24, 0, 0, NULL, 1, NULL, NULL),
(8, 25, 0, 0, '2018-01-13', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_taken`
--

CREATE TABLE `leave_taken` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `leave_date` date DEFAULT NULL,
  `reason` text,
  `leave_type` int(11) DEFAULT NULL COMMENT '1-casual,2-sick,3-loss_of_pay',
  `day_type` int(11) NOT NULL COMMENT '1-fullday,2-halfday',
  `slot` varchar(30) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_taken`
--

INSERT INTO `leave_taken` (`id`, `user_id`, `leave_date`, `reason`, `leave_type`, `day_type`, `slot`, `is_delete`, `created_at`, `updated_at`) VALUES
(2, 7, '2018-01-02', 'test', 1, 1, NULL, 1, '2018-01-10 06:02:21', NULL),
(3, 7, '2018-01-02', 'test', 1, 1, NULL, 1, '2018-01-10 06:02:21', NULL),
(4, 7, '2018-01-02', 'test', 1, 2, 'afternoon', 1, '2018-01-10 06:02:21', NULL),
(5, 11, '2018-01-04', 'test', 1, 1, '', 1, '2018-01-10 11:39:35', NULL),
(6, 11, '2018-01-04', 'test', 1, 2, 'afternoon', 1, '2018-01-10 11:39:35', NULL),
(7, 11, '2018-01-04', 'test', 1, 2, 'afternoon', 1, '2018-01-10 11:39:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2017_12_13_081600_create_stores_table', 1),
(19, '2017_12_13_102900_create_sizes_table', 2),
(20, '2017_12_14_081028_create_colors_table', 3),
(21, '2017_12_15_070132_create_tasks_table', 4),
(22, '2017_12_20_110508_add_fields_to_tasks', 5),
(23, '2017_12_21_091811_create_order_table', 6),
(24, '2018_01_11_061943_create_sales_table', 7),
(25, '2018_01_16_092254_create_cash_in_hand_table', 8),
(26, '2018_01_16_094353_create_cash_in_hands_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `displayname`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Designation', 'Designation', 'Designation for  Employee', '2017-11-29 04:30:27', '2017-11-29 04:43:47'),
(2, 'Work Allot', 'Allotment', 'Work Allotment', '2017-11-30 01:02:18', '2017-11-30 01:02:18'),
(3, 'Products', 'Product s', 'Products', '2017-12-01 01:06:53', '2017-12-01 01:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `taskid`, `store`, `employee`, `quantity`, `billno`, `status`, `created_at`, `updated_at`) VALUES
(1, '12/21/2017', '11', 'Store3', 'employee2', '30', 'BILLNO55566', 'oncourier', '2017-12-21 04:35:33', '2017-12-22 05:35:44'),
(2, '12/22/2017', '10', 'Store3', 'employee2', '60', 'BILLNO9909', 'oncourier', '2017-12-22 04:42:27', '2017-12-22 05:46:04'),
(3, '12/22/2017', '5', 'Store3', 'employee1', '50', 'BILL8900', 'oncourier', '2017-12-22 05:06:24', '2017-12-22 05:42:21'),
(4, '12/22/2017', '3', NULL, 'employee1', '17', 'BILL66677', 'oncourier', '2017-12-22 05:08:06', '2017-12-22 05:08:06'),
(5, '12/22/2017', '1', NULL, 'employee1', '56', 'BILL8889', 'oncourier', '2017-12-22 05:09:04', '2017-12-22 05:09:04'),
(6, '12/22/2017', '6', 'Store3', 'employee1', '50', 'BILLNO5555', 'oncourier', '2017-12-22 05:42:50', '2017-12-22 05:42:50'),
(7, '12/22/2017', '2', 'Store1', 'employee1', '67', 'BIL888776', 'oncourier', '2017-12-22 05:52:40', '2017-12-22 05:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdate` date DEFAULT NULL,
  `mode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `date`, `taskid`, `paymentid`, `employee`, `store`, `bank`, `cheque`, `cdate`, `mode`, `amount`, `created_at`, `updated_at`) VALUES
(1, '2018-01-23', '142', 'PAY5a66c92787cd0', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:27', '2018-01-23 00:03:27'),
(2, '2018-01-23', '143', 'PAY5a66c9293b46f', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:29', '2018-01-23 00:03:29'),
(3, '2018-01-23', '144', 'PAY5a66c92ab32c5', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:30', '2018-01-23 00:03:30'),
(4, '2018-01-23', '145', 'PAY5a66c92c3a087', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:32', '2018-01-23 00:03:32'),
(5, '2018-01-23', '146', 'PAY5a66c92d3c783', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:33', '2018-01-23 00:03:33'),
(6, '2018-01-23', '147', 'PAY5a66c92e2a015', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:34', '2018-01-23 00:03:34'),
(7, '2018-01-23', '148', 'PAY5a66c92f13541', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:35', '2018-01-23 00:03:35'),
(8, '2018-01-23', '149', 'PAY5a66c92feee0a', '7', '1', NULL, NULL, NULL, 'cash', '200', '2018-01-23 00:03:36', '2018-01-23 00:03:36'),
(9, '2018-01-23', '150', 'PAY5a66c9c287314', '7', '1', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 00:06:02', '2018-01-23 00:06:02'),
(10, '2018-01-23', '151', 'PAY5a66c9c3b0302', '7', '1', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 00:06:03', '2018-01-23 00:06:03'),
(11, '2018-01-23', '152', 'PAY5a66c9c4e0b4e', '7', '1', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 00:06:04', '2018-01-23 00:06:04'),
(12, '2018-01-23', '153', 'PAY5a66d6d357290', '8', '1', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:01:47', '2018-01-23 01:01:47'),
(13, '2018-01-23', '154', 'PAY5a66d6d4c6bab', '8', '1', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:01:48', '2018-01-23 01:01:48'),
(14, '2018-01-23', '155', 'PAY5a66d6d5d7097', '8', '1', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:01:49', '2018-01-23 01:01:49'),
(15, '2018-01-23', '156', 'PAY5a66d6e9991f9', '11', '3', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:02:09', '2018-01-23 01:02:09'),
(16, '2018-01-24', '157', 'PAY5a66d6ead2aba', '11', '3', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:02:10', '2018-01-23 01:02:10'),
(17, '2018-01-24', '158', 'PAY5a66d6eba7f33', '11', '3', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:02:11', '2018-01-23 01:02:11'),
(18, '2018-01-23', '159', 'PAY5a66d6ec9474b', '11', '3', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:02:12', '2018-01-23 01:02:12'),
(19, '2018-01-23', '160', 'PAY5a66d6ed5d964', '11', '3', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-23 01:02:13', '2018-01-23 01:02:13'),
(20, '2018-01-24', '161', 'PAY5a686b9dbc9e0', '11', '9', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-24 05:48:53', '2018-01-24 05:48:53'),
(21, '2018-01-24', '162', 'PAY5a686ba28b223', '11', '9', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-24 05:48:58', '2018-01-24 05:48:58'),
(22, '2018-01-24', '163', 'PAY5a686ba37f831', '11', '9', 'SBI', 'sbin55555', '2018-01-28', 'cheque', '100', '2018-01-24 05:48:59', '2018-01-24 05:48:59'),
(23, '2018-01-24', '164', 'PAY5a686bc05446a', '7', '9', NULL, NULL, NULL, 'cash', '200', '2018-01-24 05:49:28', '2018-01-24 05:49:28'),
(24, '2018-01-24', '165', 'PAY5a686bc182782', '7', '9', NULL, NULL, NULL, 'cash', '200', '2018-01-24 05:49:29', '2018-01-24 05:49:29'),
(25, '2018-01-24', '166', 'PAY5a686bc283c56', '7', '9', NULL, NULL, NULL, 'cash', '200', '2018-01-24 05:49:30', '2018-01-24 05:49:30'),
(26, '2018-01-24', '167', 'PAY5a686bc3220d9', '7', '9', NULL, NULL, NULL, 'cash', '200', '2018-01-24 05:49:31', '2018-01-24 05:49:31');

-- --------------------------------------------------------

--
-- Table structure for table `placeorders`
--

CREATE TABLE `placeorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unitprice` int(50) DEFAULT NULL,
  `price` int(50) DEFAULT NULL,
  `courrier` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processdate` date DEFAULT NULL,
  `billno` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceno` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processsqty` int(100) DEFAULT NULL,
  `processamount` int(100) DEFAULT NULL,
  `login` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partial` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `placeorders`
--

INSERT INTO `placeorders` (`id`, `date`, `taskid`, `placeid`, `employee`, `store`, `product`, `quantity`, `size`, `color`, `unitprice`, `price`, `courrier`, `processdate`, `billno`, `invoiceno`, `processsqty`, `processamount`, `login`, `partial`, `status`, `created_at`, `updated_at`) VALUES
(58, '1900-01-08', '39', 'ODR5a534edc0a6f1', '14', '7', '2', '2', '6', '5', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:28:36', '2018-01-08 05:28:36'),
(59, '1900-01-08', '39', 'ODR5a534edc0a6f1', '14', '7', '2', '2', '7', '7', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:28:36', '2018-01-08 05:28:36'),
(60, '1900-01-08', '39', 'ODR5a53534fc5aba', '14', '7', '2', '2', '6', '5', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:47:35', '2018-01-08 05:47:35'),
(61, '1900-01-08', '39', 'ODR5a53534fc5aba', '14', '7', '2', '2', '7', '7', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', '', '2018-01-08 05:47:35', '2018-01-08 05:47:35'),
(62, '2018-01-08', '0', 'ODR5a54a97141e4f', '14', '7', '2', '2', '6', '5', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', 'po', '2018-01-09 06:07:21', '2018-01-09 06:07:21'),
(63, '2018-01-08', '0', 'ODR5a54a97141e4f', '14', '7', '9', '2', '7', '7', 0, NULL, '', '0000-00-00', '', '', NULL, 0, '', 'no', 'po', '2018-01-09 06:07:21', '2018-01-09 06:07:21'),
(64, '2018-01-09', '0', 'ODR5a54ada0d2538', '14', '7', '2', '2', '6', '5', 0, NULL, 'FIRST FLIGHT', '2018-01-10', 'BIll 88877', 'INV 5555', NULL, 0, '', 'no', 'cu', '2018-01-09 06:25:12', '2018-01-10 00:32:51'),
(65, '2018-01-09', '0', 'ODR5a54ada0d2538', '14', '7', '9', '2', '7', '7', 0, NULL, 'DTTC', '2018-01-10', 'BillNo  344458', 'INV56668', NULL, 0, '', 'no', 'cu', '2018-01-09 06:25:12', '2018-01-10 00:25:45'),
(66, '2018-01-10', '0', 'ODR5a55b836521e4', '14', '7', '2', '2', '6', '5', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-10 01:22:38', '2018-01-10 01:22:38'),
(67, '2018-01-10', '0', 'ODR5a55b836521e4', '14', '7', '9', '2', '7', '7', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-10 01:22:38', '2018-01-10 01:22:38'),
(68, '2018-01-11', '0', 'ODR5a56f40f87209', '14', '7', '2', '2', '6', '5', 0, 460, 'DTTC', '2018-01-13', 'Billno44433', 'Inv44455', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:50:15', '2018-01-13 00:25:48'),
(69, '2018-01-11', '0', 'ODR5a56f40f87209', '14', '7', '9', '2', '7', '7', 0, 700, 'DTTC', '2018-01-13', '4445676', 'INV4444', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:50:15', '2018-01-12 23:55:21'),
(70, '2018-01-11', '0', 'ODR5a56f4ba691f5', '14', '7', '2', '2', '6', '5', 230, 460, 'DTTC', '2018-01-11', 'Bill 444333', 'Inv 333445', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:53:06', '2018-01-11 01:19:50'),
(71, '2018-01-11', '0', 'ODR5a56f4ba691f5', '14', '7', '9', '2', '7', '7', 350, 700, 'Proffessional', '2018-01-11', 'Bill No 334443', 'INV 44456', NULL, 0, NULL, 'no', 'cu', '2018-01-10 23:53:06', '2018-01-11 01:17:59'),
(72, '2018-01-13', '0', 'ODR5a599d1cd21e6', '14', '7', '2', '2', '6', '5', 230, 460, 'First Flight', '2018-01-13', 'BIll77766', 'INV44455', NULL, 0, NULL, 'no', 'cu', '2018-01-13 00:16:04', '2018-01-13 00:20:34'),
(74, '2018-01-15', '0', 'ODR5a5c527768c27', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:34:23', '2018-01-15 01:34:23'),
(75, '2018-01-15', '0', 'ODR5a5c527768c27', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:34:23', '2018-01-15 01:34:23'),
(76, '2018-01-15', '0', 'ODR5a5c5406e36bb', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:41:02', '2018-01-15 01:41:02'),
(77, '2018-01-15', '0', 'ODR5a5c5406e36bb', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:41:02', '2018-01-15 01:41:02'),
(78, '2018-01-15', '0', 'ODR5a5c5519ccc26', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:45:37', '2018-01-15 01:45:37'),
(79, '2018-01-15', '0', 'ODR5a5c5519ccc26', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:45:37', '2018-01-15 01:45:37'),
(80, '2018-01-15', '0', 'ODR5a5c55656d1e5', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:46:53', '2018-01-15 01:46:53'),
(81, '2018-01-15', '0', 'ODR5a5c55656d1e5', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:46:53', '2018-01-15 01:46:53'),
(82, '2018-01-15', '0', 'ODR5a5c55869ce0e', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:26', '2018-01-15 01:47:26'),
(83, '2018-01-15', '0', 'ODR5a5c55869ce0e', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:26', '2018-01-15 01:47:26'),
(84, '2018-01-15', '0', 'ODR5a5c559f26b1d', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:51', '2018-01-15 01:47:51'),
(85, '2018-01-15', '0', 'ODR5a5c559f26b1d', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:47:51', '2018-01-15 01:47:51'),
(86, '2018-01-15', '0', 'ODR5a5c5621b402d', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:50:01', '2018-01-15 01:50:01'),
(87, '2018-01-15', '0', 'ODR5a5c5621b402d', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:50:01', '2018-01-15 01:50:01'),
(88, '2018-01-15', '0', 'ODR5a5c567c50d25', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:32', '2018-01-15 01:51:32'),
(89, '2018-01-15', '0', 'ODR5a5c567c50d25', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:32', '2018-01-15 01:51:32'),
(90, '2018-01-15', '0', 'ODR5a5c568b7dc4f', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:47', '2018-01-15 01:51:47'),
(91, '2018-01-15', '0', 'ODR5a5c568b7dc4f', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:51:47', '2018-01-15 01:51:47'),
(92, '2018-01-15', '0', 'ODR5a5c56c09f1c8', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:52:40', '2018-01-15 01:52:40'),
(93, '2018-01-15', '0', 'ODR5a5c56e68adba', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:53:18', '2018-01-15 01:53:18'),
(94, '2018-01-15', '0', 'ODR5a5c56e68adba', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:53:18', '2018-01-15 01:53:18'),
(95, '2018-01-15', '0', 'ODR5a5c5782093f4', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:55:54', '2018-01-15 01:55:54'),
(96, '2018-01-15', '0', 'ODR5a5c5782093f4', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:55:54', '2018-01-15 01:55:54'),
(97, '2018-01-15', '0', 'ODR5a5c57d7dac67', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:57:19', '2018-01-15 01:57:19'),
(98, '2018-01-15', '0', 'ODR5a5c57d7dac67', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:57:19', '2018-01-15 01:57:19'),
(99, '2018-01-15', '0', 'ODR5a5c58042958c', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:58:04', '2018-01-15 01:58:04'),
(100, '2018-01-15', '0', 'ODR5a5c58042958c', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:58:04', '2018-01-15 01:58:04'),
(101, '2018-01-15', '0', 'ODR5a5c586af2cbb', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:59:46', '2018-01-15 01:59:46'),
(102, '2018-01-15', '0', 'ODR5a5c586af2cbb', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 01:59:47', '2018-01-15 01:59:47'),
(103, '2018-01-15', '0', 'ODR5a5c58cd67cce', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:25', '2018-01-15 02:01:25'),
(104, '2018-01-15', '0', 'ODR5a5c58cd67cce', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:25', '2018-01-15 02:01:25'),
(105, '2018-01-15', '0', 'ODR5a5c58e114f6f', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:45', '2018-01-15 02:01:45'),
(106, '2018-01-15', '0', 'ODR5a5c58e114f6f', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:01:45', '2018-01-15 02:01:45'),
(107, '2018-01-15', '0', 'ODR5a5c59096ca9a', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:02:25', '2018-01-15 02:02:25'),
(108, '2018-01-15', '0', 'ODR5a5c59096ca9a', '14', '7', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-25', 'Billno33334', 'inv4445', 1, 350, NULL, 'yes', 'cu', '2018-01-15 02:02:25', '2018-01-25 02:27:25'),
(109, '2018-01-15', '0', 'ODR5a5c59171ce05', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:02:39', '2018-01-15 02:02:39'),
(110, '2018-01-15', '0', 'ODR5a5c59171ce05', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:02:39', '2018-01-15 02:02:39'),
(111, '2018-01-15', '0', 'ODR5a5c5997d2bb0', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:04:47', '2018-01-15 02:04:47'),
(112, '2018-01-15', '0', 'ODR5a5c5997d2bb0', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:04:47', '2018-01-15 02:04:47'),
(113, '2018-01-15', '0', 'ODR5a5c5a0dac34a', '14', '7', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:06:45', '2018-01-15 02:06:45'),
(114, '2018-01-15', '0', 'ODR5a5c5a0dac34a', '14', '7', '9', '2', '7', '7', 350, 700, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-15 02:06:45', '2018-01-15 02:06:45'),
(115, '2018-01-15', '0', 'ODR5a5c5a373989d', '14', '7', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', '66677', '77888', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:07:27', '2018-01-15 02:46:02'),
(116, '2018-01-15', '0', 'ODR5a5c5a373989d', '14', '7', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', 'bill999', 'inv5555', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:07:27', '2018-01-15 02:11:24'),
(117, '2018-01-15', '0', 'ODR5a5c5b6270641', '14', '7', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', '666777', '777777', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:12:26', '2018-01-15 02:42:17'),
(118, '2018-01-15', '0', 'ODR5a5c5b6270641', '14', '7', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', '6666', '777', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:12:26', '2018-01-15 02:32:35'),
(119, '2018-01-15', '0', 'ODR5a5c5ba0b3da2', '14', '3', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', '66677', '78888', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:13:28', '2018-01-15 02:39:30'),
(120, '2018-01-15', '0', 'ODR5a5c5ba0b3da2', '14', '3', '9', '2', '7', '7', 350, 700, 'First Flight', '2018-01-15', 'bill7777', 'Inv 444', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:13:28', '2018-01-15 02:30:57'),
(121, '2018-01-15', '0', 'ODR5a5c5cd439512', '14', '3', '2', '2', '6', '5', 230, 460, 'dttc', '2018-01-15', 'billno77788', 'inv666', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:18:36', '2018-01-15 02:29:19'),
(122, '2018-01-15', '0', 'ODR5a5c5cd439512', '14', '3', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', 'bill9999', 'inv5555', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:18:36', '2018-01-15 02:28:07'),
(123, '2018-01-15', '0', 'ODR5a5c6439425a6', '14', '4', '2', '2', '6', '5', 230, 460, 'DTTC4444', '2018-01-29', 'Billno444', 'INV444', 2, 460, NULL, 'no', 'cu', '2018-01-15 02:50:09', '2018-01-29 07:13:04'),
(124, '2018-01-15', '0', 'ODR5a5c6439425a6', '14', '4', '9', '2', '7', '7', 350, 700, 'dttc', '2018-01-15', 'B777', 'I888', NULL, 0, NULL, 'no', 'cu', '2018-01-15 02:50:09', '2018-01-15 02:52:07'),
(125, '2018-01-17', '0', 'ODR5a5f1113cbac8', '14', '4', '2', '2', '6', '5', 230, 460, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no', 'po', '2018-01-17 03:32:11', '2018-01-17 03:32:11'),
(126, '2018-01-17', '0', 'ODR5a5f1113cbac8', '14', '4', '9', '2', '7', '7', 350, 700, 'FFcourrier', '2018-01-25', 'Billno7777', 'Inv4444', 2, 700, NULL, 'no', 'cu', '2018-01-17 03:32:11', '2018-01-25 03:22:50'),
(127, '2018-01-25', '0', 'ODR5a6997b879e11', '14', '4', '2', '7', '6', '5', 230, 1610, 'DTTC5555', '2018-01-25', 'Billno777', 'Inv7777', 2, 460, NULL, 'yes', 'cu', '2018-01-25 03:09:20', '2018-01-25 03:16:20'),
(128, '2018-01-25', '0', 'ODR5a6997b879e11', '14', '4', '9', '8', '7', '7', 350, 2800, 'FFCUR', '2018-01-25', 'Bill666', 'INV555', 3, 1050, NULL, 'yes', 'cu', '2018-01-25 03:09:20', '2018-01-25 03:36:29'),
(129, '2018-02-02', '0', 'ODR5a73e9d6c5bb9', '14', '4', '2', '7', '6', '5', 230, 1610, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', 'po', '2018-02-01 23:02:22', '2018-02-01 23:02:22'),
(130, '2018-02-02', '0', 'ODR5a73e9d6c5bb9', '14', '4', '9', '8', '7', '7', 350, 2800, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', 'po', '2018-02-01 23:02:22', '2018-02-01 23:02:22');

-- --------------------------------------------------------

--
-- Table structure for table `productcolors`
--

CREATE TABLE `productcolors` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colorid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productcolors`
--

INSERT INTO `productcolors` (`id`, `productid`, `productcode`, `colorid`, `colorname`, `colorcode`, `created_at`, `updated_at`) VALUES
(3, '5', 'OTH', NULL, 'Yellow', '#hhhj', '2017-12-09 03:22:29', '2017-12-09 03:22:29'),
(5, '7', 'OTH', '1', 'Yellow', '#23df30', '2018-01-03 06:43:39', '2018-01-03 06:43:39'),
(6, '7', 'OTH', '4', 'Pale yellow', '#dfb923', '2018-01-03 08:11:38', '2018-01-03 08:11:38'),
(7, '2', 'OTH', '1', 'Yellow', '#e0d077', '2018-01-03 08:16:00', '2018-01-03 08:16:00'),
(9, '2', 'OTH', '5', 'Red', '#ca3b5c', '2018-01-03 08:22:58', '2018-01-03 08:22:58'),
(10, '8', 'OTH', '1', 'Yellow', '#e0d077', '2018-01-03 08:28:26', '2018-01-03 08:28:26'),
(11, '8', 'OTH', '5', 'Pale yellow', '#f5f4eb', '2018-01-03 08:29:07', '2018-01-03 08:29:07'),
(12, '9', 'OTH', '1', 'Yellow', '#e0d077', '2018-01-03 11:33:47', '2018-01-03 11:33:47'),
(13, '10', 'OTH', '1', 'Yellow', '#e0d077', '2018-01-06 07:24:07', '2018-01-06 07:24:07'),
(14, '11', 'OTH', '1', 'Yellow', '#e0d077', '2018-01-29 01:21:41', '2018-01-29 01:21:41'),
(15, '12', 'OTH', '1', 'Yellow', '#e0d077', '2018-01-29 01:38:58', '2018-01-29 01:38:58'),
(16, '13', 'OTH', '4', 'Pale yellow', '#f5f4eb', '2018-01-29 02:13:17', '2018-01-29 02:13:17'),
(17, '15', 'NDLEG', '4', 'Pale yellow', '#f5f4eb', '2018-01-29 23:36:10', '2018-01-31 00:50:37'),
(18, '16', 'NDLEG', '2', 'Yellow', '#e0d077', '2018-01-29 23:38:34', '2018-01-29 23:38:34'),
(19, '17', 'DXSCOTRB', '5', 'Red', '#ca3b5c', '2018-01-30 00:00:34', '2018-01-30 02:08:57'),
(20, '18', 'NDLEG', '1', 'Yellow', '#e0d077', '2018-01-31 02:05:17', '2018-01-31 02:05:17'),
(21, '19', 'DXSCOTRB', '5', 'Red', '#ca3b5c', '2018-02-02 05:26:48', '2018-02-02 05:26:48');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `itemcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priceunit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brandid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `itemcode`, `productcode`, `product`, `price`, `priceunit`, `brandid`, `catid`, `description`, `pimage`, `active`, `created_at`, `updated_at`) VALUES
(2, 'ITEM15396', 'OTH', 'p1', '230', '56', '2', '1', 'desc', 'theme/image/product/1512621019mort.jpg', 0, '2017-12-06 06:34:55', '2018-01-29 02:23:44'),
(7, 'ITEM15395', 'OTH', 'check product pr1', '1200', '1', '2', '1', 'desc check pr1', 'theme/image/product/1515665268goldloan1.jpg', 1, '2018-01-03 06:43:39', '2018-01-11 04:37:48'),
(8, 'ITEM15394', 'OTH', 'Product 1', '200', 'Piece', '2', '1', 'Description', 'theme/image/product/1515826031Solidarity-Group-Loans-Large.jpg', 1, '2018-01-03 08:28:26', '2018-01-13 01:17:11'),
(9, 'ITEM15393', 'OTH', 'Product 3', '350', 'Piece', '3', '1', 'Product 3 description', 'theme/image/product/1517216450vegcultivation.jpg', 0, '2018-01-03 11:33:47', '2018-01-29 03:30:50'),
(10, 'ITEM15392', 'OTH', 'Prod', '400', 'Box', '2', '1', 'Descr', 'theme/image/product/1517216356interest.jpg', 1, '2018-01-06 07:24:07', '2018-01-29 03:29:16'),
(11, 'ITEM15391', 'OTH', 'np1', '150', '1', '2', '1', 'np1 desc', 'theme/image/product/1517208701.jpg', 1, '2018-01-29 01:21:41', '2018-01-29 01:24:28'),
(12, 'ITEM15390', 'OTH', 'np2', '300', '1', '2', '1', 'np2 desc', 'theme/image/product/1517211999industrial-loans.jpg', 0, '2018-01-29 01:38:57', '2018-01-29 02:16:39'),
(13, 'ITEM15389', 'OTH', 'np3', '500', '2', '3', '3', 'np2 desc', 'theme/image/product/1517211797.jpg', 1, '2018-01-29 02:13:17', '2018-01-29 02:13:17'),
(15, 'ITEM86546', 'NDLEG', 'ND Leggings 3001 White L', '266', '30', '3', '1', 'ND Leggings 3001 White L', 'theme/image/product/1517288770.jpg', 1, '2018-01-29 23:36:10', '2018-01-30 23:50:30'),
(16, 'ITEM25080', 'NDLEG', 'ND Leggings 3001 White XL', '220', '36', '3', '1', 'ND Leggings 3001 White L', 'theme/image/product/1517288914.jpg', 1, '2018-01-29 23:38:34', '2018-01-29 23:38:34'),
(17, 'ITEM32576', 'DXSCOTRB', 'Dixcy Scott Replay Brief 80(1*10)', '68', '200', '6', '1', 'Dixcy Scott Replay Brief 80(1*10)', 'theme/image/product/1517292250slider.jpg', 1, '2018-01-30 00:00:34', '2018-01-30 01:55:03'),
(18, 'ITEM25655', 'NDLEG', 'ND Leggings', '600', '30', '6', '4', 'ND Leggings', 'theme/image/product/1517384117.jpg', 1, '2018-01-31 02:05:17', '2018-01-31 02:05:17'),
(19, 'ITEM15331', 'DXSCOTRB', 'Dixcy Scott Replay Brief 80(1*10)', '69', '1', '6', '1', 'Dixcy Scott Replay Brief 80(1*10)', 'noimage1517569008', 1, '2018-02-02 05:26:48', '2018-02-02 05:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `productsizes`
--

CREATE TABLE `productsizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sizeid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productsizes`
--

INSERT INTO `productsizes` (`id`, `productid`, `productcode`, `sizeid`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(2, '5', 'OTH', NULL, 'Medium', 'M', '2017-12-11 01:19:09', '2017-12-11 01:19:09'),
(3, '5', 'OTH', NULL, 'Small', 'S', '2017-12-11 01:20:52', '2017-12-11 01:20:52'),
(7, '6', 'OTH', NULL, 'Baby', 'BB', '2017-12-12 03:43:31', '2017-12-12 03:43:31'),
(8, '6', 'OTH', NULL, 'Large', 'XL', '2017-12-12 03:43:36', '2017-12-12 03:43:36'),
(9, '7', 'OTH', '2', 'Baby', 'BB', '2018-01-03 06:43:39', '2018-01-03 06:43:39'),
(10, '7', 'OTH', '1', 'Medium', 'M', '2018-01-03 08:10:32', '2018-01-03 08:10:32'),
(11, '2', 'OTH', '2', 'Baby', 'BB', '2018-01-03 08:14:14', '2018-01-03 08:14:14'),
(12, '2', 'OTH', '1', 'Medium', 'M', '2018-01-03 08:14:23', '2018-01-03 08:14:23'),
(13, '8', 'OTH', '1', 'Medium', 'M', '2018-01-03 08:28:26', '2018-01-03 08:28:26'),
(14, '9', 'OTH', '4', '85', 'EF', '2018-01-03 11:33:47', '2018-01-03 11:33:47'),
(15, '10', 'OTH', '1', 'Medium', 'M', '2018-01-06 07:24:07', '2018-01-06 07:24:07'),
(16, '11', 'OTH', '1', 'Medium', 'M', '2018-01-29 01:21:41', '2018-01-29 01:21:41'),
(17, '12', 'OTH', '1', 'Medium', 'M', '2018-01-29 01:38:58', '2018-01-29 01:38:58'),
(18, '13', 'OTH', '1', 'Medium', 'M', '2018-01-29 02:13:17', '2018-01-29 02:13:17'),
(19, '15', 'NDLEG', '2', 'Baby', 'BB', '2018-01-29 23:36:10', '2018-01-30 02:35:55'),
(20, '16', 'NDLEG', '7', 'XL', 'XL', '2018-01-29 23:38:34', '2018-01-29 23:38:34'),
(21, '17', 'DXSCOTRB', '7', 'XL', 'XL', '2018-01-30 00:00:34', '2018-01-30 02:35:36'),
(22, '18', 'NDLEG', '6', '65', '55566', '2018-01-31 02:05:17', '2018-01-31 02:05:17'),
(23, '19', 'DXSCOTRB', '7', 'XL', 'XL', '2018-02-02 05:26:48', '2018-02-02 05:26:48');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modules` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `displayname`, `modules`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'For Administration full control', 'Admin', '1,2', '2017-11-29 02:00:28', '2017-12-01 00:57:50'),
(2, 'user', 'User', 'User', '1,2', '2017-11-30 01:18:20', '2017-12-01 00:58:38'),
(4, 'Employee', 'Employee role', 'Employee', '0', '2017-12-02 00:28:24', '2017-12-02 00:28:24'),
(5, 'superadmin', 'Superadmin', 'Superadmin', '1,2,3', '2018-01-13 01:41:36', '2018-01-23 04:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `orderid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales` int(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `date`, `orderid`, `employee`, `store`, `sales`, `created_at`, `updated_at`) VALUES
(1, '2018-01-11', '71', '14', '1', 700, '2018-01-11 01:17:59', '2018-01-11 01:17:59'),
(2, '2018-01-11', '70', '14', '1', 460, '2018-01-11 01:19:50', '2018-01-11 01:19:50'),
(3, '2018-01-13', '69', '14', NULL, 700, '2018-01-12 23:55:21', '2018-01-12 23:55:21'),
(4, '2018-01-13', '72', '14', NULL, 460, '2018-01-13 00:20:34', '2018-01-13 00:20:34'),
(5, '2018-01-13', '68', '14', '7', 460, '2018-01-13 00:25:48', '2018-01-13 00:25:48'),
(6, '2018-01-15', '116', '14', '7', 700, '2018-01-15 02:11:24', '2018-01-15 02:11:24'),
(7, '2018-01-15', '119', '14', '3', 460, '2018-01-15 02:39:30', '2018-01-15 02:39:30'),
(8, '2018-01-15', '117', '14', '7', 460, '2018-01-15 02:42:17', '2018-01-15 02:42:17'),
(9, '2018-01-15', '115', '14', '7', 460, '2018-01-15 02:46:02', '2018-01-15 02:46:02'),
(10, '2018-01-15', '124', '14', '4', 700, '2018-01-15 02:52:07', '2018-01-15 02:52:07'),
(11, '2018-01-25', '108', '14', '7', 700, '2018-01-25 02:27:25', '2018-01-25 02:27:25'),
(12, '2018-01-25', '127', '14', '4', 1610, '2018-01-25 03:16:20', '2018-01-25 03:16:20'),
(13, '2018-01-25', '126', '14', '4', 700, '2018-01-25 03:22:50', '2018-01-25 03:22:50'),
(14, '2018-01-25', '128', '14', '4', 1050, '2018-01-25 03:36:29', '2018-01-25 03:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(1, 'Medium', 'M', '2017-12-14 02:27:43', '2017-12-14 02:27:43'),
(2, 'Baby', 'BB', '2017-12-14 02:28:03', '2017-12-14 02:28:03'),
(4, '85', 'EF', '2017-12-14 02:38:56', '2017-12-14 02:38:56'),
(5, 'Pink', '#009', '2018-01-06 07:39:03', '2018-01-06 07:39:03'),
(6, '65', '55566', '2018-01-29 03:53:07', '2018-01-29 03:53:07'),
(7, 'XL', 'XL', '2018-01-29 23:37:37', '2018-01-29 23:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `item_code`, `stock`, `status`, `created_at`, `updated_at`) VALUES
(14, 'ITEM15389', 60, 1, '2018-01-29 03:57:49', NULL),
(15, 'ITEM15390', 70, 1, '2018-01-29 03:57:49', NULL),
(16, 'ITEM15391', 30, 1, '2018-01-29 03:57:49', NULL),
(17, 'ITEM15392', 60, 1, '2018-01-29 03:57:49', NULL),
(18, 'ITEM15393', 110, 1, '2018-01-29 03:57:49', NULL),
(19, 'ITEM15394', 20, 1, '2018-01-29 03:57:49', NULL),
(21, 'ITEM15396', 333, 1, '2018-01-29 03:57:49', NULL),
(22, 'ITEM15396', -10, 1, '2018-01-29 04:26:55', NULL),
(23, 'ITEM15389', 60, 1, '2018-01-29 05:18:18', NULL),
(24, 'ITEM15390', 70, 1, '2018-01-29 05:18:18', NULL),
(25, 'ITEM15391', 30, 1, '2018-01-29 05:18:18', NULL),
(26, 'ITEM15392', 60, 1, '2018-01-29 05:18:18', NULL),
(27, 'ITEM15393', 110, 1, '2018-01-29 05:18:18', NULL),
(28, 'ITEM15394', 20, 1, '2018-01-29 05:18:18', NULL),
(29, 'ITEM15395', 0, 1, '2018-01-29 05:18:18', NULL),
(30, 'ITEM15396', 333, 1, '2018-01-29 05:18:18', NULL),
(31, 'ITEM15389', 60, 1, '2018-01-29 05:18:56', NULL),
(32, 'ITEM15390', 70, 1, '2018-01-29 05:18:56', NULL),
(33, 'ITEM15391', 30, 1, '2018-01-29 05:18:56', NULL),
(34, 'ITEM15392', 60, 1, '2018-01-29 05:18:56', NULL),
(35, 'ITEM15393', 110, 1, '2018-01-29 05:18:56', NULL),
(36, 'ITEM15394', 20, 1, '2018-01-29 05:18:56', NULL),
(37, 'ITEM15395', 0, 1, '2018-01-29 05:18:56', NULL),
(38, 'ITEM15396', 333, 1, '2018-01-29 05:18:56', NULL),
(39, 'ITEM15389', 60, 1, '2018-01-30 02:42:59', NULL),
(40, 'ITEM15390', 70, 1, '2018-01-30 02:42:59', NULL),
(41, 'ITEM15391', 30, 1, '2018-01-30 02:42:59', NULL),
(42, 'ITEM15392', 60, 1, '2018-01-30 02:42:59', NULL),
(43, 'ITEM15393', 110, 1, '2018-01-30 02:42:59', NULL),
(44, 'ITEM15394', 20, 1, '2018-01-30 02:42:59', NULL),
(45, 'ITEM15395', 0, 1, '2018-01-30 02:42:59', NULL),
(46, 'ITEM15396', 333, 1, '2018-01-30 02:42:59', NULL),
(47, 'ITEM25080', 60, 1, '2018-01-30 02:45:14', NULL),
(48, 'ITEM11373', 60, 1, '2018-01-30 05:41:29', NULL),
(49, 'ITEM14117', 60, 1, '2018-01-30 05:41:29', NULL),
(50, 'ITEM31492', 60, 1, '2018-01-30 05:41:29', NULL),
(51, 'ITEM13393', 60, 1, '2018-01-30 05:41:29', NULL),
(52, 'ITEM14834', 60, 1, '2018-01-30 05:41:29', NULL),
(53, 'ITEM83269', 60, 1, '2018-01-30 05:41:29', NULL),
(54, 'ITEM15994', 60, 1, '2018-01-30 05:41:29', NULL),
(55, 'ITEM20292', 60, 1, '2018-01-30 05:41:29', NULL),
(56, 'ITEM11373', 60, 1, '2018-01-30 05:41:56', NULL),
(57, 'ITEM14117', 60, 1, '2018-01-30 05:41:56', NULL),
(58, 'ITEM31492', 60, 1, '2018-01-30 05:41:56', NULL),
(59, 'ITEM13393', 60, 1, '2018-01-30 05:41:56', NULL),
(60, 'ITEM14834', 60, 1, '2018-01-30 05:41:56', NULL),
(61, 'ITEM83269', 60, 1, '2018-01-30 05:41:56', NULL),
(62, 'ITEM15994', 60, 1, '2018-01-30 05:41:56', NULL),
(63, 'ITEM20292', 60, 1, '2018-01-30 05:41:56', NULL),
(64, 'ITEM32576', 70, 1, '2018-02-02 01:05:32', NULL),
(65, 'ITEM25655', 0, 1, '2018-02-02 01:18:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `paid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `balance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `gst`, `description`, `contactname`, `contactno`, `address`, `location`, `zipcode`, `city`, `state`, `country`, `total`, `paid`, `balance`, `created_at`, `updated_at`) VALUES
(1, 'Store1', '', 'Sub Store', 'Mr. Jeevanand Raj', '8887766655', 'Adresss store 1', 'P M Kutty Road', '8776555', 'Calicut', 'Kerala', 'India', '100000', '6400', '93600', '2017-12-13 05:41:52', '2018-01-23 01:01:49'),
(3, 'Store3', '', 'Main Store', 'Mr. Geo Jacob', '8888877766', 'Adresss store 2', 'Kaloor Stadium Junction', '677855566', 'Ernakulam', 'kerala', 'India', '101620', '900', '100720', '2017-12-13 06:29:17', '2018-01-23 01:02:13'),
(4, 'store 5', '', 'Main Store', 'Mr. Naveen', '8887766655', 'Adresss store 5', 'Thiruvananthapuram', '677855566', 'Ernakulam', 'Kerala', 'India', '404520', '2000', '402520', '2018-01-02 06:35:26', '2018-01-29 07:13:04'),
(5, 'store 6', '', 'new store', 'Mr. Jeeva', '7778899955', 'address store 6', 'Near KSRTC', '673003', 'Palakkad', 'Kerala', 'India', '500000', '404400', '95600', '2018-01-02 07:08:30', '2018-01-17 06:02:19'),
(6, 'store 7', '', 'new store kannur', 'Mr. Sandeep', '7778899955', 'address store 7', 'Thane', '673003', 'Kannur', 'Kerala', 'India', '400000', '200000', '200000', '2018-01-02 07:11:15', '2018-01-05 06:30:49'),
(7, 'store 9', '', 'new store 9', 'Mr. Naveen', '8887766655', 'Padmavilasom Street, FORT P.O, Kannur', 'Caltex  junction', '55566777', 'Kannur', 'Kerala', 'India', '8580', '0', '8580', '2018-01-02 07:24:28', '2018-01-25 02:27:25'),
(9, 'Ammucreation Store 20', 'GST666778', 'Ammucreation Store 20', 'Mr. Geo Jacob', '6667755544', 'Address Ammucreation Store 20', 'Kurupumbady', '678999', 'Perumbavoor', 'Kerala', 'India', '600000', '1100', '598900', '2018-01-24 05:18:08', '2018-01-24 05:49:31');

-- --------------------------------------------------------

--
-- Table structure for table `store_allot`
--

CREATE TABLE `store_allot` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_allot`
--

INSERT INTO `store_allot` (`id`, `employee_id`, `store_id`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 7, 1, 1, '2018-01-08 09:29:47', NULL),
(2, 7, 3, 0, '2018-01-09 03:50:25', NULL),
(3, 7, 4, 0, '2018-01-09 04:03:42', NULL),
(4, 7, 3, 0, '2018-01-09 04:03:32', '2018-01-11 04:57:46'),
(5, 8, 1, 1, '2018-01-09 04:04:58', NULL),
(6, 8, 4, 0, '2018-01-09 04:04:58', '2018-01-09 05:03:38'),
(7, 8, 3, 0, '2018-01-09 04:05:09', '2018-01-24 10:35:58'),
(8, 8, 5, 0, '2018-01-09 04:05:16', NULL),
(9, 10, 5, 1, '2018-01-09 04:19:17', NULL),
(10, 12, 1, 1, '2018-01-09 04:46:27', NULL),
(11, 12, 5, 1, '2018-01-09 04:46:27', NULL),
(12, 12, 3, 0, '2018-01-09 04:46:34', '2018-01-09 04:48:25'),
(13, 12, 4, 0, '2018-01-09 04:46:50', NULL),
(14, 11, 1, 1, '2018-01-09 04:47:14', NULL),
(15, 12, 4, 1, '2018-01-09 04:48:10', NULL),
(16, 14, 3, 1, '2018-01-09 04:58:25', NULL),
(17, 7, 7, 1, '2018-01-11 04:16:19', NULL),
(18, 14, 1, 1, '2018-01-15 11:52:09', NULL),
(19, 25, 1, 1, '2018-01-16 05:28:03', NULL),
(20, 25, 3, 1, '2018-01-16 05:28:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE `targets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sales` float DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targets`
--

INSERT INTO `targets` (`id`, `user_id`, `sales`, `date`, `created_at`, `updated_at`) VALUES
(14, 14, 4000, '2018-01-09', '2018-01-09 01:21:32', '2018-01-09 01:40:46'),
(15, 12, 90, '2018-01-25', '2018-01-25 05:08:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeeid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tdate` date DEFAULT NULL,
  `ttime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `torder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tpayment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interest` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tremark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tstatus` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `employeeid`, `store`, `address`, `location`, `mode`, `description`, `mobile`, `tdate`, `ttime`, `placeid`, `paymentid`, `torder`, `created_at`, `updated_at`, `tpayment`, `interest`, `tremark`, `tstatus`) VALUES
(1, '12', '1', 'employee23 addresss', 'P M Kutty Road', 'pay', 'desc', '6666688888', '2018-01-04', '3:20am', '', 'PAY5a5343200d115', '56', '2017-12-15 01:57:57', '2018-01-10 00:20:31', 'nil', NULL, 'good', 'payment'),
(2, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'sale', 'desc', '6666688888', '2018-01-20', '3.00am', '', '', '67', '2017-12-15 01:58:16', '2017-12-22 05:52:12', 'nil', NULL, 'good', 'ongoing'),
(3, '11', '3', 'employee1 Address', 'Kaloor Stadium Junction', 'sale', 'desc12 emp12', '6666688888', '2018-01-08', '3.00am', '', '', '17', '2017-12-15 01:58:33', '2018-01-08 23:15:05', 'nil', NULL, 'satisfied', 'ongoing'),
(4, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '2018-01-15', '3.00am', '', '', '', '2017-12-15 01:58:53', '2018-01-02 11:19:56', '', NULL, '', 'completed'),
(5, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'desc', '6666688888', '2018-01-10', '3.00am', '', '', '50', '2017-12-15 01:59:00', '2017-12-22 04:54:08', 'nil', NULL, 'good', 'ongoing'),
(6, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'Description', '6666688888', '2018-01-05', '5.00pm', 'ODR5a534c2979acf', '', '50', '2017-12-15 02:06:37', '2018-01-08 05:17:05', 'nil', NULL, 'good', 'po completed'),
(7, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'sale', 'Desc', '6666688888', '2018-01-09', '3.00pm', '', '', '50', '2017-12-15 02:07:32', '2017-12-22 01:39:10', 'nil', NULL, 'good', 'ongoing'),
(51, '11', '3', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-08', '10:57:06am', 'ODR5a534e82a9a09', NULL, NULL, '2018-01-08 05:27:06', '2018-01-08 05:27:06', NULL, NULL, 'good', 'po completed'),
(53, '14', '4', 'add', 'loc', 'sale', 'store test emp7', '8887766677', '2018-02-05', '3.00pm', NULL, NULL, NULL, '2018-01-09 00:07:56', '2018-01-09 00:07:56', NULL, NULL, NULL, 'ongoing'),
(54, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-09', NULL, NULL, NULL, NULL, '2018-01-09 00:59:23', '2018-01-09 00:59:23', NULL, NULL, 'good', 'visit completed'),
(55, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-09', '3.00pm', NULL, NULL, NULL, '2018-01-09 01:02:33', '2018-01-09 01:02:33', NULL, NULL, 'good', 'visit completed'),
(56, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-09', '3.00pm', NULL, NULL, NULL, '2018-01-09 01:07:42', '2018-01-09 01:07:42', NULL, NULL, 'interested', 'visit completed'),
(57, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-09', '11:55:12am', 'ODR5a54ada0d2538', NULL, NULL, '2018-01-09 06:25:12', '2018-01-09 06:25:12', NULL, NULL, 'good', 'po completed'),
(58, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-10', '06:52:38am', 'ODR5a55b836521e4', NULL, NULL, '2018-01-10 01:22:38', '2018-01-10 01:22:38', NULL, NULL, 'good', 'po completed'),
(63, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-11', '05:20:15am', 'ODR5a56f40f87209', NULL, NULL, '2018-01-10 23:50:15', '2018-01-10 23:50:15', NULL, NULL, 'good', 'po completed'),
(64, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-11', '05:23:06am', 'ODR5a56f4ba691f5', NULL, NULL, '2018-01-10 23:53:06', '2018-01-10 23:53:06', NULL, NULL, 'good', 'po completed'),
(67, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-12', '9:18 am', NULL, NULL, NULL, '2018-01-12 03:48:22', '2018-01-12 03:48:22', NULL, NULL, 'interested remark', 'visit completed'),
(68, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-12', '9:20 am', NULL, NULL, NULL, '2018-01-12 03:50:50', '2018-01-12 03:50:50', NULL, 'interest', 'interested remark', 'visit completed'),
(69, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'sale 10 nos', '8887766655', '2018-01-25', '3:00pm', NULL, NULL, NULL, '2018-01-12 04:49:34', '2018-01-12 04:49:34', NULL, NULL, NULL, 'ongoing'),
(70, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'test desc', '8888877766', '2018-01-12', '3.00pm', NULL, NULL, NULL, '2018-01-12 05:42:32', '2018-01-12 05:42:32', NULL, NULL, NULL, 'ongoing'),
(71, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'test desc', '8888877766', '2018-01-12', '3.00pm', NULL, NULL, NULL, '2018-01-12 05:46:49', '2018-01-12 05:46:49', NULL, NULL, NULL, 'ongoing'),
(72, '11', '6', 'address store 7', 'Thane', 'pay', 'tst desc', '7778899955', '2018-01-13', '3.00pm', NULL, NULL, NULL, '2018-01-12 05:47:45', '2018-01-12 05:47:45', NULL, NULL, NULL, 'ongoing'),
(73, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-12', '11:43 am', NULL, NULL, NULL, '2018-01-12 06:13:43', '2018-01-12 06:13:43', NULL, 'interest', 'interested remark', 'visit completed'),
(74, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-13', '4:14 am', NULL, NULL, NULL, '2018-01-12 22:44:09', '2018-01-12 22:44:09', NULL, 'interested', 'ok', 'visit completed'),
(75, '11', '3', NULL, NULL, 'visit', NULL, NULL, '2018-01-13', '4:15 am', NULL, NULL, NULL, '2018-01-12 22:45:49', '2018-01-12 22:45:49', NULL, 'interested', 'ok', 'visit completed'),
(76, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-13', '05:46:05am', 'ODR5a599d1cd21e6', NULL, NULL, '2018-01-13 00:16:05', '2018-01-13 00:16:05', NULL, NULL, 'good', 'po completed'),
(77, '19', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'collect pay', '8888877766', '2017-01-26', '5.00pm', NULL, NULL, NULL, '2018-01-13 03:26:57', '2018-01-13 03:44:34', NULL, NULL, NULL, 'ongoing'),
(78, '12', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-13', '10:32:06am', NULL, 'PAY5a59e02604304', NULL, '2018-01-13 05:02:06', '2018-01-13 05:02:06', NULL, NULL, 'good', 'payment completed'),
(79, '12', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-13', '10:32:27am', NULL, 'PAY5a59e03be3af3', NULL, '2018-01-13 05:02:27', '2018-01-13 05:02:27', NULL, NULL, 'good', 'payment completed'),
(80, '12', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-13', '10:33:14am', NULL, 'PAY5a59e06ad1ee7', NULL, '2018-01-13 05:03:14', '2018-01-13 05:03:14', NULL, NULL, 'good', 'payment completed'),
(82, '12', '5', 'address store 6', 'Near KSRTC', 'pay', 'payment collect', '7778899955', '2018-01-18', '3.00pm', NULL, 'PAY5a5df151e758b', NULL, '2018-01-15 01:17:22', '2018-01-16 07:04:25', NULL, NULL, 'good', 'payment completed'),
(83, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:04:23am', 'ODR5a5c527768c27', NULL, NULL, '2018-01-15 01:34:23', '2018-01-15 01:34:23', NULL, NULL, 'good', 'po completed'),
(84, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:11:03am', 'ODR5a5c5406e36bb', NULL, NULL, '2018-01-15 01:41:03', '2018-01-15 01:41:03', NULL, NULL, 'good', 'po completed'),
(85, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:15:37am', 'ODR5a5c5519ccc26', NULL, NULL, '2018-01-15 01:45:37', '2018-01-15 01:45:37', NULL, NULL, 'good', 'po completed'),
(86, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:16:53am', 'ODR5a5c55656d1e5', NULL, NULL, '2018-01-15 01:46:53', '2018-01-15 01:46:53', NULL, NULL, 'good', 'po completed'),
(87, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:17:26am', 'ODR5a5c55869ce0e', NULL, NULL, '2018-01-15 01:47:26', '2018-01-15 01:47:26', NULL, NULL, 'good', 'po completed'),
(88, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-13', '07:17:51am', 'ODR5a5c559f26b1d', NULL, NULL, '2018-01-15 01:47:51', '2018-01-15 01:47:51', NULL, NULL, 'good', 'ongoing'),
(89, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:20:01am', 'ODR5a5c5621b402d', NULL, NULL, '2018-01-15 01:50:01', '2018-01-15 01:50:01', NULL, NULL, 'good', 'po completed'),
(90, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:21:32am', 'ODR5a5c567c50d25', NULL, NULL, '2018-01-15 01:51:32', '2018-01-15 01:51:32', NULL, NULL, 'good', 'po completed'),
(91, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:21:47am', 'ODR5a5c568b7dc4f', NULL, NULL, '2018-01-15 01:51:47', '2018-01-15 01:51:47', NULL, NULL, 'good', 'po completed'),
(92, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:23:18am', 'ODR5a5c56e68adba', NULL, NULL, '2018-01-15 01:53:18', '2018-01-15 01:53:18', NULL, NULL, 'good', 'po completed'),
(93, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:25:54am', 'ODR5a5c5782093f4', NULL, NULL, '2018-01-15 01:55:54', '2018-01-15 01:55:54', NULL, NULL, 'good', 'po completed'),
(94, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:27:19am', 'ODR5a5c57d7dac67', NULL, NULL, '2018-01-15 01:57:19', '2018-01-15 01:57:19', NULL, NULL, 'good', 'po completed'),
(95, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:28:04am', 'ODR5a5c58042958c', NULL, NULL, '2018-01-15 01:58:04', '2018-01-15 01:58:04', NULL, NULL, 'good', 'po completed'),
(96, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:29:47am', 'ODR5a5c586af2cbb', NULL, NULL, '2018-01-15 01:59:47', '2018-01-15 01:59:47', NULL, NULL, 'good', 'po completed'),
(97, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:31:25am', 'ODR5a5c58cd67cce', NULL, NULL, '2018-01-15 02:01:25', '2018-01-15 02:01:25', NULL, NULL, 'good', 'po completed'),
(98, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:31:45am', 'ODR5a5c58e114f6f', NULL, NULL, '2018-01-15 02:01:45', '2018-01-15 02:01:45', NULL, NULL, 'good', 'po completed'),
(99, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:32:25am', 'ODR5a5c59096ca9a', NULL, NULL, '2018-01-15 02:02:25', '2018-01-15 02:02:25', NULL, NULL, 'good', 'po completed'),
(100, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:32:39am', 'ODR5a5c59171ce05', NULL, NULL, '2018-01-15 02:02:39', '2018-01-15 02:02:39', NULL, NULL, 'good', 'po completed'),
(101, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:34:47am', 'ODR5a5c5997d2bb0', NULL, NULL, '2018-01-15 02:04:47', '2018-01-15 02:04:47', NULL, NULL, 'good', 'po completed'),
(102, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:36:45am', 'ODR5a5c5a0dac34a', NULL, NULL, '2018-01-15 02:06:45', '2018-01-15 02:06:45', NULL, NULL, 'good', 'po completed'),
(103, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:37:27am', 'ODR5a5c5a373989d', NULL, NULL, '2018-01-15 02:07:27', '2018-01-15 02:07:27', NULL, NULL, 'good', 'po completed'),
(104, '14', '7', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:42:26am', 'ODR5a5c5b6270641', NULL, NULL, '2018-01-15 02:12:26', '2018-01-15 02:12:26', NULL, NULL, 'good', 'po completed'),
(105, '14', '3', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:43:28am', 'ODR5a5c5ba0b3da2', NULL, NULL, '2018-01-15 02:13:28', '2018-01-15 02:13:28', NULL, NULL, 'good', 'po completed'),
(106, '14', '3', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '07:48:36am', 'ODR5a5c5cd439512', NULL, NULL, '2018-01-15 02:18:36', '2018-01-15 02:18:36', NULL, NULL, 'good', 'po completed'),
(107, '14', '4', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-15', '08:20:09am', 'ODR5a5c6439425a6', NULL, NULL, '2018-01-15 02:50:09', '2018-01-15 02:50:09', NULL, NULL, 'good', 'po completed'),
(108, '7', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'store 1 desc', '8887766655', '2018-01-16', NULL, NULL, NULL, NULL, '2018-01-15 06:48:55', '2018-01-15 06:48:55', NULL, NULL, NULL, 'ongoing'),
(109, '7', '7', 'address store 9', 'Caltex  junction', 'pay', 'desc', '8887766655', '2018-01-20', NULL, NULL, NULL, NULL, '2018-01-15 06:55:23', '2018-01-15 06:55:23', NULL, NULL, NULL, 'ongoing'),
(110, '25', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'desc12 emp12', '8888877766', '2018-01-20', NULL, NULL, NULL, NULL, '2018-01-15 23:58:21', '2018-01-15 23:58:21', NULL, NULL, NULL, 'ongoing'),
(111, '25', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'desc12 emp12', '8887766655', '2018-01-16', NULL, NULL, NULL, NULL, '2018-01-16 00:08:38', '2018-01-16 00:08:38', NULL, NULL, NULL, 'ongoing'),
(112, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-16', '11:28:01am', NULL, 'PAY5a5de1c1385f3', NULL, '2018-01-16 05:58:01', '2018-01-16 05:58:01', NULL, NULL, 'good', 'payment completed'),
(113, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-16', '11:28:40am', NULL, 'PAY5a5de1e886c85', NULL, '2018-01-16 05:58:40', '2018-01-16 05:58:40', NULL, NULL, 'good', 'payment completed'),
(114, '14', '4', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-24', '09:02:11am', 'ODR5a5f1113cbac8', NULL, NULL, '2018-01-17 03:32:11', '2018-01-17 03:32:11', NULL, NULL, 'good', 'po completed'),
(115, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:14:03am', NULL, 'PAY5a5f13db72fb7', NULL, '2018-01-17 03:44:03', '2018-01-17 03:44:03', NULL, NULL, 'good', 'payment completed'),
(116, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:17:35am', NULL, 'PAY5a5f14af148aa', NULL, '2018-01-17 03:47:35', '2018-01-17 03:47:35', NULL, NULL, 'good', 'payment completed'),
(117, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:21:29am', NULL, 'PAY5a5f1599b8c5c', NULL, '2018-01-17 03:51:29', '2018-01-17 03:51:29', NULL, NULL, 'good', 'payment completed'),
(118, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:29:46am', NULL, 'PAY5a5f178a028f5', NULL, '2018-01-17 03:59:46', '2018-01-17 03:59:46', NULL, NULL, 'good', 'payment completed'),
(119, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:29:47am', NULL, 'PAY5a5f178b1d21f', NULL, '2018-01-17 03:59:47', '2018-01-17 03:59:47', NULL, NULL, 'good', 'payment completed'),
(120, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:30:00am', NULL, 'PAY5a5f179870425', NULL, '2018-01-17 04:00:00', '2018-01-17 04:00:00', NULL, NULL, 'good', 'payment completed'),
(121, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:30:01am', NULL, 'PAY5a5f17996fa7c', NULL, '2018-01-17 04:00:01', '2018-01-17 04:00:01', NULL, NULL, 'good', 'payment completed'),
(122, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '09:32:20am', NULL, 'PAY5a5f18241eeb3', NULL, '2018-01-17 04:02:20', '2018-01-17 04:02:20', NULL, NULL, 'good', 'payment completed'),
(123, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '10:37:12am', NULL, 'PAY5a5f27588fa0c', NULL, '2018-01-17 05:07:12', '2018-01-17 05:07:12', NULL, NULL, 'good', 'payment completed'),
(124, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '10:37:14am', NULL, 'PAY5a5f275a893da', NULL, '2018-01-17 05:07:14', '2018-01-17 05:07:14', NULL, NULL, 'good', 'payment completed'),
(125, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:29:41am', NULL, 'PAY5a5f33a511116', NULL, '2018-01-17 05:59:41', '2018-01-17 05:59:41', NULL, NULL, 'good', 'payment completed'),
(126, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:29:43am', NULL, 'PAY5a5f33a75c765', NULL, '2018-01-17 05:59:43', '2018-01-17 05:59:43', NULL, NULL, 'good', 'payment completed'),
(127, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:29:50am', NULL, 'PAY5a5f33ae7b2b8', NULL, '2018-01-17 05:59:50', '2018-01-17 05:59:50', NULL, NULL, 'good', 'payment completed'),
(128, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:29:51am', NULL, 'PAY5a5f33af4f2ae', NULL, '2018-01-17 05:59:51', '2018-01-17 05:59:51', NULL, NULL, 'good', 'payment completed'),
(129, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:29:52am', NULL, 'PAY5a5f33b02a0e4', NULL, '2018-01-17 05:59:52', '2018-01-17 05:59:52', NULL, NULL, 'good', 'payment completed'),
(130, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:30:29am', NULL, 'PAY5a5f33d5f3dd0', NULL, '2018-01-17 06:00:29', '2018-01-17 06:00:29', NULL, NULL, 'good', 'payment completed'),
(131, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:30:30am', NULL, 'PAY5a5f33d6df410', NULL, '2018-01-17 06:00:30', '2018-01-17 06:00:30', NULL, NULL, 'good', 'payment completed'),
(132, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:30:32am', NULL, 'PAY5a5f33d816986', NULL, '2018-01-17 06:00:32', '2018-01-17 06:00:32', NULL, NULL, 'good', 'payment completed'),
(133, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:30:35am', NULL, 'PAY5a5f33db02cc4', NULL, '2018-01-17 06:00:35', '2018-01-17 06:00:35', NULL, NULL, 'good', 'payment completed'),
(134, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:30:35am', NULL, 'PAY5a5f33dbcda3c', NULL, '2018-01-17 06:00:35', '2018-01-17 06:00:35', NULL, NULL, 'good', 'payment completed'),
(135, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:30:37am', NULL, 'PAY5a5f33dd33761', NULL, '2018-01-17 06:00:37', '2018-01-17 06:00:37', NULL, NULL, 'good', 'payment completed'),
(136, '12', '5', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-17', '11:32:19am', NULL, 'PAY5a5f34435e85f', NULL, '2018-01-17 06:02:19', '2018-01-17 06:02:19', NULL, NULL, 'good', 'payment completed'),
(137, '26', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-18', '08:24:58am', NULL, 'PAY5a6059da8f2ed', NULL, '2018-01-18 02:54:58', '2018-01-18 02:54:58', NULL, NULL, 'good', 'payment completed'),
(138, '26', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-18', '08:25:01am', NULL, 'PAY5a6059ddbc0a1', NULL, '2018-01-18 02:55:01', '2018-01-18 02:55:01', NULL, NULL, 'good', 'payment completed'),
(139, '26', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-18', '08:25:02am', NULL, 'PAY5a6059deefe7a', NULL, '2018-01-18 02:55:02', '2018-01-18 02:55:02', NULL, NULL, 'good', 'payment completed'),
(140, '26', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-18', '08:25:06am', NULL, 'PAY5a6059e2ee8aa', NULL, '2018-01-18 02:55:06', '2018-01-18 02:55:06', NULL, NULL, 'good', 'payment completed'),
(141, '26', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-18', '08:25:07am', NULL, 'PAY5a6059e3e64a0', NULL, '2018-01-18 02:55:07', '2018-01-18 02:55:07', NULL, NULL, 'good', 'payment completed'),
(142, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '05:33:27am', NULL, 'PAY5a66c92787cd0', NULL, '2018-01-23 00:03:27', '2018-01-23 00:03:27', NULL, NULL, 'good', 'visit completed'),
(143, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '05:33:29am', NULL, 'PAY5a66c9293b46f', NULL, '2018-01-23 00:03:29', '2018-01-23 00:03:29', NULL, NULL, 'good', 'payment completed'),
(144, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:33:30am', NULL, 'PAY5a66c92ab32c5', NULL, '2018-01-23 00:03:30', '2018-01-23 00:03:30', NULL, NULL, 'good', 'payment completed'),
(145, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:33:32am', NULL, 'PAY5a66c92c3a087', NULL, '2018-01-23 00:03:32', '2018-01-23 00:03:32', NULL, NULL, 'good', 'payment completed'),
(146, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:33:33am', NULL, 'PAY5a66c92d3c783', NULL, '2018-01-23 00:03:33', '2018-01-23 00:03:33', NULL, NULL, 'good', 'payment completed'),
(147, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:33:34am', NULL, 'PAY5a66c92e2a015', NULL, '2018-01-23 00:03:34', '2018-01-23 00:03:34', NULL, NULL, 'good', 'payment completed'),
(148, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:33:35am', NULL, 'PAY5a66c92f13541', NULL, '2018-01-23 00:03:35', '2018-01-23 00:03:35', NULL, NULL, 'good', 'payment completed'),
(149, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:33:35am', NULL, 'PAY5a66c92feee0a', NULL, '2018-01-23 00:03:35', '2018-01-23 00:03:35', NULL, NULL, 'good', 'payment completed'),
(150, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:36:02am', NULL, 'PAY5a66c9c287314', NULL, '2018-01-23 00:06:02', '2018-01-23 00:06:02', NULL, NULL, 'good', 'payment completed'),
(151, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:36:03am', NULL, 'PAY5a66c9c3b0302', NULL, '2018-01-23 00:06:03', '2018-01-23 00:06:03', NULL, NULL, 'good', 'payment completed'),
(152, '7', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '05:36:04am', NULL, 'PAY5a66c9c4e0b4e', NULL, '2018-01-23 00:06:04', '2018-01-23 00:06:04', NULL, NULL, 'good', 'payment completed'),
(153, '8', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:31:47am', NULL, 'PAY5a66d6d357290', NULL, '2018-01-23 01:01:47', '2018-01-23 01:01:47', NULL, NULL, 'good', 'payment completed'),
(154, '8', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:31:48am', NULL, 'PAY5a66d6d4c6bab', NULL, '2018-01-23 01:01:48', '2018-01-23 01:01:48', NULL, NULL, 'good', 'payment completed'),
(155, '8', '1', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:31:49am', NULL, 'PAY5a66d6d5d7097', NULL, '2018-01-23 01:01:49', '2018-01-23 01:01:49', NULL, NULL, 'good', 'payment completed'),
(156, '11', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:32:09am', NULL, 'PAY5a66d6e9991f9', NULL, '2018-01-23 01:02:09', '2018-01-23 01:02:09', NULL, NULL, 'good', 'payment completed'),
(157, '11', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:32:10am', NULL, 'PAY5a66d6ead2aba', NULL, '2018-01-23 01:02:10', '2018-01-23 01:02:10', NULL, NULL, 'good', 'payment completed'),
(158, '11', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:32:11am', NULL, 'PAY5a66d6eba7f33', NULL, '2018-01-23 01:02:11', '2018-01-23 01:02:11', NULL, NULL, 'good', 'payment completed'),
(159, '11', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:32:12am', NULL, 'PAY5a66d6ec9474b', NULL, '2018-01-23 01:02:12', '2018-01-23 01:02:12', NULL, NULL, 'good', 'payment completed'),
(160, '11', '3', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-23', '06:32:13am', NULL, 'PAY5a66d6ed5d964', NULL, '2018-01-23 01:02:13', '2018-01-23 01:02:13', NULL, NULL, 'good', 'payment completed'),
(161, '11', '9', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '11:18:53am', NULL, 'PAY5a686b9dbc9e0', NULL, '2018-01-24 05:48:53', '2018-01-24 05:48:53', NULL, NULL, 'good', 'payment completed'),
(162, '11', '9', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '11:18:58am', NULL, 'PAY5a686ba28b223', NULL, '2018-01-24 05:48:58', '2018-01-24 05:48:58', NULL, NULL, 'good', 'payment completed'),
(163, '11', '9', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '11:18:59am', NULL, 'PAY5a686ba37f831', NULL, '2018-01-24 05:48:59', '2018-01-24 05:48:59', NULL, NULL, 'good', 'payment completed'),
(164, '7', '9', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '11:19:28am', NULL, 'PAY5a686bc05446a', NULL, '2018-01-24 05:49:28', '2018-01-24 05:49:28', NULL, NULL, 'good', 'payment completed'),
(165, '7', '9', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '11:19:29am', NULL, 'PAY5a686bc182782', NULL, '2018-01-24 05:49:29', '2018-01-24 05:49:29', NULL, NULL, 'good', 'payment completed'),
(166, '7', '9', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '11:19:30am', NULL, 'PAY5a686bc283c56', NULL, '2018-01-24 05:49:30', '2018-01-24 05:49:30', NULL, NULL, 'good', 'payment completed'),
(167, '7', '9', NULL, NULL, 'pay', 'new task with out tid', NULL, '2018-01-24', '11:19:31am', NULL, 'PAY5a686bc3220d9', NULL, '2018-01-24 05:49:31', '2018-01-24 05:49:31', NULL, NULL, 'good', 'payment completed'),
(168, '14', '4', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-01-25', '08:39:20am', 'ODR5a6997b879e11', NULL, NULL, '2018-01-25 03:09:20', '2018-01-25 03:09:20', NULL, NULL, 'good', 'po completed'),
(169, '14', '4', NULL, NULL, 'sale', 'new order with out tid', NULL, '2018-02-02', '04:32:22am', 'ODR5a73e9d6c5bb9', NULL, NULL, '2018-02-01 23:02:22', '2018-02-01 23:02:22', NULL, NULL, 'good', 'po completed');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_role` int(50) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_role`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin1', 'admin1@gmail.com', '$2y$10$OQkkPxvNNhLVzpdUs946..ADHsL5gRYEi86pLih37D1sdlN6SH5Dm', '48JUTvNXGzIw7LU6lH1tlEALG54FI8BozvZiMjFkKvyuXgjmSm0sxfpuHlms', '2017-11-23 03:42:16', '2017-11-23 03:42:16'),
(7, 4, 'emp1', 'emp1@gmail.com', '$2y$10$smaKquyuNuFBI2tgTDcyUuyqVyl4nNmyJs2f/potHB9AXOwnOqKi6', '0Q4CYPOsoWJgKRPTy8HUHq4AyjffDU0RMABcAuxIziqgil88r3JlqC5sNfuC', '2017-11-28 05:59:11', '2018-01-12 01:18:21'),
(8, 4, 'emp5', 'emp5@gmail.com', '$2y$10$Agj3jurxDyHgQEhslPLVjeW7jYRwaLnL6ntddGbUdnfx4byRp3Zii', NULL, '2017-11-28 06:56:58', '2018-01-12 01:18:12'),
(10, 5, ' 	admin2', 'admin2@gmail.com', '$2y$10$V.c6FLs38Ki1RKYS9kF87.yGrXnRtwfnCYp0qjkfFGc9iA217rXVq', 'aLXP8CwcUZ8g17uIQF4cjzW0T1Jlsi1yCJCd9c96WZL8jTx0TZYVO1d2sNiY', '2017-12-01 02:07:39', '2018-01-14 22:20:05'),
(11, 4, 'employee1', 'employee1@gmail.com', '$2y$10$U/oc.FgDHFAy4G9A5pj3RuLhPUfBl4EV56jkPiExvcvgBU9ETAqJq', 'SzBUtn5RBF6vFT9xsVk6qAwdJUbmuNwF4YIkS6oMcJ3OaJnhuB8STPu4gb37', '2017-12-01 02:12:59', '2018-01-12 01:17:43'),
(12, 4, 'employee2', 'employee2@gmail.com', '$2y$10$PAxNd9R3G1yWsfihIkUBhup2OsqzwILH1M6JMmKFVwr4QUsawhR3O', 'J5wTjQY8m5UXSCjNFXPOsJtgUfTp6S1gJr6bMDMkiOosyjPxLhkGPAkepkON', '2017-12-01 02:13:53', '2018-01-12 01:17:20'),
(14, 4, 'emp7', 'emp7@gmail.com', '$2y$10$Lk9NMSgkgsuL/HnNm89OMOhdJ2Vp./e45ZNF0r/oS3hwCkQKqcrXS', 'mhwasenu6bSYtZYtewPZcrR6fBDNNIy78A63diURs0qxoGoGNG2uzf8GJWZa', '2017-12-23 02:58:06', '2018-01-12 01:16:14'),
(16, 4, 'employee 10', 'employee10@gmail.com', '$2y$10$F.9g/KcWQ7cqNfuhkIvirOgLE4ApGSoB0IRBVETqduS7SKLnI58du', NULL, '2018-01-12 04:14:15', '2018-01-13 01:56:22'),
(17, 4, 'employee 11', 'employee11@gmail.com', '$2y$10$emGalXMt7gkvWT9WiyjG0OGHqAe0sMLJqbNAAhKVYFT5hLAdwVq42', 'mfXfG8Z2yLOusB6JbUunJbbUsTwF4znS6u1TLpFTAYJ4vMdMSunHLDRF88GM', '2018-01-12 06:03:21', '2018-01-29 03:42:52'),
(18, 1, 'superadmin', 'superadmin@gmail.com', '$2y$10$Y/Ql02A5m6Tt.OG12ggCEelEfFc3wqCTbSb/lIgyhqw8hWL5Tv5Wi', 'zVQSIoJi7atUE8iBdA6keJqr8ggnlzauMhfl3P5H84e1eKIDCf4uwFYPYAce', '2018-01-13 01:29:50', '2018-01-23 04:39:12'),
(19, 4, 'Deepak K. K', 'deepak@gmail.com', '$2y$10$fSpq2eMMW12g9BKhkV9e.er8EVgTaX3ZHQs.eqKWeMo.xA/T4cFb.', '7YvfmP7djLNC9bBJt1WYlry7HtT0oq6WYK6IlcGWNRz4DFQCaN2VcMkHWYuE', '2018-01-13 02:13:11', '2018-01-13 02:14:34'),
(20, 4, 'Steve', 'steve@gmail.com', '$2y$10$mJLpIaUAkuyhFjrL2/IOUuxyXzShsJT8x3VN8iZMq1Cm4IwPIZzpy', NULL, '2018-01-14 22:52:34', '2018-01-14 22:52:34'),
(21, 4, 'Renjith', 'renjith@gmail.com', '$2y$10$vxWkT4aljBqmFnECBzBAx.8TlQ/1Kt8Fh9eOzoMpKADyMkU6vZ.tm', NULL, '2018-01-14 23:09:53', '2018-01-14 23:09:53'),
(23, 4, 'Ranju', 'ranju@gmail.com', '$2y$10$4kbKF1Ui9QDRBNPqrguxbeXOy59Ng1AuXfSa6PdAX2Rp1PNg3miEq', NULL, '2018-01-14 23:13:07', '2018-01-14 23:13:07'),
(24, 4, 'Mithun', 'mithun@gmail.com', '$2y$10$gTi3MSKxjkeC3HcGwu280.3oCDnymEIZYto1EqdyXV.FJAKQTycxa', NULL, '2018-01-14 23:14:12', '2018-01-14 23:14:12'),
(25, 4, 'Manjunath', 'manjunath@gmail.com', '$2y$10$UMElArCpzb52QCMchXSZyefVbwkRcI8aJoouGxYa6vEjQW.j9TXbW', NULL, '2018-01-14 23:18:13', '2018-01-14 23:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `user_informations`
--

CREATE TABLE `user_informations` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doj` date DEFAULT NULL,
  `designation` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_informations`
--

INSERT INTO `user_informations` (`id`, `empname`, `email`, `phone`, `doj`, `designation`, `photo`, `address`, `is_active`, `created_at`, `updated_at`) VALUES
('7', 'emp1', 'emp1@gmail.com', '5556677788', '2017-12-09', 'salesman', '1511868584interest.jpg', 'emp1 addresss', '1', '2017-11-28 05:59:11', '2018-01-12 06:06:48'),
('8', 'emp5', 'emp5@gmail.com', '6666677777', '2017-12-09', 'salesman', '1511872018interest.jpg', 'address', '1', '2017-11-28 06:56:58', '2018-01-12 06:06:35'),
('10', 'admin2', 'admin2@gmail.com', '8887755566', '2017-12-09', 'admin', '1512113859self-employment-ideas.jpg', 'address', '1', '2017-12-01 02:07:39', '2018-01-12 06:08:06'),
('11', 'employee1', 'employee1@gmail.com', '6666688888', '2017-12-09', 'salesman', '1512114179ems.jpg', 'employee1 Address', '1', '2017-12-01 02:12:59', '2018-01-12 06:06:23'),
('12', 'employee2', 'employee2@gmail.com', '4445566677', '2017-12-09', 'salesman', '1512114233industrial-loans.jpg', 'employee23 addresss', '1', '2017-12-01 02:13:53', '2018-01-12 06:06:13'),
('14', 'emp7', 'emp7@gmail.com', '7778866655', '2017-12-09', 'salesman', '1514017686ems.png', 'emp7 address', '1', '2017-12-23 02:58:06', '2018-01-12 06:05:11'),
('16', 'employee 10', 'employee10@gmail.com', '9877766655', '2017-01-09', 'salesman', '1515750255interest.jpg', 'emp 10 address', '1', '2018-01-12 04:14:15', '2018-01-12 06:06:00'),
('17', 'employee 11', 'employee11@gmail.com', '6667755566', '2018-01-11', 'salesman', '1515756801nabard.jpeg', 'employee 1 address', '1', '2018-01-12 06:03:21', '2018-01-12 06:03:21'),
('18', 'superadmin', 'superadmin@gmail.com', '7778899999', '2017-07-05', 'admin', '1515826790vyapara.jpg', 'company address  superadmin', '1', '2018-01-13 01:29:50', '2018-01-13 01:29:50'),
('19', 'Deepak K. K', 'deepak@gmail.com', '9847788888', '2018-01-12', 'salesman', '1515829391mort.jpg', 'deepak address', '1', '2018-01-13 02:13:11', '2018-01-13 02:13:11'),
('20', 'Steve', 'steve@gmail.com', '9899966677', '2018-01-10', 'salesman', '1515990154mort.jpg', 'steve addtress', '1', '2018-01-14 22:52:34', '2018-01-14 22:52:34'),
('24', 'Mithun', 'mithun@gmail.com', '9899966677', '2018-01-15', 'salesman', '1515991452goldloan1.jpg', 'mithun addresss', '1', '2018-01-14 23:14:12', '2018-01-14 23:14:12'),
('25', 'Manjunath', 'manjunath@gmail.com', '5556677788', '2018-01-13', 'salesman', '1515991693ems.png', 'manunath address', '1', '2018-01-14 23:18:13', '2018-01-14 23:18:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_in_hands`
--
ALTER TABLE `cash_in_hands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catagories`
--
ALTER TABLE `catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_management`
--
ALTER TABLE `leave_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_taken`
--
ALTER TABLE `leave_taken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `placeorders`
--
ALTER TABLE `placeorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcolors`
--
ALTER TABLE `productcolors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productsizes`
--
ALTER TABLE `productsizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_allot`
--
ALTER TABLE `store_allot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cash_in_hands`
--
ALTER TABLE `cash_in_hands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `catagories`
--
ALTER TABLE `catagories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `leave_management`
--
ALTER TABLE `leave_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `leave_taken`
--
ALTER TABLE `leave_taken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `placeorders`
--
ALTER TABLE `placeorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `productcolors`
--
ALTER TABLE `productcolors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `productsizes`
--
ALTER TABLE `productsizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `store_allot`
--
ALTER TABLE `store_allot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `targets`
--
ALTER TABLE `targets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
