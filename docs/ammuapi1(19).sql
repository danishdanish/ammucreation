-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2017 at 11:39 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ammuapi1`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand`, `bimage`, `created_at`, `updated_at`) VALUES
(2, 'Allen Solly Shirts', '1512463322thaali3.jpg', '2017-12-05 01:59:13', '2017-12-05 03:12:02'),
(3, 'Bata1', '1512462093Solidarity-Group-Loans-Large.jpg', '2017-12-05 02:01:47', '2017-12-05 02:51:33'),
(4, 'Zero Shirts', '1512462083goldloan1.jpg', '2017-12-05 02:02:44', '2017-12-05 02:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `catagories`
--

CREATE TABLE `catagories` (
  `id` int(10) UNSIGNED NOT NULL,
  `catagory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catagories`
--

INSERT INTO `catagories` (`id`, `catagory`, `cimage`, `created_at`, `updated_at`) VALUES
(1, 'T Shirts', '1512469509ems.png', NULL, '2017-12-05 04:55:09'),
(2, 'Shirts Cotton', '1512472910cash-credit-250x250.jpg', '2017-12-05 04:34:08', '2017-12-05 05:51:50');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `colorname`, `colorcode`, `created_at`, `updated_at`) VALUES
(1, 'Yellow', '#23df30', '2017-12-14 02:52:11', '2017-12-14 02:52:11'),
(2, 'Green', '#hhhj', '2017-12-14 02:52:34', '2017-12-14 02:52:34'),
(4, 'Pale yellow', '#dfb923', '2017-12-14 03:17:33', '2017-12-14 03:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2017_12_13_081600_create_stores_table', 1),
(19, '2017_12_13_102900_create_sizes_table', 2),
(20, '2017_12_14_081028_create_colors_table', 3),
(21, '2017_12_15_070132_create_tasks_table', 4),
(22, '2017_12_20_110508_add_fields_to_tasks', 5),
(23, '2017_12_21_091811_create_order_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `displayname`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Designation', 'Designation', 'Designation for  Employee', '2017-11-29 04:30:27', '2017-11-29 04:43:47'),
(2, 'Work Allot', 'Allotment', 'Work Allotment', '2017-11-30 01:02:18', '2017-11-30 01:02:18'),
(3, 'Products', 'Product s', 'Products', '2017-12-01 01:06:53', '2017-12-01 01:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `taskid`, `store`, `employee`, `quantity`, `billno`, `status`, `created_at`, `updated_at`) VALUES
(1, '12/21/2017', '11', 'Store3', 'employee2', '30', 'BILLNO55566', 'oncourier', '2017-12-21 04:35:33', '2017-12-22 05:35:44'),
(2, '12/22/2017', '10', 'Store3', 'employee2', '60', 'BILLNO9909', 'oncourier', '2017-12-22 04:42:27', '2017-12-22 05:46:04'),
(3, '12/22/2017', '5', 'Store3', 'employee1', '50', 'BILL8900', 'oncourier', '2017-12-22 05:06:24', '2017-12-22 05:42:21'),
(4, '12/22/2017', '3', NULL, 'employee1', '17', 'BILL66677', 'oncourier', '2017-12-22 05:08:06', '2017-12-22 05:08:06'),
(5, '12/22/2017', '1', NULL, 'employee1', '56', 'BILL8889', 'oncourier', '2017-12-22 05:09:04', '2017-12-22 05:09:04'),
(6, '12/22/2017', '6', 'Store3', 'employee1', '50', 'BILLNO5555', 'oncourier', '2017-12-22 05:42:50', '2017-12-22 05:42:50'),
(7, '12/22/2017', '2', 'Store1', 'employee1', '67', 'BIL888776', 'oncourier', '2017-12-22 05:52:40', '2017-12-22 05:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productcolors`
--

CREATE TABLE `productcolors` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productcolors`
--

INSERT INTO `productcolors` (`id`, `productid`, `colorname`, `colorcode`, `created_at`, `updated_at`) VALUES
(3, '5', 'Yellow', '#hhhj', '2017-12-09 03:22:29', '2017-12-09 03:22:29'),
(4, '2', 'Green', '#23df30', '2017-12-11 06:39:43', '2017-12-11 06:39:43');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priceunit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brandid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product`, `price`, `priceunit`, `brandid`, `catid`, `description`, `pimage`, `created_at`, `updated_at`) VALUES
(2, 'p1', '230', '56', '2', '1', 'desc', 'theme/image/product/1512621019mort.jpg', '2017-12-06 06:34:55', '2017-12-06 23:00:19'),
(5, 'p3', '45', '45', '2', '2', 'p3 desc', 'theme/image/product/1512621497.jpg', '2017-12-06 23:08:17', '2017-12-06 23:08:17'),
(6, 'p5', '67', '56', '4', '1', 'p5 desc', 'theme/image/product/1513069993.jpg', '2017-12-12 03:43:13', '2017-12-12 06:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `productsizes`
--

CREATE TABLE `productsizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productsizes`
--

INSERT INTO `productsizes` (`id`, `productid`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(2, '5', 'Medium', 'M', '2017-12-11 01:19:09', '2017-12-11 01:19:09'),
(3, '5', 'Small', 'S', '2017-12-11 01:20:52', '2017-12-11 01:20:52'),
(4, '2', 'Medium', 'M', '2017-12-11 05:37:39', '2017-12-11 05:37:39'),
(5, '2', 'Baby', 'BBY', '2017-12-11 05:37:51', '2017-12-11 05:37:51'),
(6, '2', 'Large', 'XL', '2017-12-12 03:42:44', '2017-12-12 03:42:44'),
(7, '6', 'Baby', 'BB', '2017-12-12 03:43:31', '2017-12-12 03:43:31'),
(8, '6', 'Large', 'XL', '2017-12-12 03:43:36', '2017-12-12 03:43:36');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modules` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `displayname`, `modules`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'For Administration full control', 'Admin', '1,2', '2017-11-29 02:00:28', '2017-12-01 00:57:50'),
(2, 'user', 'User', 'User', '1,2', '2017-11-30 01:18:20', '2017-12-01 00:58:38'),
(3, 'Role1', 'Role1 Desc', 'Role1', '2,3', '2017-12-01 01:05:54', '2017-12-02 05:14:30'),
(4, 'Employee', 'Employee role', 'Employee', '0', '2017-12-02 00:28:24', '2017-12-02 00:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(1, 'Medium', 'M', '2017-12-14 02:27:43', '2017-12-14 02:27:43'),
(2, 'Baby', 'BB', '2017-12-14 02:28:03', '2017-12-14 02:28:03'),
(4, '85', 'EF', '2017-12-14 02:38:56', '2017-12-14 02:38:56');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `contactname`, `contactno`, `address`, `location`, `zipcode`, `city`, `state`, `country`, `total`, `paid`, `balance`, `created_at`, `updated_at`) VALUES
(1, 'Store1', 'Mr. Jeevanand Raj', '8887766655', 'Adresss store 1', 'P M Kutty Road', '8776555', 'Calicut', 'Kerala', 'India', NULL, NULL, NULL, '2017-12-13 05:41:52', '2017-12-15 02:13:42'),
(3, 'Store3', 'Mr. Geo Jacob', '8888877766', 'Adresss store 2', 'Kaloor Stadium Junction', '677855566', 'Ernakulam', 'kerala', 'India', NULL, NULL, NULL, '2017-12-13 06:29:17', '2017-12-13 06:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE `targets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `torder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tfrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targets`
--

INSERT INTO `targets` (`id`, `user_id`, `user_name`, `sales`, `torder`, `tfrom`, `tto`, `created_at`, `updated_at`) VALUES
(9, '11', 'employee1', '80000', NULL, '11/01/2017', '12/01/2017', '2017-12-04 04:44:08', '2017-12-04 04:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeeid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `torder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tpayment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tremark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tstatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `employeeid`, `store`, `address`, `location`, `mode`, `description`, `mobile`, `tdate`, `ttime`, `torder`, `created_at`, `updated_at`, `tpayment`, `tremark`, `tstatus`) VALUES
(1, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '12/15/2017', '3.00am', '56', '2017-12-15 01:57:57', '2017-12-22 05:08:48', 'nil', 'good', 'ongoing'),
(2, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '12/22/2017', '3.00am', '67', '2017-12-15 01:58:16', '2017-12-22 05:52:12', 'nil', 'good', 'completed'),
(3, '11', '3', 'employee1 Address', 'Kaloor Stadium Junction', 'pay', 'desc12 emp12', '6666688888', '12/22/2017', '3.00am', '17', '2017-12-15 01:58:33', '2017-12-22 05:07:40', 'nil', 'satisfied', 'ongoing'),
(4, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '12/15/2017', '3.00am', '', '2017-12-15 01:58:53', '2017-12-15 01:58:53', '', '', ''),
(5, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'desc', '6666688888', '12/15/2017', '3.00am', '50', '2017-12-15 01:59:00', '2017-12-22 04:54:08', 'nil', 'good', 'ongoing'),
(6, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'Description', '6666688888', '12/30/2017', '5.00pm', '50', '2017-12-15 02:06:37', '2017-12-22 05:06:00', 'nil', 'good', 'completed'),
(7, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'sale', 'Desc', '6666688888', '12/29/2017', '3.00pm', '50', '2017-12-15 02:07:32', '2017-12-22 01:39:10', 'nil', 'good', 'completed'),
(10, '12', '3', 'employee23 addresss', 'Kaloor Stadium Junction', 'pay', 'desc12 emp12', '4445566677', '12/22/2017', '3.00am', '60', '2017-12-15 04:50:52', '2017-12-20 06:40:49', 'nil', 'good', 'ongoing'),
(11, '12', '3', 'Adresss store 3', 'Kaloor Stadium Junction', 'sale', 'sale', '4445566677', '12/22/2017', '4.00pm', '30', '2017-12-20 05:49:30', '2017-12-21 06:48:16', 'nil', 'good', 'completed'),
(12, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'gmlfmldg', '8887766655', '01/04/2018', '2.00am', NULL, '2017-12-23 03:01:39', '2017-12-23 03:01:39', NULL, NULL, 'ongoing'),
(13, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'gmlfmldg', '8887766655', '01/04/2018', '2.30am', NULL, '2017-12-23 03:01:54', '2017-12-23 03:01:54', NULL, NULL, 'ongoing'),
(14, '12', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'sale desc', '8887766655', '12/13/2017', '3.00pm', NULL, '2017-12-23 04:06:26', '2017-12-23 04:06:26', NULL, NULL, 'ongoing');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin1', 'admin1@gmail.com', '$2y$10$OQkkPxvNNhLVzpdUs946..ADHsL5gRYEi86pLih37D1sdlN6SH5Dm', 'Y8sOhEomND7XGQXMWlLBMbDsaIRskcTrujgVKlnZz2pHbA8KS4ozgbGdLCqO', '2017-11-23 03:42:16', '2017-11-23 03:42:16'),
(7, 'usr725509', 'emp1@gmail.com', '$2y$10$smaKquyuNuFBI2tgTDcyUuyqVyl4nNmyJs2f/potHB9AXOwnOqKi6', 'U2Bkn8lkX9zkGL7eHPBgfLp6tD40XuUtHj5q94QHozesKfNHRp7BaJpd4XIW', '2017-11-28 05:59:11', '2017-12-07 01:57:52'),
(8, 'usr136497', 'emp5@gmail.com', '$2y$10$Agj3jurxDyHgQEhslPLVjeW7jYRwaLnL6ntddGbUdnfx4byRp3Zii', NULL, '2017-11-28 06:56:58', '2017-11-28 06:56:58'),
(10, 'usr624571', 'admin2@gmail.com', '$2y$10$V.c6FLs38Ki1RKYS9kF87.yGrXnRtwfnCYp0qjkfFGc9iA217rXVq', 'IOfAnbIBnALETPpVOGXY5SjniTcL689wB5RkQqOn6PsskEHshtL5kXrwdLuG', '2017-12-01 02:07:39', '2017-12-01 02:07:39'),
(11, 'usr369800', 'employee1@gmail.com', '$2y$10$U/oc.FgDHFAy4G9A5pj3RuLhPUfBl4EV56jkPiExvcvgBU9ETAqJq', NULL, '2017-12-01 02:12:59', '2017-12-01 02:12:59'),
(12, 'usr780052', 'employee2@gmail.com', '$2y$10$PAxNd9R3G1yWsfihIkUBhup2OsqzwILH1M6JMmKFVwr4QUsawhR3O', NULL, '2017-12-01 02:13:53', '2017-12-01 02:13:53'),
(14, 'usr486226', 'emp7@gmail.com', '$2y$10$Lk9NMSgkgsuL/HnNm89OMOhdJ2Vp./e45ZNF0r/oS3hwCkQKqcrXS', NULL, '2017-12-23 02:58:06', '2017-12-23 02:58:06');

-- --------------------------------------------------------

--
-- Table structure for table `user_informations`
--

CREATE TABLE `user_informations` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doj` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` int(11) DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_informations`
--

INSERT INTO `user_informations` (`id`, `empname`, `user_role`, `email`, `phone`, `doj`, `designation`, `photo`, `address`, `is_active`, `created_at`, `updated_at`) VALUES
('7', 'emp1', '3', 'emp1@gmail.com', '5556677788', '11/03/2017', 2, '1511868584interest.jpg', 'emp1 addresss', '1', '2017-11-28 05:59:11', '2017-12-01 23:15:15'),
('8', 'emp5', '', 'emp5@gmail.com', '6666677777', '11/09/2017', 1, '1511872018interest.jpg', 'address', '1', '2017-11-28 06:56:58', '2017-11-29 04:44:43'),
('10', 'admin2', '3', 'admin2@gmail.com', '8887755566', '11/01/2017', 4, '1512113859self-employment-ideas.jpg', 'address', '1', '2017-12-01 02:07:39', '2017-12-02 00:27:28'),
('11', 'employee1', '4', 'employee1@gmail.com', '6666688888', '12/01/2017', 5, '1512114179ems.jpg', 'employee1 Address', '1', '2017-12-01 02:12:59', '2017-12-02 00:30:27'),
('12', 'employee2', '4', 'employee2@gmail.com', '4445566677', '12/02/2017', 6, '1512114233industrial-loans.jpg', 'employee23 addresss', '1', '2017-12-01 02:13:53', '2017-12-02 00:30:39'),
('14', 'emp7', '4', 'emp7@gmail.com', '7778866655', '12/21/2017', 6, '1514017686ems.png', 'emp7 address', '1', '2017-12-23 02:58:06', '2017-12-23 02:59:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catagories`
--
ALTER TABLE `catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `productcolors`
--
ALTER TABLE `productcolors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productsizes`
--
ALTER TABLE `productsizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `catagories`
--
ALTER TABLE `catagories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `productcolors`
--
ALTER TABLE `productcolors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `productsizes`
--
ALTER TABLE `productsizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `targets`
--
ALTER TABLE `targets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
