-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 06, 2018 at 09:35 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sysboonc_ammucre`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand`, `bimage`, `created_at`, `updated_at`) VALUES
(2, 'Allen Solly Shirts', '1512463322thaali3.jpg', '2017-12-05 01:59:13', '2017-12-05 03:12:02'),
(3, 'Bata1', '1512462093Solidarity-Group-Loans-Large.jpg', '2017-12-05 02:01:47', '2017-12-05 02:51:33'),
(4, 'Zero Shirts', '1512462083goldloan1.jpg', '2017-12-05 02:02:44', '2017-12-05 02:51:23'),
(5, 'Peter England', '1515224242place_orange.png', '2018-01-06 07:37:22', '2018-01-06 07:37:22');

-- --------------------------------------------------------

--
-- Table structure for table `catagories`
--

CREATE TABLE `catagories` (
  `id` int(10) UNSIGNED NOT NULL,
  `catagory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catagories`
--

INSERT INTO `catagories` (`id`, `catagory`, `cimage`, `created_at`, `updated_at`) VALUES
(1, 'T Shirts', '1512469509ems.png', NULL, '2017-12-05 04:55:09'),
(2, 'Shirts Cotton', '1512472910cash-credit-250x250.jpg', '2017-12-05 04:34:08', '2017-12-05 05:51:50'),
(3, 'cat1', '1515224301speciality_orange.png', '2018-01-06 07:38:21', '2018-01-06 07:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `colorname`, `colorcode`, `created_at`, `updated_at`) VALUES
(1, 'Yellow', '#e0d077', '2017-12-14 02:52:11', '2017-12-14 02:52:11'),
(4, 'Pale yellow', '#f5f4eb', '2017-12-14 03:17:33', '2017-12-14 03:17:33'),
(5, 'Red', '#ca3b5c', '2017-12-29 04:58:22', '2017-12-29 04:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2017_12_13_081600_create_stores_table', 1),
(19, '2017_12_13_102900_create_sizes_table', 2),
(20, '2017_12_14_081028_create_colors_table', 3),
(21, '2017_12_15_070132_create_tasks_table', 4),
(22, '2017_12_20_110508_add_fields_to_tasks', 5),
(23, '2017_12_21_091811_create_order_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `displayname`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Designation', 'Designation', 'Designation for  Employee', '2017-11-29 04:30:27', '2017-11-29 04:43:47'),
(2, 'Work Allot', 'Allotment', 'Work Allotment', '2017-11-30 01:02:18', '2017-11-30 01:02:18'),
(3, 'Products', 'Product s', 'Products', '2017-12-01 01:06:53', '2017-12-01 01:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `taskid`, `store`, `employee`, `quantity`, `billno`, `status`, `created_at`, `updated_at`) VALUES
(1, '12/21/2017', '11', 'Store3', 'employee2', '30', 'BILLNO55566', 'oncourier', '2017-12-21 04:35:33', '2017-12-22 05:35:44'),
(2, '12/22/2017', '10', 'Store3', 'employee2', '60', 'BILLNO9909', 'oncourier', '2017-12-22 04:42:27', '2017-12-22 05:46:04'),
(3, '12/22/2017', '5', 'Store3', 'employee1', '50', 'BILL8900', 'oncourier', '2017-12-22 05:06:24', '2017-12-22 05:42:21'),
(4, '12/22/2017', '3', NULL, 'employee1', '17', 'BILL66677', 'oncourier', '2017-12-22 05:08:06', '2017-12-22 05:08:06'),
(5, '12/22/2017', '1', NULL, 'employee1', '56', 'BILL8889', 'oncourier', '2017-12-22 05:09:04', '2017-12-22 05:09:04'),
(6, '12/22/2017', '6', 'Store3', 'employee1', '50', 'BILLNO5555', 'oncourier', '2017-12-22 05:42:50', '2017-12-22 05:42:50'),
(7, '12/22/2017', '2', 'Store1', 'employee1', '67', 'BIL888776', 'oncourier', '2017-12-22 05:52:40', '2017-12-22 05:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `date`, `taskid`, `employee`, `store`, `amount`, `created_at`, `updated_at`) VALUES
(5, '01/02/2018', '1', '14', '3', '100', '2018-01-02 00:09:29', '2018-01-02 00:09:29'),
(6, '01/02/2018', '1', '14', '3', '100', '2018-01-02 00:10:07', '2018-01-02 00:10:07'),
(7, '01/02/2018', '1', '14', '3', '100', '2018-01-02 00:11:50', '2018-01-02 00:11:50'),
(8, '01/02/2018', '1', '14', '4', '100000', '2018-01-02 06:41:02', '2018-01-02 06:41:02'),
(9, '01/02/2018', '1', '14', '4', '50000', '2018-01-02 06:48:48', '2018-01-02 06:48:48'),
(10, '01/02/2018', '17', '14', '1', '10', '2018-01-02 11:07:48', '2018-01-02 11:07:48'),
(11, '01/02/2018', '17', '14', '1', '900', '2018-01-02 11:09:48', '2018-01-02 11:09:48'),
(12, '01/02/2018', '4', '11', '1', '100', '2018-01-02 11:13:59', '2018-01-02 11:13:59'),
(13, '01/02/2018', '4', '11', '1', '100', '2018-01-02 11:16:05', '2018-01-02 11:16:05'),
(14, '01/02/2018', '4', '11', '1', '100', '2018-01-02 11:16:27', '2018-01-02 11:16:27'),
(15, '01/02/2018', '4', '11', '1', '100', '2018-01-02 11:18:29', '2018-01-02 11:18:29'),
(16, '01/02/2018', '4', '11', '1', '100', '2018-01-02 11:19:56', '2018-01-02 11:19:56'),
(17, '01/02/2018', '17', '14', '1', '589', '2018-01-02 11:23:28', '2018-01-02 11:23:28'),
(18, '01/02/2018', '17', '14', '1', '1000', '2018-01-02 11:25:11', '2018-01-02 11:25:11'),
(19, '01/02/2018', '17', '14', '1', '1000', '2018-01-02 11:28:22', '2018-01-02 11:28:22'),
(20, '01/02/2018', '17', '14', '1', '10000', '2018-01-02 11:28:30', '2018-01-02 11:28:30'),
(21, '01/02/2018', '16', '14', '3', '20000', '2018-01-02 11:30:36', '2018-01-02 11:30:36'),
(22, '01/02/2018', '18', '14', '5', '100000', '2018-01-02 14:39:43', '2018-01-02 14:39:43'),
(23, '01/02/2018', '21', '14', '5', '200000', '2018-01-02 14:40:11', '2018-01-02 14:40:11'),
(24, '01/04/2018', '19', '14', '6', '2000000000000', '2018-01-04 05:31:31', '2018-01-04 05:31:31'),
(25, '01/04/2018', '24', '14', '7', '1000', '2018-01-04 06:07:34', '2018-01-04 06:07:34'),
(26, '01/04/2018', '25', '14', '1', '5000', '2018-01-04 06:11:16', '2018-01-04 06:11:16'),
(27, '01/04/2018', '26', '14', '3', '100', '2018-01-04 06:14:05', '2018-01-04 06:14:05'),
(28, '01/04/2018', '27', '14', '3', '1', '2018-01-04 22:05:40', '2018-01-04 22:05:40'),
(29, '01/04/2018', '28', '14', '6', '1', '2018-01-04 22:06:06', '2018-01-04 22:06:06'),
(30, '01/04/2018', '29', '14', '6', '18000', '2018-01-04 22:06:25', '2018-01-04 22:06:25'),
(31, '01/05/2018', '30', '14', '1', '76000', '2018-01-05 07:00:11', '2018-01-05 07:00:11'),
(32, '01/05/2018', '31', '14', '3', '70101', '2018-01-05 07:01:49', '2018-01-05 07:01:49'),
(33, '01/05/2018', '38', '14', '4', '150000', '2018-01-05 10:21:22', '2018-01-05 10:21:22'),
(34, '01/05/2018', '32', '14', '3', '140202', '2018-01-05 11:25:59', '2018-01-05 11:25:59'),
(35, '01/05/2018', '36', '14', '8', '980000', '2018-01-05 11:44:34', '2018-01-05 11:44:34'),
(36, '01/05/2018', '42', '14', '1', '1000', '2018-01-05 14:26:45', '2018-01-05 14:26:45'),
(37, '01/06/2018', '45', '14', '7', '99000', '2018-01-06 08:21:03', '2018-01-06 08:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `placeorders`
--

CREATE TABLE `placeorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `placeorders`
--

INSERT INTO `placeorders` (`id`, `date`, `taskid`, `employee`, `store`, `product`, `quantity`, `size`, `color`, `created_at`, `updated_at`) VALUES
(22, '01/02/2018', '6', '11', '3', '56', '2', '6', '5', '2018-01-02 11:27:08', '2018-01-02 11:27:08'),
(23, '01/02/2018', '6', '11', '3', '57', '2', '7', '7', '2018-01-02 11:27:08', '2018-01-02 11:27:08'),
(24, '01/03/2018', '13', '14', '1', '2', '10', '4', '4', '2018-01-03 03:56:22', '2018-01-03 03:56:22'),
(25, '01/03/2018', '13', '14', '1', '2', '10', '5', '4', '2018-01-03 03:56:22', '2018-01-03 03:56:22'),
(26, '01/03/2018', '0', '14', '4', '2', '10', '11', '9', '2018-01-03 10:01:40', '2018-01-03 10:01:40'),
(27, '01/03/2018', '0', '14', '4', '7', '10', '9', '6', '2018-01-03 10:01:40', '2018-01-03 10:01:40'),
(28, '01/04/2018', '0', '14', '1', '2', '20', '12', '7', '2018-01-04 04:28:55', '2018-01-04 04:28:55'),
(29, '01/04/2018', '0', '14', '1', '9', '10', '14', '12', '2018-01-04 04:28:55', '2018-01-04 04:28:55'),
(30, '01/04/2018', '12', '14', '1', '2', '35', '11', '7', '2018-01-04 22:07:50', '2018-01-04 22:07:50'),
(31, '01/04/2018', '12', '14', '1', '2', '35', '12', '7', '2018-01-04 22:07:50', '2018-01-04 22:07:50'),
(32, '01/04/2018', '12', '14', '1', '2', '35', '11', '9', '2018-01-04 22:07:50', '2018-01-04 22:07:50'),
(33, '01/04/2018', '12', '14', '1', '2', '35', '12', '9', '2018-01-04 22:07:50', '2018-01-04 22:07:50'),
(34, '01/04/2018', '0', '14', '1', '9', '40', '14', '12', '2018-01-04 22:12:26', '2018-01-04 22:12:26'),
(35, '01/04/2018', '0', '14', '1', '2', '10', '12', '7', '2018-01-04 22:12:26', '2018-01-04 22:12:26'),
(36, '01/05/2018', '39', '14', '7', '7', '10', '9', '5', '2018-01-05 10:21:47', '2018-01-05 10:21:47'),
(37, '01/05/2018', '39', '14', '7', '7', '10', '10', '5', '2018-01-05 10:21:47', '2018-01-05 10:21:47'),
(38, '01/05/2018', '0', '14', '4', '2', '10', '11', '7', '2018-01-05 14:25:39', '2018-01-05 14:25:39'),
(39, '01/05/2018', '0', '14', '4', '2', '10', '12', '7', '2018-01-05 14:25:39', '2018-01-05 14:25:39'),
(40, '01/05/2018', '0', '14', '4', '2', '10', '11', '9', '2018-01-05 14:25:39', '2018-01-05 14:25:39'),
(41, '01/05/2018', '0', '14', '4', '2', '10', '12', '9', '2018-01-05 14:25:39', '2018-01-05 14:25:39'),
(42, '01/06/2018', '0', '14', '3', '7', '30', '10', '6', '2018-01-06 04:56:47', '2018-01-06 04:56:47'),
(43, '01/06/2018', '0', '14', '1', '7', '10', '10', '6', '2018-01-06 04:58:31', '2018-01-06 04:58:31'),
(44, '01/06/2018', '0', '14', '3', '2', '10', '12', '9', '2018-01-06 04:58:58', '2018-01-06 04:58:58'),
(45, '01/06/2018', '0', '14', '3', '2', '15', '11', '7', '2018-01-06 07:26:27', '2018-01-06 07:26:27'),
(46, '01/06/2018', '0', '14', '3', '2', '15', '12', '7', '2018-01-06 07:26:27', '2018-01-06 07:26:27'),
(47, '01/06/2018', '0', '14', '3', '2', '15', '11', '9', '2018-01-06 07:26:27', '2018-01-06 07:26:27'),
(48, '01/06/2018', '0', '14', '3', '2', '15', '12', '9', '2018-01-06 07:26:27', '2018-01-06 07:26:27'),
(49, '01/06/2018', '0', '14', '3', '10', '10', '15', '13', '2018-01-06 07:26:27', '2018-01-06 07:26:27');

-- --------------------------------------------------------

--
-- Table structure for table `productcolors`
--

CREATE TABLE `productcolors` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productcolors`
--

INSERT INTO `productcolors` (`id`, `productid`, `colorid`, `colorname`, `colorcode`, `created_at`, `updated_at`) VALUES
(3, '5', NULL, 'Yellow', '#hhhj', '2017-12-09 03:22:29', '2017-12-09 03:22:29'),
(5, '7', '1', 'Yellow', '#23df30', '2018-01-03 06:43:39', '2018-01-03 06:43:39'),
(6, '7', '4', 'Pale yellow', '#dfb923', '2018-01-03 08:11:38', '2018-01-03 08:11:38'),
(7, '2', '1', 'Yellow', '#e0d077', '2018-01-03 08:16:00', '2018-01-03 08:16:00'),
(9, '2', '5', 'Red', '#ca3b5c', '2018-01-03 08:22:58', '2018-01-03 08:22:58'),
(10, '8', '1', 'Yellow', '#e0d077', '2018-01-03 08:28:26', '2018-01-03 08:28:26'),
(11, '8', '5', 'Pale yellow', '#f5f4eb', '2018-01-03 08:29:07', '2018-01-03 08:29:07'),
(12, '9', '1', 'Yellow', '#e0d077', '2018-01-03 11:33:47', '2018-01-03 11:33:47'),
(13, '10', '1', 'Yellow', '#e0d077', '2018-01-06 07:24:07', '2018-01-06 07:24:07');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priceunit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brandid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product`, `price`, `priceunit`, `brandid`, `catid`, `description`, `pimage`, `created_at`, `updated_at`) VALUES
(2, 'p1', '230', '56', '2', '3', 'desc', 'theme/image/product/1512621019mort.jpg', '2017-12-06 06:34:55', '2018-01-06 07:39:51'),
(7, 'check product pr1', '1200', '1', '3', '2', 'desc check pr1', 'theme/image/product/1514961819.png', '2018-01-03 06:43:39', '2018-01-03 06:43:39'),
(8, 'Product 1', '200', 'Piece', '3', '2', 'Description', 'theme/image/product/1514968106.png', '2018-01-03 08:28:26', '2018-01-03 08:28:26'),
(9, 'Product 3', '350', 'Piece', '4', '1', 'Product 3 description', 'theme/image/product/1514979227.png', '2018-01-03 11:33:47', '2018-01-03 11:33:47'),
(10, 'Prod', '400', 'Box', '3', '1', 'Descr', 'theme/image/product/1515223447.png', '2018-01-06 07:24:07', '2018-01-06 07:24:07');

-- --------------------------------------------------------

--
-- Table structure for table `productsizes`
--

CREATE TABLE `productsizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizeid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productsizes`
--

INSERT INTO `productsizes` (`id`, `productid`, `sizeid`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(2, '5', NULL, 'Medium', 'M', '2017-12-11 01:19:09', '2017-12-11 01:19:09'),
(3, '5', NULL, 'Small', 'S', '2017-12-11 01:20:52', '2017-12-11 01:20:52'),
(7, '6', NULL, 'Baby', 'BB', '2017-12-12 03:43:31', '2017-12-12 03:43:31'),
(8, '6', NULL, 'Large', 'XL', '2017-12-12 03:43:36', '2017-12-12 03:43:36'),
(9, '7', '2', 'Baby', 'BB', '2018-01-03 06:43:39', '2018-01-03 06:43:39'),
(10, '7', '1', 'Medium', 'M', '2018-01-03 08:10:32', '2018-01-03 08:10:32'),
(11, '2', '2', 'Baby', 'BB', '2018-01-03 08:14:14', '2018-01-03 08:14:14'),
(12, '2', '1', 'Medium', 'M', '2018-01-03 08:14:23', '2018-01-03 08:14:23'),
(13, '8', '1', 'Medium', 'M', '2018-01-03 08:28:26', '2018-01-03 08:28:26'),
(14, '9', '4', '85', 'EF', '2018-01-03 11:33:47', '2018-01-03 11:33:47'),
(15, '10', '1', 'Medium', 'M', '2018-01-06 07:24:07', '2018-01-06 07:24:07');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modules` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `displayname`, `modules`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'For Administration full control', 'Admin', '1,2', '2017-11-29 02:00:28', '2017-12-01 00:57:50'),
(2, 'user', 'User', 'User', '1,2', '2017-11-30 01:18:20', '2017-12-01 00:58:38'),
(3, 'Role1', 'Role1 Desc', 'Role1', '2,3', '2017-12-01 01:05:54', '2017-12-02 05:14:30'),
(4, 'Employee', 'Employee role', 'Employee', '0', '2017-12-02 00:28:24', '2017-12-02 00:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `sizename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizecode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `sizename`, `sizecode`, `created_at`, `updated_at`) VALUES
(1, 'Medium', 'M', '2017-12-14 02:27:43', '2017-12-14 02:27:43'),
(2, 'Baby', 'BB', '2017-12-14 02:28:03', '2017-12-14 02:28:03'),
(4, '85', 'EF', '2017-12-14 02:38:56', '2017-12-14 02:38:56'),
(5, 'Pink', '#009', '2018-01-06 07:39:03', '2018-01-06 07:39:03');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `description`, `contactname`, `contactno`, `address`, `location`, `zipcode`, `city`, `state`, `country`, `total`, `paid`, `balance`, `created_at`, `updated_at`) VALUES
(1, 'Store1', 'Sub Store', 'Mr. Jeevanand Raj', '8887766655', 'Adresss store 1', 'P M Kutty Road', '8776555', 'Calicut', 'Kerala', 'India', '100000', '2000', '98000', '2017-12-13 05:41:52', '2018-01-05 14:26:45'),
(3, 'Store3', 'Main Store', 'Mr. Geo Jacob', '8888877766', 'Adresss store 2', 'Kaloor Stadium Junction', '677855566', 'Ernakulam', 'kerala', 'India', '100000', '200', '99800', '2017-12-13 06:29:17', '2018-01-05 11:48:01'),
(4, 'store 5', 'Main Store', 'Mr. Naveen', '8887766655', 'Adresss store 5', 'Thiruvananthapuram', '677855566', 'Ernakulam', 'Kerala', 'India', '400000', '2000', '398000', '2018-01-02 06:35:26', '2018-01-05 11:47:18'),
(5, 'store 6', 'new store', 'Mr. Jeeva', '7778899955', 'address store 6', 'Near KSRTC', '673003', 'Palakkad', 'Kerala', 'India', '500000', '400000', '100000', '2018-01-02 07:08:30', '2018-01-02 14:40:11'),
(6, 'store 7', 'new store kannur', 'Mr. Sandeep', '7778899955', 'address store 7', 'Thane', '673003', 'Kannur', 'Kerala', 'India', '400000', '200000', '200000', '2018-01-02 07:11:15', '2018-01-05 06:30:49'),
(7, 'store 9', 'new store 9', 'Mr. Naveen', '8887766655', 'address store 9', 'Caltex  junction', '55566777', 'Kannur', 'Kerala', 'India', '400000', '200000', '200000', '2018-01-02 07:24:28', '2018-01-06 08:21:03'),
(8, 'Store Test', 'des', 'name', '1236547890', 'add', 'loc', '852369', 'dis', 'state', 'country', '10000000', '1000000', '9000000', '2018-01-02 09:52:52', '2018-01-05 11:44:34');

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE `targets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `torder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tfrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targets`
--

INSERT INTO `targets` (`id`, `user_id`, `user_name`, `sales`, `torder`, `tfrom`, `tto`, `created_at`, `updated_at`) VALUES
(9, '11', 'employee1', '80000', NULL, '11/01/2017', '12/01/2017', '2017-12-04 04:44:08', '2017-12-04 04:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeeid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `torder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tpayment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tremark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tstatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `employeeid`, `store`, `address`, `location`, `mode`, `description`, `mobile`, `tdate`, `ttime`, `torder`, `created_at`, `updated_at`, `tpayment`, `tremark`, `tstatus`) VALUES
(1, '12', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '12/15/2017', '3:20am', '56', '2017-12-15 01:57:57', '2018-01-02 09:14:21', 'nil', 'good', 'ongoing'),
(2, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '12/22/2017', '3.00am', '67', '2017-12-15 01:58:16', '2017-12-22 05:52:12', 'nil', 'good', 'completed'),
(3, '11', '3', 'employee1 Address', 'Kaloor Stadium Junction', 'pay', 'desc12 emp12', '6666688888', '12/22/2017', '3.00am', '17', '2017-12-15 01:58:33', '2017-12-22 05:07:40', 'nil', 'satisfied', 'ongoing'),
(4, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'pay', 'desc', '6666688888', '12/15/2017', '3.00am', '', '2017-12-15 01:58:53', '2018-01-02 11:19:56', '', '', 'completed'),
(5, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'desc', '6666688888', '12/15/2017', '3.00am', '50', '2017-12-15 01:59:00', '2017-12-22 04:54:08', 'nil', 'good', 'ongoing'),
(6, '11', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'sale', 'Description', '6666688888', '12/30/2017', '5.00pm', '50', '2017-12-15 02:06:37', '2018-01-02 11:27:08', 'nil', 'good', 'completed'),
(7, '11', '1', 'Adresss store 1  new', 'P M Kutty Road', 'sale', 'Desc', '6666688888', '12/29/2017', '3.00pm', '50', '2017-12-15 02:07:32', '2017-12-22 01:39:10', 'nil', 'good', 'completed'),
(10, '12', '3', 'employee23 addresss', 'Kaloor Stadium Junction', 'pay', 'desc12 emp12', '4445566677', '12/22/2017', '3.00am', '60', '2017-12-15 04:50:52', '2017-12-20 06:40:49', 'nil', 'good', 'ongoing'),
(11, '12', '3', 'Adresss store 3', 'Kaloor Stadium Junction', 'sale', 'sale', '4445566677', '12/22/2017', '4.00pm', '30', '2017-12-20 05:49:30', '2017-12-21 06:48:16', 'nil', 'good', 'completed'),
(12, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'gmlfmldg', '8887766655', '01/06/2018', '9:54 AM', NULL, '2017-12-23 03:01:39', '2018-01-04 22:07:51', NULL, NULL, 'completed'),
(13, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'gmlfmldg', '8887766655', '01/04/2018', '2.30am', NULL, '2017-12-23 03:01:54', '2018-01-03 03:56:22', NULL, NULL, 'completed'),
(14, '12', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'sale desc', '8887766655', '12/13/2017', '3.00pm', NULL, '2017-12-23 04:06:26', '2017-12-23 04:06:26', NULL, NULL, 'ongoing'),
(15, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'sale', 'desc12 emp7', '7778866655', '12/26/2017', '3.00pm', NULL, '2017-12-23 10:36:41', '2017-12-23 10:36:41', NULL, NULL, 'ongoing'),
(16, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'collect payment', '8888877766', '01/24/2018', '3.01pm', NULL, '2018-01-01 05:19:40', '2018-01-02 11:30:36', NULL, NULL, 'completed'),
(17, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'collect payment', '8887766655', '01/24/2018', '2.59pm', NULL, '2018-01-01 05:20:19', '2018-01-02 11:23:28', NULL, NULL, 'completed'),
(18, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'collect pay', '7778899955', '01/28/2018', '6:03 PM', NULL, '2018-01-02 11:41:26', '2018-01-02 14:39:43', NULL, NULL, 'completed'),
(19, '14', '6', 'address store 7', 'Thane', 'pay', 'sale 100 nos', '7778899955', '01/26/2018', '4.00pm', NULL, '2018-01-02 11:41:58', '2018-01-04 05:31:31', NULL, NULL, 'completed'),
(20, '12', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'sale', '8887766655', '02/07/2018', '2.30pm', NULL, '2018-01-02 11:42:47', '2018-01-02 11:42:47', NULL, NULL, 'ongoing'),
(21, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'sale', '7778899955', '01/06/2018', '4:06 AM', NULL, '2018-01-02 11:43:27', '2018-01-02 14:40:11', NULL, NULL, 'completed'),
(22, '12', '7', 'address store 9', 'Caltex  junction', 'sale', 'sale', '8887766655', '02/28/2018', '12.30pm', NULL, '2018-01-02 11:43:52', '2018-01-02 11:43:52', NULL, NULL, 'ongoing'),
(23, '12', '5', 'address store 6', 'Near KSRTC', 'sale', 'sale 20 nos', '7778899955', '02/06/2018', '2.20pm', NULL, '2018-01-02 11:44:54', '2018-01-02 11:44:54', NULL, NULL, 'ongoing'),
(24, '14', '7', 'address store 9', 'Caltex  junction', 'pay', 'Payment Description', '8887766655', '01/27/2018', '10.20 AM', NULL, '2018-01-04 05:33:33', '2018-01-04 06:07:34', NULL, NULL, 'completed'),
(25, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'Decription will be shown here', '8887766655', '01/18/2018', '10.20 AM', NULL, '2018-01-04 06:08:52', '2018-01-04 06:11:16', NULL, NULL, 'completed'),
(26, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'Description will be shown here', '8888877766', '01/23/2018', '100.20 AM', NULL, '2018-01-04 06:12:16', '2018-01-04 06:14:06', NULL, NULL, 'completed'),
(27, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'Description will be shown here', '8888877766', '01/09/2018', '10.20 AM', NULL, '2018-01-04 06:14:23', '2018-01-04 22:05:40', NULL, NULL, 'completed'),
(28, '14', '6', 'address store 7', 'Thane', 'pay', 'Description will be shown here', '7778899955', '01/01/2018', '10.20 AM', NULL, '2018-01-04 06:15:19', '2018-01-04 22:06:06', NULL, NULL, 'completed'),
(29, '14', '6', 'address store 7', 'Thane', 'pay', 'Description will be shown here', '7778899955', '01/02/2018', '10.20 AM', NULL, '2018-01-04 06:16:16', '2018-01-04 22:06:25', NULL, NULL, 'completed'),
(30, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'des', '8887766655', '01/05/2018', '12.20AM', NULL, '2018-01-05 06:39:47', '2018-01-05 07:00:11', NULL, NULL, 'completed'),
(31, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'des', '8888877766', '01/05/2018', '1.20 PM', NULL, '2018-01-05 06:40:28', '2018-01-05 07:01:49', NULL, NULL, 'completed'),
(32, '14', '3', 'Adresss store 2', 'Kaloor Stadium Junction', 'pay', 'des', '8888877766', '12/29/2017', '1.20 PM', NULL, '2018-01-05 07:05:48', '2018-01-05 11:25:59', NULL, NULL, 'completed'),
(33, '14', '6', 'address store 7', 'Thane', 'pay', 'des', '7778899955', '12/01/2017', '1.20 PM', NULL, '2018-01-05 07:06:25', '2018-01-05 07:06:25', NULL, NULL, 'ongoing'),
(34, '14', '6', 'address store 7', 'Thane', 'pay', 'des', '7778899955', '12/01/2017', '1.20 PM', NULL, '2018-01-05 07:06:42', '2018-01-05 07:06:42', NULL, NULL, 'ongoing'),
(35, '14', '6', 'address store 7', 'Thane', 'pay', 'des', '7778899955', '08/16/2017', '1.20 PM', NULL, '2018-01-05 07:06:56', '2018-01-05 07:06:56', NULL, NULL, 'ongoing'),
(36, '14', '8', 'add', 'loc', 'pay', 'des', '1236547890', '01/17/2018', '1.20 PM', NULL, '2018-01-05 07:08:36', '2018-01-05 11:44:34', NULL, NULL, 'completed'),
(37, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'dfdsfsdfsdfs', '8887766655', '01/11/2018', '1.00 PM', NULL, '2018-01-05 09:21:16', '2018-01-05 09:21:16', NULL, NULL, 'ongoing'),
(38, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'pay', 'dfdsfsdfsdfs', '8887766655', '01/05/2018', '1.00 PM', NULL, '2018-01-05 09:21:35', '2018-01-05 10:21:22', NULL, NULL, 'completed'),
(39, '14', '7', 'address store 9', 'Caltex  junction', 'sale', 'dfdsfsdfsdfs', '8887766655', '01/05/2018', '1.00 PM', NULL, '2018-01-05 09:21:52', '2018-01-05 10:21:47', NULL, NULL, 'completed'),
(40, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'pay', 'description', '8887766655', '01/05/2018', '8.00 PM', NULL, '2018-01-05 11:45:51', '2018-01-05 11:45:51', NULL, NULL, 'ongoing'),
(41, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'description', '7778899955', '01/08/2018', '8.00 PM', NULL, '2018-01-05 11:46:04', '2018-01-05 11:46:04', NULL, NULL, 'ongoing'),
(42, '14', '1', 'Adresss store 1', 'P M Kutty Road', 'pay', 'description', '8887766655', '01/10/2018', '8.00 PM', NULL, '2018-01-05 11:46:19', '2018-01-05 14:26:45', NULL, NULL, 'completed'),
(43, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'sale', 'Sales details', '8887766655', '01/06/2018', '10.00 AM', NULL, '2018-01-05 11:50:37', '2018-01-05 11:50:37', NULL, NULL, 'ongoing'),
(44, '14', '6', 'address store 7', 'Thane', 'sale', 'Sales details', '7778899955', '01/06/2018', '11.00 AM', NULL, '2018-01-05 11:50:55', '2018-01-05 11:50:55', NULL, NULL, 'ongoing'),
(45, '14', '7', 'address store 9', 'Caltex  junction', 'pay', 'Sales details', '8887766655', '01/07/2018', '11.00 AM', NULL, '2018-01-05 11:51:29', '2018-01-06 08:21:03', NULL, NULL, 'completed'),
(46, '14', '5', 'address store 6', 'Near KSRTC', 'sale', 'Sales details', '7778899955', '01/12/2018', '11.00 AM', NULL, '2018-01-05 11:52:19', '2018-01-05 11:52:19', NULL, NULL, 'ongoing'),
(47, '14', '5', 'address store 6', 'Near KSRTC', 'pay', 'Sales details', '7778899955', '01/16/2018', '9.00 AM', NULL, '2018-01-05 11:52:42', '2018-01-05 11:52:42', NULL, NULL, 'ongoing'),
(48, '14', '4', 'Adresss store 5', 'Thiruvananthapuram', 'pay', 'To collect', '8887766655', '01/08/2018', '10.00pm', NULL, '2018-01-06 07:42:01', '2018-01-06 07:42:01', NULL, NULL, 'ongoing');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin1', 'admin1@gmail.com', '$2y$10$OQkkPxvNNhLVzpdUs946..ADHsL5gRYEi86pLih37D1sdlN6SH5Dm', '382pBdGuJDoRo6xW46QaL9XUB4IR6yKMMt7aQf9RhzUI3CczchNSHIWEMUlz', '2017-11-23 03:42:16', '2017-11-23 03:42:16'),
(7, 'usr725509', 'emp1@gmail.com', '$2y$10$smaKquyuNuFBI2tgTDcyUuyqVyl4nNmyJs2f/potHB9AXOwnOqKi6', '0Q4CYPOsoWJgKRPTy8HUHq4AyjffDU0RMABcAuxIziqgil88r3JlqC5sNfuC', '2017-11-28 05:59:11', '2017-12-07 01:57:52'),
(8, 'usr136497', 'emp5@gmail.com', '$2y$10$Agj3jurxDyHgQEhslPLVjeW7jYRwaLnL6ntddGbUdnfx4byRp3Zii', NULL, '2017-11-28 06:56:58', '2017-11-28 06:56:58'),
(10, 'usr624571', 'admin2@gmail.com', '$2y$10$V.c6FLs38Ki1RKYS9kF87.yGrXnRtwfnCYp0qjkfFGc9iA217rXVq', 'IOfAnbIBnALETPpVOGXY5SjniTcL689wB5RkQqOn6PsskEHshtL5kXrwdLuG', '2017-12-01 02:07:39', '2017-12-01 02:07:39'),
(11, 'usr369800', 'employee1@gmail.com', '$2y$10$U/oc.FgDHFAy4G9A5pj3RuLhPUfBl4EV56jkPiExvcvgBU9ETAqJq', NULL, '2017-12-01 02:12:59', '2017-12-01 02:12:59'),
(12, 'usr780052', 'employee2@gmail.com', '$2y$10$PAxNd9R3G1yWsfihIkUBhup2OsqzwILH1M6JMmKFVwr4QUsawhR3O', NULL, '2017-12-01 02:13:53', '2017-12-01 02:13:53'),
(14, 'usr486226', 'emp7@gmail.com', '$2y$10$Lk9NMSgkgsuL/HnNm89OMOhdJ2Vp./e45ZNF0r/oS3hwCkQKqcrXS', 'ceZx9rnfery2VF6DD7DcyDhus7YKeVj0qf1wnTlnqVA63GM33hA2eeZljBEI', '2017-12-23 02:58:06', '2017-12-23 02:58:06');

-- --------------------------------------------------------

--
-- Table structure for table `user_informations`
--

CREATE TABLE `user_informations` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doj` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` int(11) DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_informations`
--

INSERT INTO `user_informations` (`id`, `empname`, `user_role`, `email`, `phone`, `doj`, `designation`, `photo`, `address`, `is_active`, `created_at`, `updated_at`) VALUES
('7', 'emp1', '3', 'emp1@gmail.com', '5556677788', '11/03/2017', 2, '1511868584interest.jpg', 'emp1 addresss', '1', '2017-11-28 05:59:11', '2017-12-01 23:15:15'),
('8', 'emp5', '', 'emp5@gmail.com', '6666677777', '11/09/2017', 1, '1511872018interest.jpg', 'address', '1', '2017-11-28 06:56:58', '2017-11-29 04:44:43'),
('10', 'admin2', '3', 'admin2@gmail.com', '8887755566', '11/01/2017', 4, '1512113859self-employment-ideas.jpg', 'address', '1', '2017-12-01 02:07:39', '2017-12-02 00:27:28'),
('11', 'employee1', '4', 'employee1@gmail.com', '6666688888', '12/01/2017', 5, '1512114179ems.jpg', 'employee1 Address', '1', '2017-12-01 02:12:59', '2017-12-02 00:30:27'),
('12', 'employee2', '4', 'employee2@gmail.com', '4445566677', '12/02/2017', 6, '1512114233industrial-loans.jpg', 'employee23 addresss', '1', '2017-12-01 02:13:53', '2017-12-02 00:30:39'),
('14', 'emp7', '4', 'emp7@gmail.com', '7778866655', '12/21/2017', 6, '1514017686ems.png', 'emp7 address', '1', '2017-12-23 02:58:06', '2017-12-23 02:59:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catagories`
--
ALTER TABLE `catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `placeorders`
--
ALTER TABLE `placeorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcolors`
--
ALTER TABLE `productcolors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productsizes`
--
ALTER TABLE `productsizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `catagories`
--
ALTER TABLE `catagories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `placeorders`
--
ALTER TABLE `placeorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `productcolors`
--
ALTER TABLE `productcolors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `productsizes`
--
ALTER TABLE `productsizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `targets`
--
ALTER TABLE `targets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
