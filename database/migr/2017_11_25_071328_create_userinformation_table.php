<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserinformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('user_informations', function (Blueprint $table) {
            $table->string('id');
            $table->string('empname',50);
            $table->string('email',30);
            $table->string('phone',15);
            $table->string('doj',15);
            $table->integer('designation')->nullable();
            $table->text('photo')->nullable();
            $table->text('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('user_informations');
    }
}
