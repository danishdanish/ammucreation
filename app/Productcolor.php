<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productcolor extends Model
{
  protected $fillable = [
        'productid', 'colorname', 'colorcode','colorid','productcode',
    ];



  protected $hidden = ['created_at','updated_at' ];
}
