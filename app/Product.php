<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = ['product','price','priceunit','brandid','catid','description','pimage','itemcode','active','productcode'];  
  protected $hidden = ['created_at','updated_at'];
}
