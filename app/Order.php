<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
protected $fillable = ['date','taskid','store','employee','quantity','billno','status'];  
  protected $hidden = ['created_at','updated_at'];
}
