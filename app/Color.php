<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
 protected $fillable = ['colorname','colorcode'];  
  protected $hidden = ['created_at','updated_at'];
}
