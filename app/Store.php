<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
 protected $fillable = ['name','contactname','contactno','address','location','zipcode','city','state','country','total','paid','balance','description','gst'];  
  protected $hidden = ['created_at','updated_at'];
}
