<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
 protected $fillable = ['sizename','sizecode'];  
  protected $hidden = ['created_at','updated_at'];
}
