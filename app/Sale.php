<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
 protected $fillable = ['date','orderid','employee','sales','store'];  
  protected $hidden = ['created_at','updated_at'];
}
