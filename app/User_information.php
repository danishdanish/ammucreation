<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_information extends Model
{

  protected $fillable = ['username', 'email', 'phone','doj','designation','photo', 'address','is_active'];
 
}
