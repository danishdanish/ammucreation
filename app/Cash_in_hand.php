<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash_in_hand extends Model
{
  protected $fillable = ['date','employee','amount','cheque','store','transfer','transferdate','mode'];


    protected $hidden = ['created_at','updated_at'];
}
