<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productsize extends Model
{
  protected $fillable = [
        'productid', 'sizename', 'sizecode','sizeid','productcode'
    ];



  protected $hidden = ['created_at','updated_at' ];
}
