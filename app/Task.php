<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
protected $fillable = ['employeeid','store','address','location','mode','description','mobile','tdate','ttime','torder','tpayment','tremark','tstatus','interest','paymentid','placeid'];  
  protected $hidden = ['created_at','updated_at'];
}
