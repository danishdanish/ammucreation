<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
   protected $fillable = ['date','taskid','employee','store','amount','paymentid','bank','cheque','cdate','mode'];    
  protected $hidden = ['created_at','updated_at'];
}
