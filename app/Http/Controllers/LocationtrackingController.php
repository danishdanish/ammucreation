<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User_information;



class LocationtrackingController extends Controller
{

	public function employee_list()
	{
		return view('pages.location.list');
	}
    
    public function get_location($id)
    {
    	$user = User_information::where('id',$id)->first();

    	$data['employee_name'] = $user->empname;

    	$data['employee'] = $id;

    	return view('pages.location.details',$data);
    }

    public function ajax_data()
  {
          
           $user =  User_information::get();
            
            $i=1;

            foreach ($user as $u) {

            	
             
                        $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$u->empname,
                               'phone'=>$u->phone,
                                 'actions' =>'<div class="action_div"><a href="location/details/'.$u->id.'"><button class="btn btn-info" title="view"><span class="glyphicon glyphicon-eye-open"></span></button></a></div>'                   
                               );

            }

      if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }


}
