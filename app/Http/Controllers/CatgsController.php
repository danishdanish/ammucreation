<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Target;
 use App\Catagory;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;



    class CatgsController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


           public function catgs_list()
        {
            
            return view('pages/catagory/list');
        }

        public function ajax_data()
        {
          
            
             $user = Catagory::orderBy('active','DESC')->get();
            
            $i=1;

            foreach ($user as $u) {
             $image =  '<img width="80px" height="80px" src="'.url('theme/image/catagory/'.$u->cimage).'"/>';
                        $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$u->catagory,
                               'image' =>$image, 
                               'active' =>$u->active,                            
                                   'actions' =>'<a href="catagory/catgs_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button>'                   
                               );

            }

             $response = array(
                           "draw" => 0,
                           "recordsTotal" => count($row),
                           "recordsFiltered" => count($row),
                           "data" => $row
        );
        echo json_encode($response);

        }


            public function catgs_add()
    {
        return view('pages/catagory/add');
    }

    public function insert_catgs_data(Request $request)
    {


             $this->validate($request,[
                                'catagory'=>'required',                             
                               ],[
                                   'catagory.required' =>'Catagory Name is required'
                                
                               ]);
  
      
    
        if($request->catgs_image!='')
        {
            $file = $request->catgs_image;

            $file_name = time().$file->getClientOriginalName();

            $path =  public_path('/theme/image/catagory/');

            $file->move($path,$file_name);
            
        } 

        $cat = new Catagory;
        $cat->catagory = $request->catagory;
        $cat->cimage = $request->catgs_image!=''?$file_name:$request->catgs_image;   
        $cat->save();

      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();

    }
       


  public function edit_catgs_data($id){
       $catgs['catgs'] = Catagory::where('id',$id)->first();
        return view('pages/catagory/edit',$catgs); 
       }



   public function post_edit_catgs_data(Request $request)
    {

        $this->validate($request,[
                                'catagory'=>'required',
                               
                               ],[
                                   'catagory.required' =>'Catagory Name is required'                                
                               ]); 
         $id = $request->id;
         $data = Catagory::find($id);
         $input = $request->all();
     
       
         if($request->cimage!='')
        {
            $file = $request->cimage;

            $file_name = time().$file->getClientOriginalName();

            $path =  public_path('/theme/image/catagory/');

            $file->move($path,$file_name);
             $input['cimage'] = $file_name;
            File::delete('theme/image/catagory/'.$data->cimage);          
            
    } 
         $data->fill($input)->save();
         Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
    }


           public function delete_catgs_data(Request $request){
        $id = $request->id;
        Catagory::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }





    }
