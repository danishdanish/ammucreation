<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User_information;

use App\Store ;

use DB;

use Session;

class AllotstoreController extends Controller
{
    
    public function allot_store_list()
    {

    	$data['employee'] = User_information::get();

    	$data['store'] = Store::get();



    	return view('pages.store_allot.list',$data);

    }

    public function ajax_view($id=0)
    {


        $sql = "select *,
                    CASE WHEN f.id IS NOT NULL 
                     THEN 1
                     ELSE 0
                     END AS is_check 
               from (select 
                       * 
                     from (select store_id from store_allot where employee_id!=".$id." AND is_delete=1) as st
                     LEFT JOIN (select id,store_id as store from store_allot where employee_id=".$id." AND is_delete=1 ) as s on s.store=st.store_id) 
                as f ";

        $selected_id = DB::select($sql); 

          foreach($selected_id as $se)
          {
              if($se->is_check==0)
              {

                  $ids[] = $se->store_id;
              }
          }  

          if(!empty($ids))
          {
            $final_id = implode("," ,$ids);

            $sql = "select s.id ,s.name,a.id as allot_id,
                   CASE WHEN a.store_id IS NOT NULL 
                     THEN 1
                     ELSE 0
                     END AS is_check 
                from (select * from stores where id NOT IN (".$final_id.")) as s
                LEFT JOIN (select store_id,id from store_allot where is_delete=1 AND employee_id=".$id.") as a on a.store_id = s.id";
          } 
          else
          {

            $sql = "select s.id ,s.name,a.id as allot_id,
                   CASE WHEN a.store_id IS NOT NULL 
                     THEN 1
                     ELSE 0
                     END AS is_check 
                from stores s
                LEFT JOIN (select store_id,id from store_allot where is_delete=1 AND employee_id=".$id.") as a on a.store_id = s.id";

          }    
      
    	

       
       
        $data['store'] = DB::select($sql);
        
      
       
      
      return view('pages.store_allot.ajax_view',$data);          

    }



    public function employee_ajax_view($id=0)
    {

       
        $sql = "select 
                   u.id,u.empname,s.id as allot_id,
                   CASE WHEN s.employee_id IS NOT NULL 
                        THEN 1
                        ELSE 0
                        END AS is_check 
                from user_informations u
                LEFT JOIN (select id,employee_id from store_allot s where is_delete=1 AND store_id=".$id.") as s on s.employee_id = u.id";

        $data['employee'] = DB::select($sql); 


        return view('pages.store_allot.employee_ajax_view',$data);       
     

    }

    public function insert_allot_store(Request $request)
    {

    	$this->validate($request,[
                                'employee'=>'required',
                                 'store_id' =>'required'
                               ],[

                                 'employee.required' =>'please select employee',
                                 'store_id.required' =>'please select atleast one store'
                               ]);
       
    	$check_employee = DB::table('store_allot')->where('employee_id',$request->employee)->where('is_delete',1)->count();
        
    	if($check_employee==0)
    	{

        foreach($request->store_id as $s){
    			
    		
    		$data[] = array(
    			          'employee_id'=>$request->employee,
    			          'store_id'=>$s,
    			          'created_at'=>date('Y-m-d H:i:s')
    			          );
    		
    	}

    		DB::table('store_allot')->insert($data);
    	}
    	else
    	{
         
          $table_store_id = DB::table('store_allot')
                              ->select('store_id')
                              ->where('employee_id',$request->employee)
                              ->where('is_delete',1)
                              ->get();

            foreach ($table_store_id as $t) 
            {

            	$ids[] = $t->store_id;
                              
            } 

            if(count($ids)>count($request->store_id))
            {
              $array_diff = array_diff($ids,$request->store_id);
            }
            else
             {
               $array_diff = array_diff($request->store_id,$ids);
             } 	
            if(!empty($array_diff))
            {

             foreach ($array_diff as $a)
              {
                 if(in_array($a,$ids))
                 {
                   
                 	$data = array('is_delete'=>0,'updated_at'=>date('Y-m-d H:i:s'));

                 	DB::table('store_allot')
                 	 ->where('employee_id',$request->employee)
                 	 ->where('is_delete',1)
                 	 ->where('store_id',$a)
                 	 ->update($data);
                 }
                 else
                 {
                    
                     $data = array(
    			              'employee_id'=>$request->employee,
    			              'store_id'=>$a,
    			              'created_at'=>date('Y-m-d H:i:s')
    			              );

                     DB::table('store_allot')->insert($data);

                 }             	
              }                 

           }
         
    	  
    	      

    	}

    	Session::flash('success',1);

    	return redirect('/store_allot');
    }



    public function insert_allot_employee(Request $request)
    {

        $this->validate($request,[
                                'store'=>'required',
                                 'employee_ids' =>'required'
                               ],[

                                 'store.required' =>'please select store',
                                 'employee_ids.required' =>'please select atleast one employee'
                               ]);
        
       
        $check_store = DB::table('store_allot')->where('store_id',$request->store)->where('is_delete',1)->count();
        
        if($check_store==0)
        {

        foreach($request->employee_ids as $e){
                
            
            $data[] = array(
                          'employee_id'=>$e,
                          'store_id'=>$request->store,
                          'created_at'=>date('Y-m-d H:i:s')
                          );
            
        }

            DB::table('store_allot')->insert($data);
        }
        else
        {
         
          $table_store_id = DB::table('store_allot')
                              ->select('employee_id')
                              ->where('store_id',$request->store)
                              ->where('is_delete',1)
                              ->get();

            foreach ($table_store_id as $t) 
            {

                $ids[] = $t->employee_id;
                              
            } 

            if(count($ids)>count($request->employee_ids))
            {
              $array_diff = array_diff($ids,$request->employee_ids);
            }
            else
             {
               $array_diff = array_diff($request->employee_ids,$ids);
             }  
            if(!empty($array_diff))
            {

             foreach ($array_diff as $a)
              {
                 if(in_array($a,$ids))
                 {
                   
                    $data = array('is_delete'=>0,'updated_at'=>date('Y-m-d H:i:s'));

                    DB::table('store_allot')
                     ->where('store_id',$request->store)
                     ->where('is_delete',1)
                     ->where('employee_id',$a)
                     ->update($data);
                 }
                 else
                 {
                    
                     $data = array(
                              'store_id'=>$request->store,
                              'employee_id'=>$a,
                              'created_at'=>date('Y-m-d H:i:s')
                              );

                     DB::table('store_allot')->insert($data);

                 }              
              }                 

           }
         
          
              

        }

        Session::flash('success',1);

        return redirect('/store_allot');
    }











}
