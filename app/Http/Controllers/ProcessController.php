<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Color;
 use App\User_information;
 use App\Store;
 use App\Task;
  use App\Order;
   use App\Placeorder;
    use App\Product;
    use App\Sale;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;
 Use DB;



    class ProcessController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


     public function alloted_task_data()
        {
            
            return view('pages/order/allotedlist');
        }


         public function ajax_data_emps()
        {
          

    $user = Placeorder::join('user_informations', 'placeorders.employee', '=', 'user_informations.id')
->join('stores', 'placeorders.store', '=', 'stores.id')->join('products', 'placeorders.product', '=', 'products.id')->where('placeorders.status','=', 'po')->orderBy('placeorders.id', 'desc')->get([
      'placeorders.id as placeorder','placeorders.date as date','placeorders.product as product','placeorders.quantity as quantity','placeorders.price as price',
      'user_informations.empname as employeename' ,
     'stores.name as store','products.product as productname'      
        ]);

        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                          'date' =>$u->date,
                          'employee' =>$u->employeename,
                          'store' =>$u->store,
                          'product' =>$u->productname,
                          'quantity' =>$u->quantity,
                          'price' =>$u->price,     
                             'actions' =>'<a href="process/process_rep/'.$u->placeorder.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon-plus"></span>Process Order</button>  <a href="process/process_view/'.$u->placeorder.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon-eye-open"></span></button></a> <button title="delete" id="podelbtn" class="delete btn btn-danger" data-id="'.$u->placeorder.'"><span class="glyphicon glyphicon-remove"></span></button></a>'                 
                           );

        }

         if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    echo json_encode($response);

        }


          public function processold($id)
        {            
        
$process = DB::table('tasks')
        ->leftJoin('user_informations', function($join) use ($id){
            $join->on('tasks.employeeid','=','user_informations.id')
                ->where('tasks.id','=',$id);})
        ->selectRaw('tasks.*, user_informations.empname as empynme,tasks.id as taskid,tasks.address as storeaddress')->first();
     

  
        
             $order = Order::where('taskid',$process->taskid)->first(); 
              //  DB::table('orders')->distinct()->get();

          $store = Store::where('id',$process->store)->first(); 

                $data['stores'] = array(
                        'store'=>$store->name                      
                       );  
                if(count($order)){
           $data['orders'] = array(
                       'id'=>$order->id,
                        'quantity'=>$order->quantity,
                        'billno'=>$order->billno,  
                        'status'=>$order->status                         
                       ); }else{

              $data['orders'] = array(
                       'id'=>'nil',
                        'quantity'=>'nil',
                         'billno'=>'nil',  
                        'status'=>'nil'                        
                       );

           }


            $data['process'] = array(
                    'empname'=>$process->empynme,
                      'taskid'=>$process->id,
                        'store'=>$process->store,  
                         'address'=>$process->address,
                          'location'=>$process->location, 
                           'mode'=>$process->mode,
                            'description'=>$process->description,
                             'date'=>$process->tdate, 
                              'time'=>$process->ttime, 
                                 'order'=>$process->torder,
                                 'payment'=>$process->tpayment                        
                       ); 



      return view('pages/order/allotedprocess',$data);

        }  



public function process($id)
        {            
        
 $data['data'] = Placeorder::join('user_informations', 'placeorders.employee', '=', 'user_informations.id')
->join('stores', 'placeorders.store', '=', 'stores.id')->join('products', 'placeorders.product', '=', 'products.id')->where('placeorders.id','=', $id)->orderBy('placeorders.id', 'desc')->first([
      'placeorders.id as placeorder','placeorders.date as date','placeorders.product as product','placeorders.quantity as quantity',
      'user_informations.empname as employee' ,
     'stores.name as store','stores.id as storeid','products.product as productname','placeorders.price as price'      
        ]);  

         $po['po'] = Placeorder::where('id',$id)->first();

        // print_r($po['po']['product']);
 // exit; 
      $product['product'] = Product::where('id',$po['po']['product'])->first(); 

          $sql = "select SUM(stock) as stock_status  from stocks where item_code='".$product['product']['itemcode']."'"; 

          $stock['stock'] = DB::select($sql); 
  //           print_r($stock['stock'][0]->stock_status);
  // exit; 

          $totalstock = $stock['stock'][0]->stock_status;
 
      return view('pages/order/placeorderprocess',$data,['totalstock'=>$totalstock]);

        }  




public function processView($id)
        {            
        
 $data['data'] = Placeorder::join('user_informations', 'placeorders.employee', '=', 'user_informations.id')
->join('stores', 'placeorders.store', '=', 'stores.id')->join('products', 'placeorders.product', '=', 'products.id')->where('placeorders.id','=', $id)->orderBy('placeorders.id', 'desc')->first([
      'placeorders.id as placeorder','placeorders.date as date','placeorders.product as product','placeorders.quantity as quantity','placeorders.price as price',
      'user_informations.empname as employee' ,
     'stores.name as store','stores.id as storeid','products.product as productname','placeorders.courrier','placeorders.billno',
     'placeorders.invoiceno','stores.description as storedesc','stores.address as storeaddress'      
        ]);     
 
      return view('pages/order/placeorderview',$data);

        }  


   public function post_order_process(Request $request)

   {
       $validator = Validator::make($request->all(), [

               'courrier' => 'required', 
                'billno' => 'required', 
                'invoiceno' => 'required',
                 'quantity' => 'required',                

          ],[
              
                                        
                                 ]);     


          if ($validator->passes()) {
        $tmpprice = 0;
                  $data = Placeorder::find($request->id);
                     $pid = $data->product;
                
           $data->courrier = $request->courrier;
          $data->processdate = date("Y-m-d");
         $data->billno = $request->billno;
          $data->invoiceno = $request->invoiceno;          
           $data->status = 'cu';
           $data->processsqty = $request->quantity;
           $data->processamount = $request->orderamount;
            if(($request->quantity)< ($data->quantity))
         {  $data->partial = 'yes';}     
        $data->save();

 $tmpprice = $tmpprice+$request->orderamount;
           $data1 = Store::find($request->storeid); 
 $totalamount =$data1['total']+ $tmpprice;
  $totalbal = $totalamount - $data1['paid'];
$value = array(
                            'total'=> $totalamount,
                            'balance'=> $totalbal                         
                      );
  $data1->fill($value)->save();

          $sales= array(
                            'date'=> date("Y-m-d"),
                            'orderid'=>$data->id,
                            'employee'=>$data->employee,
                            'store'=>$data->store,
                            'sales'=>$data->processamount                           
                      );


            Sale::create($sales);

         return response()->json(['success'=>'Added new records.'.$request->id ]);  
          }
         
       else{
        return response()->json(['error'=>$validator->errors()->all()]);}

 
}


 public function post_order_process_qty(Request $request)

   {
       $validator = Validator::make($request->all(), [

               'quantity' => 'required',


          ],[
              
                                        
                                 ]);     


          if ($validator->passes()) {
        $tmpprice = 0;
                  $data = Placeorder::find($request->id);
                  $unitprice = $data->unitprice;
                  $orderprice = $unitprice*$request->quantity;
  if(($request->quantity)>($data->quantity))
    {
return response()->json(['success'=>'Quantity no exceeds place order.']);

    }else    { 
      return response()->json(['unitprice'=>$unitprice,'orderprice'=>$orderprice]); 
       }
          } // end validator
         
       else{
        return response()->json(['error'=>$validator->errors()->all()]);}

 
}


public function post_order_process_qty_keyup(Request $request)

   {
      $data = Placeorder::find($request->id);
                  $unitprice = $data->unitprice;
                 $orderprice = $unitprice*$request->quantity;

 if(($request->quantity)>($data->quantity)||($request->quantity==0))
    {
return response()->json(['success'=>'Quantity no exceeds place order.','dataqty'=>$data->quantity,'reqqty'=>$request->quantity,'datazero'=>0 , 'datastock'=>0]); }

 else
  {

          return response()->json(['unitprice'=>$unitprice,'orderprice'=>$orderprice,'datazero'=>1]); 

  }
 
}


       public function delete_order(Request $request){
        $id = $request->id;
        Placeorder::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }



    }
