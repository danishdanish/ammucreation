<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Module;
use Hash;
use Session;

		class ModuleController extends Controller
		{
		  

		 public function __construct()
		    {
		        $this->middleware('auth');
		    }


		       public function module_list()
		    {
		        
		        return view('pages/module/list');
		    }

		     public function ajax_data()
		    {
		      
		        // $user = User::get();
		         $user = Module::all();
		        
		        $i=1;

		        foreach ($user as $u) {
		         
		                    $row[] = array(
		                           'si_no' =>$i++,
		                           'name' =>$u->name,
		                           'displayname' =>$u->displayname,
		                           'description' =>$u->description,
		                             'actions' =>'<a href="module/module_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a> <button title="delete" id="deletebtnmodule" class="delete btn btn-danger" data-id="'.$u->id.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
		                           );

		        }

		         $response = array(
		                       "draw" => 0,
		                       "recordsTotal" => count($row),
		                       "recordsFiltered" => count($row),
		                       "data" => $row
		    );
		    echo json_encode($response);

		    }

		         public function module_add()
		    {
		        return view('pages/module/add');
		    }



	public function insert_module_data(Request $request){

		$role = new Module;
	         $this->validate($request,[
	         'name'=>'required|max:13', 
			  'description'=>'required',
			  'displayname'=>'required',
			     ]);
	 	$input = $request->all();
	    Module::create($input);
	 Session::flash('flash_message', 'Module Created Successfully');
		return back(); return redirect()->back();  

	    }

	       public function edit_module_data($id){
       $data['data'] = Module::where('id',$id)->first();
        return view('pages/module/edit',$data); 
       }



     public function post_edit_module_data(Request $request)
    {

        $this->validate($request,[
                                'name'=>'required|max:13',
                                'description' =>'required',
                                'displayname' =>'required',
                               
                               ],[
                                   'name.required' =>'Name field is required',
                                 'description.required'=>'Description field is required',
                                 'displayname.required' =>'Display name is required'
                                
                               ]); 
         $uid = $request->uid;
         $data = Module::find($uid);
         $input = $request->all();
           
        
         $data->fill($input)->save();
         Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
    }



       public function delete_module_data(Request $request){
        $id = $request->id;
        Module::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
       
   }





	}
