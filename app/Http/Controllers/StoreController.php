<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Target;
 use App\Brand;
 use App\Store;
 use App\Payment;
 use Session;
 use DB;
 use Validator;
 use Illuminate\Support\Facades\File;



    class StoreController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


           public function store_list()
        {
            
            return view('pages/store/list');
        }

        public function ajax_data()
        {
          
            
             $user = Store::all();
            
            $i=1;

            foreach ($user as $u) {
             $image =  '<img width="80px" height="80px" src="'.url('theme/image/brand/'.$u->bimage).'"/>';
                        $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$u->name,
                               'contactname' =>$u->contactname,
                               'contactno' =>$u->contactno, 
                               'address' =>$u->address,                            
                                   'actions' =>'<a href="store/store_report/'.$u->id.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon-eye-open"></span> Store Report</button></a>  <a href="store/store_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button>'                   
                               );

            }

                       if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }


            public function store_add()
    {
        return view('pages/store/add');
    }

    public function insert_store_data(Request $request)
    {


             $this->validate($request,[
                                'name'=>'required',
                                'contactname'=>'required',
                                  'contactno'=>'required',
                                'address'=>'required',
                                 'zipcode'=>'required',
                                'city'=>'required',
                                  'state'=>'required',
                                'country'=>'required',
                                   'location'=>'required',
                                'description'=>'required'                              
                               ],[
                                   'name.required' =>'Store Name is required',
                                   'contactname.required' =>'Contact Name is required',
                                       'contactno.required' =>'Contact Phone is required',
                                           'adress.required' =>'Address is required',
                                             'zipcode.required' =>'Zip Code is required',
                                   'city.required' =>'City Name is required',
                                       'state.required' =>'State is required',
                                           'country.required' =>'Country is required'
                               ]);
  
      
    
      $input = $request->all();   
     Store::create($input);
      Session::flash('flash_message', 'Successfully Created!');
        return back(); return redirect()->back();

    }
       


  public function edit_store_data($id){
       $data['data'] = Store::where('id',$id)->first();
        return view('pages/store/edit',$data); 
       }



   public function post_edit_store_data(Request $request)
    {

         $this->validate($request,[
                                'name'=>'required',
                                'contactname'=>'required',
                                  'contactno'=>'required',
                                'address'=>'required',
                                 'zipcode'=>'required',
                                'city'=>'required',
                                  'state'=>'required',
                                'country'=>'required',
                                'location'=>'required',
                                'description'=>'required'                              
                               ],[
                                   'name.required' =>'Store Name is required',
                                   'contactname.required' =>'Contact Name is required',
                                       'contactno.required' =>'Contact Phone is required',
                                           'adress.required' =>'Address is required',
                                             'zipcode.required' =>'Zip Code is required',
                                   'city.required' =>'City Name is required',
                                       'state.required' =>'State is required',
                                           'country.required' =>'Country is required'
                               ]);
  
         $id = $request->id;
         $data = Store::find($id);
         $input = $request->all();
        $input['balance'] = $request->total - $request->paid;
         $data->fill($input)->save();
         Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
    }


           public function delete_store_data(Request $request){
        $id = $request->id;
        Store::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }


   // store report

       public function store_data_report($id)
        {


              $cash = Payment::join('user_informations', 'payments.employee', '=', 'user_informations.id')->select([
    'payments.date as paymentdate',
      'payments.amount as payamount',
      'user_informations.empname as empname'
              ])->where('store',$id)->where('mode','cash')->orderBy('payments.date', 'desc')->paginate(5);

                 $sqlc = "select SUM(amount) as sumcash  from payments  where store = $id and mode = 'cash'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 

       $cheque = Payment::join('user_informations', 'payments.employee', '=', 'user_informations.id')->select([
    'payments.date as paymentdate',
      'payments.amount as payamount',
      'payments.cheque as chequeid',
      'user_informations.empname as empname'
              ])->where('store',$id)->where('mode','cheque')->orderBy('payments.date', 'desc')->paginate(5);
 
  $sqlch = "select SUM(amount) as sumcheque  from payments  where store = $id and mode = 'cheque'";
       $chequetot = DB::select($sqlch);
     $chequetot=$chequetot[0]->sumcheque; 

            $store['store'] = Store::where('id',$id)->first();
            
            return view('pages/store/storereport',['cash' => $cash,'cheque' => $cheque,'cashTotal'=>$cashtot,'chequeTotal'=>$chequetot],$store);
        }





    }
