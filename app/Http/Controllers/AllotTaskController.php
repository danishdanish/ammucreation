<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Color;
 use App\User_information;
 use App\Store;
 use App\Task;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;
 Use DB;



    class AllotTaskController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }

//alotted tasks
     public function alloted_task_data()
        {
            
            return view('pages/taskprocess/allotedlist');
        }


         public function ajax_data_emps()
        {
          
       $tdate = date("Y-m-d");

           $user = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')
->join('stores', 'tasks.store', '=', 'stores.id')->where('tasks.tstatus','=', 'ongoing')->where('tasks.tdate','>=', $tdate)->orderBy('tasks.tdate', 'desc')->get([
      'user_informations.empname as employeename','tasks.id as taskid','tasks.tdate as taskdate','tasks.description as taskdesc','tasks.tstatus as taskstatus','stores.name as storename','stores.contactname as storecontact','stores.address as storeaddress','stores.contactname as storecontact','stores.*'     
        ]);

       
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                           'task_id' => $u->taskid,
                           'date' =>$u->taskdate,
                          'name' =>$u->employeename,
                          'store' =>$u->storename,
                          'status' =>$u->taskstatus, 
                             'actions' =>'<a href="task/ongoing/'.$u->taskid.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-eye-open"></span></button></a>  <button title="delete" id="deletetask" class="delete btn btn-danger" data-id="'.$u->taskid.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                           );

        }

            if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }


        public function onGoingTask($id)
        {            
        
 $data['data'] = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')
->join('stores', 'tasks.store', '=', 'stores.id')->where('tasks.id','=', $id)->first([
      'user_informations.empname as employeename','tasks.id as taskid','tasks.tdate as taskdate','tasks.ttime as tasktime','tasks.description as taskdesc','tasks.tstatus as taskstatus','tasks.tremark as taskremark','stores.name as storename','stores.contactname as storecontact','stores.address as storeaddress','stores.contactname as storecontact','stores.*'     
        ]);         
      

      return view('pages/taskprocess/ongoing',$data);

        } 


//common

             public function delete_task_data(Request $request){
        $id = $request->id;
        Task::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }



//  pending tasks

        public function pending_task_data()
        {
            
            return view('pages/taskprocess/pendinglist');
        }



        public function ajax_data_emps_pending()
        {
          
       $tdate = date("Y-m-d");

           $user = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')
->join('stores', 'tasks.store', '=', 'stores.id')->where('tasks.tstatus','=', 'ongoing')->where('tasks.tdate','<', $tdate)->orderBy('tasks.tdate', 'desc')->get([
      'user_informations.empname as employeename','tasks.id as taskid','tasks.tdate as taskdate','tasks.description as taskdesc','tasks.tstatus as taskstatus','tasks.tremark as taskremark','stores.name as storename','stores.contactname as storecontact','stores.address as storeaddress','stores.contactname as storecontact','stores.*'     
        ]);

       
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                           'date' =>$u->taskdate,
                          'name' =>$u->employeename,
                          'store' =>$u->storename,
                          'status' =>$u->taskstatus, 
                             'actions' =>'<a href="task/ongoing/'.$u->taskid.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-eye-open"></span></button></a>  <button title="delete" id="deletetaskpending" class="delete btn btn-danger" data-id="'.$u->taskid.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                           );

        }

           if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



// visted tasks


           public function visited_task_data()
        {
            
            return view('pages/taskprocess/visitedlist');
        }



               public function ajax_data_emps_visited()
        {
          
       $tdate = date("Y-m-d");

           $user = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')
->join('stores', 'tasks.store', '=', 'stores.id')->where('tasks.mode','=', 'visit')->where('tasks.tdate','<=', $tdate)->orderBy('tasks.tdate', 'desc')->get([
      'user_informations.empname as employeename','tasks.id as taskid','tasks.tdate as taskdate','tasks.tremark as taskremark','tasks.description as taskdesc','tasks.tstatus as taskstatus','stores.name as storename','stores.contactname as storecontact','stores.address as storeaddress','stores.contactname as storecontact','stores.*'     
        ]);

       
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                           'date' =>$u->taskdate,
                          'name' =>$u->employeename,
                          'store' =>$u->storename,
                          'status' =>$u->taskstatus, 
                             'actions' =>'<a href="task/ongoing/'.$u->taskid.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-eye-open"></span></button></a>  <button title="delete" id="deletetaskpending" class="delete btn btn-danger" data-id="'.$u->taskid.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                           );

        }

           if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }


        // end visited


        // individual tasks



        public function ajax_data_emps_ind($id)
        {
          
       $tdate = date("Y-m-d");

           $user = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')
->join('stores', 'tasks.store', '=', 'stores.id')->where('tasks.employeeid','=', $id)->orderBy('tasks.tstatus', 'asc')->get([
      'user_informations.empname as employeename','tasks.id as taskid','tasks.tdate as taskdate','tasks.description as taskdesc','tasks.tstatus as taskstatus','stores.name as storename','stores.contactname as storecontact','stores.address as storeaddress','stores.contactname as storecontact','stores.*'     
        ]);

       
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                           'date' =>$u->taskdate,
                          'name' =>$u->employeename,
                          'store' =>$u->storename,
                          'status' =>$u->taskstatus, 
                             'actions' =>'<a href="#"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-eye-open"></span></button></a>'                   
                           );

        }

         if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



        public function alloted_task_data_emp()
        {
            
            return view('pages/taskprocess/allotedlistemp');
        }


//individual



          public function alloted_task_process($id)
        {            
        
$results['results'] = DB::table('tasks')
        ->leftJoin('user_informations', function($join) use ($id){
            $join->on('tasks.employeeid','=','user_informations.id')
                ->where('tasks.id','=',$id);})
        ->selectRaw('tasks.*, user_informations.*,tasks.id as taskid,tasks.address as storeaddress')->first();
           foreach ($results as $result)
    {
      $storeid[] = $result->store;
    }
        
              $stores['stores'] = Store::where('id',$storeid[0])->first();       

      return view('pages/taskprocess/allotedprocess',$stores,$results);

        }  



   public function post_task_process(Request $request)

   {
       $validator = Validator::make($request->all(), [

              'torder' => 'required',
               'tpayment' => 'required', 
                'tremark' => 'required', 
                'tstatus' => 'required',              

          ],[
                'torder.required' =>'Order entry is required',
                'tpayment.required' =>'Payment entry is required',
                'tremark.required' =>'Remark is required',
                'tstatus.required' =>'Status is required'
                                        
                                 ]);     


          if ($validator->passes()) {

                $inputval = array(
          'torder'=>$request->torder,
          'tpayment'=>$request->tpayment,
           'tremark'=>$request->tremark,
            'tstatus'=>$request->tstatus              
        );

             $data = Task::find($request->taskid);
         $data->fill($inputval)->save(); 
           
          return response()->json(['success'=>'Added new records.'.$request->tstatus ]);
         
          }else{
        return response()->json(['error'=>$validator->errors()->all()]);}



}

 


    }
