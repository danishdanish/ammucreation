<?php
namespace App\Http\Controllers;
  use Illuminate\Http\Request;
  use App\Http\Requests;
  use App\Http\Controllers\Controller;
  use Carbon\Carbon;
  use JWTAuth;
  use App\User;
  use App\Brand;
  use App\Catagory;
  use App\Product;
  use App\Productcolor;
  use App\Productsize;
  use App\Placeorder;
  use App\Payment;
  use App\Store;
  Use App\Task;
  use App\Size;
  use App\Color;
  use App\Cash_in_hand;
  use DateTime;
  use Response;
  use JWTAuthException;
  use DB;
  use Date;
  use Hash ;

  class UserController extends Controller
  {   
    private $user;
    public function __construct(User $user){
      $this->user = $user;
    }

    public function register(Request $request){
      $user = $this->user->create([
        'name' => $request->get('name'),
        'email' => $request->get('email'),
        'password' => bcrypt($request->get('password'))
      ]);
      return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);
    }

    public function login(Request $request){
      $credentials = $request->only('email', 'password');
      $token = null;
      try {
       if (!$token = JWTAuth::attempt($credentials)) {
           //   return response()->json(['status'=>false,'message'=>'Invalid Login'], 422);
         $user= User::where('email',$request->get('email'))->get();
         if(!count($user))
           {   return response()->json(['status'=>false,'message'=>'Invalid User Name']);
       }else{

         return response()->json(['status'=>false,'message'=>'Invalid Password']);
       } 

     }
   } catch (JWTAuthException $e) {
    return response()->json(['status'=>false,'message'=>'Failed to create token'],500);
  }
  $user = JWTAuth::toUser($token);
  return response()->json(['status'=>true,'message'=>'Login Successfully','token'=>$token,'id'=>$user->id,'passwordreset'=>$user->res]);
  }
  public function getAuthUser(Request $request){
    $user = JWTAuth::toUser($request->token);
    $brand = Brand::where('active',1)->get(); ;
    $catagory = Catagory::where('active',1)->get();

    foreach ($brand as $b){  
      $data[] = array(
        'id'=>$b->id,
        'brand'=>$b->brand,
        'bimage' =>url('theme/image/brand/'.$b->bimage)                        
      );      
    }

    foreach ($catagory as $c){  
      $data1[] = array(
        'id'=>$c->id,
        'catagory'=>$c->catagory,
        'cimage' =>url('theme/image/catagory/'.$c->cimage)                        
      );      
    }


    return response()->json(['brands' => $data,'catagories' => $data1]);
  }


  public function getProductsold(){

   $products= Product::where('active',1)->get();
   

   foreach ($products as $product){
       //  $color =  DB::table('productcolors')->where('productid',$product->id)->get();
     $color= Productcolor::where('productid',$product->id)->get(); 
     $size= Productsize::where('productid',$product->id)->get();
     $sql = "select IF(SUM(stock)>0,'InStock','Outof stock') as stock_status  from stocks where item_code='".$product->itemcode."'"; 

     $stock_status = DB::select($sql);
     $data[] = array(
      'id'=>$product->id,
      'product'=>$product->product,
      'itemcode'=>$product->itemcode,
      'price' =>$product->price,
      'price unit' =>$product->priceunit,
      'brand id' =>$product->brandid,
      'catagory id' =>$product->catid,
      'description' =>$product->description,
      'product image' =>url($product->pimage),
      'color' =>$color,
      'size' =>$size,
      'status'=>$stock_status[0]->stock_status
    );      
   }
   
   
   return response()->json(['product details' =>$data]);


  }



  public function getProducts(){
    $products= Product::distinct()->get(['productcode']);
    foreach ($products as $product){
      $productarrays[] = array('cid'=>$product->productcode            

    );
    }



    foreach ($productarrays as $product){

      $productind = Product::where('productcode',$product['cid'])->first(); 
      $color= Productcolor::where('productcode',$product['cid'])->groupBy('colorid')->get(); 
      $size= Productsize::where('productcode',$product['cid'])->groupBy('sizeid')->get(); 

      $data[] = array(
        'id'=>$product['cid'],
        'product'=>$productind->product,                        
        'brand id' =>$productind->brandid,
        'catagory id' =>$productind->catid,
        'description' =>$productind->description,
        'product image' =>url($productind->pimage),
        'color' =>$color,
        'size' =>$size                              

      );     
    }

    return response()->json(['product details' =>$data]);


  }

  public function getProductsBrand($id){

    $products= Product::where('brandid',$id)->distinct()->get(['productcode']);


    for($i=0; $i<count($products); $i++){
      $productind = Product::where('productcode',$products[$i]->productcode)->first(); 
    //          print_r($productind->product);
    // exit; 

      $color= Productcolor::where('productcode',$products[$i]->productcode)->groupBy('colorid')->get(); 
      $size= Productsize::where('productcode',$products[$i]->productcode)->groupBy('sizeid')->get();        
      $data[] = array(
        'id'=>$products[$i]->productcode,
        'product'=>$productind->product,                        
        'brand id' =>$productind->brandid,
        'catagory id' =>$productind->catid,
        'description' =>$productind->description,
        'product image' =>url($productind->pimage),
        'color' =>$color,
        'size' =>$size                              

      );     
    }

    return response()->json(['product details' =>$data]);

  }

  public function getProductsBrandold($id){

    $brands= Brand::where('id',$id)->get();
    if(count($brands))      {
     $products= Product::where('brandid',$id)->get();


     foreach ($products as $product){
       $color =  DB::table('productcolors')->where('productid',$product->id)->get();
       $size= Productsize::where('productid',$product->id)->get(); 
       $sql = "select IF(SUM(stock)>0,'InStock','Outof stock') as stock_status  from stocks where item_code='".$product->itemcode."'"; 

       $stock_status = DB::select($sql);
       $data[] = array(
        'id'=>$product->id,
        'product'=>$product->product,
        'price' =>$product->price,
        'price unit' =>$product->priceunit,
        'brand id' =>$product->brandid,
        'catagory id' =>$product->catid,
        'description' =>$product->description,
        'product image' =>url($product->pimage),
        'color' =>$color,
        'size' =>$size,
        'status'=>$stock_status[0]->stock_status
      );      
     }

     if (empty($data))
       {return response()->json(['false'=> 'No data']);} 
     else{
      return response()->json(['product details'=> $data]);
    }
  }else
  { return response()->json(['status'=>false,'message'=>'No brand entered for this Brand Id']);}
     //end if loop

  }



  public function getProductsCat($id){

    $products= Product::where('active',1)->where('catid',$id)->distinct()->get(['productcode']);


    for($i=0; $i<count($products); $i++){
      $productind = Product::where('productcode',$products[$i]->productcode)->first(); 

      $color= Productcolor::where('productcode',$products[$i]->productcode)->groupBy('colorid')->get(); 
      $size= Productsize::where('productcode',$products[$i]->productcode)->groupBy('sizeid')->get();        
      $data[] = array(
        'id'=>$products[$i]->productcode,
        'product'=>$productind->product,                        
        'brand id' =>$productind->brandid,
        'catagory id' =>$productind->catid,
        'description' =>$productind->description,
        'product image' =>url($productind->pimage),
        'color' =>$color,
        'size' =>$size                             

      );     
    }

    return response()->json(['product details' =>$data]);


  }


  public function getProductsSorted(Request $request){        
    $products= Product::orderBy($request->field,$request->order)->distinct()->get(['productcode']);
    foreach ($products as $product){
      $productarrays[] = array('cid'=>$product->productcode            

    );
    }


    foreach ($productarrays as $product){

      $productind = Product::where('productcode',$product['cid'])->first(); 
      $color= Productcolor::where('productcode',$product['cid'])->groupBy('colorid')->get(); 
      $size= Productsize::where('productcode',$product['cid'])->groupBy('sizeid')->get(); 

      $data[] = array(
        'id'=>$product['cid'],
        'product'=>$productind->product,                        
        'brand id' =>$productind->brandid,
        'catagory id' =>$productind->catid,
        'description' =>$productind->description,
        'product image' =>url($productind->pimage),
        'color' =>$color,
        'size' =>$size                              

      );     
    }


  if (!empty($data))
    {return response()->json(['product details' =>$data]);}
  else
  { return response()->json(['status'=>false,'message'=>'No Products to Display']);}


  }


  public function getColors(Request $request){

   $colors= Color::all();
   $sizes= Size::all();

   $data[] = array(
     'colors' =>$colors,
     'sizes' =>$sizes
   );


   return response()->json(['Colors'=> $colors,'Sizes'=>$sizes]);


  }


  public function getColorsAll(Request $request){

   $colors= Color::where('def',0)->get();

   $data[] = array(
     'colors' =>$colors                           
   );


   return response()->json(['Colors'=> $colors]);


  }  


  public function getProductsFiltercheck(Request $request){ 
    $checklists[]= $request->input('color');
    $sizes[]= $request->input('size');
    $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');

    if($sizes){
      foreach ($sizes as $size)
      { 
        $size[] = array('size'=>$size
      ); 

      }
    }



    $data[] = array(
      'brand'=>$brand,
      'catagory'=>$catagory,
      'color' =>$checklists,
      'size' =>$sizes
    );   


    return response()->json([$data]);
  }


  public function getStoreforSales($id){

    $check_data = DB::table('store_allot')
    ->where('employee_id',$id)
    ->where('is_delete',1)
    ->count();

    if($check_data>0)
    {

      $sql = "select st.employee_id,s.* from store_allot st
      LEFT JOIN stores s on s.id=st.store_id
      where st.is_delete=1 AND st.employee_id=".$id." ORDER BY st.id DESC";

      $response['data'] = DB::select($sql);

      $response['status'] = array('status'=>'true','message'=>'success');         

    }
    else
    {

      $response['status'] = array('status'=>'false','message'=>'data not found');
    }               

    
    return response()->json($response);


  }



  public function getPendingWorks($id){

    $todate = date("Y-m-d");

    $sql = "select tasks.id as taskid,
    tasks.employeeid, tasks.mode,tasks.tdate as taskdate,
    tasks.ttime,
    tasks.tstatus,
    stores.id as storeid,
    stores.name,
    stores.description,
    stores.contactname,
    stores.contactno,
    stores.address,
    stores.location,
    stores.zipcode,
    stores.city,
    stores.state,
    stores.country,
    stores.total,
    stores.paid,
    stores.balance from tasks LEFT JOIN stores ON tasks.store = stores.id where tasks.tstatus = 'ongoing' and tasks.employeeid = $id and tasks.mode = 'sale' and tasks.tdate < '$todate' ORDER BY tasks.tdate DESC,tasks.ttime DESC";

    $sales = DB::select($sql);

    $sql = "select tasks.id as taskid,
    tasks.employeeid, tasks.mode,tasks.tdate as taskdate,
    tasks.ttime,
    tasks.tstatus,
    stores.id as storeid,
    stores.name,
    stores.description,
    stores.contactname,
    stores.contactno,
    stores.address,
    stores.location,
    stores.zipcode,
    stores.city,
    stores.state,
    stores.country,
    stores.total,
    stores.paid,
    stores.balance from tasks LEFT JOIN stores ON tasks.store = stores.id where tasks.tstatus = 'ongoing' and tasks.employeeid = $id and tasks.mode = 'pay' and tasks.tdate < '$todate' ORDER BY tasks.tdate DESC,tasks.ttime DESC";

    $payment = DB::select($sql);

    return response()->json(['status'=>true,'date'=>$todate,'dataSales'=>$sales,'dataPayment'=> $payment]);


  }


  public function getSchedule($id){
    $tdate = date("Y-m-d");
    $sales = Task::leftJoin('stores', function($join) use($id) {
      $join->on('tasks.store', '=', 'stores.id');
    })->where('employeeid',$id)
    ->where('mode','sale')->where('tstatus','ongoing')->where('tdate','>=',$tdate)
    ->get([
      'tasks.id as taskid',
      'tasks.employeeid',
      'tasks.mode',
      'tasks.tdate',
      'tasks.ttime',
      'stores.id as storeid',
      'stores.name',
      'stores.description',
      'stores.contactname',
      'stores.contactno',
      'stores.address',
      'stores.location',
      'stores.zipcode',
      'stores.city',
      'stores.state',
      'stores.country',
      'stores.total',
      'stores.paid',
      'stores.balance'
    ]);


    $payment = Task::leftJoin('stores', function($join) use($id){
      $join->on('tasks.store', '=', 'stores.id');
    })->where('employeeid',$id)
    ->where('mode','pay')->where('tstatus','ongoing')->where('tdate','>=',$tdate)
    ->get([
      'tasks.id as taskid',
      'tasks.employeeid',
      'tasks.mode',
      'tasks.tdate',
      'tasks.ttime',
      'stores.id as storeid',
      'stores.name',
      'stores.description',
      'stores.contactname',
      'stores.contactno',
      'stores.address',
      'stores.location',
      'stores.zipcode',
      'stores.city',
      'stores.state',
      'stores.country',
      'stores.total',
      'stores.paid',
      'stores.balance'
    ]);

    return response()->json(['status'=>true,'date'=>$tdate,'dataSales'=>$sales,'dataPayment'=>$payment]);
  }


  public function placeOrder(Request $request){
    $stk = 0;
    $task= $request->input('task');
    $salesman= $request->input('salesman');
    $store= $request->input('store');
    $orders[]= $request->input('orderdetails');
    $orderid = 'ODR'.uniqid();

    
    $tmpprice = 0;

    if($orders){
      foreach ($orders as $order)
      { $len = $order;}
    $value = count($len);    
      for ($i = 0; $i < $value; $i++)
      {

        $data = Product::join('productcolors', 'products.id', '=', 'productcolors.productid')->join('productsizes', 'products.id', '=', 'productsizes.productid')
      ->where('products.id', $order[$i]['productid'])->get();
        $itemcode = $data[0]->itemcode; 
        $sql = "select SUM(stock) as stocktotal from stocks where item_code = '".$itemcode."'";
        $snos = DB::select($sql);
      if(($snos[0]->stocktotal) >= ($order[$i]['qty'])){ 
         $quantity = 0;  
         $stk =1; 
        $user = new Placeorder;
        $quantity = $order[$i]['qty'];
        $user->date = date("Y-m-d");
        $user->taskid = $task;
        $user->placeid = $orderid;
        $user->employee = $salesman;
        $user->store = $store;
        $user->product = $order[$i]['productid'];
        $user->quantity = $quantity;
        $user->size = $order[$i]['sizecode'];
        $user->color = $order[$i]['colorcode'];
        $user->unitprice = $data[0]->price;
        $user->price = $data[0]->price*$quantity;
        $user->status = 'po';
         $user->save();
        $tmpprice = $tmpprice+$user->price; 


          
             DB::table('stocks')->insert([
    ['item_code' => $itemcode, 'stock' => -$quantity,'created_at'=>date('Y-m-d H:i:s')]
]);

               $sql = "select SUM(stock) as stocktotal from stocks where item_code = '".$itemcode."'";
        $snosafter = DB::select($sql);

        $bookeditems[]  = array('Id'=>$order[$i]['productid'],'Name'=>$data[0]->product,'SizeCode'=>$data[0]->sizecode,'ColorCode'=>$data[0]->colorcode,'OrderCount'=>$order[$i]['qty'],'BookedCount'=>$order[$i]['qty'],'Earlystock'=>$snos[0]->stocktotal,'CurrentStock'=>$snosafter[0]->stocktotal);    
      }
        else
        {
        $user = new Placeorder;
        $quantity = $snos[0]->stocktotal;
        $user->date = date("Y-m-d");
        $user->taskid = $task;
        $user->placeid = $orderid;
        $user->employee = $salesman;
        $user->store = $store;
        $user->product = $order[$i]['productid'];
        $user->quantity = $quantity;
        $user->size = $order[$i]['sizecode'];
        $user->color = $order[$i]['colorcode'];
        $user->unitprice = $data[0]->price;
        $user->price = $data[0]->price*$quantity;
        $user->status = 'po';
        if($snos[0]->stocktotal != 0)
               { 
               $stk = 1;
                $user->save();
        $tmpprice = $tmpprice+$user->price;    
          
               DB::table('stocks')->insert([
    ['item_code' => $itemcode, 'stock' => -$quantity,'created_at'=>date('Y-m-d H:i:s')]
]);

             }
      $sql = "select SUM(stock) as stocktotal from stocks where item_code = '".$itemcode."'";
        $snosafter = DB::select($sql);

        $unbookeditems[]  = array('Id'=>$order[$i]['productid'],'Name'=>$data[0]->product,'SizeCode'=>$data[0]->sizecode,'ColorCode'=>$data[0]->colorcode ,'OrderCount'=>$order[$i]['qty'],'BookedCount'=>$quantity,'EarlyStock'=>$snos[0]->stocktotal,'CurrentStock'=>$snosafter[0]->stocktotal);
        }
   
      }

    }


if((!empty($bookeditems)) || (!empty($unbookeditems))){

  if($task!=0 && $stk==1){
    $completed ='completed';
    $data = Task::find($task);
    $value = array(
      'placeid'=> $orderid ,
      'tstatus'=>'po completed',
      'tremark'=>'good'                           

    );
    $data->fill($value)->save();
  }
  elseif($stk==1)
  {
    $task = array(
      'employeeid'=> $salesman,
      'store'=>$store,
      'mode'=>'sale',
      'description'=>'new order with out tid',
      'tdate' => date("Y-m-d"),
      'ttime' => date("h:i:sa"),
      'placeid'=> $orderid,
      'tstatus'=>'po completed',
      'tremark'=>'good'                           

    );


    Task::create($task);}

  }
  else{
// do nothing
  }


  if (empty($bookeditems)){$bookeditems=[];
  }
  if (empty($unbookeditems)){$unbookeditems=[];
  }


   return response()->json(['status'=>true,'message'=>'Order booked successfully','BookedItems'=>$bookeditems,'OutOfStockItems'=>$unbookeditems]);

  }



  public function payMent(Request $request){
    $input = $request->all();
    $task= $request->input('taskid');
    $employeeid= $request->input('employee');
    $storeid= $request->input('store');
    $amount= $request->input('amount');
    $input['date'] = date("Y-m-d");
    $paymentid = 'PAY'.uniqid();


    if($task!=0){
      $completed ='completed';
      $datatb = Task::find($task);
      $value1 = array(
        'paymentid'=> $paymentid ,
        'tstatus'=>'payment completed',
        'tremark'=>'good'                           

      );


      $datatb->fill($value1)->save();
    }
    else
    {

      $task = array(


        'employeeid'=> $employeeid,
        'store'=>$storeid,
        'mode'=>'pay',
        'description'=>'new task with out tid',
        'tdate' => date("Y-m-d"),
        'ttime' => date("h:i:sa"),
        'paymentid'=> $paymentid,
        'tstatus'=>'payment completed',
        'tremark'=>'good'                           

      );


      Task::create($task); 
    }
    $task1= Task::where('paymentid',$paymentid)->first();
    $input['paymentid'] = $paymentid;
    $input['taskid'] = $task1->id;  
    $input['mode'] = 'cash';
    Payment::create($input);

    $data = Store::find($storeid); 
    $value['paid'] = $data['paid']+$amount;
    $value['balance'] = $data['total']-$value['paid'];
    $data->fill($value)->save();

    $cash = array(

      'date'=> date("Y-m-d"),
      'employee'=> $employeeid,
      'amount'=>$amount,
      'store'=>$storeid,
      'cheque'=>'no',
      'transfer'=>'no',
      'mode'=>'cash'
    );

    Cash_in_hand::create($cash);


    return response()->json(['status'=>true,'message'=>'Payment booked successfully','balance'=>$value['balance']]);

  }



  public function addStore(Request $request){
    if(!empty($request->all()))
    {
     $input = $request->all();   
     $save = Store::create($input);

     $data = array(
       'employee_id'=>$request->employee_id,
       'store_id' =>$save->id,
       'created_at'=>date('Y-m-d H:i:s')
     );

     DB::table('store_allot')->insert($data);

     $response['status'] = array('status'=>'true','message'=>'successfully created');


   }
   else
   {
    $response['status'] = array('status'=>'false','message'=>'data not found');
  }

  return response()->json($response);
  }

  public function reSchedule(Request $request){
    $taskid= $request->input('taskid');
    $employeeid= $request->input('employeeid');
    $tdate= $request->input('tdate');
    $ttime= $request->input('ttime');
    $data = Task::find($taskid);
    $value['employeeid'] = $employeeid;
    $value['ttime'] = $ttime;
    $value['tdate'] = date('Y-m-d',strtotime($tdate));

    $data->fill($value)->save();
    return response()->json(['status'=>true,'message'=>'task rescheduled']);
  }


  public function getStores(){


   $store = Store::all();

   return response()->json(['status'=>true,'stores'=>$store]);


  }



  public function getProductsFilterold1(Request $request){ 

    $colors[]= $request->input('color');
    $sizes[]= $request->input('size');
    $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');
    $sizeids = array();
    if($sizes){
      foreach ($sizes as $size)
      {     
        $sizeids = $size; 
      }
    } 


    $colorids = array();
    if($colors){
      foreach ($colors as $color)
      {     
        $colorids = $color; 
      }
    } 

    $pids = Product::whereIn('id',function($query) use($sizeids){
      $query->select('productid')
      ->from(with(new Productsize)->getTable())
      ->whereIn('id',$sizeids);
    })->orwhereIn('id',function($query) use($colorids){
      $query->select('productid')
      ->from(with(new Productcolor)->getTable())
      ->whereIn('id',$colorids);
    })->get();


    $psize =array();

    foreach ($pids as $pid){
    // $plist= Product::where([['id', '=', $pid->id]])->get(); 
     $plist = DB::table('products')->where('id',$pid->id)->first(); 
   //$plist = DB::select("select * from products where id = $pid->id AND $pid->catid = 1"); 
   //  $brand['brand'] = Brand::where('id',$id)->first();
     if($plist->catid==$catagory&&$plist->brandid==$brand) {
       $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
       $psize= Productsize::where('productid',$pid->id)->get(); 

       $data[] = array(
        'id'=>$pid->id,
        'product'=>$pid->product,
        'price' =>$pid->price,
        'price unit' =>$pid->priceunit,
        'brand id' =>$pid->brandid,
        'catagory id' =>$pid->catid,
        'description' =>$pid->description,
        'product image' =>$pid->pimage,
        'color' =>$pcolor,
        'size' =>$psize
      );

     } elseif ($catagory==0&&$brand==0) {
       $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
       $psize= Productsize::where('productid',$pid->id)->get(); 

       $data[] = array(
        'id'=>$pid->id,
        'product'=>$pid->product,
        'price' =>$pid->price,
        'price unit' =>$pid->priceunit,
        'brand id' =>$pid->brandid,
        'catagory id' =>$pid->catid,
        'description' =>$pid->description,
        'product image' =>$pid->pimage,
        'color' =>$pcolor,
        'size' =>$psize
      );

     }
     elseif ($catagory==0&&$plist->brandid==$brand) {

      $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
      $psize= Productsize::where('productid',$pid->id)->get(); 

      $data[] = array(
        'id'=>$pid->id,
        'product'=>$pid->product,
        'price' =>$pid->price,
        'price unit' =>$pid->priceunit,
        'brand id' =>$pid->brandid,
        'catagory id' =>$pid->catid,
        'description' =>$pid->description,
        'product image' =>$pid->pimage,
        'color' =>$pcolor,
        'size' =>$psize
      );

     # code...
    }

    elseif ($plist->catid==$catagory&&$brand==0) {

      $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
      $psize= Productsize::where('productid',$pid->id)->get(); 

      $data[] = array(
        'id'=>$pid->id,
        'product'=>$pid->product,
        'price' =>$pid->price,
        'price unit' =>$pid->priceunit,
        'brand id' =>$pid->brandid,
        'catagory id' =>$pid->catid,
        'description' =>$pid->description,
        'product image' =>$pid->pimage,
        'color' =>$pcolor,
        'size' =>$psize
      );

     # code...
    }




  }




  if (empty($data))
   {return response()->json(['false'=> 'No data']);} 
  else{
    return response()->json(['product details'=> $data]);}

  }


  public function getProductsFilterold2(Request $request){ 

    $colors[]= $request->input('color');
    $sizes[]= $request->input('size');
    $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');

    $sizeids = array();
    if($sizes){
      foreach ($sizes as $size)
      {     
        $sizeids = $size; 
      }
    } 

    $colorids = array();
    if($colors){
      foreach ($colors as $color)
      {     
        $colorids = $color; 
      }
    }

    $results1 = DB::table('productsizes')->select('productid')->whereIn('sizeid', $sizeids)->get();
    $results2 = DB::table('productcolors')->select('productid')->whereIn('colorid', $colorids)->get();
    $results3 =  DB::table('products')->select('id as productid')->where('brandid',$brand)->orWhere('catid',$catagory)->get();

    $value = count($colorids); 
    print_r($value);
    exit;

  // print_r($results1[0][0]->productid);
  //exit;
    $results4 = $results1->merge($results2);
    $results5 = $results4->merge($results3);

   //print_r($results5);
  //exit;

    $results5u = $results5->unique();
  //$results5us = sort($results5u);

    foreach ($results5u as $results5uind)
    {
     $result[] = $results5uind;

   }

   $value = count($result); 
   for ($i = 0; $i < $value; $i++){

    $product = Product::where('id',$result[$i]->productid)->get();
    $data[] = array(
      'product'=>$product,

    );

  }

  return response()->json(['r1'=> $results1,'r2'=> $results2,'r3'=> $results3,'r5'=> $results5u,'value'=>$value,'total'=>$result[0]->productid,'records'=>$data]);
  }



  public function getProductsFilter(Request $request){ 

    $colors[]= $request->input('color');
    $sizes[]= $request->input('size');
    $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');

    $sizeids = array();
    if($sizes){
      foreach ($sizes as $size)
      {     
        $sizeids = $size; 
      }
    } 

    $colorids = array();
    if($colors){
      foreach ($colors as $color)
      {     
        $colorids = $color; 
      }
    }
    $value1c = count($catagory);
    $value1b = count($brand); 
    $value1 = count($colorids); 
    $value2 = count($sizeids);
  //print_r($value);
   // exit;


    $results3 =  DB::table('products')->select('id as productid')->where('brandid',$brand)->orWhere('catid',$catagory)->get();
    $results1 = DB::table('productsizes')->select('productid')->whereIn('sizeid', $sizeids)->get();
    $results2 = DB::table('productcolors')->select('productid')->whereIn('colorid', $colorids)->get();

    $results4 = $results1->merge($results2);
    $results5 = $results4->merge($results3);

   //print_r($results5);
  //exit;

    $results5u = $results5->unique();
  //$results5us = sort($results5u);

    foreach ($results5u as $results5uind)
    {
     $result[] = $results5uind;

   }

  // no 9

   if(($value1==0)&&($value2==0)&&($value1b==0)&&($value1c!=0)){

    $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
    if(count($products)){
      foreach ($products as $product){

        $color= Productcolor::where('productid',$product->productid)->get(); 
        $size= Productsize::where('productid',$product->productid)->get(); 

        $datas[] = array(
         'id'=>$product->productid,
         'product'=>$product->product,
         'price' =>$product->price,
         'price unit' =>$product->priceunit,
         'brand id' =>$product->brandid,
         'catagory id' =>$product->catid,
         'description' =>$product->description,
         'product image' =>$product->pimage,
         'color' =>$color,
         'size' =>$size                       
       );

  // print_r($datas);
  // exit;

      }
    }
  }

  //no 8
  elseif(($value1==0)&&($value2==0)&&($value1c==0)&&($value1b!=0)){

    $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
    if(count($products)){
      foreach ($products as $product){

        $color= Productcolor::where('productid',$product->productid)->get(); 
        $size= Productsize::where('productid',$product->productid)->get(); 

        $datas[] = array(
         'id'=>$product->productid,
         'product'=>$product->product,
         'price' =>$product->price,
         'price unit' =>$product->priceunit,
         'brand id' =>$product->brandid,
         'catagory id' =>$product->catid,
         'description' =>$product->description,
         'product image' =>$product->pimage,
         'color' =>$color,
         'size' =>$size                       
       );

  // print_r($datas);
  // exit;

      }
    }
  }

  // no1
  elseif(($value1!=0)&&($value2!=0)&&($value1b==0)&&($value1c!=0)){

   $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
   if(count($products)){
    foreach ($products as $product){

     $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  

     $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();

     if ((!$color->isEmpty())&&(!$size->isEmpty())){ 


      $datas[] = array(
       'id'=>$product->productid,
       'product'=>$product->product,
       'price' =>$product->price,
       'price unit' =>$product->priceunit,
       'brand id' =>$product->brandid,
       'catagory id' =>$product->catid,
       'description' =>$product->description,
       'product image' =>$product->pimage,
       'color' =>$color,
       'size' =>$size                       
     );

    } else{}


  }
  }
  }


  //no 6

  elseif(($value1==0)&&($value2!=0)&&($value1b!=0)&&($value1c==0)){

   $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
   if(count($products)){
    foreach ($products as $product){



      $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();
      if (!$size->isEmpty()){      

        $color= Productcolor::where('productid',$product->productid)->get(); 
        $datas[] = array(
         'id'=>$product->productid,
         'product'=>$product->product,
         'price' =>$product->price,
         'price unit' =>$product->priceunit,
         'brand id' =>$product->brandid,
         'catagory id' =>$product->catid,
         'description' =>$product->description,
         'product image' =>$product->pimage,
         'color' =>$color,
         'size' =>$size                       
       );


      }

      

      else{



      }


    }
  }
  }

  //no 3
  elseif(($value1!=0)&&($value2==0)&&($value1b==0)&&($value1c!=0)){

   $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
   if(count($products)){
    foreach ($products as $product){

     $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  


     if (!$color->isEmpty()){ 
      $size= Productsize::where('productid',$product->productid)->get();     
      
      $datas[] = array(
       'id'=>$product->productid,
       'product'=>$product->product,
       'price' =>$product->price,
       'price unit' =>$product->priceunit,
       'brand id' =>$product->brandid,
       'catagory id' =>$product->catid,
       'description' =>$product->description,
       'product image' =>$product->pimage,
       'color' =>$color,
       'size' =>$size                       
     );


    }



    else{



    }


  }
  }
  }

  // no4
  elseif(($value1==0)&&($value2!=0)&&($value1b==0)&&($value1c!=0)){

   $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
   if(count($products)){
    foreach ($products as $product){    

     $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();
     if (!$size->isEmpty()){      
      $color= Productcolor::where('productid',$product->productid)->get(); 

      $datas[] = array(
       'id'=>$product->productid,
       'product'=>$product->product,
       'price' =>$product->price,
       'price unit' =>$product->priceunit,
       'brand id' =>$product->brandid,
       'catagory id' =>$product->catid,
       'description' =>$product->description,
       'product image' =>$product->pimage,
       'color' =>$color,
       'size' =>$size                       
     );


    }



    else{



    }


  }
  }
  }


  // no 5

  elseif(($value1!=0)&&($value2==0)&&($value1b!=0)&&($value1c==0)){

   $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
   if(count($products)){
    foreach ($products as $product){

      $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  






      if (!$color->isEmpty()){      
        $size= Productsize::where('productid',$product->productid)->get();
        $datas[] = array(
         'id'=>$product->productid,
         'product'=>$product->product,
         'price' =>$product->price,
         'price unit' =>$product->priceunit,
         'brand id' =>$product->brandid,
         'catagory id' =>$product->catid,
         'description' =>$product->description,
         'product image' =>$product->pimage,
         'color' =>$color,
         'size' =>$size                       
       );


      }

      

      else{



      }

    }
  }
  }



  //no 7

  elseif(($value1==0)&&($value2==0)&&($value1b==0)&&($value1c==0)){

   $products = Product::all();

   if(count($products)){
    foreach ($products as $product){

     $color= Productcolor::where('productid',$product->id)->get();  

     $size= Productsize::where('productid',$product->id)->get();

     $datas[] = array(
       'id'=>$product->id,
       'product'=>$product->product,
       'price' =>$product->price,
       'price unit' =>$product->priceunit,
       'brand id' =>$product->brandid,
       'catagory id' =>$product->catid,
       'description' =>$product->description,
       'product image' =>$product->pimage,
       'color' =>$color,
       'size' =>$size                       
     );




   }
  }







  }

  //full



  //no 2

  elseif(($value1!=0)&&($value2!=0)&&($value1b!=0)&&($value1c==0)){

   $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
   if(count($products)){
    foreach ($products as $product){

     $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  

     $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();

     if ((!$color->isEmpty())&&(!$size->isEmpty())){ 


      $datas[] = array(
       'id'=>$product->productid,
       'product'=>$product->product,
       'price' =>$product->price,
       'price unit' =>$product->priceunit,
       'brand id' =>$product->brandid,
       'catagory id' =>$product->catid,
       'description' =>$product->description,
       'product image' =>$product->pimage,
       'color' =>$color,
       'size' =>$size                       
     );

    } else{}

  }
  }
  }


  //no 10

  elseif(($value1!=0)&&($value2!=0)&&($value1b!=0)&&($value1c!=0)){

   $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
   if(count($products)){
    foreach ($products as $product){

     $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  

     $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();

     if ((!$color->isEmpty())&&(!$size->isEmpty())){ 


      $datas[] = array(
       'id'=>$product->productid,
       'product'=>$product->product,
       'price' =>$product->price,
       'price unit' =>$product->priceunit,
       'brand id' =>$product->brandid,
       'catagory id' =>$product->catid,
       'description' =>$product->description,
       'product image' =>$product->pimage,
       'color' =>$color,
       'size' =>$size                       
     );

    } else{}

  }
  }
  }


  //no 11

  elseif(($value1!=0)&&($value2==0)&&($value1b==0)&&($value1c==0)){
    // $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();
   $colors = Productcolor::select('productid')->whereIn('colorid', $colorids)->get();

   foreach ($colors as $color){

     $product = Product::select('id','product','price','priceunit','brandid','catid','description','pimage')->where('id',$color->productid)->get(); 

     $color= Productcolor::where('productid',$product[0]->id)->get();  

     $size= Productsize::where('productid',$product[0]->id)->get();

     $datas[] = array(
       'id'=>$product[0]->id,
       'product'=>$product[0]->product,
       'price' =>$product[0]->price,
       'price unit' =>$product[0]->priceunit,
       'brand id' =>$product[0]->brandid,
       'catagory id' =>$product[0]->catid,
       'description' =>$product[0]->description,
       'product image' =>$product[0]->pimage,
       'color' =>$color,
       'size' =>$size                       
     );
   }
   
  }




  //no 12

  elseif(($value1==0)&&($value2!=0)&&($value1b==0)&&($value1c==0)){
    // $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();
   $sizes = Productsize::select('productid')->whereIn('sizeid', $sizeids)->get();

   foreach ($sizes as $size){

     $product = Product::select('id','product','price','priceunit','brandid','catid','description','pimage')->where('id',$size->productid)->get(); 

     $color= Productcolor::where('productid',$product[0]->id)->get();  

     $size= Productsize::where('productid',$product[0]->id)->get();

     $datas[] = array(
       'id'=>$product[0]->id,
       'product'=>$product[0]->product,
       'price' =>$product[0]->price,
       'price unit' =>$product[0]->priceunit,
       'brand id' =>$product[0]->brandid,
       'catagory id' =>$product[0]->catid,
       'description' =>$product[0]->description,
       'product image' =>$product[0]->pimage,
       'color' =>$color,
       'size' =>$size                       
     );
   }
   
  }



  //no 13

  elseif(($value1!=0)&&($value2!=0)&&($value1b==0)&&($value1c==0)){
   $color= Productcolor::select('productid')->whereIn('colorid', $colorids)->get();
   $sizes = Productsize::select('productid')->whereIn('sizeid', $sizeids)->get();

   foreach ($sizes as $size){

     $product = Product::select('id','product','price','priceunit','brandid','catid','description','pimage')->where('id',$size->productid)->get(); 

     $color= Productcolor::where('productid',$product[0]->id)->get();  

     $size= Productsize::where('productid',$product[0]->id)->get();

     $datas[] = array(
       'id'=>$product[0]->id,
       'product'=>$product[0]->product,
       'price' =>$product[0]->price,
       'price unit' =>$product[0]->priceunit,
       'brand id' =>$product[0]->brandid,
       'catagory id' =>$product[0]->catid,
       'description' =>$product[0]->description,
       'product image' =>$product[0]->pimage,
       'color' =>$color,
       'size' =>$size                       
     );
   }
   
  }




  else{

   $value = count($result); 
   for ($i = 0; $i < $value; $i++){

    $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('id',$result[$i]->productid)->get();


    $color= Productcolor::where('productid',$result[$i]->productid)->whereIn('colorid', $colorids)->get();      

    $size= Productsize::where('productid',$result[$i]->productid)->whereIn('sizeid', $sizeids)->get(); 

    
    if ((!$color->isEmpty())&&(!$size->isEmpty())){      

    }



    else{



    }

  }


  }
  //end else



  //print_r($results5u[1]->productid);
  //exit;

  if (empty($datas))
   {return response()->json(['false'=> 'No data']);} 
  else{
  // return response()->json($datas);
   return response()->json(['product details'=> $datas]);

  }
  }



  public function postVisit(Request $request){
    $ttime = date("g:i a");
    $data = array(
      'employeeid'=>$request->employee,
      'store'=>$request->store,
      'tdate'=>date('Y-m-d',strtotime($request->date)),
      'ttime'=>$ttime ,
      'mode'=>'visit',
      'interest'=>$request->interest,
      'tremark'=>$request->remark,
      'tstatus'=>'visit completed'

    ); 


    Task::create($data);  
    return response()->json(['status'=>true,'message'=>'New task visit created']);

  }




  public function getWorklist($id){
    $todate = date("Y-m-d");
    
    $sql = "select tasks.id as taskid,
    tasks.employeeid, tasks.mode,tasks.tdate,
    tasks.ttime,
    tasks.tstatus,
    stores.id as storeid,
    stores.name,
    stores.description,
    stores.contactname,
    stores.contactno,
    stores.address,
    stores.location,
    stores.zipcode,
    stores.city,
    stores.state,
    stores.country,
    stores.total,
    stores.paid,
    stores.balance from tasks LEFT JOIN stores ON tasks.store = stores.id where tasks.tstatus != 'ongoing' and tasks.employeeid = $id and tasks.tdate <= '$todate' ORDER BY tasks.tdate DESC,tasks.ttime DESC";
    $data = DB::select($sql);


    $sql = "select user_id,leave_date,reason,
    CASE
    WHEN leave_type=1 THEN 'casual leave'
    WHEN leave_type=2 THEN 'sick leave'
    ELSE 'Loss of pay' 
    END as leave_type,

    CASE
    WHEN day_type=1 THEN 'full day'
    ELSE 'half day' 
    END as day_type,
    slot     
    from leave_taken where user_id=".$id." AND is_delete=1 AND leave_date<CURDATE()";

    $leave = DB::select($sql);         

      //  $nrd = DB::delete('SQL QUERY HERE');
     //   DB::update('SQL QUERY HERE');



    return response()->json(['status'=>true,'WorkLog'=>$data,'leave_details'=>$leave]);
  }


  public function getPaystorelist($id){
    $tdate = date("Y-m-d");
    $data = Task::leftJoin('stores', function($join) use($id) {
      $join->on('tasks.store', '=', 'stores.id');
    })->where('employeeid',$id)->where('tdate','<=',$tdate)->orderBy('tdate', 'DESC')
    ->get([
      'tasks.id as taskid',
      'tasks.employeeid',
      'tasks.mode',
      'tasks.tdate',
      'tasks.ttime',
      'stores.id as storeid',
      'stores.name',
      'stores.description',
      'stores.contactname',
      'stores.contactno',
      'stores.address',
      'stores.location',
      'stores.zipcode',
      'stores.city',
      'stores.state',
      'stores.country',
      'stores.total',
      'stores.paid',
      'stores.balance'
    ]);




    return response()->json(['status'=>true,'WorkLog'=>$data]);
  }



  public function payCheque(Request $request){

    $input = $request->all();
    $task= $request->input('taskid');
    $employeeid= $request->input('employee');
    $storeid= $request->input('store');
    $amount= $request->input('amount');
    $cdate= $request->input('cdate');
    $cheque= $request->input('cheque');
    $bank= $request->input('bank');
    $input['date'] = date("Y-m-d");
    $input['cdate'] =  date('Y-m-d',strtotime($cdate));
    $paymentid = 'PAY'.uniqid();


    if($task!=0){
      $completed ='completed';
      $datatb = Task::find($task);
      $value1 = array(
        'paymentid'=> $paymentid ,
        'tstatus'=>'payment completed',
        'tremark'=>'good'                           

      );


      $datatb->fill($value1)->save();
    }
    else
    {

      $task = array(


        'employeeid'=> $employeeid,
        'store'=>$storeid,
        'mode'=>'pay',
        'description'=>'new task with out tid',
        'tdate' => date("Y-m-d"),
        'ttime' => date("h:i:sa"),
        'paymentid'=> $paymentid,
        'tstatus'=>'payment completed',
        'tremark'=>'good'                           

      );


      Task::create($task); 
    }
    $task1= Task::where('paymentid',$paymentid)->first();
    $input['paymentid'] = $paymentid;
    $input['taskid'] = $task1->id;
    $input['mode'] = 'cheque';
    Payment::create($input);

    $data = Store::find($storeid); 
    $value['paid'] = $data['paid']+$amount;
    $value['balance'] = $data['total']-$value['paid'];
    $data->fill($value)->save();


    $cash = array(

      'date'=> date("Y-m-d"),
      'employee'=> $employeeid,
      'amount'=>$amount,
      'store'=>$storeid,
      'cheque'=>$cheque,
      'transfer'=>'no',
      'mode'=>'cheque'

    );

    Cash_in_hand::create($cash);

    return response()->json(['status'=>true,'message'=>'Payment booked successfully','balance'=>$value['balance']]);
  }



  public function payHand($id){

    $sql = "select SUM(amount) as cam from cash_in_hands  where employee = $id and cheque = 'no' and transfer = 'no'";

    $sql1 = "select SUM(amount) as chm  from cash_in_hands  where employee = $id and cheque != 'no' and transfer = 'no'";


    $cash = DB::select($sql);

  //       print_r($cash);
  // exit;
    $cheque = DB::select($sql1);

    if(is_null ($cash[0]->cam)){
      $cash[0]->cam = 0;
    }

    if(is_null ($cheque[0]->chm)){
      $cheque[0]->chm = 0;
    }

    $casht = array(
      'status'=>'true',
      'CashInHandToatalAmount'=>$cash[0]->cam,
      'ChequeInHandTotalAmount'=>$cheque[0]->chm,
      'TotalAmount'=>$cheque[0]->chm+$cash[0]->cam

    );


    return response()->json($casht);

  }


  public function transferPay(Request $request){

    $userid= $request->input('userid');
    $mode= $request->input('paymode');


    if($mode == 'cash')
    {
      Cash_in_hand::where('employee',$userid)->where('mode','cash')->where('transfer','no')->update(['transfer' => 'yes','transferdate'=>date("Y-m-d")]);
    }elseif($mode == 'cheque')
    {

      Cash_in_hand::where('employee',$userid)->where('mode','cheque')->where('transfer','no')->update(['transfer' => 'yes','transferdate'=>date("Y-m-d")]);

    }
    else
    {

     return response()->json(['status'=>'false','message'=>'wrong paymode']);
   }

   return response()->json(['status'=>'true','message'=>'succesfully transfered']);

  }


  public function getStoreHistory($id){


    $storedets= Placeorder::where('store',$id)->where('status','cu')->orderBy('date', 'DESC')->get();

    if(!$storedets->isEmpty()){


     foreach ($storedets as $storedet){  

      $data[] = array(
        'OrderId'=>$storedet->id,
        'DateofOrder'=>$storedet->date,
        'OrderPlacedDate' =>$storedet->processdate,
        'InvoiceNo' =>$storedet->invoiceno,
        'BillNo' =>$storedet->billno,
        'Amount' =>$storedet->price                          
      );      
    }
  }
  else
  {
   $data=[]; 
  }





  $storedets1= Payment::where('store',$id)->orderBy('date', 'DESC')->get();  

  if(!$storedets1->isEmpty()){

   foreach ($storedets1 as $storedet1){  

    $data1[] = array(

      'DateOfPayment'=>$storedet1->date,
      'Amount'=>$storedet1->amount,
      'Mode' =>$storedet1->mode,
      'Bank' =>$storedet1->bank,
      'Cheque' =>$storedet1->cheque,
      'Cheque Date' =>$storedet1->cdate                          
    );      
  }

  }
  else
  {
   $data1=[];
  }




  return response()->json(['BookingDetails' =>$data,'PayementDetails' =>$data1]);




  }


  public function password_edit(Request $request)
  {

   $data = array('password'=>Hash::make($request->password),'res'=>0);

   $save  = User::where('id',$request->user_id)->update($data);

   if($save)
   {

    $response['status'] = array('status'=>'true','message'=>'successfully updated');
  }
  else
  {
    $response['status'] = array('status'=>'false','message'=>'error occured');
  }



  return response()->json($response);

  }


  public function ProductsCart(Request $request)
  {
   $productsall[]= $request->input('Products');
   // print_r(count($productsall[0]));
   // exit;
   for ($j=0; $j<count($productsall[0]); $j++){

    $products = Product::where('productcode',$productsall[0][$j]['ProductId'])->get();

    for ($i = 0; $i < count($products); $i++)
    {
      $productcode=$productsall[0][$j]['ProductId'];
      $qty= $productsall[0][$j]['Qty'];
      $sizeid= $productsall[0][$j]['SizeId'];
      $colorid= $productsall[0][$j]['ColorId'];

      $data = Product::join('productcolors', 'products.id', '=', 'productcolors.productid')->join('productsizes', 'products.id', '=', 'productsizes.productid')
      ->where('products.itemcode', $products[$i]['itemcode'])->where('productcolors.colorid',$colorid)->where('productsizes.sizeid',$sizeid)->get(['products.*',
        'productcolors.*','productsizes.*','products.id as productid']);
      if(!$data->isEmpty()){
       $itemcode =$data[0]->itemcode;
       $sql = "select IF(SUM(stock)>0,'InStock','OutOfStock') as stock_status  from stocks where item_code='$itemcode'";
       $stock = DB::select($sql); 
       $res[] = array(        
         'ItemId'=>$data[0]->productid,              
         'ProductId'=>$productcode,
         'ProductName'=>$data[0]->product,
         'SizeId'=>$data[0]->sizeid,
         'ColorId'=>$data[0]->colorid,
         'Price'=>$data[0]->price,
         'PriceUnit'=>$data[0]->priceunit,
         'Qty'=>$qty,
         'StockStatus'=>$stock[0]->stock_status                                            
       ); 


       break(1);
     }else{
      continue;
    }

  }

  } // j loop


  if(isset($res)){
    return response()->json(['status'=>true,'RequestedStatusProducts'=> $res]);}
    else{
      return response()->json(['status'=>true,'RequestedStatusProducts'=> []]);  
    }

  }




  }  