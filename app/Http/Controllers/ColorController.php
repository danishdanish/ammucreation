<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Color;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;



    class ColorController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


           public function color_list()
        {
            
            return view('pages/color/list');
        }

        public function ajax_data()
        {
          
            
             $user = Color::all();
            
            $i=1;

            foreach ($user as $u) {
                                    $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$u->colorname,
                               'code' =>$u->colorcode,                             
                                   'actions' =>'<button title="delete" id="deletebtncolor" class="delete btn btn-danger" data-id="'.$u->id.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                               );

            }

             $response = array(
                           "draw" => 0,
                           "recordsTotal" => count($row),
                           "recordsFiltered" => count($row),
                           "data" => $row
        );
        echo json_encode($response);

        }


            public function color_add()
    {
        return view('pages/color/color');
    }

    public function insert_color_data(Request $request)
    {


      $this->validate($request,[
                                'colorname'=>'required',
                                'colorcode' =>'required',
                                                          
                               ],[
                                   'colorname.required' =>'Color name required',
                                 'colorcode.required'=>'Color code is required'
                                                           
                               ]); 

      $input = $request->all();   
     Color::create($input);
      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();

    }



           public function delete_color_data(Request $request){
        $id = $request->id;
        Color::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }





    }
