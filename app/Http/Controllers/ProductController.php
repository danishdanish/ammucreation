<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Productcolor;
use App\Productsize;
use App\Catagory;
use App\Size;
use App\Color;
use Hash;
use Session;
use Illuminate\Support\Facades\File;


class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


     public function product_list()
    {
        
        return view('pages/product/list');
    }


    public function ajax_data()
    {
      
       $user = Product::orderBy('itemcode','ASC')->get();
        
        $i=1;

        foreach ($user as $u) {
         
        // $image =  '<img width="80px" height="80px" src="'.url($u->pimage).'"/>';
            $row[] = array(
                           'si_no' =>$i++,
                           'itemcode'=>$u->itemcode,
                           'productcode'=>$u->productcode,
                           'product' =>$u->product,
                           'price' =>$u->price,
                           // 'image' =>$image,
                            'active' =>$u->active,
                             'actions' =>'</a> <a href="product/product_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a>'                   
                           );

        }

         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
    );
    echo json_encode($response);

    }


       public function product_add()
    {
     
     $itemcode = 'ITEM'.substr(number_format(time() * rand(),0,'',''),0,5); 
      $brands = Brand::where('active',1)->get();
      $catagories = Catagory::where('active',1)->get();
      $sizes = Size::all();
       $colors = Color::all();
        return view('pages/product/add',['brands' => $brands,'catagories' => $catagories,'sizes'=>$sizes,'colors'=> $colors,'itemcode'=>$itemcode]);
    }



     public function insert_product_data(Request $request)
    {


             $this->validate($request,[
              'itemcode'=>'required|unique:products',
                'productcode'=>'required',
                                'product'=>'required',
                                'price' =>'required',
                                'priceunit'=>'required',
                                'description' =>'required',
                                'sizeid' =>'required',
                                'colorid' =>'required',
                               
                               ],[
                                    'productcode.required' =>'Product code is required',
                                    'itemcodecode.required' =>'Item code is required',
                                   'product.required' =>'Product field is required',
                                 'price.required'=>'Price field is required',
                                 'priceunit.required' =>'Price unit is required',
                                 'description.required' =>'Description is required',
                                 'sizeid.required' =>'Size required',
                                 'colorid.required' =>'Color required'                                
                               ]);
  
        $file = $request->file('product_image');
         if($file)
        { $profileimagenme = $request->file('product_image')->getClientOriginalName();
        $profileimageext = $request->file('product_image')->getClientOriginalExtension();
        $profileimagenme = time().'.'.$profileimageext;        
        $file->move(public_path('theme/image/product'),$profileimagenme);
        $productimage = 'theme/image/product/'.time().'.'.$profileimageext;

    }
        else
         {$productimage = 'noimage'.time();}

     $input = $request->all();
     // $input['pimage'] = $productimage;
       $product = new Product;
        $product->product = $request->product;
        $product->price = $request->price;
        $product->priceunit = $request->priceunit;
        $product->brandid = $request->brandid;
        $product->catid = $request->catid;
        $product->description = $request->description;
        $product->pimage = $productimage;
          $product->itemcode = $request->itemcode;
          $product->productcode = $request->productcode;
        $product->save();
        $rowsize = Size::find($request->sizeid); 
      $productsize = new Productsize;
       $productsize->productid = $product->id;
      $productsize->productcode = $request->productcode;      
       $productsize->sizeid = $request->sizeid;
       $productsize->sizename = $rowsize->sizename;
        $productsize->sizecode = $rowsize->sizecode;
      $productsize->save();

      $rowcolor = Color::find($request->colorid); 
        $productcolor = new Productcolor;
       $productcolor->productid = $product->id;
       $productcolor->productcode = $request->productcode;
       $productcolor->colorid = $request->colorid;
       $productcolor->colorname = $rowcolor->colorname;
        $productcolor->colorcode = $rowcolor->colorcode;
      $productcolor->save();

   // Product::create($input);
     Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();

    }


      public function edit_product_data($id){
    // $pcolor['pcolor'] = Productcolor::where('productid',$id)->first();
    // $psize['psize'] = Productsize::where('productid',$id)->first();
  
    // $productsize = $psize['psize']['sizeid'];
    // $productcolor = $pcolor['pcolor']['colorid'];
        $brands = Brand::where('active',1)->get();
      $catagories = Catagory::where('active',1)->get();
      $sizes = Size::all();
      $colors=Color::all();

//  $product['product'] = Product::join('productsizes', 'products.productcode', '=', 'productsizes.productcode')
// ->join('productcolors', 'products.productcode', '=', 'productcolors.productcode')->where('products.id','=', $id)->first(['products.*','productsizes.*','productcolors.*','products.id as productid']);

       $product['product'] = Product::where('id','=', $id)->first();


        return view('pages/product/edit',['brands' => $brands,'catagories' => $catagories,'sizes'=>$sizes,'colors'=>$colors],$product); 
       }


     public function post_edit_product_data(Request $request)
    {

     $this->validate($request,[
                               'product'=>'required',
                               'itemcode'=>'required',
                                 'productcode'=>'required',                                       
                                'price' =>'required',
                                'priceunit'=>'required',
                                'description' =>'required',
                               
                               ],[
                                'productcode.required' =>'Product code is required',
                                    'itemcode.required' =>'Item code is required',
                                   'product.required' =>'Product field is required',
                                 'price.required'=>'Price field is required',
                                 'priceunit.required' =>'Price unit is required',
                                 'description.required' =>'Description is required'
                                
                               ]);
  
         $id = $request->id;
         $data = Product::find($id);
    

         $input = $request->all();
             
         if($request->pimage!='')
        {
            $file = $request->pimage;

            $file_name = time().$file->getClientOriginalName();

            $path =  public_path('/theme/image/product/');

            $file->move($path,$file_name);

             $input['pimage'] = 'theme/image/product/'.$file_name;
             File::delete('theme/image/product/'.$data->pimage);          
            
    } 


         $data->fill($input)->save();
         $cname['cname'] = Color::where('id',$request->colorid)->first();
         $sname['sname'] = Size::where('id',$request->sizeid)->first();
  //             print_r($sname['sname']['sizename']);
  // exit;

          $pcolor = Productcolor::where('productid',$id)->first();
         $value1 = array(
                            'colorid'=> $request->colorid,
                            'colorname'=>$cname['cname']['colorname'],
                             'colorcode'=>$cname['cname']['colorcode'],
                            'productcode'=>$request->productcode                                                
                     );
            $pcolor->fill($value1)->save();

             $psize = Productsize::where('productid',$id)->first();
         $value2 = array(
                            'sizeid'=> $request->sizeid,
                            'sizename'=>$sname['sname']['sizename'],
                            'sizecode'=>$sname['sname']['sizecode'],
                             'productcode'=>$request->productcode                                                
                     );
            $psize->fill($value2)->save();


         Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
    }




         public function edit_product_color($id){
       $product['product'] = Product::where('id',$id)->first();
      $colors = Productcolor::where('productid',$id)->get();
      $pcolors = Color::all();
        return view('pages/product/color',['colors' => $colors,'pcolors'=>$pcolors],$product); 
       }



       public function post_edit_product_color(Request $request)
    {

    $coloravl = Productcolor::where('productid',$request->productid)->where('colorid',$request->colorid)->get();

      if((count($coloravl)) == 1){
         Session::flash('flash_message', 'Already added!');
        return back(); return redirect()->back();
      } else
      {
        $rowcolor1 = Color::find($request->colorid); 
      $productcolor = new Productcolor;
       $productcolor->productid = $request->productid;
       $productcolor->colorid = $request->colorid;
       $productcolor->colorname = $rowcolor1->colorname;
       $productcolor->colorcode = $rowcolor1->colorcode;
      $productcolor->save();
      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
      }
    }


        public function edit_product_size($id){
       $product['product'] = Product::where('id',$id)->first();
      $colors = Productsize::where('productid',$id)->get();
      $sizes = Size::all();
        return view('pages/product/size',['colors' => $colors,'sizes' => $sizes],$product); 
       }



 public function post_edit_product_size(Request $request)
    {

  $sizeavl = Productsize::where('productid',$request->productid)->where('sizeid',$request->sizeid)->get();

      if((count($sizeavl)) == 1){
         Session::flash('flash_message', 'Already added!');
        return back(); return redirect()->back();
      } else
      {
       $rowsize1 = Size::find($request->sizeid); 
      $productsize = new Productsize;
       $productsize->productid = $request->productid;
       $productsize->sizeid = $request->sizeid;
       $productsize->sizename = $rowsize1->sizename;
        $productsize->sizecode = $rowsize1->sizecode;
      $productsize->save();
      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
}
    }



         public function delete_product_data(Request $request){
         $id = $request->id;
          Product::destroy($id);        
           return response()->json(['success'=>'Succesfully Deleted!']);
       
   }


       public function checkmodal(){
         return response()->json(['success'=>'connected ok.']);
   }


    public function user_permissions($id)
    {
        $data['data'] = User_information::where('id',$id)->first();
        $roles = Role::all();
        return view('pages/user/permissions',['roles' => $roles],$data);
    }


     public function user_permissions_add(Request $request) {
   $rid=$request->uid;
     $role_sel=$request->role_sel;
     $data = User_information::find($rid);   
      $data->user_role=$role_sel;         
      $data->save();     
     Session::flash('message', "Status Updated");
    return back(); return redirect()->back();

  }



       public function size_delete(Request $request){
         $id = $request->id;
         $pid = $request->pid;
       $rowsize2 = Productsize::where('productid',$pid)->get();
         if((count($rowsize2)) > 1)
      { 
       Productsize::destroy($id);        
        return response()->json(['success'=>'Delete Size Success']);}
        else
        {
            return response()->json(['success'=>'Cannot Delete Defualt Size']);
        }
       
   }

   public function color_delete(Request $request){
         $id = $request->id;
          $pid = $request->pid;
            $rowcolor2 = Productcolor::where('productid',$pid)->get();
         if((count($rowcolor2)) > 1)
{
       Productcolor::destroy($id);        
        return response()->json(['success'=>'Delete Color Success']);}
        else
        {

         return response()->json(['success'=>'Cannot Delete Defualt Color']);   
        }
       
   }

}
