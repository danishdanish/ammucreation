<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class LeaveapiController extends Controller
{
   
    public function check_leave($id)
    {

     $check_user = DB::table('user_informations')->where('id',$id)->count();
     
      if($check_user>0)
      {	
       
      $sql = "select 
               IF(TRIM((ca.full_day+ca.half_day))+0 IS NULL,0,TRIM((ca.full_day+ca.half_day))+0) as total_casual_leave_take
              from((select 
              SUM(case when day_type=1 then 1 else 0 end) full_day,
              (SUM(case when day_type=2 then 1 else 0 end))/2 half_day
               from leave_taken where leave_type=1 AND user_id=".$id." AND is_delete=1 AND YEAR(leave_date)= YEAR(CURDATE())) as ca)";

      $casual_leave = DB::select($sql);

      $sql = "select 
               IF(TRIM((si.full_day+si.half_day))+0 IS NULL,0,TRIM((si.full_day+si.half_day))+0) as total_sick_leave_take
              from((select 
               SUM(case when day_type=1 then 1 else 0 end) full_day,
               (SUM(case when day_type=2 then 1 else 0 end))/2 half_day
               from leave_taken where leave_type=2 AND user_id=".$id." AND is_delete=1 AND YEAR(leave_date)= YEAR(CURDATE())) as si)";

      $sick_leave = DB::select($sql);
      
     $sql="select * from leave_management 
           where is_delete=1 AND user_id=".$id." AND YEAR(date)=YEAR(CURDATE())";         
     $allot_leave = DB::select($sql);
       if(!empty($allot_leave))
       {

       	 $balance_casual_leave = ($allot_leave[0]->total_casual_leave)-($casual_leave[0]->total_casual_leave_take);

        $balance_sick_leave = ($allot_leave[0]->total_sick_leave)-($sick_leave[0]->total_sick_leave_take);

        $response['data'] = array(
       	                         'balance_casual_leave'=>$balance_casual_leave<0?0:$balance_casual_leave,
       	                         'balance_sick_leave'=>$balance_sick_leave<0?0:$balance_sick_leave
       	                        );

       $response['status'] = array('status'=>'true','message'=>'success');

       }
       else
       {
        
        $response['status'] = array('status'=>'false','message'=>'leave details not found');

       }
      
      }
      else
      {
      	 $response['status'] = array('status'=>'false','message'=>'user not exisit');
      } 

       return response()->json($response);
    }

    public function leave_apply(Request $request)
    {
    	if($request->data!='')
    	{

         $insert_data = [];
       
            foreach($request->data as $leave )
            {
            
             if(count($request->data)>1)
             {
                
                $check_leave =  DB::table('leave_taken')->where('user_id',$leave['user_id'])->where('leave_date',$leave['date_of_leave'])->where('leave_type',$leave['leave_type'])->where('day_type',$leave['day_type'])->where('slot',array_key_exists('slot',$leave)==1?$leave['slot']:NULL)->where('is_delete',1)->first();
                
                if(count($check_leave)==0)
                {

                   $insert_data[] = array(
                                   'user_id'=>$leave['user_id'],
                                   'leave_date' =>$leave['date_of_leave'],
                                   'reason'=>$leave['reason'],
                                   'leave_type'=>$leave['leave_type'],
                                   'day_type'=>$leave['day_type'],
                                   'slot'=>array_key_exists('slot',$leave)==1?$leave['slot']:"",
                                   'created_at'=>date('Y-m-d H:i:s')
                                   );

                }
        

             }
             else
             {
               
                 $check_leave =  DB::table('leave_taken')->where('user_id',$leave['user_id'])->where('leave_date',$leave['date_of_leave'])->where('leave_type',$leave['leave_type'])->where('day_type',$leave['day_type'])->where('slot',array_key_exists('slot',$leave)==1?$leave['slot']:NULL)->where('is_delete',1)->first();

                 if(count($check_leave)==0)
                 {

                    $insert_data[] = array(
                                   'user_id'=>$leave['user_id'],
                                   'leave_date' =>$leave['date_of_leave'],
                                   'reason'=>$leave['reason'],
                                   'leave_type'=>$leave['leave_type'],
                                   'day_type'=>$leave['day_type'],
                                   'slot'=>array_key_exists('slot',$leave)==1?$leave['slot']:"",
                                   'created_at'=>date('Y-m-d H:i:s')
                                   );
                  
                 
                  
                 }
                 

             }    
            	

            }
          
             if(!empty($insert_data))
             {
                
               DB::table('leave_taken')->insert($insert_data);

              $response['status'] = array('status'=>'true','message'=>'successfully applyed');
             
             }
             else
             {
                $response['status'] = array('status'=>'false','message'=>'leave already applied in this date');
             }    
    	}
    	else
    	{
    	 
    	   $response['status'] = array('status'=>'false','message'=>'data not found');	
    	}

    	return response()->json($response);
    }


    public function leave_cancel(Request $request)
    {

       if($request->user_id=='')
       {
       
         $response['status']  = array('status'=>'false','message'=>'user id not found');

         return response()->json($response);

       }

       if($request->leave_date=='')
       {

         $response['status']  = array('status'=>'false','message'=>'leave date not found');

         return response()->json($response);

       }

       $check_leave = DB::table('leave_taken')
                       ->where('user_id',$request->user_id)
                       ->where('leave_date',$request->leave_date)
                       ->where('is_delete',1)
                       ->first();

         if(count($check_leave)>0)
         {

            $data = array('is_delete'=>0,'updated_at'=>date('Y-m-d H:i:s'));

                 DB::table('leave_taken')
                       ->where('user_id',$request->user_id)
                       ->where('leave_date',$request->leave_date)
                       ->where('is_delete',1)
                       ->update($data);

            $response['status'] = array('status'=>'true','message'=>'successfully canceld');           
         }
         else
         {

          $response['status'] = array('status'=>'false','message'=>'leave already not applyed in this date');

         } 

         return response()->json($response);              

    }


    public function graph_details($id)
    {

      if($id!='')
      {
     
        $user_check = DB::table('user_informations')->where('id',$id)->count();
        
         if($user_check>0)
         {
          
          $last_month = date('Y-m-01', strtotime('-5 month'));
          //print_r( $last_month);exit;
          for ($i = 0; $i < 6; $i++) 
          {
            $months[] = date("Y-m", strtotime( $last_month." +$i months"));
          }

           //print_r($months);exit;

           foreach($months as $m)
           {

             $year = date('Y',strtotime($m));

             $month = date('m',strtotime($m));
             
              $sql = "select SUM(sales) as sales from sales where employee=".$id." AND YEAR(date)=".$year." AND MONTH(date)=".$month;

              $sales = DB::select($sql);
             
              $sql = "select SUM(quantity) as orderd_box_count,SUM(price) as order_amount from placeorders where employee=".$id." AND YEAR(date)=".$year." AND MONTH(date)=".$month;

              $order_details = DB::select($sql);
              
              $sql = "select SUM(processsqty) as purchase_box_count from placeorders 
                       where employee=".$id." AND status='cu' AND YEAR(date)=".$year." AND MONTH(date)=".$month;

              $purchasebox = DB::select($sql);
              

              $graph_details[] = array(
                                       'month'=>date('M',strtotime($m)),
                                       'sales'=>$sales[0]->sales==''?0:$sales[0]->sales,
                                       'order'=>$order_details[0]->order_amount==''?0:$order_details[0]->order_amount,
                                       'orderboxcount'=>$order_details[0]->orderd_box_count==''?0:$order_details[0]->orderd_box_count,
                                       'purchaseboxcount'=>$purchasebox[0]->purchase_box_count==''?0:$purchasebox[0]->purchase_box_count
                                      );         




           }
         
          //print_r($graph_details);exit;
            /*$sql = "select DATE_FORMAT(sa.date, '%b') as month, SUM(sa.sales) as sales from (select * from sales 
                     where (date BETWEEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 6 MONTH),'%Y-%m-01') AND CURDATE()) AND employee=".$id.") as sa  GROUP BY YEAR(sa.date), MONTH(sa.date)";

            $response['graph_details']  = $graph_details;*/ 
            $response['graph_details']  = $graph_details;
             
             $sql = "select IFNULL(SUM(quantity),0) as order_count from placeorders where employee=".$id;

             $orders_count = DB::select($sql);
              
            //$response['total_orders_count'] = DB::table('tasks')->where('employeeid',$id)->where('tstatus','pocompleted')->count();

             $response['total_orders_count'] = $orders_count[0]->order_count;

            $sql = "select count(store_id) as total_store from store_allot where employee_id=".$id." AND is_delete=1";

            $total_store_count = DB::select($sql);
           
            $response['total_store_count'] = !empty($total_store_count)?$total_store_count[0]->total_store:0;

            $sql = "select count(sales) as total_sale_count from sales where employee=".$id;

            $total_sales_count = DB::select($sql);

            $response['total_sales_count'] =  !empty($total_sales_count)? $total_sales_count[0]->total_sale_count:0;

            $sql = "select sales as target_amount from targets where user_id=".$id." AND MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";

            $target_amount = DB::select($sql);
             
            $response['target_box'] = !empty($target_amount)?$target_amount[0]->target_amount:0;
           
            $sql = "select SUM(quantity) as sale_box from placeorders where employee=".$id." AND MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE()) AND status='cu'";
             
            $sale_amount = DB::select($sql); 
             
            $response['sale_box'] = $sale_amount[0]->sale_box!=''?$sale_amount[0]->sale_box:0;


            $sql = "select 
               IF(TRIM((ca.full_day+ca.half_day))+0 IS NULL,0,TRIM((ca.full_day+ca.half_day))+0) as total_casual_leave_take
              from((select 
              SUM(case when day_type=1 then 1 else 0 end) full_day,
              (SUM(case when day_type=2 then 1 else 0 end))/2 half_day
               from leave_taken where leave_type=1 AND user_id=".$id." AND is_delete=1 AND YEAR(leave_date)= YEAR(CURDATE())) as ca)";

      $casual_leave = DB::select($sql);

      $sql = "select 
               IF(TRIM((si.full_day+si.half_day))+0 IS NULL,0,TRIM((si.full_day+si.half_day))+0) as total_sick_leave_take
              from((select 
               SUM(case when day_type=1 then 1 else 0 end) full_day,
               (SUM(case when day_type=2 then 1 else 0 end))/2 half_day
               from leave_taken where leave_type=2 AND user_id=".$id." AND is_delete=1 AND YEAR(leave_date)= YEAR(CURDATE())) as si)";

      $sick_leave = DB::select($sql);
      
     $sql="select * from leave_management 
           where is_delete=1 AND user_id=".$id." AND YEAR(date)=YEAR(CURDATE())";

     $allot_leave = DB::select($sql);
       if(!empty($allot_leave))
       {

         $balance_casual_leave = ($allot_leave[0]->total_casual_leave)-($casual_leave[0]->total_casual_leave_take);

        $balance_sick_leave = ($allot_leave[0]->total_sick_leave)-($sick_leave[0]->total_sick_leave_take);

        $response['leave_details'] = array(
                                 'balance_casual_leave'=>$balance_casual_leave<0?0:$balance_casual_leave,
                                 'balance_sick_leave'=>$balance_sick_leave<0?0:$balance_sick_leave
                                );

       }
       else
       {

         $response['leave_details'] = array(
                                 'balance_casual_leave'=>0,
                                 'balance_sick_leave'=>0
                                );

      
       }
   
       $response['leave_details']['current_day_leave_status'] = DB::table('leave_taken')
                                               ->where('user_id',$id)
                                               ->where('leave_date',date('Y-m-d'))
                                               ->where('is_delete',1)
                                               ->count();

          $response['status'] = array('status'=>'true','message'=>'success');                                      
       }
       else
       {
         $response['status'] = array('status'=>'false','message'=>'This user not exisit');
       }                                        
        

      }
      else
      {
        $response['status'] = array('status'=>'false','message'=>'user_id not correct');
      }

     return response()->json($response);
    }


    public function get_payment_details($id)
    {

       $check_user = DB::table('user_informations')->where('id',$id)->count();

       if($check_user>0)
       {

           $sql = "select s.* from store_allot st
                    LEFT JOIN stores s on s.id=st.store_id
                     where st.employee_id=".$id." AND st.is_delete=1 AND s.balance!=0";

           $response['data'] = DB::select($sql);

          $response['status'] = array('status'=>'true','message'=>'success');           

       }
       else
       {
         $response['status'] = array('status'=>'false','message'=>'user not exisit');
       }
      
      return response()->json($response);
    }

}








// print_r($insert_data);exit;
           //exit;
         
            /*if(count($leave_date)>1)
            {
               foreach ($insert_data as $in) {
                  
               }
            }
            else
            {

            }*/
           //$check_leave = DB::table('leave_taken')->whereIn('user_id',$user_id)->whereIn('leave_date',$leave_date)->whereIn('leave_type',$leave_type)->whereIn('day_type',$day_type)->whereIn('slot',$slot)->where('is_delete',1)->get();
         //print_r($check_leave);exit;
           /*if(count($check_leave)>0)
           {
             $response['status'] = array('status'=>'false','message'=>'leave already applied in this date');

           }
           else
           {
            
              DB::table('leave_taken')->insert($insert_data);

              $response['status'] = array('status'=>'true','message'=>'successfully applyed');
           }*/
