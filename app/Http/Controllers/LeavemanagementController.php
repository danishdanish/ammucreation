<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User_information;
use DB;
use Session;

//use Illuminate\Pagination\Paginator;

class LeavemanagementController extends Controller
{
  
    public function __construct()
        {
            $this->middleware('auth');
        }

   public function list_leave()
   {
   	 return view('pages.leave.list');
   }


	public function ajax_data()
  {
          
            $leave = DB::table('leave_management')
                        ->select('leave_management.*','user_informations.empname')
                        ->leftJoin('user_informations','leave_management.user_id','=','user_informations.id')
                        ->where('leave_management.is_delete',1)
                        ->orderBy('leave_management.date', 'desc')
                        ->get();
            
            $i=1;

            foreach ($leave as $l) {

            	$link = '<a href="leave/leave_details/'.$l->id.'">'.$l->empname.'</a>';
             
                        $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$link,
                               'casual'=>$l->total_casual_leave,
                               'sick'=>$l->total_sick_leave,
                               'year'=>date('Y',strtotime($l->date)),
                                 'actions' =>'<div class="action_div"><a href="leave/leave_edit/'.$l->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a> <button  title="delete" class="delete btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button><input type="hidden" id="delete_id" value="'.$l->id.'"/></div>'                   
                               );

            }

      if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }
    

    public function add_leave()
    {
    	$sql = "select COUNT(*) as check_leave
                 FROM leave_management
                 WHERE  is_delete=1 AND YEAR(date) = YEAR(CURDATE())";

         $leave = DB::select($sql);

         $data['leave_count'] = $leave[0]->check_leave;
                
    	 return view('pages.leave.add',$data);
    }

    public function insert_leave(Request $request)
    {
     
       $user = User_information::get();
     if(!$user->isEmpty())
      {
        foreach ($user as $u) 
        {
        	 
        	 $data[] = array(
        	 	            'user_id'=>$u->id,
        	 	            'total_casual_leave'=>$request->casual,
        	 	            'total_sick_leave'=>$request->sick,
        	 	            'date'=>date('Y-m-d'),
        	 	            'created_at'=>date('Y-m-d H:i:s')
        	 	            );

        }


        DB::table('leave_management')->insert($data);

        Session::flash('update',1);
      }
        return redirect('/leave');


    }

    public function leave_edit($id)
    {

    	$data['data'] = DB::table('leave_management')
    	        ->select('leave_management.*','user_informations.empname')
    	        ->leftJoin('user_informations','leave_management.user_id','=','user_informations.id')
    	        ->where('leave_management.id',$id)
    	        ->first();

    	return view('pages.leave.edit',$data);
    }

    public function update_leave(Request $request)
    {

    	$data = array(
    		         'user_id'=>$request->user_id,
        	 	     'total_casual_leave'=>$request->casual,
        	 	     'total_sick_leave'=>$request->sick,
        	 	     'date'=>date('Y-m-d'),
        	 	     'updated_at'=>date('Y-m-d H:i:s')
    		         );

    	DB::table('leave_management')->where('id',$request->edit_id)->update($data);

    	Session::flash('update',1);

    	return redirect('/leave');
    }


    public function leave_delete($id)
    {
    	$data = array('is_delete'=>0,'updated_at'=>date('Y-m-d H:i:s'));

    	DB::table('leave_management')->where('id',$id)->update($data);

    	return 1 ;
    }

    public function leave_details($i)
    {

      $sql="select * from leave_management where id=".$i; 

     $allot_leave = DB::select($sql);
      $id = $allot_leave[0]->user_id;
       $sql = "select 
               IF(TRIM((ca.full_day+ca.half_day))+0 IS NULL,0,TRIM((ca.full_day+ca.half_day))+0) as total_casual_leave_take
              from((select 
              SUM(case when day_type=1 then 1 else 0 end) full_day,
              (SUM(case when day_type=2 then 1 else 0 end))/2 half_day
               from leave_taken where leave_type=1 AND user_id=".$id." AND is_delete=1 AND YEAR(leave_date)= YEAR(CURDATE())) as ca)";

      $casual_leave = DB::select($sql);

      $sql = "select 
               IF(TRIM((si.full_day+si.half_day))+0 IS NULL,0,TRIM((si.full_day+si.half_day))+0) as total_sick_leave_take
              from((select 
               SUM(case when day_type=1 then 1 else 0 end) full_day,
               (SUM(case when day_type=2 then 1 else 0 end))/2 half_day
               from leave_taken where leave_type=2 AND user_id=".$id." AND is_delete=1 AND YEAR(leave_date)= YEAR(CURDATE())) as si)";

      $sick_leave = DB::select($sql);

      
     
     $sql="select 
               IF(TRIM((l.full_day+l.half_day))+0 IS NULL,0,TRIM((l.full_day+l.half_day))+0) as lose_of_pay
              from((select 
               SUM(case when day_type=1 then 1 else 0 end) full_day,
               (SUM(case when day_type=2 then 1 else 0 end))/2 half_day
               from leave_taken where leave_type=3 AND user_id=".$id." AND is_delete=1 AND YEAR(leave_date)= YEAR(CURDATE())) as l)";

       $loss_of_pay = DB::select($sql);        

     $balance_casual_leave = ($allot_leave[0]->total_casual_leave)-($casual_leave[0]->total_casual_leave_take);

        $balance_sick_leave = ($allot_leave[0]->total_sick_leave)-($sick_leave[0]->total_sick_leave_take);

       $data['casual_leave_taken'] = $casual_leave[0]->total_casual_leave_take;

       $data['sick_leave_taken'] = $sick_leave[0]->total_sick_leave_take;

       $data['loss_of_pay'] = $loss_of_pay[0]->lose_of_pay;

       $data['balance_casual_leave'] = $balance_casual_leave<0?0:$balance_casual_leave;

       $data['balance_sick_leave'] = $balance_sick_leave<0?0:$balance_sick_leave;

       $data['user_id'] = $id;

       $sql = "select * from leave_taken 
                  where user_id=".$id." AND is_delete=1 AND  YEAR(leave_date)= YEAR(CURDATE())";

      $data['leave_details'] = DB::select($sql);

       //print_r($data['leave_details']);exit;
    	return view('pages.leave.leave_details',$data);
    }

 public function leave_report_page()
 {

   $data['employee'] = DB::table('user_informations')->get();

   return view('pages.leave.report',$data);
 }


 public function filter_leave()
 {
     $employee = $_GET['employee'];

     $date_from = $_GET['date_from'];

     $date_to = $_GET['date_to'] ;

     $sql = "select u.empname,l.leave_date, l.reason,
                  CASE
                      WHEN l.leave_type=1 THEN 'casual leave'
                      WHEN l.leave_type=2 THEN 'sick leave'
                      ELSE 'Loss of pay' 
                  END as leave_type,

                  CASE
                      WHEN l.day_type=1 THEN 'full day'
                      ELSE 'half day' 
                  END as day_type,
                  l.slot     
            from leave_taken l
            LEFt JOIN user_informations u on u.id=l.user_id
           where l.is_delete=1 AND (l.leave_date BETWEEN '".$date_from."' AND '".$date_to."')";

          if($employee!='')
          {
            $sql .= "AND l.user_id=".$employee;
          } 

          $leave = DB::select($sql);
        
            
           if(!empty($leave))
           {
               $i=1;
              foreach ($leave as $l) {

           
             
                        $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$l->empname,
                               'leave_date'=>$l->leave_date,
                               'reason'=>$l->reason,
                               'leave_type'=>$l->leave_type,
                               'day_type'=>$l->day_type                    
                               );

            }
      }
      else
      {
         $row = [];
      }
    
        echo json_encode($row);
             
          

 }

    
}

?>