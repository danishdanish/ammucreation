<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Color;
 use App\User_information;
 use App\Store;
 use App\Task;
 use DateTime;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;
 Use DB;



    class TaskController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


           public function task_list()
        {
            
            return view('pages/task/list');
        }

        public function ajax_data()
        {
          
            
             $user = Task::all();
            
            $i=1;

            foreach ($user as $u) {
                                    $row[] = array(
                               'si_no' =>$i++,
                               'desc' =>$u->description,
                               'date' =>$u->tdate,                             
                                   'actions' =>'<a href="task/task_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a> <button title="delete" id="deletebtntask" class="delete btn btn-danger" data-id="'.$u->id.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                               );

            }

             $response = array(
                           "draw" => 0,
                           "recordsTotal" => count($row),
                           "recordsFiltered" => count($row),
                           "data" => $row
        );
        echo json_encode($response);

        }


    public function insert_color_data(Request $request)
    {


      $this->validate($request,[
                                'colorname'=>'required',
                                'colorcode' =>'required',
                                                          
                               ],[
                                   'colorname.required' =>'Color name required',
                                 'colorcode.required'=>'Color code is required'
                                                           
                               ]); 

      $input = $request->all();   
     Color::create($input);
      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();

    }


 public function store_add_details(Request $request){
        $id = $request->id;
        $data = User_information::find($id);     
           return response()->json(['phone'=>$data->phone]);
   }


 

 public function task_add_store(Request $request){
        $id = $request->id;
      $data = Store::find($id);     
           return response()->json(['location'=>$data->location,'address'=>$data->address,'mobile'=>$data->contactno]);
   }

 public function post_task_add(Request $request){

 $validator = Validator::make($request->all(), [

              'employeeid' => 'required',
               'store' => 'required', 
                'address' => 'required',
                 'location' => 'required',
               'mode' => 'required', 
                'description' => 'required',
                'mobile' => 'required',
               'tdate' => 'required' 
                        

          ],[
                'employeeid.required' =>'Please select employee',
                'store.required' =>'Please select store',
                'address.required' =>'Address required',
                 'location.required' =>'Location',
                'mode.required' =>'Please select mode',
                'tdate.required' =>'Please select date'             
                                        
                                 ]); 



 if ($validator->passes()) {
$date = $request->tdate;
$tdate = DateTime::createFromFormat("m/d/Y" , $date);
$changedDate=$tdate->format('Y-m-d');


  $tdate = date("Y-m-d");
    $id= $request->id; 
        $data = array(
          'employeeid'=>$request->employeeid,
          'store'=>$request->store,
           'address'=>$request->address,
            'location'=>$request->location,
              'mode'=>$request->mode,
                 'description'=>$request->description,
                    'mobile'=>$request->mobile,
                    'tdate'=> $changedDate,
                     'ttime'=>$request->ttime,
                     'tstatus'=>'ongoing'

        ); 

        $input = $request->all();
         $input['tdate'] = $changedDate;
        $input['tstatus'] = 'ongoing';
   Task::create($input);  
     return response()->json($data);

   }
     else{
        return response()->json(['error'=>$validator->errors()->all()]);}

   

   }




           public function edit_task_data($id){
    //        $results['results'] = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')
    // ->select('tasks.address', 'user_informations.empname')->where('tasks.employeeid', '=', $id)->first();

    // $orders = DB::table('user_profiles')
    //     ->leftJoin('orders', function($join) use ($status){
    //         $join->on('user_profiles.id','=','orders.id_user')
    //             ->where('orders.status','=',$status);
    //     })
    //     ->selectRaw('user_profiles.*, count(orders.id_user) as order_try_count')
    //     ->groupBy('user_profiles.id')
    //     ->orderBy('order_try_count',$order)
    //     ->paginate(15);

$results['results'] = DB::table('tasks')
        ->leftJoin('user_informations', function($join) use ($id){
            $join->on('tasks.employeeid','=','user_informations.id')
                ->where('tasks.id','=',$id);
        })
        ->selectRaw('tasks.*, user_informations.*,tasks.id as taskid')->first();
          $emps = User_information::where('user_role',4)->get();
           $stores = Store::all();

      return view('pages/task/edit',['emps' => $emps,'stores'=>$stores],$results); 
   }



   public function post_edit_task_data(Request $request){

 $validator = Validator::make($request->all(), [

              'employeeid' => 'required',
               'store' => 'required', 
                'address' => 'required',
                 'location' => 'required',
               'mode' => 'required', 
                'description' => 'required',
                'mobile' => 'required',
               'tdate' => 'required', 
                'ttime' => 'required',                 

          ],[
                'employeeid.required' =>'Please select employee',
                'store.required' =>'Please select store',
                'address.required' =>'Address required',
                 'location.required' =>'Location',
                'mode.required' =>'Please select mode',
                'tdate.required' =>'Please select date',
                'ttime.required'=>'Please select time'
                                        
                                 ]); 



 if ($validator->passes()) {

    $id= $request->taskid; 
  
        $data = array(
          'employeeid'=>$request->employeeid,
          'store'=>$request->store,
           'address'=>$request->address,
            'location'=>$request->location,
              'mode'=>$request->mode,
                 'description'=>$request->description,
                    'mobile'=>$request->mobile,
                    'tdate'=>$request->tdate,
                     'ttime'=>$request->ttime,
                     'id'=>$request->taskid

        ); 

       $input = $request->all();
        $data = Task::find($id);
   $data->fill($input)->save(); 
     return response()->json($data);

   }
     else{
        return response()->json(['error'=>$validator->errors()->all()]);}

   

   }



           public function delete_task_data(Request $request){
        $id = $request->id;
        Task::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }



   public function task_add_mapped(Request $request){
        $id = $request->id;
                        $data = DB::table('store_allot')
    ->leftjoin('stores', 'store_allot.store_id','=','stores.id')
    ->select ('stores.id', 'stores.name')
    ->where('store_allot.employee_id', '=', $id)->get();

           return response()->json(['data'=>$data]);
   }



             public function task_add()
    {

     $stores = Store::all();
      $emps = User_information::where('designation','salesman')->get();
        return view('pages/task/add',['emps'=>$emps]);

    }


             public function task_add_emp(Request $request)
    {

    $employee= $request->empsel;
    $data['data'] = User_information::where('id', $employee)->first();
     $stores = DB::table('store_allot')->leftjoin('stores', 'store_allot.store_id','=','stores.id')
    ->select ('stores.id as storeid', 'stores.name as storename')->where('employee_id', '=', $employee)->where('is_delete', '=', 1)->get();
    
        return view('pages/task/addstore',['stores'=>$stores],$data);

    }
    


    }
