<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use JWTAuth;
use App\User;
 use App\Brand;
 use App\Catagory;
  use App\Product;
    use App\Productcolor;
    use App\Productsize;
    use App\Placeorder;
    use App\Payment;
    use App\Store;
    Use App\Task;
    use App\Size;
use App\Color;
  use Response;
use JWTAuthException;
use DB;

class UserController extends Controller
{   
    private $user;
    public function __construct(User $user){
        $this->user = $user;
    }
   
    public function register(Request $request){
        $user = $this->user->create([
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'password' => bcrypt($request->get('password'))
        ]);
        return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);
    }
    
    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
         //   return response()->json(['status'=>false,'message'=>'Invalid Login'], 422);
             $user= User::where('email',$request->get('email'))->get();
        if(!count($user))
         {   return response()->json(['status'=>false,'message'=>'Invalid User Name']);
        }else{

             return response()->json(['status'=>false,'message'=>'Invalid Password']);
        } 

           }
        } catch (JWTAuthException $e) {
            return response()->json(['status'=>false,'message'=>'Failed to create token'],500);
        }
         $user = JWTAuth::toUser($token);
        return response()->json(['status'=>true,'message'=>'Login Successfully','token'=>$token,'id'=>$user->id]);
    }
    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        $brand = Brand::all();
        $catagory = Catagory::all();
        return response()->json(['brands' => $brand,'catagories' => $catagory]);
    }


       public function getProducts(){
       
     $products= Product::all();
    //     $product = Product::leftJoin('productcolors', function($join) {
    //   $join->on('products.id', '=', 'productcolors.productid');
    // })->get();

     
     
    foreach ($products as $product){
     //  $color =  DB::table('productcolors')->where('productid',$product->id)->get();
         $color= Productcolor::where('productid',$product->id)->get(); 
           $size= Productsize::where('productid',$product->id)->get(); 
        $data[] = array(
                        'id'=>$product->id,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                        'color' =>$color,
                        'size' =>$size
                       );      
    }
 
 
     return response()->json(['product details' =>$data]);
   

    }



     public function getProductsBrand($id){

        $brands= Brand::where('id',$id)->get();
        if(count($brands))      {
       $products= Product::where('brandid',$id)->get();
       
    
    foreach ($products as $product){
       $color =  DB::table('productcolors')->where('productid',$product->id)->get();
       $size= Productsize::where('productid',$product->id)->get(); 
        $data[] = array(
                        'id'=>$product->id,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                             'color' =>$color,
                             'size' =>$size
                       );      
    }

  if (empty($data))
 {return response()->json(['false'=> 'No data']);} 
else{
  return response()->json(['Product Details'=> $data]);
}
 }else
 { return response()->json(['status'=>false,'message'=>'No brand entered for this Brand Id']);}
   //end if loop

    }



 public function getProductsCat($id){
       
       $products= Product::where('catid',$id)->get(); 
     
    foreach ($products as $product){
       $color =  DB::table('productcolors')->where('productid',$product->id)->get();
              $size= Productsize::where('productid',$product->id)->get(); 
        $data[] = array(
                        'id'=>$product->id,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                             'color' =>$color,
                             'size' =>$size
                       );      
    }
 
 if (empty($data))
 {return response()->json(['false'=> 'No data']);} else
   {  return response()->json(['Product Details'=> $data]);}
   

    }


     public function getProductsSorted(Request $request){        
    $products = Product::orderBy($request->field,$request->order)->get();
    if(count($products)){
    foreach ($products as $product){
       $color =  DB::table('productcolors')->where('productid',$product->id)->get();
           $size= Productsize::where('productid',$product->id)->get(); 
        $data[] = array(
                        'id'=>$product->id,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                             'color' =>$color,
                             'size' =>$size
                       );      
    }
 
  
     return response()->json(['Product Details'=> $data]);
   

    }
    else{

return response()->json(['status'=>false,'message'=>'No Products to Display']);

    }

    //end if loop
}


public function getColors(Request $request){

   $colors= Color::all();
   $sizes= Size::all();

$data[] = array(
                             'colors' =>$colors,
                             'sizes' =>$sizes
                           );


 return response()->json(['Colors'=> $colors,'Sizes'=>$sizes]);


} 


public function getProductsFiltercheck(Request $request){ 
  $checklists[]= $request->input('color');
  $sizes[]= $request->input('size');
   $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');

      if($sizes){
    foreach ($sizes as $size)
     { 
    $size[] = array('size'=>$size
                       ); 

}
}

//input
// {
// "token": "vbm",
//  "brandid": "2",
//  "categoryid": "2",
// "color": ["red","green"],
// "size": ["X","XL"]
// }


  $data[] = array(
                            'brand'=>$brand,
                            'catagory'=>$catagory,
                            'color' =>$checklists,
                             'size' =>$sizes
                       );   


  return response()->json([$data]);
}


     public function getStoreforSales($id){

      //  $store= Task::distinct()->where('employeeid',$id)->get(['store']);

    $store = Task::leftJoin('stores', function($join)use($id) {
      $join->on('tasks.store', '=', 'stores.id');
    })
    ->where('employeeid',$id)->distinct()
    ->get([
      'tasks.employeeid',
        'stores.*'
        ]);
       
  return response()->json(['status'=>true,'stores'=>$store]);


}



  public function getPendingWorks($id){

      //  $store= Task::distinct()->where('employeeid',$id)->get(['store']);

    $sales = Task::leftJoin('stores', function($join)use($id) {
      $join->on('tasks.store', '=', 'stores.id');
    })
    ->where('employeeid',$id)->where('mode','sale')->where('tstatus','ongoing')
    ->get([
      'tasks.id as taskid',
      'tasks.employeeid',
      'tasks.mode',
      'tasks.tdate',
      'tasks.ttime',
      'stores.id as storeid',
      'stores.name',
      'stores.description',
      'stores.contactname',
      'stores.contactno',
      'stores.address',
      'stores.location',
      'stores.zipcode',
      'stores.city',
      'stores.state',
      'stores.country',
      'stores.total',
      'stores.paid',
      'stores.balance'
        ]);


       $payment = Task::leftJoin('stores', function($join)use($id) {
      $join->on('tasks.store', '=', 'stores.id');
    })
    ->where('employeeid',$id)->where('mode','pay')->where('tstatus','ongoing')
    ->get([
     'tasks.id as taskid',
      'tasks.employeeid',
      'tasks.mode',
      'tasks.tdate',
      'tasks.ttime',
      'stores.id as storeid',
      'stores.name',
      'stores.description',
      'stores.contactname',
      'stores.contactno',
      'stores.address',
      'stores.location',
      'stores.zipcode',
      'stores.city',
      'stores.state',
      'stores.country',
       'stores.total',
      'stores.paid',
      'stores.balance'        
        ]);
       
  return response()->json(['status'=>true,'dataSales'=>$sales,'dataPayment'=>$payment]);


}


public function getSchedule($id){
      $tdate = date("m/d/Y");
       $sales = Task::leftJoin('stores', function($join) use($id) {
      $join->on('tasks.store', '=', 'stores.id');
    })->where('employeeid',$id)
    ->where('mode','sale')->where('tstatus','ongoing')->where('tdate','>=',$tdate)
    ->get([
    'tasks.id as taskid',
      'tasks.employeeid',
      'tasks.mode',
      'tasks.tdate',
      'tasks.ttime',
      'stores.id as storeid',
      'stores.name',
      'stores.description',
      'stores.contactname',
      'stores.contactno',
      'stores.address',
      'stores.location',
      'stores.zipcode',
      'stores.city',
      'stores.state',
      'stores.country',
       'stores.total',
      'stores.paid',
      'stores.balance'
        ]);


       $payment = Task::leftJoin('stores', function($join) use($id){
      $join->on('tasks.store', '=', 'stores.id');
    })->where('employeeid',$id)
   ->where('mode','pay')->where('tstatus','ongoing')->where('tdate','>=',$tdate)
    ->get([
      'tasks.id as taskid',
      'tasks.employeeid',
      'tasks.mode',
      'tasks.tdate',
      'tasks.ttime',
      'stores.id as storeid',
      'stores.name',
      'stores.description',
      'stores.contactname',
      'stores.contactno',
      'stores.address',
      'stores.location',
      'stores.zipcode',
      'stores.city',
      'stores.state',
      'stores.country',
       'stores.total',
      'stores.paid',
      'stores.balance'
        ]);
       
  return response()->json(['status'=>true,'dataSales'=>$sales,'dataPayment'=>$payment]);
}


public function placeOrder(Request $request){
  $task= $request->input('task');
 $salesman= $request->input('salesman');
  $store= $request->input('store');
 $orders[]= $request->input('orderdetails');
   $orderid = 'ODR'.uniqid();


      if($orders){
    foreach ($orders as $order)
     $value = count($order);
     { 
        for ($i = 0; $i < $value; $i++)
{

  $user = new Placeorder;
        $user->date = date("m/d/Y");
        $user->taskid = $task;
        $user->placeid = $orderid;
         $user->employee = $salesman;
          $user->store = $store;
        $user->product = $order[$i]['productid'];
        $user->quantity = $order[$i]['qty'];
         $user->size = $order[$i]['sizecode'];
        $user->color = $order[$i]['colorcode'];
        $user->save();

}
 

}
}

if($task!=0){
$completed ='completed';
$data = Task::find($task);
$value = array(
                            'placeid'=> $orderid ,
                            'tstatus'=>'po completed',
                            'tremark'=>'good'                           

                      );
$data->fill($value)->save();
}
else
{
$task = array(
                            'employeeid'=> $salesman,
                            'store'=>$store,
                            'mode'=>'sale',
                            'description'=>'new order with out tid',
                            'tdate' => date("m/d/Y"),
                            'ttime' => date("h:i:sa"),
                            'placeid'=> $orderid,
                            'tstatus'=>'po completed',
                            'tremark'=>'good'                           

                      );


 Task::create($task);

}

 return response()->json(['status'=>true,'message'=>'Order booked successfully']);
 
}


public function payMent(Request $request){
  $input = $request->all();
  $task= $request->input('taskid');
  $employeeid= $request->input('employee');
  $storeid= $request->input('store');
  $amount= $request->input('amount');
      $input['date'] = date("m/d/Y");
     $paymentid = 'PAY'.uniqid();


  if($task!=0){
$completed ='completed';
$datatb = Task::find($task);
$value1 = array(
                            'paymentid'=> $paymentid ,
                            'tstatus'=>'payment completed',
                            'tremark'=>'good'                           

                      );


$datatb->fill($value1)->save();
}
else
{

$task = array(
                            'employeeid'=> $employeeid,
                            'store'=>$storeid,
                            'mode'=>'pay',
                            'description'=>'new task with out tid',
                            'tdate' => date("m/d/Y"),
                            'ttime' => date("h:i:sa"),
                            'paymentid'=> $paymentid,
                            'tstatus'=>'payment completed',
                            'tremark'=>'good'                           

                      );


 Task::create($task);
}

  $input['paymentid'] = $paymentid;
   Payment::create($input);
    $data = Store::find($storeid);
 
    $value['paid'] = $data['paid']+$amount;
    $value['balance'] = $data['total']-$value['paid'];
  $data->fill($value)->save();


return response()->json(['status'=>true,'message'=>'Payment booked successfully','balance'=>$value['balance']]);

}



public function addStore(Request $request){
  $input = $request->all();   
     Store::create($input);
return response()->json(['status'=>true,'message'=>'Store created']);
  }

public function reSchedule(Request $request){
$taskid= $request->input('taskid');
$employeeid= $request->input('employeeid');
$tdate= $request->input('tdate');
$ttime= $request->input('ttime');
$data = Task::find($taskid);
$value['employeeid'] = $employeeid;
  $value['ttime'] = $ttime;
  $value['tdate'] = $tdate;

  $data->fill($value)->save();
return response()->json(['status'=>true,'message'=>'task rescheduled']);
}


     public function getStores(){
  

     $store = Store::all();
       
  return response()->json(['status'=>true,'stores'=>$store]);


}



public function getProductsFilterold1(Request $request){ 

  $colors[]= $request->input('color');
  $sizes[]= $request->input('size');
   $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');
 $sizeids = array();
      if($sizes){
    foreach ($sizes as $size)
     {     
    $sizeids = $size; 
 }
 } 


 $colorids = array();
      if($colors){
    foreach ($colors as $color)
     {     
    $colorids = $color; 
 }
 } 
 
 $pids = Product::whereIn('id',function($query) use($sizeids){
    $query->select('productid')
    ->from(with(new Productsize)->getTable())
    ->whereIn('id',$sizeids);
  })->orwhereIn('id',function($query) use($colorids){
    $query->select('productid')
    ->from(with(new Productcolor)->getTable())
    ->whereIn('id',$colorids);
  })->get();


 $psize =array();

 foreach ($pids as $pid){
  // $plist= Product::where([['id', '=', $pid->id]])->get(); 
 $plist = DB::table('products')->where('id',$pid->id)->first(); 
 //$plist = DB::select("select * from products where id = $pid->id AND $pid->catid = 1"); 
 //  $brand['brand'] = Brand::where('id',$id)->first();
   if($plist->catid==$catagory&&$plist->brandid==$brand) {
   $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
  $psize= Productsize::where('productid',$pid->id)->get(); 

 $data[] = array(
                        'id'=>$pid->id,
                        'product'=>$pid->product,
                        'price' =>$pid->price,
                        'price unit' =>$pid->priceunit,
                         'brand id' =>$pid->brandid,
                          'catagory id' =>$pid->catid,
                           'description' =>$pid->description,
                            'product image' =>$pid->pimage,
                             'color' =>$pcolor,
                             'size' =>$psize
                           );

 } elseif ($catagory==0&&$brand==0) {
 $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
  $psize= Productsize::where('productid',$pid->id)->get(); 

 $data[] = array(
                        'id'=>$pid->id,
                        'product'=>$pid->product,
                        'price' =>$pid->price,
                        'price unit' =>$pid->priceunit,
                         'brand id' =>$pid->brandid,
                          'catagory id' =>$pid->catid,
                           'description' =>$pid->description,
                            'product image' =>$pid->pimage,
                             'color' =>$pcolor,
                             'size' =>$psize
                           );

 }
 elseif ($catagory==0&&$plist->brandid==$brand) {

  $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
  $psize= Productsize::where('productid',$pid->id)->get(); 

 $data[] = array(
                        'id'=>$pid->id,
                        'product'=>$pid->product,
                        'price' =>$pid->price,
                        'price unit' =>$pid->priceunit,
                         'brand id' =>$pid->brandid,
                          'catagory id' =>$pid->catid,
                           'description' =>$pid->description,
                            'product image' =>$pid->pimage,
                             'color' =>$pcolor,
                             'size' =>$psize
                           );

   # code...
 }

  elseif ($plist->catid==$catagory&&$brand==0) {

  $pcolor =  DB::table('productcolors')->where('productid',$pid->id)->get();
  $psize= Productsize::where('productid',$pid->id)->get(); 

 $data[] = array(
                        'id'=>$pid->id,
                        'product'=>$pid->product,
                        'price' =>$pid->price,
                        'price unit' =>$pid->priceunit,
                         'brand id' =>$pid->brandid,
                          'catagory id' =>$pid->catid,
                           'description' =>$pid->description,
                            'product image' =>$pid->pimage,
                             'color' =>$pcolor,
                             'size' =>$psize
                           );

   # code...
 }




}




if (empty($data))
 {return response()->json(['false'=> 'No data']);} 
else{
  return response()->json(['Product Details'=> $data]);}

}


public function getProductsFilterold2(Request $request){ 

  $colors[]= $request->input('color');
  $sizes[]= $request->input('size');
   $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');
 
  $sizeids = array();
      if($sizes){
    foreach ($sizes as $size)
     {     
    $sizeids = $size; 
 }
 } 

  $colorids = array();
      if($colors){
    foreach ($colors as $color)
     {     
    $colorids = $color; 
 }
 }

$results1 = DB::table('productsizes')->select('productid')->whereIn('sizeid', $sizeids)->get();
$results2 = DB::table('productcolors')->select('productid')->whereIn('colorid', $colorids)->get();
$results3 =  DB::table('products')->select('id as productid')->where('brandid',$brand)->orWhere('catid',$catagory)->get();

// $res1 = array();
//       if($results1){
//     foreach ($results1 as $result1)
//      {     
//     $res1 = $result1; }}

 //$value = count($res1);    
//    for ($i = 0; $i < $value; $i++){
// $data = array(
//                         $i=>$results1[0][$i]
                      
//                            );

//  }
//$total = $result1+$result2+$result3;
// print_r($results1[0][1]);
//  print_r($data);
//exit;
// foreach ($results1 as $result1)
// {
//  $res1 = $result1;

// }

 $value = count($colorids); 
print_r($value);
  exit;






 
// print_r($results1[0][0]->productid);
//exit;
 $results4 = $results1->merge($results2);
 $results5 = $results4->merge($results3);

 //print_r($results5);
//exit;

$results5u = $results5->unique();
//$results5us = sort($results5u);

foreach ($results5u as $results5uind)
{
 $result[] = $results5uind;

}

 $value = count($result); 
  for ($i = 0; $i < $value; $i++){

  $product = Product::where('id',$result[$i]->productid)->get();
     $data[] = array(
                      'product'=>$product,
                       
                           );

  }

//print_r($results5u[1]->productid);
//exit;



 return response()->json(['r1'=> $results1,'r2'=> $results2,'r3'=> $results3,'r5'=> $results5u,'value'=>$value,'total'=>$result[0]->productid,'records'=>$data]);
}



public function getProductsFilter(Request $request){ 

  // $datas[] = array(
  //                     'product'=>'no data',
  //                      'color' =>'no data',
  //                       'size' =>'no data'                       
  //                          );

  $colors[]= $request->input('color');
  $sizes[]= $request->input('size');
   $brand= $request->input('brandid');
    $catagory= $request->input('categoryid');
 
  $sizeids = array();
      if($sizes){
    foreach ($sizes as $size)
     {     
    $sizeids = $size; 
 }
 } 

  $colorids = array();
      if($colors){
    foreach ($colors as $color)
     {     
    $colorids = $color; 
 }
 }
$value1c = count($catagory);
$value1b = count($brand); 
 $value1 = count($colorids); 
  $value2 = count($sizeids);
//print_r($value);
 // exit;


$results3 =  DB::table('products')->select('id as productid')->where('brandid',$brand)->orWhere('catid',$catagory)->get();
$results1 = DB::table('productsizes')->select('productid')->whereIn('sizeid', $sizeids)->get();
$results2 = DB::table('productcolors')->select('productid')->whereIn('colorid', $colorids)->get();


// $res1 = array();
//       if($results1){
//     foreach ($results1 as $result1)
//      {     
//     $res1 = $result1; }}

 //$value = count($res1);    
//    for ($i = 0; $i < $value; $i++){
// $data = array(
//                         $i=>$results1[0][$i]
                      
//                            );

//  }
//$total = $result1+$result2+$result3;
// print_r($results1[0][1]);
//  print_r($colors);
//exit;
// foreach ($results1 as $result1)
// {
//  $res1 = $result1;

// }

//  $value = count($res1); 
// print_r($value






 
// print_r($results1[0][0]->productid);
//exit;
 $results4 = $results1->merge($results2);
 $results5 = $results4->merge($results3);

 //print_r($results5);
//exit;

$results5u = $results5->unique();
//$results5us = sort($results5u);

foreach ($results5u as $results5uind)
{
 $result[] = $results5uind;

}

// no 9

  if(($value1==0)&&($value2==0)&&($value1b==0)&&($value1c!=0)){

  $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
  if(count($products)){
foreach ($products as $product){

        $color= Productcolor::where('productid',$product->productid)->get(); 
           $size= Productsize::where('productid',$product->productid)->get(); 

 $datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );

// print_r($datas);
// exit;

}
}
   }

//no 8
 elseif(($value1==0)&&($value2==0)&&($value1c==0)&&($value1b!=0)){

  $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
  if(count($products)){
foreach ($products as $product){

        $color= Productcolor::where('productid',$product->productid)->get(); 
           $size= Productsize::where('productid',$product->productid)->get(); 

$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );

// print_r($datas);
// exit;

}
}
   }

// no1
   elseif(($value1!=0)&&($value2!=0)&&($value1b==0)&&($value1c!=0)){

 $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
  if(count($products)){
foreach ($products as $product){

   $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  
      
           $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();

 if ((!$color->isEmpty())&&(!$size->isEmpty())){ 


$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );

} else{}


}
}
}


//no 6

 elseif(($value1==0)&&($value2!=0)&&($value1b!=0)&&($value1c==0)){

 $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
  if(count($products)){
foreach ($products as $product){

    
      
  $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();
  if (!$size->isEmpty()){      

$color= Productcolor::where('productid',$product->productid)->get(); 
$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );


   }

    

   else{
 


   }
 

}
}
}

//no 3
 elseif(($value1!=0)&&($value2==0)&&($value1b==0)&&($value1c!=0)){

 $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
  if(count($products)){
foreach ($products as $product){

   $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  
     
         
  if (!$color->isEmpty()){ 
    $size= Productsize::where('productid',$product->productid)->get();     
    
$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );


   }

    

   else{
 


   }
 

}
}
}

// no4
elseif(($value1==0)&&($value2!=0)&&($value1b==0)&&($value1c!=0)){

 $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('catid',$catagory)->get();
  if(count($products)){
foreach ($products as $product){    
      
           $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();
  if (!$size->isEmpty()){      
  $color= Productcolor::where('productid',$product->productid)->get(); 

$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );


   }

    

   else{
 


   }
 

}
}
}


// no 5

elseif(($value1!=0)&&($value2==0)&&($value1b!=0)&&($value1c==0)){

 $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
  if(count($products)){
foreach ($products as $product){

  $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  
      
   




  if (!$color->isEmpty()){      
    $size= Productsize::where('productid',$product->productid)->get();
$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );


   }

    

   else{
 


   }

}
}
}



//no 7

elseif(($value1==0)&&($value2==0)&&($value1b==0)&&($value1c==0)){

 $products = Product::all();

  if(count($products)){
foreach ($products as $product){

   $color= Productcolor::where('productid',$product->id)->get();  
      
           $size= Productsize::where('productid',$product->id)->get();

 $datas[] = array(
                       'id'=>$product->id,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );

                      


}
}







}

//full



//no 2

 elseif(($value1!=0)&&($value2!=0)&&($value1b!=0)&&($value1c==0)){

 $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
  if(count($products)){
foreach ($products as $product){

   $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  
      
           $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();

 if ((!$color->isEmpty())&&(!$size->isEmpty())){ 


$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );

} else{}

}
}
}


//no 10

 elseif(($value1!=0)&&($value2!=0)&&($value1b!=0)&&($value1c!=0)){

 $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('brandid',$brand)->get();
  if(count($products)){
foreach ($products as $product){

   $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();  
      
           $size= Productsize::where('productid',$product->productid)->whereIn('sizeid', $sizeids)->get();

 if ((!$color->isEmpty())&&(!$size->isEmpty())){ 


$datas[] = array(
                       'id'=>$product->productid,
                        'product'=>$product->product,
                        'price' =>$product->price,
                        'price unit' =>$product->priceunit,
                         'brand id' =>$product->brandid,
                          'catagory id' =>$product->catid,
                           'description' =>$product->description,
                            'product image' =>$product->pimage,
                          'color' =>$color,
                           'size' =>$size                       
                           );

} else{}

}
}
}


//no 11

 elseif(($value1!=0)&&($value2==0)&&($value1b==0)&&($value1c==0)){
  // $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();
 $colors = Productcolor::select('productid')->whereIn('colorid', $colorids)->get();

foreach ($colors as $color){

 $product = Product::select('id','product','price','priceunit','brandid','catid','description','pimage')->where('id',$color->productid)->get(); 

$color= Productcolor::where('productid',$product[0]->id)->get();  
      
    $size= Productsize::where('productid',$product[0]->id)->get();

$datas[] = array(
                     'id'=>$product[0]->id,
                        'product'=>$product[0]->product,
                       'price' =>$product[0]->price,
                        'price unit' =>$product[0]->priceunit,
                         'brand id' =>$product[0]->brandid,
                          'catagory id' =>$product[0]->catid,
                           'description' =>$product[0]->description,
                            'product image' =>$product[0]->pimage,
                       'color' =>$color,
                        'size' =>$size                       
                           );
}
 
}




//no 12

 elseif(($value1==0)&&($value2!=0)&&($value1b==0)&&($value1c==0)){
  // $color= Productcolor::where('productid',$product->productid)->whereIn('colorid', $colorids)->get();
 $sizes = Productsize::select('productid')->whereIn('sizeid', $sizeids)->get();

foreach ($sizes as $size){

 $product = Product::select('id','product','price','priceunit','brandid','catid','description','pimage')->where('id',$size->productid)->get(); 

$color= Productcolor::where('productid',$product[0]->id)->get();  
      
    $size= Productsize::where('productid',$product[0]->id)->get();

$datas[] = array(
                     'id'=>$product[0]->id,
                        'product'=>$product[0]->product,
                       'price' =>$product[0]->price,
                        'price unit' =>$product[0]->priceunit,
                         'brand id' =>$product[0]->brandid,
                          'catagory id' =>$product[0]->catid,
                           'description' =>$product[0]->description,
                            'product image' =>$product[0]->pimage,
                       'color' =>$color,
                        'size' =>$size                       
                           );
}
 
}



//no 13

 elseif(($value1!=0)&&($value2!=0)&&($value1b==0)&&($value1c==0)){
 $color= Productcolor::select('productid')->whereIn('colorid', $colorids)->get();
 $sizes = Productsize::select('productid')->whereIn('sizeid', $sizeids)->get();

foreach ($sizes as $size){

 $product = Product::select('id','product','price','priceunit','brandid','catid','description','pimage')->where('id',$size->productid)->get(); 

$color= Productcolor::where('productid',$product[0]->id)->get();  
      
    $size= Productsize::where('productid',$product[0]->id)->get();

$datas[] = array(
                     'id'=>$product[0]->id,
                        'product'=>$product[0]->product,
                       'price' =>$product[0]->price,
                        'price unit' =>$product[0]->priceunit,
                         'brand id' =>$product[0]->brandid,
                          'catagory id' =>$product[0]->catid,
                           'description' =>$product[0]->description,
                            'product image' =>$product[0]->pimage,
                       'color' =>$color,
                        'size' =>$size                       
                           );
}
 
}




   else{

 $value = count($result); 
  for ($i = 0; $i < $value; $i++){

  $products = Product::select('id as productid','product','price','priceunit','brandid','catid','description','pimage')->where('id',$result[$i]->productid)->get();


        $color= Productcolor::where('productid',$result[$i]->productid)->whereIn('colorid', $colorids)->get();      
      
           $size= Productsize::where('productid',$result[$i]->productid)->whereIn('sizeid', $sizeids)->get(); 
        
  
  if ((!$color->isEmpty())&&(!$size->isEmpty())){      
// $datas[] = array(
//                        'id'=>$product->productid,
//                         'product'=>$product->product,
//                         'price' =>$product->price,
//                         'price unit' =>$product->priceunit,
//                          'brand id' =>$product->brandid,
//                           'catagory id' =>$product->catid,
//                            'description' =>$product->description,
//                             'product image' =>$product->pimage,
//                           'color' =>$color,
//                            'size' =>$size                       
//                            );
   }

    

   else{
 


   }

  }


}
//end else



//print_r($results5u[1]->productid);
//exit;

if (empty($datas))
 {return response()->json(['false'=> 'No data']);} 
else{
// return response()->json($datas);
   return response()->json(['Product Details'=> $datas]);

}
}


}  