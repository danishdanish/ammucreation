<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Color;
 use App\User_information;
 use App\Store;
 use App\Task;
  use App\Order;
   use App\Placeorder;
    use App\Cash_in_hand;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;
 Use DB;



    class CashController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }

//cash in hand - sotrewise
     public function cash_list()
        {
            
            return view('pages/cash/list');
        }


         public function ajax_data()
        {
          

    $cash=Cash_in_hand::distinct()->get(['store']);


        $user = Store::join('cash_in_hands', 'stores.id', '=', 'cash_in_hands.store')->where('cash_in_hands.transfer','no')
->orderBy('cash_in_hands.date', 'desc')->distinct()->get(['stores.name as storename','stores.id as storeid']);


        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,                       
                          'store' =>$u->storename,                                                       
                             'actions' =>'  <a href="cash/view/'.$u->storeid.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
                           );

        }

             if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



//cash in hand - emplyeeewise
     public function cash_list_empwise()
        {
            
            return view('pages/cash/listempwise');
        }


  public function ajax_data_empwise()
        {
          

          $user = User_information::join('cash_in_hands', 'user_informations.id', '=', 'cash_in_hands.employee')->where('cash_in_hands.transfer','no')
->orderBy('cash_in_hands.date', 'desc')->distinct()->get(['user_informations.empname as empname','user_informations.id as userid']);


        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,                       
                          'employee' =>$u->empname,                                                       
                             'actions' =>'  <a href="cash/view_empwise/'.$u->userid.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
                           );

        }

             if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



        public function cashView_Empwise($id)
        {            
        
 
    $cash = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->where('employee',$id)->where('mode','cash')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.cheque',
       'cash_in_hands.amount',
      'user_informations.empname as empname'
              ]);

     $sqlc = "select SUM(amount) as sumcash  from cash_in_hands  where employee = $id and mode = 'cash' and transfer = 'no'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 


//       print_r($cashtot);
// exit;

        $cheque = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->join('stores', 'cash_in_hands.store', '=', 'stores.id')->where('employee',$id)->where('mode','cheque')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
       'cash_in_hands.cheque',
      'user_informations.empname as empname',
       'stores.name as storename'
              ]);

     $sqlch = "select SUM(amount) as sumcheque  from cash_in_hands  where employee = $id and mode = 'cheque' and transfer = 'no'";
       $chequetot = DB::select($sqlch);
     $chequetot=$chequetot[0]->sumcheque; 

         $emp['emp'] = User_information::where('id',$id)->first();
 
      return view('pages/cash/cashinhandempwise',['cash' => $cash,'cashTotal'=>$cashtot,'cheque' => $cheque,'chequeTotal'=>$chequetot],$emp);
        }  


         
//cash transfered

     public function cash_list_transfer()
        {
            
            return view('pages/cash/listtranfer');
        }


         public function ajax_data_transfer()
        {
          

    $user = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->where('transfer','yes')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.transferdate',
       'cash_in_hands.amount',    
      'user_informations.empname'
              ]);

    
        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                          'date' =>$u->transferdate,
                          'employee' =>$u->empname,
                          'amount' =>$u->amount,                              
                             'actions' =>'  <a href="#"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
                           );

        }

              if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }


         
//cash in hand  salesman 
     public function cash_list_ind()
        {
            
            return view('pages/cash/listind');
        }


         public function ajax_data_ind($id)
        {
          

    $user = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->Where('transfer','no')->Where('cash_in_hands.employee',$id)->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
      'user_informations.empname'
              ]);

 
        
        $i=1;

       

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                          'date' =>$u->date,
                          'employee' =>$u->empname,
                          'amount' =>$u->amount,                              
                             'actions' =>'<a href="cash/transfer/'.$u->cashid.'"><button class="btn btn-info" title="Process Order">Transfer</button>'                   
                           );

        }

              if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



public function cashView($id)
        {            
        
 
    $cash = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->where('store',$id)->where('mode','cash')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
        'cash_in_hands.cheque',
      'user_informations.empname as empname'
              ]);

     $sqlc = "select SUM(amount) as sumcash  from cash_in_hands  where store = $id and mode = 'cash' and transfer = 'no'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 


//       print_r($cashtot);
// exit;

        $cheque = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->join('stores', 'cash_in_hands.store', '=', 'stores.id')->where('store',$id)->where('mode','cheque')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
        'cash_in_hands.cheque',
      'user_informations.empname as empname',
       'stores.name as storename'
              ]);

     $sqlch = "select SUM(amount) as sumcheque  from cash_in_hands  where store = $id and mode = 'cheque' and transfer = 'no'";
       $chequetot = DB::select($sqlch);
     $chequetot=$chequetot[0]->sumcheque; 

         $store['store'] = Store::where('id',$id)->first();
 
      return view('pages/cash/cashinhandstore',['cash' => $cash,'cashTotal'=>$cashtot,'cheque' => $cheque,'chequeTotal'=>$chequetot],$store);
        }  


   public function post_cash_process(Request $request)

   {
       $validator = Validator::make($request->all(), [

               'courrier' => 'required', 
                'billno' => 'required', 
                'invoiceno' => 'required',              

          ],[
              
                                        
                                 ]);     


          if ($validator->passes()) {
        
                  $data = Placeorder::find($request->id);
           $data->courrier = $request->courrier;
          $data->processdate = date("Y-m-d");
         $data->billno = $request->billno;
          $data->invoiceno = $request->invoiceno;
           $data->status = 'cu';
          $data->save();

            return response()->json(['success'=>'Added new records.'.$request->id ]);  
          }
         
       else{
        return response()->json(['error'=>$validator->errors()->all()]);}

 
}

  public function cash_list_ind_tranfer($id)
{

$datatc = Cash_in_hand::find($id);
$value1 = array(
                            'transfer'=> 'yes' ,
                            'transferdate'=>date("Y-m-d")                                                    

                      );


$datatc->fill($value1)->save();
return redirect('/cash_in_hand_emp');
  
}



    }
