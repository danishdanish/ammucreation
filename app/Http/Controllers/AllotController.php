<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Role;
 use App\Module;
 use App\User_information;
 use App\Target;
 use Session;
 use Validator;
 use DB;


    class AllotController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


           public function allot_list()
        {
            
            return view('pages/allot/list');
        }

        public function ajax_data()
        {
          
            $target = DB::table('targets')
                        ->select('targets.id','targets.sales','targets.date','user_informations.empname')
                        ->leftJoin('user_informations','targets.user_id','=','user_informations.id')
                        ->orderBy('targets.date', 'desc')
                        ->get();
            
            $i=1;

            foreach ($target as $t) {
             
                        $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$t->empname,
                               'sales'=>$t->sales,
                               'target_month'=>date('F-Y',strtotime($t->date)),
                                 'actions' =>'<a href="allotment/allot_edit/'.$t->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a> <button title="delete" id="deletebtallot" class="delete btn btn-danger" data-id="'.$t->id.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                               );

            }

      if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }

             public function allot_add()
        {

            $emps = User_information::where('designation','salesman')->get();

            return view('pages/allot/add',['emps' => $emps]);
        }


    public function add_allot(Request $request){
     
         foreach($request->employee as $e)
         {

            $data[] = array(
                          'user_id'=>$e,
                          'sales'=>$request->box,
                          'date'=>date('Y-m-d'),
                          'created_at'=>date('Y-m-d H:i:s')
                         );
         }

         Target::insert($data);

         Session::flash('add',1);

         return redirect('/allotment');

      }

      public function allot_edit($id)
      {

        $data['emps'] = User_information::get();

        $data['edit_data'] = Target::where('id',$id)->first();

        return view('pages.allot.edit',$data);
      }


      public function update_allot(Request $request)
      {

          $data = array(
                        'user_id'=>$request->employee,
                        'sales' =>$request->sales,
                        'updated_at'=>date('Y-m-d H:i:s')
                       );

          Target::where('id',$request->edit_id)->update($data);

          Session::flash('update',1);

          return redirect('/allotment');

      }
   

  


           public function delete(Request $request){
            $id = $request->id;
            Target::destroy($id);
               return response()->json(['success'=>'Succesfully Deleted!']);
           
       }





    }
