<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Color;
 use App\User_information;
 use App\Store;
 use App\Task;
  use App\Order;
   use App\Placeorder;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;
 Use DB;



    class DespatchController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


     public function despatch_data()
        {
            
            return view('pages/despatch/despatchlist');
        }


         public function ajax_data_emps()
        {
          

    $user = Placeorder::join('user_informations', 'placeorders.employee', '=', 'user_informations.id')
->join('stores', 'placeorders.store', '=', 'stores.id')->join('products', 'placeorders.product', '=', 'products.id')->where('placeorders.status','=', 'cu')->where('placeorders.status','=', 'cu')->orderBy('placeorders.processdate', 'desc')->get(['placeorders.id as placeorder','placeorders.date as date','placeorders.processdate as pdate','placeorders.product as product','placeorders.quantity as quantity','placeorders.price as price','placeorders.processsqty as processquantity','user_informations.empname as employeename',
     'stores.name as store','products.product as productname','placeorders.processamount as processamount'      
        ]);

        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                          'date' =>$u->pdate,
                          'employee' =>$u->employeename,
                          'store' =>$u->store,
                          'product' =>$u->productname,
                          'quantity' =>$u->quantity,
                          'price' =>$u->price,
                          'pquantity' =>$u->processquantity,
                          'pprice' =>$u->processamount,
                          'status' =>'Despatched',    
                             'actions' =>'  <a href="despatch/despatch_view/'.$u->placeorder.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
                           );

        }

            if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }
         





public function despatchView($id)
        {            
        
 $data['data'] = Placeorder::join('user_informations', 'placeorders.employee', '=', 'user_informations.id')
->join('stores', 'placeorders.store', '=', 'stores.id')->join('products', 'placeorders.product', '=', 'products.id')->where('placeorders.id','=', $id)->orderBy('placeorders.id', 'desc')->first([
      'placeorders.id as placeorder','placeorders.date as date','placeorders.processdate as pdate','placeorders.product as product','placeorders.quantity as quantity','placeorders.price as price',
      'user_informations.empname as employee' ,
     'stores.name as store','products.product as productname','placeorders.courrier','placeorders.billno',
     'placeorders.invoiceno','stores.description as storedesc','stores.address as storeaddress','placeorders.processamount as processamount','placeorders.processsqty as processquantity'     
        ]);     
 
      return view('pages/despatch/despatchview',$data);

        }  


   public function post_order_process(Request $request)

   {
       $validator = Validator::make($request->all(), [

               'courrier' => 'required', 
                'billno' => 'required', 
                'invoiceno' => 'required',              

          ],[
              
                                        
                                 ]);     


          if ($validator->passes()) {
        
                  $data = Placeorder::find($request->id);
           $data->courrier = $request->courrier;
          $data->processdate = date("Y-m-d");
         $data->billno = $request->billno;
          $data->invoiceno = $request->invoiceno;
           $data->status = 'cu';
          $data->save();

            return response()->json(['success'=>'Added new records.'.$request->id ]);  
          }
         
       else{
        return response()->json(['error'=>$validator->errors()->all()]);}

 
}


//pending order

 public function despatch_data_pending()
        {
            
            return view('pages/despatch/despatchlistpending');
        }


   public function ajax_data_emps_pending()
        {
          

    $user = Placeorder::join('user_informations', 'placeorders.employee', '=', 'user_informations.id')
->join('stores', 'placeorders.store', '=', 'stores.id')->join('products', 'placeorders.product', '=', 'products.id')->where('placeorders.partial','=', 'yes')->where('placeorders.status','=', 'cu')->orderBy('placeorders.id', 'desc')->get(['placeorders.id as placeorder','placeorders.date as date','placeorders.processdate as pdate','placeorders.product as product','placeorders.quantity as quantity','placeorders.price as price','placeorders.processsqty as processquantity','user_informations.empname as employeename',
     'stores.name as store','products.product as productname','placeorders.processamount as processamount'      
        ]);

        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                          'date' =>$u->pdate,
                          'employee' =>$u->employeename,
                          'store' =>$u->store,
                          'product' =>$u->productname,
                          'quantity' =>$u->quantity,
                          'price' =>$u->price,
                          'pquantity' =>$u->processquantity,
                          'pprice' =>$u->processamount,
                          'status' =>'Despatched',    
                             'actions' =>'<a href="#">PROCESS ORDER</a><br><a href="despatch/despatch_view/'.$u->placeorder.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button></a>'                   
                           );

        }

            if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



    }
