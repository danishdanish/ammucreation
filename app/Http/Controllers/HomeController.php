<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\User_information;
use App\Role;
use App\Sale;
Use App\Placeorder;
Use App\Payment;
Use App\Task;
use Khill\Lavacharts\Lavacharts;
use Hash;
use Session;
use DB;
use Illuminate\Support\Facades\File;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

        {

    // $viewer = Placeorder::select(DB::raw("SUM(price) as price"))->orderBy("date")

    //     ->groupBy(DB::raw("month(date)"))->get()->toArray();


    // $viewer = array_column($viewer, 'price');

    

    // $click = Placeorder::select(DB::raw("SUM(processamount) as sales"))->orderBy("date")

    //     ->groupBy(DB::raw("month(date)"))->get()->toArray();

    // $click = array_column($click, 'sales');

    // return view('home')

    //         ->with('viewer',json_encode($viewer,JSON_NUMERIC_CHECK))

    //         ->with('click',json_encode($click,JSON_NUMERIC_CHECK));



$lava = new Lavacharts; // See note below for Laravel
$finances = $lava->DataTable();

$finances->addStringColumn('Year')
         ->addNumberColumn('Orders')
         ->addNumberColumn('Sales')
         ->setDateTimeFormat('Y');


     $sql= "SELECT SUM(price) as orders,SUM(processamount) as sales,DATE_FORMAT(date, '%M') as smonth FROM placeorders GROUP BY month(date)";
      $datas = DB::select($sql);
   
   
        foreach ($datas as $data){
       
                $finances->addRow([$data->smonth, $data->orders,$data->sales]);
       }




$lava->ColumnChart('Finances', $finances, [
    'title' => 'Orders and Sales',
    'titleTextStyle' => [
        'color'    => '#eb6b2c',
        'fontSize' => 14
    ]
]);

$date = date('n');
$payment= Payment::whereMonth('date', '=',$date)->sum('amount');

for ($i = 0; $i < 6; $i++){
$sale= Placeorder::whereMonth('processdate', '=',date('n')-$i)->sum('processsqty');
$sales[] = array($i =>$sale);
}

for ($i = 0; $i < 6; $i++){
$order= Placeorder::whereMonth('date', '=',date('n')-$i)->sum('quantity');
$orders[] = array($i =>$order);
}

$visit= Task::whereMonth('tdate', '=',$date)->where('tstatus','=','visit completed')->get();
$count = $visit->count();
   // return view('home',['lava' => $lava]);
return view('home',['payment'=>$payment,'salecount'=>$sales,'visit'=>$count,'ordercount'=>$orders]);
    }


     public function user_list()
    {
        
        return view('pages/user/list');
    }


    public function ajax_data()
    {
      
        // $user = User::get();
         $user = User::all();
        
        $i=1;

        foreach ($user as $u) {
         
        $image =  '<img width="80px" height="80px" src="'.url('public/theme/image/profile_photo/'.$u->profile_photo).'"/>';
            $row[] = array(
                           'si_no' =>$i++,
                           'name' =>$u->name,
                           'email' =>$u->email,
                             'actions' =>'<a href="user/user_view/'.$u->id.'"><button class="btn btn-info" title="User View"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button><a href="user/user_profile/'.$u->id.'">  <button class="btn btn-info" title="User Password"><span class="glyphicon glyphicon-user"></span></button></a> <a href="user/user_permissions/'.$u->id.'"><button class="btn btn-info" title="Permissions"><span class="glyphicon glyphicon-asterisk"></span></button></a> <a href="user/user_edit/'.$u->id.'"><button class="btn btn-info" title="User Edit"><span class="glyphicon glyphicon-edit"></span></button></a>'                   
                           );

        }

         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
    );
    echo json_encode($response);

    }


       public function user_add()
    {
       $roles = Role::all();
        return view('pages/user/add',['roles' => $roles]);    
    }



     public function insert_user_data(Request $request)
    {


             $this->validate($request,[
                                'empname'=>'required',
                                'email' =>'required|email',
                                'password'=>'required',
                                'phone' =>'required|regex:/[\d ()+-]+$/',
                                'designation' =>'required',
                                'doj' =>'required',
                                 'role' =>'required',
                                 'designation' =>'required',
                                'address' =>'required'
                              

                               ],[
                                   'empname.required' =>'Name field is required',
                                 'email.required'=>'Email field is required',
                                 'email.email' =>'Please enter valid email',
                                 'password.required' =>'Password is required',
                                 'phone.required' =>'Phone number is required',
                                 'phone.regex'=>'please enter valide phone number',
                                 'designation.required' =>'Designation is required',
                                 'doj.required'=>'Join date is required',
                                 'address.required' =>'Address is required'
                               ]);
  
         $username = mt_rand(100000, 999999);
         $username = 'usr'.$username;
         // $password = mt_rand(100000, 999999);
         // $password = 'pass'.$password;
         $user = new User;

        $user->name = $request->empname;

        $user->email = $request->email;
        $user->user_role = $request->role;

        $user->password = Hash::make($request->password);
        $user->save();

      DB::table('leave_management')->insert([
    ['user_id' => $user->id,'date' => date('Y-m-d',strtotime($request->doj))]
]);


    
        if($request->profile_photo!='')
        {
            $file = $request->profile_photo;

            $file_name = time().$file->getClientOriginalName();

            $path =  public_path('/theme/image/profile_photo/');

            $file->move($path,$file_name);
            
        } 

        $information = new User_information;
        $information->id = $user->id;    
        $information->empname = $request->empname;
        $information->email = $request->email;
        $information->phone = $request->phone;
        $information->doj = date('Y-m-d',strtotime($request->doj)); 
        $information->designation = $request->designation;   
        $information->address = $request->address;
        $information->is_active = $request->is_active;   
        $information->photo = $request->profile_photo!=''?$file_name:$request->profile_photo;          
        $information->save(); 

      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();

    }


      public function edit_user_data($id){
       $profile['profile'] = User_information::where('id',$id)->first();
        return view('pages/user/edit',$profile); 
       }

public function view_user_data($id){
       $profile['profile'] = User_information::where('id',$id)->first();
        return view('pages/user/view',$profile); 
       }

    public function post_edit_user_data(Request $request)
    {

        $this->validate($request,[
                                'empname'=>'required',
                                'email' =>'required|email',
                                'phone' =>'required|regex:/[\d ()+-]+$/',
                                'designation' =>'required',
                                'doj' =>'required',
                                'address' =>'required',
                               ],[
                                   'empname.required' =>'Name field is required',
                                 'email.required'=>'Email field is required',
                                 'email.email' =>'Please enter valid email',
                                 'phone.required' =>'Phone number is required',
                                 'phone.regex'=>'please enter valide phone number',
                                 'designation.required' =>'Designation is required',
                                 'doj.required'=>'Join date is required',
                                 'address.required' =>'Address is required'
                               ]); 
         $uid = $request->uid;
         $data = User_information::find($uid);
         $input = $request->all();
        $input['doj']= date('Y-m-d',strtotime($request->doj));
       
         if($request->profile_photo!='')
        {
            $file = $request->profile_photo;

            $file_name = time().$file->getClientOriginalName();

            $path =  public_path('/theme/image/profile_photo/');

            $file->move($path,$file_name);
             $input['photo'] = $file_name;
             File::delete('theme/image/profile_photo/'.$data->photo);          
            
    } 
         $data->fill($input)->save();
         Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
    }




       public function delete_user_data(Request $request){
         $uid = $request->id;
       //    return response()->json(['success'=>$uid]);
        $data = User_information::find($uid);
         $data->is_active = 0;
          $data->save();
           return response()->json(['success'=>'Succesfully Deleted!']);
       
   }


       public function checkmodal(){
         return response()->json(['success'=>'connected ok.']);
   }


    public function user_permissions($id)
    {
      $data['data'] = User_information::leftJoin('users', function($join) use($id) {
      $join->on('user_informations.id', '=', 'users.id');
    })->where('user_informations.id',$id)->first([
     'user_informations.id as id', 
    'user_informations.empname as emp',
      'users.user_role as user_role'     
        ]);

        $roles = Role::all();
        return view('pages/user/permissions',['roles' => $roles],$data);
    }


     public function user_permissions_add(Request $request) {
   $rid=$request->uid;
     $role_sel=$request->role_sel;
     // $data = User_information::find($rid);   
     //  $data->user_role=$role_sel;         
     //  $data->save();

          $data1 = User::find($rid);   
      $data1->user_role=$role_sel;         
      $data1->save();

     Session::flash('message', "Status Updated");
    return back(); return redirect()->back();

  }


      public function user_profile($id)
    {
        $data['data'] = User::where('id',$id)->first();
        return view('pages/user/profile',$data);
    }


     public function post_user_profile(Request $request) {


          $this->validate($request,[
                            'password' =>'required', 
                             'password_confirm' => 'required|same:password',                         
                               ],
                               [
                                'password.required' =>'Password is required',
                                 'password_confirm.required' =>'Password Confirmation is required'                  
                               ]); 

    $id=$request->id;    
     $data = User::find($id); 
      $password = Hash::make($request->password);
          $data->password=$password;        
      $data->save();     
     Session::flash('message', "Status Updated");
    return back(); return redirect()->back();

  }





}
