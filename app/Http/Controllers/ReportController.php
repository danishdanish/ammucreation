<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Color;
 use App\User_information;
 use App\Store;
 use App\Task;
  use App\Order;
   use App\Placeorder;
   use App\Payment;
    use App\Cash_in_hand;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;
 Use DB;



    class ReportController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }

//payment
  public function pay_rep()
        {
          $from = date('Y-m-d');
          $to = date('Y-m-d');
         $cash = Payment::join('user_informations', 'payments.employee', '=', 'user_informations.id')->join('stores', 'payments.store', '=', 'stores.id')->select([
    'payments.id as cashid',
      'payments.date',
       'payments.cheque',
       'payments.amount',
       'payments.store',
        'stores.name as storename',
       'payments.mode',
      'user_informations.empname as empname'
              ])->whereBetween('date', [$from, $to])
->orderBy('payments.date', 'desc')->paginate(8);

     $sqlc = "select SUM(amount) as sumcash  from payments where date between '$from' and '$to'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 


//       print_r($cashtot);
// exit;

         $emps = User_information::all();
          $stores= Store::all();
                   $fromlog=date('m/d/Y');
           $tolog=date('m/d/Y');
           $id='allemps';
           $store='allstores';
       return view('pages/report/paymentreport',['cash' => $cash,'cashtotal'=>$cashtot,'emps' => $emps,'stores' => $stores,'fromlog'=>$fromlog,'tolog'=>$tolog,'idlog'=>$id,'stidlog'=>$store]);
  
        }


public function pay_rep_process(Request $request)
        {

         $from=date('Y-m-d',strtotime($request->from));
         $to =date('Y-m-d',strtotime($request->to));
         $id=$request->emp;
         $store=$request->store;


             
      if(($id=='allemps')&&($store!='allstores')) {

      $cash = Payment::join('user_informations', 'payments.employee', '=', 'user_informations.id')->join('stores', 'payments.store', '=', 'stores.id')->select([
    'payments.id as cashid',
      'payments.date',
       'payments.cheque',
       'payments.amount',
       'payments.store',
        'stores.name as storename',
       'payments.mode',
      'user_informations.empname as empname'
              ])->where('store',$store)->whereBetween('date', [$from, $to])
->orderBy('payments.date', 'desc')->paginate(8);

     $sqlc = "select SUM(amount) as sumcash  from payments where store = $store and date between '$from' and '$to'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 
      } 
      else if(($id!='allemps')&&($store=='allstores'))
      {

      $cash = Payment::join('user_informations', 'payments.employee', '=', 'user_informations.id')->join('stores', 'payments.store', '=', 'stores.id')->select([
    'payments.id as cashid',
      'payments.date',
       'payments.cheque',
       'payments.amount',
       'payments.store',
        'stores.name as storename',
       'payments.mode',
      'user_informations.empname as empname'
              ])->where('employee',$id)->whereBetween('date', [$from, $to])
->orderBy('payments.date', 'desc')->paginate(8);

     $sqlc = "select SUM(amount) as sumcash  from payments where employee = $id and date between '$from' and '$to'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 
            }

                else if(($id=='allemps')&&($store=='allstores'))
      {

      $cash = Payment::join('user_informations', 'payments.employee', '=', 'user_informations.id')->join('stores', 'payments.store', '=', 'stores.id')->select([
    'payments.id as cashid',
      'payments.date',
       'payments.cheque',
       'payments.amount',
       'payments.store',
        'stores.name as storename',
       'payments.mode',
      'user_informations.empname as empname'
              ])->whereBetween('date', [$from, $to])
->orderBy('payments.date', 'desc')->paginate(8);

     $sqlc = "select SUM(amount) as sumcash  from payments where date between '$from' and '$to'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 
            }
       else{

        $cash = Payment::join('user_informations', 'payments.employee', '=', 'user_informations.id')->join('stores', 'payments.store', '=', 'stores.id')->select([
    'payments.id as cashid',
      'payments.date',
       'payments.cheque',
       'payments.amount',
       'payments.mode',
       'stores.name as storename',
      'user_informations.empname as empname'
              ])->where('employee',$id)->where('store',$store)->whereBetween('date', [$from, $to])
->orderBy('payments.date', 'desc')->paginate(8);

     $sqlc = "select SUM(amount) as sumcash  from payments  where employee = $id and store = $store and date between '$from' and '$to'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 
}
    $emps = User_information::all();
          $stores= Store::all();
           $fromlog = $request->from;
           $tolog = $request->to;
                return view('pages/report/paymentreport',['cash' => $cash,'cashtotal'=>$cashtot,'emps' => $emps,'stores' => $stores,'fromlog'=>$fromlog,'tolog'=>$tolog,'idlog'=>$id,'stidlog'=>$store]);


        }


//task report

          public function task_rep()
        {
          $from = date('Y-m-d');
          $to = date('Y-m-d');
         $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
    'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);
 $emps = User_information::all();
          $stores= Store::all();
          $fromlog=date('m/d/Y');
           $tolog=date('m/d/Y');
           $id='allemps';
           $store='allstores';
       return view('pages/report/taskreport',['cash' => $cash,'emps' => $emps,'stores' => $stores,'fromlog'=>$fromlog,'tolog'=>$tolog,'idlog'=>$id,'stidlog'=>$store]);
  
        }


        public function task_rep_process(Request $request)
        {

         $from=date('Y-m-d',strtotime($request->from));
         $to =date('Y-m-d',strtotime($request->to));
         $id=$request->emp;
         $store=$request->store;
           $po=$request->po;
           $pay=$request->pay;
           $visit=$request->visit;
           $fromlog = $request->from;
           $tolog = $request->to;


      if(($id=='allemps')&&($store!='allstores')) {
             if($po||$pay||$visit){
      $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
     'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->whereIn('tstatus', array($pay,$visit,$po))->where('store',$store)->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);
}
else
{
     $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
     'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->where('store',$store)->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);
}

 
      } 
      else if(($id!='allemps')&&($store=='allstores'))
      {
        if($po||$pay||$visit){
      $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
 'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->whereIn('tstatus', array($pay,$visit,$po))->where('employeeid',$id)->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);}
else
{
      $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
 'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->where('employeeid',$id)->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);
}


            }

                else if(($id=='allemps')&&($store=='allstores'))
      {


if($po||$pay||$visit){
    $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
   'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->whereIn('tstatus', array($pay,$visit,$po))->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);}
else{

  $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
   'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);

}


            }
       else{

 if($po||$pay||$visit){
        $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
  'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->whereIn('tstatus', array($pay,$visit,$po))->where('employeeid',$id)->where('store',$store)->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);

}else
{

   $cash = Task::join('user_informations', 'tasks.employeeid', '=', 'user_informations.id')->join('stores', 'tasks.store', '=', 'stores.id')->select([
  'tasks.id as taskid',
      'tasks.tdate as taskdate',
       'tasks.ttime as tasktime',
       'tasks.tremark as taskremark',
       'tasks.tstatus as taskstatus',     
        'stores.name as storename',
          'user_informations.empname as empname'
              ])->where('employeeid',$id)->where('store',$store)->whereBetween('tdate', [$from, $to])
->orderBy('tasks.tdate', 'desc')->paginate(8);
}

}
    $emps = User_information::all();
          $stores= Store::all();
       
       return view('pages/report/taskreport',['cash' => $cash,'emps' => $emps,'stores' => $stores,'fromlog'=>$fromlog,'tolog'=>$tolog,'idlog'=>$id,'stidlog'=>$store]);

        }


//cash in hand - storewise
     public function cash_list()
        {
            
            return view('pages/cash/list');
        }


         public function ajax_data()
        {
          

    $cash=Cash_in_hand::distinct()->get(['store']);

//           print_r($cash);
// exit;


        $user = Store::join('cash_in_hands', 'stores.id', '=', 'cash_in_hands.store')->where('cash_in_hands.transfer','no')
->orderBy('cash_in_hands.date', 'desc')->distinct()->get(['stores.name as storename','stores.id as storeid']);


        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,                       
                          'store' =>$u->storename,                                                       
                             'actions' =>'  <a href="cash/view/'.$u->storeid.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
                           );

        }

             if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



//cash in hand - emplyeeewise
     public function cash_list_empwise()
        {
            
            return view('pages/cash/listempwise');
        }


  public function ajax_data_empwise()
        {
          

          $user = User_information::join('cash_in_hands', 'user_informations.id', '=', 'cash_in_hands.employee')->where('cash_in_hands.transfer','no')
->orderBy('cash_in_hands.date', 'desc')->distinct()->get(['user_informations.empname as empname','user_informations.id as userid']);


        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,                       
                          'employee' =>$u->empname,                                                       
                             'actions' =>'  <a href="cash/view_empwise/'.$u->userid.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
                           );

        }

             if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



        public function cashView_Empwise($id)
        {            
        
 
    $cash = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->where('employee',$id)->where('mode','cash')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.cheque',
       'cash_in_hands.amount',
      'user_informations.empname as empname'
              ]);

     $sqlc = "select SUM(amount) as sumcash  from cash_in_hands  where employee = $id and mode = 'cash' and transfer = 'no'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 


//       print_r($cashtot);
// exit;

        $cheque = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->join('stores', 'cash_in_hands.store', '=', 'stores.id')->where('employee',$id)->where('mode','cheque')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
       'cash_in_hands.cheque',
      'user_informations.empname as empname',
       'stores.name as storename'
              ]);

     $sqlch = "select SUM(amount) as sumcheque  from cash_in_hands  where employee = $id and mode = 'cheque' and transfer = 'no'";
       $chequetot = DB::select($sqlch);
     $chequetot=$chequetot[0]->sumcheque; 

         $emp['emp'] = User_information::where('id',$id)->first();
 
      return view('pages/cash/cashinhandempwise',['cash' => $cash,'cashTotal'=>$cashtot,'cheque' => $cheque,'chequeTotal'=>$chequetot],$emp);
        }  


         
//cash transfered

     public function cash_list_transfer()
        {
            
            return view('pages/cash/listtranfer');
        }


         public function ajax_data_transfer()
        {
          

    $user = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->where('transfer','yes')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.transferdate',
       'cash_in_hands.amount',    
      'user_informations.empname'
              ]);

    
        
        $i=1;

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                          'date' =>$u->transferdate,
                          'employee' =>$u->empname,
                          'amount' =>$u->amount,                              
                             'actions' =>'  <a href="#"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
                           );

        }

              if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }


         
//cash in hand  salesman 
     public function cash_list_ind()
        {
            
            return view('pages/cash/listind');
        }


         public function ajax_data_ind($id)
        {
          

    $user = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->Where('transfer','no')->Where('cash_in_hands.employee',$id)->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
      'user_informations.empname'
              ]);

 
        
        $i=1;

       

        foreach ($user as $u) {         
    
            $row[] = array(
                           'si_no' =>$i++,
                          'date' =>$u->date,
                          'employee' =>$u->empname,
                          'amount' =>$u->amount,                              
                             'actions' =>'<a href="cash/transfer/'.$u->cashid.'"><button class="btn btn-info" title="Process Order">Transfer</button>'                   
                           );

        }

              if(!empty($row)){
         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
       
    );
   }

   else{
      $response = array('data'=>'');
   }
    
        echo json_encode($response);

        }



public function cashView($id)
        {            
        
 
    $cash = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->where('store',$id)->where('mode','cash')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
        'cash_in_hands.cheque',
      'user_informations.empname as empname'
              ]);

     $sqlc = "select SUM(amount) as sumcash  from cash_in_hands  where store = $id and mode = 'cash' and transfer = 'no'";
       $cashtot = DB::select($sqlc);
     $cashtot=$cashtot[0]->sumcash; 


//       print_r($cashtot);
// exit;

        $cheque = Cash_in_hand::join('user_informations', 'cash_in_hands.employee', '=', 'user_informations.id')->join('stores', 'cash_in_hands.store', '=', 'stores.id')->where('store',$id)->where('mode','cheque')->where('transfer','no')
->orderBy('cash_in_hands.date', 'desc')->get([
    'cash_in_hands.id as cashid',
      'cash_in_hands.date',
       'cash_in_hands.amount',
        'cash_in_hands.cheque',
      'user_informations.empname as empname',
       'stores.name as storename'
              ]);

     $sqlch = "select SUM(amount) as sumcheque  from cash_in_hands  where store = $id and mode = 'cheque' and transfer = 'no'";
       $chequetot = DB::select($sqlch);
     $chequetot=$chequetot[0]->sumcheque; 

         $store['store'] = Store::where('id',$id)->first();
 
      return view('pages/cash/cashinhandstore',['cash' => $cash,'cashTotal'=>$cashtot,'cheque' => $cheque,'chequeTotal'=>$chequetot],$store);
        }  


   public function post_cash_process(Request $request)

   {
       $validator = Validator::make($request->all(), [

               'courrier' => 'required', 
                'billno' => 'required', 
                'invoiceno' => 'required',              

          ],[
              
                                        
                                 ]);     


          if ($validator->passes()) {
        
                  $data = Placeorder::find($request->id);
           $data->courrier = $request->courrier;
          $data->processdate = date("Y-m-d");
         $data->billno = $request->billno;
          $data->invoiceno = $request->invoiceno;
           $data->status = 'cu';
          $data->save();

            return response()->json(['success'=>'Added new records.'.$request->id ]);  
          }
         
       else{
        return response()->json(['error'=>$validator->errors()->all()]);}

 
}

  public function cash_list_ind_tranfer($id)
{

$datatc = Cash_in_hand::find($id);
$value1 = array(
                            'transfer'=> 'yes' ,
                            'transferdate'=>date("Y-m-d")                                                    

                      );


$datatc->fill($value1)->save();
return redirect('/cash_in_hand_emp');
  
}


public function order_dispatch_report_page()
{

     $data['employee'] = DB::table('user_informations')->get();

     $data['store'] = DB::table('stores')->get();

     return view('pages.report.order_dispatch',$data);
}


public function order_dispatch_report()
{
     
      $employee = $_GET['employee'];

      $store = $_GET['store'];

      $date_from = $_GET['date_from'];

      $date_to = $_GET['date_to'];
      
      $sql = "select u.empname,s.name,p.quantity,p.processdate,p.processsqty,p.processamount from placeorders p
        LEFT JOIN user_informations u on u.id=p.employee
        LEFT JOIN stores s on s.id=p.store
       where (p.processdate BETWEEN '".$date_from."' AND '".$date_to."') ";

     if($employee!='')
     {

         $sql .="AND p.employee=".$employee;
     }

     if($store!='')
     {
       $sql .=" AND p.store=".$store;
     }

     $order_dispatch  =DB::select($sql);


     if(!empty($order_dispatch))
           {
               $i=1;
              foreach ($order_dispatch as $d) {

           
             
                        $row[] = array(
                               'si_no' =>$i++,
                               'employee_name' =>$d->empname,
                               'store_name' =>$d->name,
                               'date'=>$d->processdate,
                               'order'=>$d->quantity,
                               'dispatch'=>$d->processsqty,
                               'bill_amount'=>$d->processamount                    
                               );

            }
      }
      else
      {
         $row = [];
      }
    
        echo json_encode($row);  
}


 public function cash_in_hand()
 {
    
    $data['employee'] = DB::table('user_informations')->get();

    $data['store'] = DB::table('stores')->get();

    return view('pages.report.cash_in_hand',$data);
 }


 public function cash_in_hand_report()
 {

      $employee = $_GET['employee'];

      $store = $_GET['store'];

      $date_from = $_GET['date_from'];

      $date_to = $_GET['date_to'];


      $sql = "select u.empname,s.name,c.date,c.amount from cash_in_hands c
              LEFT JOIN stores s on s.id=c.store
              LEFT JOIN user_informations u on u.id=c.employee
              where c.transfer='no' AND (c.date BETWEEN '".$date_from."' AND '".$date_to."' )";


     if($employee!='')
     {

         $sql .="AND c.employee=".$employee;
     }

     if($store!='')
     {
       $sql .=" AND c.store=".$store;
     }


     $cash = DB::select($sql);

     
     if(!empty($cash))

           {
             $total_amount = 0;
               $i=1;
              foreach ($cash as $c) {

               $total_amount += $c->amount ; 
             
                        $row[] = array(
                               'si_no' =>$i++,
                               'employee_name' =>$c->empname,
                               'store_name' =>$c->name,
                               'date'=>$c->date,
                               'amount'=>$c->amount                  
                               );

            }
       }
           
      else
      {
         $row = [];
        
      }
    
        echo json_encode($row); 


         



 }



    }
