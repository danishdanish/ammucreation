<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
  use App\Role;
  use App\Module;
  use Hash;
  use Session;


  class RoleController extends Controller
  {

    public function __construct()
      {
          $this->middleware('auth');
      }


         public function role_list()
      {
          
          return view('pages/role/rolelist');
      }

      public function ajax_data()
      {
        
          // $user = User::get();
           $user = Role::all();
          
          $i=1;

          foreach ($user as $u) {
           
                      $row[] = array(
                             'si_no' =>$i++,
                             'name' =>$u->name,
                             'displayname' =>$u->displayname,
                             'description' =>$u->description,
                               'actions' =>'<a href="role/role_permissions/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-asterisk"></span></button></a>&nbsp;<a href="role/role_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a> <button title="delete" id="deletebtnrole" class="delete btn btn-danger" data-id="'.$u->id.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                             );

          }

           $response = array(
                         "draw" => 0,
                         "recordsTotal" => count($row),
                         "recordsFiltered" => count($row),
                         "data" => $row
      );
      echo json_encode($response);

      }

           public function role_add()
      {
          return view('pages/role/add');
      }


      public function insert_role_data(Request $request){

  	$role = new Role;
           $this->validate($request,[
           'name'=>'required|max:13', 
  		  'description'=>'required',
  		  'displayname'=>'required',
  		     ]);
   	$input = $request->all();
    $input['modules']=0;
      Role::create($input);
   Session::flash('flash_message', 'Role Created Successfully');
  	return back(); return redirect()->back();  

      }


     public function edit_role_data($id){
         $data['data'] = Role::where('id',$id)->first();
          return view('pages/role/edit',$data); 
         }




          public function post_edit_role_data(Request $request)
      {

          $this->validate($request,[
                                  'name'=>'required|max:13',
                                  'description' =>'required',
                                  'displayname' =>'required',
                                 
                                 ],[
                                     'name.required' =>'Name field is required',
                                   'description.required'=>'Description field is required',
                                   'displayname.required' =>'Display name is required'
                                  
                                 ]); 
           $uid = $request->uid;
           $data = Role::find($uid);
           $input = $request->all();            
          
           $data->fill($input)->save();
           Session::flash('flash_message', 'Successfully updated!');
          return back(); return redirect()->back();
      }



         public function delete_role_data(Request $request){
          $id = $request->id;
          Role::destroy($id);
             return response()->json(['success'=>'Succesfully Deleted!']);
         
     }


    public function role_permissions($id)
      {
      $modules = Module::all();
       $data['data'] = Role::where('id',$id)->first();
      return view('pages/role/permissions',['modules' => $modules],$data);     
      }

  public function add_permissions(Request $request) {
   $rid=$request->rid;
     $checklists=$request->checklist;
     $permis= implode(",",$checklists);
      if($checklists){  
      $data = Role::find($rid);   
      $data->modules=$permis;         
      $data->save();     
     Session::flash('message', "Status Updated");
  }
   else
     {
      Session::flash('message', "Please select");
     }
    return back(); return redirect()->back();


  }



  }
