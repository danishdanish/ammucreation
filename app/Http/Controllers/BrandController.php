<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Target;
 use App\Brand;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;



    class BrandController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


           public function brand_list()
        {
            
            return view('pages/brand/list');
        }

        public function ajax_data()
        {
          
            
             $user = Brand::orderBy('active','DESC')->get();
            
            $i=1;

            foreach ($user as $u) {
             $image =  '<img width="80px" height="80px" src="'.url('theme/image/brand/'.$u->bimage).'"/>';
                        $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$u->brand,
                               'image' =>$image,
                               'active'=>$u->active,                             
                                   'actions' =>'<a href="brand/brand_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a>'                   
                               );

            }

             $response = array(
                           "draw" => 0,
                           "recordsTotal" => count($row),
                           "recordsFiltered" => count($row),
                           "data" => $row
        );
        echo json_encode($response);

        }


            public function brand_add()
    {
        return view('pages/brand/add');
    }

    public function insert_brand_data(Request $request)
    {


             $this->validate($request,[
                                'brand'=>'required|unique:brands',                             
                               ]);
  
    
    
        if($request->brand_image!='')
        {
            $file = $request->brand_image;

            $file_name = time().$file->getClientOriginalName();

            $path =  public_path('/theme/image/brand/');

            $file->move($path,$file_name);
            
        } 

        $brand = new Brand;
         $brand->active = 1;
        $brand->brand = $request->brand;
        $brand->bimage = $request->brand_image!=''?$file_name:$request->brand_image;   
        $brand->save();

      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();

    }
       


  public function edit_brand_data($id){
       $brand['brand'] = Brand::where('id',$id)->first();
        return view('pages/brand/edit',$brand); 
       }



   public function post_edit_brand_data(Request $request)
    {

        $this->validate($request,[
                                'brand'=>'required',
                               
                               ]); 
         $id = $request->id;
         $data = Brand::find($id);
         $input = $request->all();
     
       
         if($request->bimage!='')
        {
            $file = $request->bimage;

            $file_name = time().$file->getClientOriginalName();

            $path =  public_path('/theme/image/brand/');

            $file->move($path,$file_name);
             $input['bimage'] = $file_name;
            File::delete('theme/image/brand_image/'.$data->bimage);          
            
    } 
         $data->fill($input)->save();
         Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();
    }


           public function delete_brand_data(Request $request){
        $id = $request->id;
        Brand::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }





    }
