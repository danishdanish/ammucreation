<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Excel;


use DB;

class StockController extends Controller
{
 
 public function __construct()
 {
  $this->middleware('auth');
}

public function stock_list()
{

  return view('pages.stock.list');
}


public function ajax_data()
{
  
 $sql = "select p.product,p.itemcode,IFNULL(s.stock,0) as stock from products p 
 LEFT JOIN(select SUM(stock) as stock,item_code from stocks where status=1 GROUP BY item_code) as s on s.item_code=p.itemcode";

 $stock = DB::select($sql);
 

 $i=1;

 foreach ($stock as $s) {

   
   
  $row[] = array(
   'si_no' =>$i++,
   'item_name' =>$s->product,
   'item_code'=>$s->itemcode,
   'stock'=>$s->stock,                   
 );

}

if(!empty($row)){
 $response = array(
   "draw" => 0,
   "recordsTotal" => count($row),
   "recordsFiltered" => count($row),
   "data" => $row
   
 );
}

else{
  $response = array('data'=>'');
}

echo json_encode($response);       

}

public function stock_add_page()
{
 
 $data['item_code'] = DB::table('products')->select('itemcode','product')->get();

 return view('pages.stock.add',$data);
}

public function stock_insert(Request $request)
{
 
  

  if($request->excel_sheet!='')
  {
    
    $file = $request->excel_sheet;

    $file_name = time().$file->getClientOriginalName();

    $path =  public_path('/theme/image/stock/');

    $file->move($path,$file_name);

    $ext = pathinfo($path.$file_name, PATHINFO_EXTENSION);
    
    if($ext=='csv')
    {

     Excel::load($path.$file_name, function ($reader) {
       $i=0;
       foreach ($reader->toArray() as $row) {
                  // print_r($row);exit; 
         $insert_data[] = array(
           'item_code' =>$row['item_code'],
           'stock' =>$row['stock'],
           'created_at'=>date('Y-m-d H:i:s')
         );

         
         

         
         
       }

       if(!empty($insert_data)) {
        DB::table('stocks')->insert($insert_data);
      }
    });

   }
   else
   {

    Excel::load($path.$file_name, function ($reader) {
     $i=0;
     foreach ($reader->toArray() as $row) {
       foreach($row as $r)
       {
         
         $insert_data[] = array(
           'item_code' =>$r['item_code'],
           'stock' =>$r['stock'],
           'created_at'=>date('Y-m-d H:i:s')
         );

         
         

       }
       
       

       
     }

     if(!empty($insert_data)) {
      DB::table('stocks')->insert($insert_data);
    }
  });

  }

  unlink($path.$file_name);   

}
else
{

 $data = array(
   'item_code'=>$request->item_code,
   'stock'=>$request->stock,
   'created_at'=>date('Y-m-d H:i:s')
 );

 DB::table('stocks')->insert($data);



}

return redirect('/stock');
}
}
