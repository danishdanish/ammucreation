<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth ;

class LoginController extends Controller
{
    
    public function login_page()
    {
    	return view('pages/login');
    }

    public function check_login(Request $request)
    {
       
     $credentials = [
               'username' => $request->email,
               'password' => $request->password,
              ];

    	if(Auth::attempt($credentials))
    	{
    		return 1;
    	}
    	return 0;	
    }
}
