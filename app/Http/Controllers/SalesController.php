<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Color;
use App\User_information;
use App\Store;
use App\Task;
use App\Order;
use App\Placeorder;
use App\Sale;
use Session;
use Validator;
use Illuminate\Support\Facades\File;
Use DB;



class SalesController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

// sales all

  public function sales_data()
  {
    
    return view('pages/sales/saleslist');
  }


  public function ajax_data_emps()
  {
    

    $user = Sale::join('user_informations', 'sales.employee', '=', 'user_informations.id')
    ->join('stores', 'sales.store', '=', 'stores.id')->orderBy('sales.date', 'desc')->get(['sales.*','sales.id as salesid','user_informations.*','stores.*']);

    
    
    $i=1;

    foreach ($user as $u) {         
      
      $row[] = array(
       'si_no' =>$i++,
       'date' =>$u->date, 
       'employee' =>$u->empname,
       'store' =>$u->name, 
       'amount' =>$u->sales,               
       'actions' =>'  <a href="sales/sales_view/'.$u->salesid.'"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
     );

    }

    if(!empty($row)){
     $response = array(
       "draw" => 0,
       "recordsTotal" => count($row),
       "recordsFiltered" => count($row),
       "data" => $row
       
     );
   }

   else{
    $response = array('data'=>'');
  }
  
  echo json_encode($response);

}


 //sales individual

public function sales_data_ind()
{
  
  return view('pages/sales/saleslistind');
}


public function ajax_data_emps_ind($id)
{
  

  $user = Sale::join('user_informations', 'sales.employee', '=', 'user_informations.id')
  ->join('stores', 'sales.store', '=', 'stores.id')->where('sales.employee','=', $id)->orderBy('sales.date', 'desc')->get(['sales.*','sales.id as salesid','user_informations.*','stores.*']);

  
  
  $i=1;

  foreach ($user as $u) {         
    
    $row[] = array(
     'si_no' =>$i++,
     'date' =>$u->date, 
     'employee' =>$u->empname,
     'store' =>$u->name, 
     'amount' =>$u->sales,               
     'actions' =>'  <a href="#"><button class="btn btn-info" title="Process Order"><span class="glyphicon glyphicon glyphicon-eye-open"></span></button>'                   
   );

  }
  if(!empty($row)){
   $response = array(
     "draw" => 0,
     "recordsTotal" => count($row),
     "recordsFiltered" => count($row),
     "data" => $row
     
   );
 }

 else{
  $response = array('data'=>'');
}

echo json_encode($response);

}





public function saleView($id)
{            
  
  $data['data'] = Sale::join('user_informations', 'sales.employee', '=', 'user_informations.id')
  ->join('stores', 'sales.store', '=', 'stores.id')->where('sales.id','=', $id)->orderBy('sales.date', 'desc')->first(['sales.*','sales.id as salesid','user_informations.*','stores.*']);

  
  return view('pages/sales/salesview',$data);

}  




public function post_order_process(Request $request)

{
 $validator = Validator::make($request->all(), [

   'courrier' => 'required', 
   'billno' => 'required', 
   'invoiceno' => 'required',              

 ],[
  
  
 ]);     


 if ($validator->passes()) {
  
  $data = Placeorder::find($request->id);
  $data->courrier = $request->courrier;
  $data->processdate = date("Y-m-d");
  $data->billno = $request->billno;
  $data->invoiceno = $request->invoiceno;
  $data->status = 'cu';
  $data->save();

  return response()->json(['success'=>'Added new records.'.$request->id ]);  
}

else{
  return response()->json(['error'=>$validator->errors()->all()]);}

  
}



}
