<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
 use App\Size;
 use App\Brand;
 use Session;
 use Validator;
 use Illuminate\Support\Facades\File;



    class SizeController extends Controller
    {

      public function __construct()
        {
            $this->middleware('auth');
        }


           public function size_list()
        {
            
            return view('pages/size/list');
        }

        public function ajax_data()
        {
          
            
             $user = Size::all();
            
            $i=1;

            foreach ($user as $u) {
                                    $row[] = array(
                               'si_no' =>$i++,
                               'name' =>$u->sizename,
                               'code' =>$u->sizecode,                             
                                   'actions' =>'<button title="delete" id="deletebtnsize" class="delete btn btn-danger" data-id="'.$u->id.'"><span class="glyphicon glyphicon-remove"></span></button>'                   
                               );

            }

             $response = array(
                           "draw" => 0,
                           "recordsTotal" => count($row),
                           "recordsFiltered" => count($row),
                           "data" => $row
        );
        echo json_encode($response);

        }


            public function size_add()
    {
        return view('pages/size/size');
    }

    public function insert_size_data(Request $request)
    {


   $this->validate($request,[
                                'sizename'=>'required',
                                'sizecode' =>'required',
                                                          
                               ],[
                                   'sizename.required' =>'Size name required',
                                 'sizecode.required'=>'Size code is required'
                                                           
                               ]); 

      $input = $request->all();   
    Size::create($input);
      Session::flash('flash_message', 'Successfully updated!');
        return back(); return redirect()->back();

    }



           public function delete_size_data(Request $request){
        $id = $request->id;
        Size::destroy($id);
           return response()->json(['success'=>'Succesfully Deleted!']);
   }





    }
